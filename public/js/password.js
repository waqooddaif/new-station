webpackJsonp([9],{

/***/ 192:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

var _retinajs = __webpack_require__(20);

var _retinajs2 = _interopRequireDefault(_retinajs);

var _vue = __webpack_require__(15);

var _vue2 = _interopRequireDefault(_vue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = new _vue2.default({
    el: '#app',
    data: function data() {
        return {
            msg: 'Password must contain letters and numbers',
            email: emailValue,
            password: null,
            passwordConfirm: null,
            passwordError: passwordHasError ? passwordHasError : false,
            passwordErrorMsg: passwordErrorMessage ? passwordErrorMessage : null,
            passwordSuccess: false,
            passwordConfirmError: passwordConfirmHasError ? passwordConfirmHasError : false,
            passwordConfirmErrorMsg: passwordConfirmErrorMessage ? passwordConfirmErrorMessage : null,
            passwordConfirmSuccess: false,
            passwordTotalSuccess: false
        };
    },

    watch: {
        'password': 'checkPassword',
        'passwordConfirm': 'checkPasswordConfirm'
    },
    mounted: function mounted() {
        (0, _retinajs2.default)($('img'));
        if (this.password) {
            this.checkPassword(this.password);
        }
    },

    methods: {
        checkPassword: function checkPassword(val) {
            var errorBag = '';

            if (val.length < 8) {
                errorBag += "Must be at least 8 characters";
            }

            if (!/^(?=.*[a-zA-Z]).+$/.test(val)) {
                var msg = "Must contain letters and numbers";
                errorBag = errorBag ? errorBag + " and " + msg : msg;
            }

            if (errorBag) {
                this.passwordError = true;
                this.passwordSuccess = false;
                this.passwordErrorMsg = errorBag;
            } else {
                this.passwordError = false;
                this.passwordSuccess = true;
                this.passwordErrorMsg = null;
            }
        },
        checkPasswordConfirm: function checkPasswordConfirm(val) {
            this.passwordConfirmError = val !== this.password;
            this.passwordConfirmErrorMsg = val !== this.password ? 'Passwords must be matched' : null;
            this.passwordConfirmSuccess = val === this.password;
            this.passwordTotalSuccess = this.passwordSuccess && this.passwordConfirmSuccess;
        },
        submitForm: function submitForm() {
            var email = document.getElementById("email");
            email.setAttribute("name", "email");
            email.setAttribute("value", this.email);
            var password = document.getElementById("password");
            password.setAttribute("name", "password");
            password.setAttribute("value", this.password);
            var passwordConfirm = document.getElementById("password-confirm");
            passwordConfirm.setAttribute("name", "password_confirmation");
            passwordConfirm.setAttribute("value", this.passwordConfirm);

            event.target.submit();
        }
    }
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(192);


/***/ })

},[280]);