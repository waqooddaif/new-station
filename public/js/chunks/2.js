webpackJsonp([2],{

/***/ 202:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(400)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(308),
  /* template */
  __webpack_require__(382),
  /* scopeId */
  "data-v-e663aca2",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/planner/PlannerView.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] PlannerView.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e663aca2", Component.options)
  } else {
    hotAPI.reload("data-v-e663aca2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 282:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(284)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 284:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 285:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(286), __esModule: true };

/***/ }),

/***/ 286:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(47);
__webpack_require__(46);
module.exports = __webpack_require__(287);


/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(6);
var get = __webpack_require__(196);
module.exports = __webpack_require__(4).getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};


/***/ }),

/***/ 308:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getIterator2 = __webpack_require__(285);

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'PlannerView',
    props: ['year', 'week'],
    data: function data() {
        return {
            weekDays: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday'],
            prevWeek: {},
            nextWeek: {}
        };
    },
    mounted: function mounted() {
        this.bootstrapActiveWeek();
    },

    watch: {
        week: function week(val) {
            this.bootstrapActiveWeek();
        },
        year: function year(val) {
            this.bootstrapActiveWeek();
        }
    },
    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        membersWeekSchedule: 'membersWeekSchedule'
    }), {
        weekDates: function weekDates() {
            var _this = this;

            return this.weekDays.map(function (day) {
                return moment().week(_this.week).day(day).format('YYYY-MM-DD');
            });
        },
        activeRecords: function activeRecords() {
            if (this.membersWeekSchedule) {
                if (this.membersWeekSchedule[this.year] && this.membersWeekSchedule[this.year][this.week]) {
                    return this.membersWeekSchedule[this.year][this.week];
                }
            }
            return [];
        },
        activeSchedule: function activeSchedule() {
            var _this2 = this;

            if (this.activeRecords) {
                return this.activeRecords.map(function (record) {
                    var member = JSON.parse((0, _stringify2.default)(record));
                    member.weekSchedule = [];
                    member.weekSchedule = _this2.weekDays.map(function (day, index) {
                        var dayDate = _this2.weekDates[index];
                        var daySchedule = [];
                        if (member.project_services.length > 0) {
                            var _iteratorNormalCompletion = true;
                            var _didIteratorError = false;
                            var _iteratorError = undefined;

                            try {
                                for (var _iterator = (0, _getIterator3.default)(member.project_services), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                                    var service = _step.value;

                                    if (moment(dayDate).isBetween(service.date_start, service.date_finish, null, '[]')) {
                                        daySchedule.push(service);
                                    }
                                }
                            } catch (err) {
                                _didIteratorError = true;
                                _iteratorError = err;
                            } finally {
                                try {
                                    if (!_iteratorNormalCompletion && _iterator.return) {
                                        _iterator.return();
                                    }
                                } finally {
                                    if (_didIteratorError) {
                                        throw _iteratorError;
                                    }
                                }
                            }
                        }
                        return daySchedule;
                    });
                    return member;
                });
            }
            return [];
        }
    }),
    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['fetchMembersWeekSchedule']), {
        unitHeight: function unitHeight(parent) {
            return 40 / parseInt(parent.length) + 'px';
        },
        bootstrapActiveWeek: function bootstrapActiveWeek() {
            if (!this.week) {
                this.week = moment().week();
            }
            if (!this.year) {
                this.year = moment().year();
            }

            var activeWeekMoment = moment(this.weekDates[0]);
            this.nextWeek.year = activeWeekMoment.add(1, 'weeks').startOf('isoWeek').year();
            this.nextWeek.week = activeWeekMoment.add(1, 'weeks').startOf('isoWeek').week();
            this.prevWeek.year = activeWeekMoment.subtract(1, 'weeks').startOf('isoWeek').year();
            this.prevWeek.week = activeWeekMoment.subtract(1, 'weeks').startOf('isoWeek').week();

            if (!this.membersWeekSchedule[this.year] || !this.membersWeekSchedule[this.year][this.week]) {
                this.fetchMembersWeekSchedule({ year: this.year, week: this.week });
            }
        }
    }),
    components: {}
};

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.member-cell[data-v-e663aca2] {\n    border-left: 3px solid;\n    padding-left: 20px;\n}\n#prev-week[data-v-e663aca2] {\n    margin-right: 60px;\n}\n#planner-nav[data-v-e663aca2] {\n    margin-bottom: 20px;\n    margin-right: 10px;\n}\n#planner-nav a[data-v-e663aca2] {\n    color: inherit;\n}\n.table th[data-v-e663aca2], .table td[data-v-e663aca2] {\n    background-color: #ffffff;\n}\n.table td[data-v-e663aca2] {\n    vertical-align: middle;\n    border-top: 2px dashed #EEF1F6;\n    height: 50px;\n}\n.day-schedule[data-v-e663aca2] {\n    border-radius: 5px;\n}\n.schedule-unit[data-v-e663aca2] {\n    padding: 5px;\n    color: #ffffff;\n}\n.schedule-unit[data-v-e663aca2]:first-child {\n    border-top-left-radius: 5px;\n    border-top-right-radius: 5px;\n}\n.schedule-unit[data-v-e663aca2]:last-child {\n    border-bottom-left-radius: 5px;\n    border-bottom-right-radius: 5px;\n}\ntd.day-schedule-cell.empty[data-v-e663aca2] {\n    background-color: #E0DFE4;\n}\n.glyphicon.fast-right-spinner[data-v-e663aca2] {\n    animation: glyphicon-spin-r 1s infinite linear;\n}\n@keyframes glyphicon-spin-r {\n0% {\n        transform: rotate(0deg);\n}\n100% {\n        transform: rotate(359deg);\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 382:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "member-index"
    }
  }, [_c('div', {
    staticClass: "row content-box"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "pull-right",
    attrs: {
      "id": "planner-nav"
    }
  }, [_c('router-link', {
    attrs: {
      "to": {
        name: 'PlannerView',
        params: _vm.prevWeek
      },
      "id": "prev-week"
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-triangle-left"
  }), _vm._v(" previous week\n                ")]), _vm._v(" "), _c('router-link', {
    attrs: {
      "to": {
        name: 'PlannerView',
        params: _vm.nextWeek
      },
      "id": "next-week"
    }
  }, [_vm._v("\n                    next week "), _c('i', {
    staticClass: "glyphicon glyphicon-triangle-right"
  })])], 1)]), _vm._v(" "), _c('table', {
    staticClass: "table table-bordered week-schedule"
  }, [_c('thead', [_c('tr', [_c('th', [_vm._v("Name")]), _vm._v(" "), _vm._l((_vm.weekDays), function(day, index) {
    return _c('th', [_c('div', [_vm._v(_vm._s(day))]), _vm._v(" "), _c('div', [_vm._v(_vm._s(_vm.weekDates[index]))])])
  })], 2)]), _vm._v(" "), _c('tbody', _vm._l((_vm.activeSchedule), function(member) {
    return _c('tr', {
      key: member.id
    }, [_c('td', {
      staticClass: "member-cell",
      style: ({
        'border-left-color': member.color
      })
    }, [_vm._v("\n                    " + _vm._s(member.name) + "\n                ")]), _vm._v(" "), _vm._l((member.weekSchedule), function(daySchedule, dayIndex) {
      return _c('td', {
        class: {
          'day-schedule-cell': true, 'empty': !daySchedule.length > 0
        }
      }, [(daySchedule.length > 0) ? _c('div', {
        staticClass: "day-schedule"
      }, _vm._l((daySchedule), function(service) {
        return (service) ? _c('div', {
          staticClass: "schedule-unit",
          style: ({
            'background-color': service.project.color,
            'min-height': _vm.unitHeight(daySchedule)
          }),
          attrs: {
            "title": service.date_start + ' - ' + service.date_finish + ' on ' + service.service.name
          }
        }, [_vm._v("\n                            " + _vm._s(service.project.name) + "\n                        ")]) : _c('div', {
          staticClass: "schedule-unit-empty"
        })
      })) : _vm._e()])
    })], 2)
  }))])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-e663aca2", module.exports)
  }
}

/***/ }),

/***/ 400:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(342);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("556adb96", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-e663aca2&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./PlannerView.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-e663aca2&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./PlannerView.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});