webpackJsonp([5],{

/***/ 200:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(390)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(306),
  /* template */
  __webpack_require__(367),
  /* scopeId */
  "data-v-32adcdc4",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/companies/CompanyIndex.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] CompanyIndex.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-32adcdc4", Component.options)
  } else {
    hotAPI.reload("data-v-32adcdc4", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 282:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(284)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 284:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'CompanyIndex',
  data: function data() {
    return {
      activeCompany: null,
      newCompany: {
        name: null,
        website: null
      },
      addNewCompanyPopOptions: {
        trigger: 'click',
        placement: 'auto',
        closeable: true,
        animation: 'pop',
        dismissible: false
      },
      formErrors: null,
      formInProgress: false
    };
  },
  mounted: function mounted() {
    this.fetchCompaniesList();
  },

  computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
    companiesList: 'companiesList',
    userPermissions: 'userPermissions'
  })),
  methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['fetchCompaniesList', 'createCompany']), {
    addNewCompany: function addNewCompany() {
      var _this = this;

      var form = event.target;
      this.formInProgress = true;
      this.createCompany(this.newCompany).then(function (response) {
        _this.newCompany = { name: null, website: null };
        _this.formErrors = null;
        $('#add-new-company').webuiPopover('hide');
        _this.formInProgress = false;
      }).catch(function (error) {
        if (error.status === 422) {
          _this.formErrors = error.body;
        } else {
          $('#add-new-company').webuiPopover('hide');
        }
        _this.formInProgress = false;
      });
    },
    makeActive: function makeActive(company) {
      if (this.activeCompany === null || this.activeCompany.id !== company.id) {
        this.activeCompany = company;
        $('tr.active').removeClass('active');
        $(event.target).parent().addClass('active');
      } else {
        this.activeCompany = null;
        $(event.target).parent().removeClass('active');
      }
    }
  }),
  components: {}
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.btn-md span[data-v-32adcdc4] {\n  padding-left: 5px;\n}\n.table[data-v-32adcdc4] {\n  margin-top: 35px;\n}\n.table td[data-v-32adcdc4] {\n  border-top: 0;\n}\n.table thead tr th[data-v-32adcdc4] {\n  text-transform: uppercase;\n  font-size: 90%;\n  border-bottom: 2px dashed #EEF1F6;\n}\n.table tbody tr[data-v-32adcdc4]:nth-of-type(odd) {\n  background-color: #FFFFFF;\n}\n.table tbody tr[data-v-32adcdc4]:nth-of-type(even) {\n  background-color: #EEF1F6;\n}\n.table tbody tr td[data-v-32adcdc4]:first-child {\n  border-radius: 32px 0 0 32px;\n  -moz-border-radius-topleft: 32px;\n  -moz-border-radius-bottomleft: 32px;\n  -webkit-border-top-left-radius: 32px;\n  -webkit-border-bottom-left-radius: 32px;\n}\n.table tbody tr td[data-v-32adcdc4]:last-child {\n  border-radius: 0 32px 32px 0;\n  -moz-border-radius-topright: 32px;\n  -moz-border-radius-bottomright: 32px;\n  -webkit-border-top-right-radius: 32px;\n  -webkit-border-bottom-right-radius: 32px;\n}\n.table:not(.info) tbody tr[data-v-32adcdc4]:hover {\n  background-color: #FCE8DF;\n  cursor: pointer;\n}\n.table:not(.info) tbody tr.active td[data-v-32adcdc4] {\n  background-color: #FCE8DF;\n}\n.table.info[data-v-32adcdc4] {\n  margin-top: 5px;\n  /*-webkit-box-shadow: 10px 10px 37px -17px rgba(0,0,0,0.75);*/\n  /*-moz-box-shadow: 10px 10px 37px -17px rgba(0,0,0,0.75);*/\n  /*box-shadow: 10px 10px 37px -17px rgba(0,0,0,0.75);*/\n}\n.table.info thead[data-v-32adcdc4] {\n  color: #ffffff;\n  background-color: #ef4036;\n}\n.table.info thead tr th[data-v-32adcdc4] {\n  border-bottom: 0;\n}\n.table.info thead tr th[data-v-32adcdc4]:first-child {\n  border-radius: 32px 0 0 32px;\n}\n.table.info thead tr th[data-v-32adcdc4]:last-child {\n  border-radius: 0 32px 32px 0;\n}\n", ""]);

// exports


/***/ }),

/***/ 367:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": "company-index"
    }
  }, [_c('div', {
    staticClass: "row content-box"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "row"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-sm-6 text-right"
  }, [(_vm.userPermissions['company.create']) ? _c('div', [_c('button', {
    directives: [{
      name: "popover",
      rawName: "v-popover",
      value: (_vm.addNewCompanyPopOptions),
      expression: "addNewCompanyPopOptions"
    }],
    staticClass: "btn btn-success btn-md",
    attrs: {
      "id": "add-new-company"
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-plus"
  }), _vm._v(" "), _c('span', [_vm._v("Add Company")])]), _vm._v(" "), _c('div', {
    staticClass: "webui-popover-content",
    attrs: {
      "id": "popover-content-add-company"
    }
  }, [_c('div', {
    staticClass: "content-padding"
  }, [_c('h1', [_vm._v("Add company")]), _vm._v(" "), _c('form', {
    staticClass: "form-horizontal",
    attrs: {
      "id": "add-company-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addNewCompany($event)
      }
    }
  }, [_c('div', {
    class: {
      'form-group': true, 'has-error': _vm.formErrors && _vm.formErrors.name
    }
  }, [_c('label', {
    attrs: {
      "for": "new-company-name"
    }
  }, [_vm._v("Name *")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newCompany.name),
      expression: "newCompany.name"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-company-name",
      "required": "",
      "disabled": _vm.formInProgress
    },
    domProps: {
      "value": (_vm.newCompany.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.newCompany, "name", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.formErrors && _vm.formErrors.name) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n                        " + _vm._s(_vm.formErrors.name[0]) + "\n                      ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    class: {
      'form-group': true, 'has-error': _vm.formErrors && _vm.formErrors.website
    }
  }, [_c('label', {
    attrs: {
      "for": "new-company-website"
    }
  }, [_vm._v("\n                        Website\n                      ")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newCompany.website),
      expression: "newCompany.website"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-company-website",
      "disabled": _vm.formInProgress
    },
    domProps: {
      "value": (_vm.newCompany.website)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.newCompany, "website", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.formErrors && _vm.formErrors.website) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n                        " + _vm._s(_vm.formErrors.website[0]) + "\n                      ")]) : _vm._e()]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-success btn-block submit",
    attrs: {
      "type": "submit"
    }
  }, [(_vm.formInProgress) ? _c('i', {
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }) : _vm._e(), _vm._v("\n                      Add\n                    ")]), _vm._v(" "), _c('p', {
    staticClass: "help-block"
  }, [_vm._v("\n                      all fields marked (*) are required.\n                    ")])])])])]) : _vm._e()])]), _vm._v(" "), _c('table', {
    staticClass: "table"
  }, [_vm._m(1), _vm._v(" "), _c('tbody', _vm._l((_vm.companiesList), function(company) {
    return _c('tr', {
      attrs: {
        "id": 'company-item-' + company.id
      },
      on: {
        "click": function($event) {
          _vm.makeActive(company)
        }
      }
    }, [_c('td', [_vm._v(_vm._s(company.name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(company.clients.length))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(company.website))])])
  }))]), _vm._v(" "), _c('table', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.activeCompany),
      expression: "activeCompany"
    }],
    staticClass: "table info",
    attrs: {
      "id": "info-table"
    }
  }, [_vm._m(2), _vm._v(" "), (_vm.activeCompany) ? _c('tbody', _vm._l((_vm.activeCompany.clients), function(employee) {
    return _c('tr', [_c('td', [_vm._v(_vm._s(employee.name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("date")(employee.created_at)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(employee.role_description))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(employee.email))])])
  })) : _vm._e()])])])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.companiesList === null),
      expression: "companiesList === null"
    }],
    staticClass: "loading"
  }, [_vm._v("Loading…")])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-sm-6"
  }, [_c('h1', [_vm._v("Companies List")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Company")]), _vm._v(" "), _c('th', [_vm._v("Employees")]), _vm._v(" "), _c('th', [_vm._v("Website")])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Name")]), _vm._v(" "), _c('th', [_vm._v("Date Joined")]), _vm._v(" "), _c('th', [_vm._v("Title")]), _vm._v(" "), _c('th', [_vm._v("Email")])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-32adcdc4", module.exports)
  }
}

/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(332);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("444b8040", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-32adcdc4&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CompanyIndex.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-32adcdc4&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CompanyIndex.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});