webpackJsonp([0],{

/***/ 204:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(393)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(325),
  /* template */
  __webpack_require__(375),
  /* scopeId */
  "data-v-7d403847",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/Show.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] Show.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7d403847", Component.options)
  } else {
    hotAPI.reload("data-v-7d403847", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 282:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(284)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 284:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 285:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(286), __esModule: true };

/***/ }),

/***/ 286:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(47);
__webpack_require__(46);
module.exports = __webpack_require__(287);


/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(6);
var get = __webpack_require__(196);
module.exports = __webpack_require__(4).getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};


/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(294)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(291),
  /* template */
  __webpack_require__(293),
  /* scopeId */
  "data-v-c26c2258",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/node_modules/vue-datepicker/vue-datepicker.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] vue-datepicker.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c26c2258", Component.options)
  } else {
    hotAPI.reload("data-v-c26c2258", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(388)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(304),
  /* template */
  __webpack_require__(364),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/common/VueModal.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] VueModal.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2a80d580", Component.options)
  } else {
    hotAPI.reload("data-v-2a80d580", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 290:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var BezierEasing = __webpack_require__(326);

var _ = exports.utils = {
    $: function(selector) {
        return document.querySelector(selector);
    },
    on: function(element, events, handler) {
        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.addEventListener(events[i], handler);
        }
    },
    off: function(element, events, handler) {
        if (!(events instanceof Array)) {
            events = [events];
        }
        for (var i = 0; i < events.length; i++) {
            element.removeEventListener(events[i], handler);
        }
    }
};

exports.install = function(Vue) {
    function handleClick(e) {
        e.preventDefault();

        if (typeof this.value === "object") {
            exports.scrollTo(
                this.value.el || this.value.element,
                this.value.duration || 500,
                {
                    easing: (typeof this.value.easing === "string"
                        ? exports.easing[this.value.easing]
                        : this.value.easing) ||
                        exports.easing["ease"],
                    offset: this.value.offset || 0,
                    onDone: this.value.onDone,
                    onCancel: this.value.onCancel
                }
            );
        } else {
            exports.scrollTo(this.value, 500, {
                easing: exports.easing["ease"]
            });
        }
    }

    Vue.directive("scroll-to", {
        bind: function(el, binding) {
            _.on(el, "click", handleClick.bind(binding));
        },
        unbind: function(el) {
            _.off(el, "click", handleClick);
        }
    });
};

exports.scrollTo = function(element, duration, options) {
    if (typeof element === "string") {
        element = _.$(element);
    }

    var page = _.$("html, body");
    var events = [
        "scroll",
        "mousedown",
        "wheel",
        "DOMMouseScroll",
        "mousewheel",
        "keyup",
        "touchmove"
    ];
    var abort = false;

    var abortFn = function() {
        abort = true;
    };

    _.on(page, events, abortFn);

    var initialY = window.pageYOffset;
    var elementY = initialY + element.getBoundingClientRect().top;
    var targetY = document.body.scrollHeight - elementY < window.innerHeight
        ? document.body.scrollHeight - window.innerHeight
        : elementY;

    if (options.offset) {
        targetY += options.offset;
    }

    var diff = targetY - initialY;
    var easing = BezierEasing.apply(BezierEasing, options.easing);
    var start;

    var done = function() {
        _.off(page, events, abortFn);
        if (abort && options.onCancel) options.onCancel();
        if (!abort && options.onDone) options.onDone();
    };

    if (!diff) return;

    window.requestAnimationFrame(function step(timestamp) {
        if (abort) return done();
        if (!start) start = timestamp;

        var time = timestamp - start;
        var progress = Math.min(time / duration, 1);
        progress = easing(progress);

        window.scrollTo(0, initialY + diff * progress);

        if (time < duration) {
            window.requestAnimationFrame(step);
        } else {
            done();
        }
    });
};

exports.easing = {
    ease: [0.25, 0.1, 0.25, 1.0],
    linear: [0.00, 0.0, 1.00, 1.0],
    "ease-in": [0.42, 0.0, 1.00, 1.0],
    "ease-out": [0.00, 0.0, 0.58, 1.0],
    "ease-in-out": [0.42, 0.0, 0.58, 1.0]
};


/***/ }),

/***/ 291:
/***/ (function(module, exports, __webpack_require__) {

"use strict";




var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault2(_stringify);

var _getIterator2 = __webpack_require__(285);

var _getIterator3 = _interopRequireDefault2(_getIterator2);

function _interopRequireDefault2(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _moment = __webpack_require__(0);

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = {
  props: {
    required: false,
    date: {
      type: Object,
      required: true
    },
    option: {
      type: Object,
      default: function _default() {
        return {
          type: 'day',
          SundayFirst: false,
          week: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
          month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          format: 'YYYY-MM-DD',
          color: {
            checked: '#F50057',
            header: '#3f51b5',
            headerText: '#fff'
          },
          inputStyle: {
            'display': 'inline-block',
            'padding': '6px',
            'line-height': '22px',
            'font-size': '16px',
            'border': '2px solid #fff',
            'box-shadow': '0 1px 3px 0 rgba(0, 0, 0, 0.2)',
            'border-radius': '2px',
            'color': '#5F5F5F'
          },
          placeholder: 'when?',
          buttons: {
            ok: 'OK',
            cancel: 'Cancel'
          },
          overlayOpacity: 0.5,
          dismissible: true
        };
      }
    },
    limit: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    function hours() {
      var list = [];
      var hour = 24;
      while (hour > 0) {
        hour--;
        list.push({
          checked: false,
          value: hour < 10 ? '0' + hour : hour
        });
      }
      return list;
    }
    function mins() {
      var list = [];
      var min = 60;
      while (min > 0) {
        min--;
        list.push({
          checked: false,
          value: min < 10 ? '0' + min : min
        });
      }
      return list;
    }
    return {
      hours: hours(),
      mins: mins(),
      showInfo: {
        hour: false,
        day: false,
        month: false,
        year: false,
        check: false
      },
      displayInfo: {
        month: ''
      },
      library: {
        week: this.option.week,
        month: this.option.month,
        year: []
      },
      checked: {
        oldtime: '',
        currentMoment: null,
        year: '',
        month: '',
        day: '',
        hour: '00',
        min: '00'
      },
      dayList: [],
      selectedDays: []
    };
  },

  methods: {
    pad: function pad(n) {
      n = Math.floor(n);
      return n < 10 ? '0' + n : n;
    },
    nextMonth: function nextMonth(type) {
      var next = null;
      type === 'next' ? next = (0, _moment2.default)(this.checked.currentMoment).add(1, 'M') : next = (0, _moment2.default)(this.checked.currentMoment).add(-1, 'M');
      this.showDay(next);
    },
    showDay: function showDay(time) {
      if (time === undefined || !Date.parse(time)) {
        this.checked.currentMoment = (0, _moment2.default)();
      } else {
        this.checked.currentMoment = (0, _moment2.default)(time, this.option.format);
      }
      this.showOne('day');
      this.checked.year = (0, _moment2.default)(this.checked.currentMoment).format('YYYY');
      this.checked.month = (0, _moment2.default)(this.checked.currentMoment).format('MM');
      this.checked.day = (0, _moment2.default)(this.checked.currentMoment).format('DD');
      this.displayInfo.month = this.library.month[(0, _moment2.default)(this.checked.currentMoment).month()];
      var days = [];
      var currentMoment = this.checked.currentMoment;
      var firstDay = (0, _moment2.default)(currentMoment).date(1).day();

      var previousMonth = (0, _moment2.default)(currentMoment);
      var nextMonth = (0, _moment2.default)(currentMoment);
      nextMonth.add(1, 'months');
      previousMonth.subtract(1, 'months');
      var monthDays = (0, _moment2.default)(currentMoment).daysInMonth();
      var oldtime = this.checked.oldtime;
      for (var i = 1; i <= monthDays; ++i) {
        days.push({
          value: i,
          inMonth: true,
          unavailable: false,
          checked: false,
          moment: (0, _moment2.default)(currentMoment).date(i)
        });
        if (i === Math.ceil((0, _moment2.default)(currentMoment).format('D')) && (0, _moment2.default)(oldtime, this.option.format).year() === (0, _moment2.default)(currentMoment).year() && (0, _moment2.default)(oldtime, this.option.format).month() === (0, _moment2.default)(currentMoment).month()) {
          days[i - 1].checked = true;
        }
        this.checkBySelectDays(i, days);
      }
      if (firstDay === 0) firstDay = 7;
      for (var _i = 0; _i < firstDay - (this.option.SundayFirst ? 0 : 1); _i++) {
        var passiveDay = {
          value: previousMonth.daysInMonth() - _i,
          inMonth: false,
          action: 'previous',
          unavailable: false,
          checked: false,
          moment: (0, _moment2.default)(currentMoment).date(1).subtract(_i + 1, 'days')
        };
        days.unshift(passiveDay);
      }
      if (this.limit.length > 0) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = (0, _getIterator3.default)(this.limit), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var li = _step.value;

            switch (li.type) {
              case 'fromto':
                days = this.limitFromTo(li, days);
                break;
              case 'weekday':
                days = this.limitWeekDay(li, days);
                break;
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      }
      var passiveDaysAtFinal = 42 - days.length;
      for (var _i2 = 1; _i2 <= passiveDaysAtFinal; _i2++) {
        var _passiveDay = {
          value: _i2,
          inMonth: false,
          action: 'next',
          unavailable: false,
          checked: false,
          moment: (0, _moment2.default)(currentMoment).add(1, 'months').date(_i2)
        };
        days.push(_passiveDay);
      }
      this.dayList = days;
    },
    checkBySelectDays: function checkBySelectDays(d, days) {
      var _this = this;

      this.selectedDays.forEach(function (day) {
        if (_this.checked.year === (0, _moment2.default)(day).format('YYYY') && _this.checked.month === (0, _moment2.default)(day).format('MM') && d === Math.ceil((0, _moment2.default)(day).format('D'))) {
          days[d - 1].checked = true;
        }
      });
    },
    limitWeekDay: function limitWeekDay(limit, days) {
      days.map(function (day) {
        if (limit.available.indexOf(Math.floor(day.moment.format('d'))) === -1) {
          day.unavailable = true;
        }
      });
      return days;
    },
    limitFromTo: function limitFromTo(limit, days) {
      var _this2 = this;

      if (limit.from || limit.to) {
        days.map(function (day) {
          if (_this2.getLimitCondition(limit, day)) {
            day.unavailable = true;
          }
        });
      }
      return days;
    },
    getLimitCondition: function getLimitCondition(limit, day) {
      var tmpMoment = (0, _moment2.default)(this.checked.year + '-' + this.pad(this.checked.month) + '-' + this.pad(day.value));
      if (limit.from && !limit.to) {
        return !tmpMoment.isAfter(limit.from);
      } else if (!limit.from && limit.to) {
        return !tmpMoment.isBefore(limit.to);
      } else {
        return !tmpMoment.isBetween(limit.from, limit.to);
      }
    },
    checkDay: function checkDay(obj) {
      if (obj.unavailable || obj.value === '') {
        return false;
      }
      if (!obj.inMonth) {
        this.nextMonth(obj.action);
      }
      if (this.option.type === 'day' || this.option.type === 'min') {
        this.dayList.forEach(function (x) {
          x.checked = false;
        });
        this.checked.day = this.pad(obj.value);
        obj.checked = true;
      } else {
        var day = this.pad(obj.value);
        var ctime = this.checked.year + '-' + this.checked.month + '-' + day;
        if (obj.checked === true) {
          obj.checked = false;
          this.selectedDays.$remove(ctime);
        } else {
          this.selectedDays.push(ctime);
          obj.checked = true;
        }
      }
      switch (this.option.type) {
        case 'day':
          this.picked();
          break;
        case 'min':
          this.showOne('hour');

          this.shiftActTime();
          break;
      }
    },
    showYear: function showYear() {
      var _this3 = this;

      var year = (0, _moment2.default)(this.checked.currentMoment).year();
      this.library.year = [];
      var yearTmp = [];
      for (var i = year - 100; i < year + 5; ++i) {
        yearTmp.push(i);
      }
      this.library.year = yearTmp;
      this.showOne('year');
      this.$nextTick(function () {
        var listDom = document.getElementById('yearList');
        listDom.scrollTop = listDom.scrollHeight - 100;
        _this3.addYear();
      });
    },
    showOne: function showOne(type) {
      switch (type) {
        case 'year':
          this.showInfo.hour = false;
          this.showInfo.day = false;
          this.showInfo.year = true;
          this.showInfo.month = false;
          break;
        case 'month':
          this.showInfo.hour = false;
          this.showInfo.day = false;
          this.showInfo.year = false;
          this.showInfo.month = true;
          break;
        case 'day':
          this.showInfo.hour = false;
          this.showInfo.day = true;
          this.showInfo.year = false;
          this.showInfo.month = false;
          break;
        case 'hour':
          this.showInfo.hour = true;
          this.showInfo.day = false;
          this.showInfo.year = false;
          this.showInfo.month = false;
          break;
        default:
          this.showInfo.day = true;
          this.showInfo.year = false;
          this.showInfo.month = false;
          this.showInfo.hour = false;
      }
    },
    showMonth: function showMonth() {
      this.showOne('month');
    },
    addYear: function addYear() {
      var _this4 = this;

      var listDom = document.getElementById('yearList');
      listDom.addEventListener('scroll', function (e) {
        if (listDom.scrollTop < listDom.scrollHeight - 100) {
          var len = _this4.library.year.length;
          var lastYear = _this4.library.year[len - 1];
          _this4.library.year.push(lastYear + 1);
        }
      }, false);
    },
    setYear: function setYear(year) {
      this.checked.currentMoment = (0, _moment2.default)(year + '-' + this.checked.month + '-' + this.checked.day);
      this.showDay(this.checked.currentMoment);
    },
    setMonth: function setMonth(month) {
      var mo = this.library.month.indexOf(month) + 1;
      if (mo < 10) {
        mo = '0' + '' + mo;
      }
      this.checked.currentMoment = (0, _moment2.default)(this.checked.year + '-' + mo + '-' + this.checked.day);
      this.showDay(this.checked.currentMoment);
    },
    showCheck: function showCheck() {
      if (this.date.time === '') {
        this.showDay();
      } else {
        if (this.option.type === 'day' || this.option.type === 'min') {
          this.checked.oldtime = this.date.time;
          this.showDay(this.date.time);
        } else {
          this.selectedDays = JSON.parse(this.date.time);
          if (this.selectedDays.length) {
            this.checked.oldtime = this.selectedDays[0];
            this.showDay(this.selectedDays[0]);
          } else {
            this.showDay();
          }
        }
      }
      this.showInfo.check = true;
    },
    setTime: function setTime(type, obj, list) {
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = (0, _getIterator3.default)(list), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var item = _step2.value;

          item.checked = false;
          if (item.value === obj.value) {
            item.checked = true;
            this.checked[type] = item.value;
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    },
    picked: function picked() {
      if (this.option.type === 'day' || this.option.type === 'min') {
        var ctime = this.checked.year + '-' + this.checked.month + '-' + this.checked.day + ' ' + this.checked.hour + ':' + this.checked.min;
        this.checked.currentMoment = (0, _moment2.default)(ctime, 'YYYY-MM-DD HH:mm');
        this.date.time = (0, _moment2.default)(this.checked.currentMoment).format(this.option.format);
      } else {
        this.date.time = (0, _stringify2.default)(this.selectedDays);
      }
      this.showInfo.check = false;
      this.$emit('change', this.date.time);
    },
    dismiss: function dismiss(evt) {
      if (evt.target.className === 'datepicker-overlay') {
        if (this.option.dismissible === undefined || this.option.dismissible) {
          this.showInfo.check = false;
          this.$emit('cancel');
        }
      }
    },
    shiftActTime: function shiftActTime() {
      this.$nextTick(function () {
        if (!document.querySelector('.hour-item.active')) {
          return false;
        }
        document.querySelector('.hour-box').scrollTop = (document.querySelector('.hour-item.active').offsetTop || 0) - 250;
        document.querySelector('.min-box').scrollTop = (document.querySelector('.min-item.active').offsetTop || 0) - 250;
      });
    }
  }
};

/***/ }),

/***/ 292:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.datepicker-overlay[data-v-c26c2258] {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  z-index: 998;\n  top: 0;\n  left: 0;\n  overflow: hidden;\n  /* Safari, Chrome and Opera > 12.1 */\n  /* Firefox < 16 */\n  /* Internet Explorer */\n  /* Opera < 12.1 */\n  animation: fadein 0.5s;\n}\n@keyframes fadein {\nfrom {\n    opacity: 0;\n}\nto {\n    opacity: 1;\n}\n}\n/* Firefox < 16 */\n/* Safari, Chrome and Opera > 12.1 */\n/* Internet Explorer */\n/* Opera < 12.1 */\n.cov-date-body[data-v-c26c2258] {\n  display: inline-block;\n  background: #3F51B5;\n  overflow: hidden;\n  position: relative;\n  font-size: 16px;\n  font-family: 'Roboto';\n  font-weight: 400;\n  position: fixed;\n  display: block;\n  width: 400px;\n  max-width: 100%;\n  z-index: 999;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);\n}\n.cov-picker-box[data-v-c26c2258] {\n  background: #fff;\n  width: 100%;\n  display: inline-block;\n  padding: 25px;\n  box-sizing: border-box !important;\n  -moz-box-sizing: border-box !important;\n  -webkit-box-sizing: border-box !important;\n  -ms-box-sizing: border-box !important;\n  width: 400px;\n  max-width: 100%;\n  height: 280px;\n  text-align: start!important;\n}\n.cov-picker-box td[data-v-c26c2258] {\n  height: 34px;\n  width: 34px;\n  padding: 0;\n  line-height: 34px;\n  color: #000;\n  background: #fff;\n  text-align: center;\n  cursor: pointer;\n}\n.cov-picker-box td[data-v-c26c2258]:hover {\n  background: #E6E6E6;\n}\ntable[data-v-c26c2258] {\n  border-collapse: collapse;\n  border-spacing: 0;\n  width: 100%;\n}\n.day[data-v-c26c2258] {\n  width: 14.2857143%;\n  display: inline-block;\n  text-align: center;\n  cursor: pointer;\n  height: 34px;\n  padding: 0;\n  line-height: 34px;\n  color: #000;\n  background: #fff;\n  vertical-align: middle;\n}\n.week ul[data-v-c26c2258] {\n  margin: 0 0 8px;\n  padding: 0;\n  list-style: none;\n}\n.week ul li[data-v-c26c2258] {\n  width: 14.2%;\n  display: inline-block;\n  text-align: center;\n  background: transparent;\n  color: #000;\n  font-weight: bold;\n}\n.passive-day[data-v-c26c2258] {\n  color: #bbb;\n}\n.checked[data-v-c26c2258] {\n  background: #F50057;\n  color: #FFF !important;\n  border-radius: 3px;\n}\n.unavailable[data-v-c26c2258] {\n  color: #ccc;\n  cursor: not-allowed;\n}\n.cov-date-monthly[data-v-c26c2258] {\n  height: 150px;\n}\n.cov-date-monthly > div[data-v-c26c2258] {\n  display: inline-block;\n  padding: 0;\n  margin: 0;\n  vertical-align: middle;\n  color: #fff;\n  height: 150px;\n  float: left;\n  text-align: center;\n  cursor: pointer;\n}\n.cov-date-previous[data-v-c26c2258],\n.cov-date-next[data-v-c26c2258] {\n  position: relative;\n  width: 20% !important;\n  text-indent: -300px;\n  overflow: hidden;\n  color: #fff;\n}\n.cov-date-caption[data-v-c26c2258] {\n  width: 60%;\n  padding: 50px 0!important;\n  box-sizing: border-box;\n  font-size: 24px;\n}\n.cov-date-caption span[data-v-c26c2258]:hover {\n  color: rgba(255, 255, 255, 0.7);\n}\n.cov-date-previous[data-v-c26c2258]:hover,\n.cov-date-next[data-v-c26c2258]:hover {\n  background: rgba(255, 255, 255, 0.1);\n}\n.day[data-v-c26c2258]:hover {\n  background: #EAEAEA;\n}\n.unavailable[data-v-c26c2258]:hover {\n  background: none;\n}\n.checked[data-v-c26c2258]:hover {\n  background: #FF4F8E;\n}\n.cov-date-next[data-v-c26c2258]::before,\n.cov-date-previous[data-v-c26c2258]::before {\n  width: 20px;\n  height: 2px;\n  text-align: center;\n  position: absolute;\n  background: #fff;\n  top: 50%;\n  margin-top: -7px;\n  margin-left: -7px;\n  left: 50%;\n  line-height: 0;\n  content: '';\n  transform: rotate(45deg);\n}\n.cov-date-next[data-v-c26c2258]::after,\n.cov-date-previous[data-v-c26c2258]::after {\n  width: 20px;\n  height: 2px;\n  text-align: center;\n  position: absolute;\n  background: #fff;\n  margin-top: 6px;\n  margin-left: -7px;\n  top: 50%;\n  left: 50%;\n  line-height: 0;\n  content: '';\n  transform: rotate(-45deg);\n}\n.cov-date-previous[data-v-c26c2258]::after {\n  transform: rotate(45deg);\n}\n.cov-date-previous[data-v-c26c2258]::before {\n  transform: rotate(-45deg);\n}\n.date-item[data-v-c26c2258] {\n  text-align: center;\n  font-size: 20px;\n  padding: 10px 0;\n  cursor: pointer;\n}\n.date-item[data-v-c26c2258]:hover {\n  background: #e0e0e0;\n}\n.date-list[data-v-c26c2258] {\n  overflow: auto;\n  vertical-align: top;\n  padding: 0;\n}\n.cov-vue-date[data-v-c26c2258] {\n  display: inline-block;\n  color: #5D5D5D;\n}\n.button-box[data-v-c26c2258] {\n  background: #fff;\n  vertical-align: top;\n  height: 50px;\n  line-height: 50px;\n  text-align: right;\n  padding-right: 20px;\n}\n.button-box span[data-v-c26c2258] {\n  cursor: pointer;\n  padding: 10px 20px;\n}\n.watch-box[data-v-c26c2258] {\n  height: 100%;\n  overflow: hidden;\n}\n.hour-box[data-v-c26c2258],\n.min-box[data-v-c26c2258] {\n  display: inline-block;\n  width: 50%;\n  text-align: center;\n  height: 100%;\n  overflow: auto;\n  float: left;\n}\n.hour-box ul[data-v-c26c2258],\n.min-box ul[data-v-c26c2258] {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n}\n.hour-item[data-v-c26c2258],\n.min-item[data-v-c26c2258] {\n  padding: 10px;\n  font-size: 36px;\n  cursor: pointer;\n}\n.hour-item[data-v-c26c2258]:hover,\n.min-item[data-v-c26c2258]:hover {\n  background: #E3E3E3;\n}\n.hour-box .active[data-v-c26c2258],\n.min-box .active[data-v-c26c2258] {\n  background: #F50057;\n  color: #FFF !important;\n}\n[data-v-c26c2258]::-webkit-scrollbar {\n  width: 2px;\n}\n[data-v-c26c2258]::-webkit-scrollbar-track {\n  background: #E3E3E3;\n}\n[data-v-c26c2258]::-webkit-scrollbar-thumb {\n  background: #C1C1C1;\n  border-radius: 2px;\n}\n", ""]);

// exports


/***/ }),

/***/ 293:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "cov-vue-date"
  }, [_c('div', {
    staticClass: "datepickbox"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.date.time),
      expression: "date.time"
    }],
    staticClass: "cov-datepicker",
    style: (_vm.option.inputStyle ? _vm.option.inputStyle : {}),
    attrs: {
      "type": "text",
      "title": "input date",
      "readonly": "readonly",
      "placeholder": _vm.option.placeholder,
      "required": _vm.required
    },
    domProps: {
      "value": (_vm.date.time)
    },
    on: {
      "click": _vm.showCheck,
      "foucus": _vm.showCheck,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.date, "time", $event.target.value)
      }
    }
  })]), _vm._v(" "), (_vm.showInfo.check) ? _c('div', {
    staticClass: "datepicker-overlay",
    style: ({
      'background': _vm.option.overlayOpacity ? 'rgba(0,0,0,' + _vm.option.overlayOpacity + ')' : 'rgba(0,0,0,0.5)'
    }),
    on: {
      "click": function($event) {
        _vm.dismiss($event)
      }
    }
  }, [_c('div', {
    staticClass: "cov-date-body",
    style: ({
      'background-color': _vm.option.color ? _vm.option.color.header : '#3f51b5'
    })
  }, [_c('div', {
    staticClass: "cov-date-monthly"
  }, [_c('div', {
    staticClass: "cov-date-previous",
    on: {
      "click": function($event) {
        _vm.nextMonth('pre')
      }
    }
  }, [_vm._v("«")]), _vm._v(" "), _c('div', {
    staticClass: "cov-date-caption",
    style: ({
      'color': _vm.option.color ? _vm.option.color.headerText : '#fff'
    })
  }, [_c('span', {
    on: {
      "click": _vm.showYear
    }
  }, [_c('small', [_vm._v(_vm._s(_vm.checked.year))])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('span', {
    on: {
      "click": _vm.showMonth
    }
  }, [_vm._v(_vm._s(_vm.displayInfo.month))])]), _vm._v(" "), _c('div', {
    staticClass: "cov-date-next",
    on: {
      "click": function($event) {
        _vm.nextMonth('next')
      }
    }
  }, [_vm._v("»")])]), _vm._v(" "), (_vm.showInfo.day) ? _c('div', {
    staticClass: "cov-date-box"
  }, [_c('div', {
    staticClass: "cov-picker-box"
  }, [_c('div', {
    staticClass: "week"
  }, [_c('ul', _vm._l((_vm.library.week), function(weekie) {
    return _c('li', [_vm._v(_vm._s(weekie))])
  }))]), _vm._v(" "), _vm._l((_vm.dayList), function(day) {
    return _c('div', {
      staticClass: "day",
      class: {
        'checked': day.checked, 'unavailable': day.unavailable, 'passive-day': !(day.inMonth)
      },
      style: (day.checked ? (_vm.option.color && _vm.option.color.checkedDay ? {
        background: _vm.option.color.checkedDay
      } : {
        background: '#F50057'
      }) : {}),
      attrs: {
        "track-by": "$index"
      },
      on: {
        "click": function($event) {
          _vm.checkDay(day)
        }
      }
    }, [_vm._v(_vm._s(day.value))])
  })], 2)]) : _vm._e(), _vm._v(" "), (_vm.showInfo.year) ? _c('div', {
    staticClass: "cov-date-box list-box"
  }, [_c('div', {
    staticClass: "cov-picker-box date-list",
    attrs: {
      "id": "yearList"
    }
  }, _vm._l((_vm.library.year), function(yearItem) {
    return _c('div', {
      staticClass: "date-item",
      attrs: {
        "track-by": "$index"
      },
      on: {
        "click": function($event) {
          _vm.setYear(yearItem)
        }
      }
    }, [_vm._v(_vm._s(yearItem))])
  }))]) : _vm._e(), _vm._v(" "), (_vm.showInfo.month) ? _c('div', {
    staticClass: "cov-date-box list-box"
  }, [_c('div', {
    staticClass: "cov-picker-box date-list"
  }, _vm._l((_vm.library.month), function(monthItem) {
    return _c('div', {
      staticClass: "date-item",
      attrs: {
        "track-by": "$index"
      },
      on: {
        "click": function($event) {
          _vm.setMonth(monthItem)
        }
      }
    }, [_vm._v(_vm._s(monthItem))])
  }))]) : _vm._e(), _vm._v(" "), (_vm.showInfo.hour) ? _c('div', {
    staticClass: "cov-date-box list-box"
  }, [_c('div', {
    staticClass: "cov-picker-box date-list"
  }, [_c('div', {
    staticClass: "watch-box"
  }, [_c('div', {
    staticClass: "hour-box"
  }, [_c('div', {
    staticClass: "mui-pciker-rule mui-pciker-rule-ft"
  }), _vm._v(" "), _c('ul', _vm._l((_vm.hours), function(hitem) {
    return _c('li', {
      staticClass: "hour-item",
      class: {
        'active': hitem.checked
      },
      on: {
        "click": function($event) {
          _vm.setTime('hour', hitem, _vm.hours)
        }
      }
    }, [_vm._v(_vm._s(hitem.value))])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "min-box"
  }, [_c('div', {
    staticClass: "mui-pciker-rule mui-pciker-rule-ft"
  }), _vm._v(" "), _vm._l((_vm.mins), function(mitem) {
    return _c('div', {
      staticClass: "min-item",
      class: {
        'active': mitem.checked
      },
      on: {
        "click": function($event) {
          _vm.setTime('min', mitem, _vm.mins)
        }
      }
    }, [_vm._v(_vm._s(mitem.value))])
  })], 2)])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "button-box"
  }, [_c('span', {
    on: {
      "click": function($event) {
        _vm.showInfo.check = false
      }
    }
  }, [_vm._v(_vm._s(_vm.option.buttons ? _vm.option.buttons.cancel : 'Cancel'))]), _vm._v(" "), _c('span', {
    on: {
      "click": _vm.picked
    }
  }, [_vm._v(_vm._s(_vm.option.buttons ? _vm.option.buttons.ok : 'Ok'))])])])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-c26c2258", module.exports)
  }
}

/***/ }),

/***/ 294:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(292);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("47b522b8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../css-loader/index.js!../vue-loader/lib/style-rewriter.js?id=data-v-c26c2258&scoped=true!../vue-loader/lib/selector.js?type=styles&index=0!./vue-datepicker.vue", function() {
     var newContent = require("!!../css-loader/index.js!../vue-loader/lib/style-rewriter.js?id=data-v-c26c2258&scoped=true!../vue-loader/lib/selector.js?type=styles&index=0!./vue-datepicker.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 295:
/***/ (function(module, exports, __webpack_require__) {

/*!
 * Name: vue-upload-component
 * Version: 2.8.2
 * Author: LianYue
 */
(function (global, factory) {
	 true ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(global.VueUploadComponent = factory());
}(this, (function () { 'use strict';

/**
 * Creates a XHR request
 *
 * @param {Object} options
 */
var createRequest = function createRequest(options) {
  var xhr = new XMLHttpRequest();
  xhr.responseType = 'json';
  xhr.open(options.method || 'GET', options.url);
  if (options.headers) {
    Object.keys(options.headers).forEach(function (key) {
      xhr.setRequestHeader(key, options.headers[key]);
    });
  }

  return xhr;
};

/**
 * Sends a XHR request with certain body
 *
 * @param {XMLHttpRequest} xhr
 * @param {Object} body
 */
var sendRequest = function sendRequest(xhr, body) {
  return new Promise(function (resolve, reject) {
    xhr.onload = function () {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(xhr.response);
      } else {
        reject(xhr.response);
      }
    };
    xhr.onerror = function () {
      return reject(xhr.response);
    };
    xhr.send(JSON.stringify(body));
  });
};

/**
 * Sends a XHR request with certain form data
 *
 * @param {XMLHttpRequest} xhr
 * @param {Object} data
 */
var sendFormRequest = function sendFormRequest(xhr, data) {
  var body = new FormData();
  for (var name in data) {
    body.append(name, data[name]);
  }

  return new Promise(function (resolve, reject) {
    xhr.onload = function () {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(xhr.response);
      } else {
        reject(xhr.response);
      }
    };
    xhr.onerror = function () {
      return reject(xhr.response);
    };
    xhr.send(body);
  });
};

/**
 * Creates and sends XHR request
 *
 * @param {Object} options
 *
 * @returns Promise
 */
var request = function (options) {
  var xhr = createRequest(options);

  return sendRequest(xhr, options.body);
};

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ChunkUploadHandler = function () {
  /**
   * Constructor
   *
   * @param {File} file
   * @param {Object} options
   */
  function ChunkUploadHandler(file, options) {
    _classCallCheck(this, ChunkUploadHandler);

    this.file = file;
    this.options = options;
  }

  /**
   * Gets the max retries from options
   */


  _createClass(ChunkUploadHandler, [{
    key: 'createChunks',


    /**
     * Creates all the chunks in the initial state
     */
    value: function createChunks() {
      this.chunks = [];

      var start = 0;
      var end = this.chunkSize;
      while (start < this.fileSize) {
        this.chunks.push({
          blob: this.file.file.slice(start, end),
          startOffset: start,
          active: false,
          retries: this.maxRetries
        });
        start = end;
        end = start + this.chunkSize;
      }
    }

    /**
     * Updates the progress of the file with the handler's progress
     */

  }, {
    key: 'updateFileProgress',
    value: function updateFileProgress() {
      this.file.progress = this.progress;
    }

    /**
     * Paues the upload process
     * - Stops all active requests
     * - Sets the file not active
     */

  }, {
    key: 'pause',
    value: function pause() {
      this.file.active = false;
      this.stopChunks();
    }

    /**
     * Stops all the current chunks
     */

  }, {
    key: 'stopChunks',
    value: function stopChunks() {
      this.chunksUploading.forEach(function (chunk) {
        chunk.xhr.abort();
        chunk.active = false;
      });
    }

    /**
     * Resumes the file upload
     * - Sets the file active
     * - Starts the following chunks
     */

  }, {
    key: 'resume',
    value: function resume() {
      this.file.active = true;
      this.startChunking();
    }

    /**
     * Starts the file upload
     *
     * @returns Promise
     * - resolve  The file was uploaded
     * - reject   The file upload failed
     */

  }, {
    key: 'upload',
    value: function upload() {
      var _this = this;

      this.promise = new Promise(function (resolve, reject) {
        _this.resolve = resolve;
        _this.reject = reject;
      });
      this.start();

      return this.promise;
    }

    /**
     * Start phase
     * Sends a request to the backend to initialise the chunks
     */

  }, {
    key: 'start',
    value: function start() {
      var _this2 = this;

      request({
        method: 'POST',
        headers: Object.assign({}, this.headers, {
          'Content-Type': 'application/json'
        }),
        url: this.action,
        body: Object.assign(this.startBody, {
          phase: 'start',
          mime_type: this.fileType,
          size: this.fileSize
        })
      }).then(function (res) {
        if (res.status !== 'success') {
          _this2.file.response = res;
          return _this2.reject('server');
        }

        _this2.sessionId = res.data.session_id;
        _this2.chunkSize = res.data.end_offset;

        _this2.createChunks();
        _this2.startChunking();
      }).catch(function (res) {
        _this2.file.response = res;
        _this2.reject('server');
      });
    }

    /**
     * Starts to upload chunks
     */

  }, {
    key: 'startChunking',
    value: function startChunking() {
      for (var i = 0; i < this.maxActiveChunks; i++) {
        this.uploadNextChunk();
      }
    }

    /**
     * Uploads the next chunk
     * - Won't do anything if the process is paused
     * - Will start finish phase if there are no more chunks to upload
     */

  }, {
    key: 'uploadNextChunk',
    value: function uploadNextChunk() {
      if (this.file.active) {
        if (this.hasChunksToUpload) {
          return this.uploadChunk(this.chunksToUpload[0]);
        }

        if (this.chunksUploading.length === 0) {
          return this.finish();
        }
      }
    }

    /**
     * Uploads a chunk
     * - Sends the chunk to the backend
     * - Sets the chunk as uploaded if everything went well
     * - Decreases the number of retries if anything went wrong
     * - Fails if there are no more retries
     *
     * @param {Object} chunk
     */

  }, {
    key: 'uploadChunk',
    value: function uploadChunk(chunk) {
      var _this3 = this;

      chunk.progress = 0;
      chunk.active = true;
      this.updateFileProgress();
      chunk.xhr = createRequest({
        method: 'POST',
        headers: this.headers,
        url: this.action
      });

      chunk.xhr.upload.addEventListener('progress', function (evt) {
        if (evt.lengthComputable) {
          chunk.progress = Math.round(evt.loaded / evt.total * 100);
        }
      }, false);

      sendFormRequest(chunk.xhr, Object.assign(this.uploadBody, {
        phase: 'upload',
        session_id: this.sessionId,
        start_offset: chunk.startOffset,
        chunk: chunk.blob
      })).then(function (res) {
        chunk.active = false;
        if (res.status === 'success') {
          chunk.uploaded = true;
        } else {
          if (chunk.retries-- <= 0) {
            _this3.stopChunks();
            return _this3.reject('upload');
          }
        }

        _this3.uploadNextChunk();
      }).catch(function () {
        chunk.active = false;
        if (chunk.retries-- <= 0) {
          _this3.stopChunks();
          return _this3.reject('upload');
        }

        _this3.uploadNextChunk();
      });
    }

    /**
     * Finish phase
     * Sends a request to the backend to finish the process
     */

  }, {
    key: 'finish',
    value: function finish() {
      var _this4 = this;

      this.updateFileProgress();

      request({
        method: 'POST',
        headers: Object.assign({}, this.headers, {
          'Content-Type': 'application/json'
        }),
        url: this.action,
        body: Object.assign(this.finishBody, {
          phase: 'finish',
          session_id: this.sessionId
        })
      }).then(function (res) {
        _this4.file.response = res;
        if (res.status !== 'success') {
          return _this4.reject('server');
        }

        _this4.resolve(res);
      }).catch(function (res) {
        _this4.file.response = res;
        _this4.reject('server');
      });
    }
  }, {
    key: 'maxRetries',
    get: function get() {
      return parseInt(this.options.maxRetries);
    }

    /**
     * Gets the max number of active chunks being uploaded at once from options
     */

  }, {
    key: 'maxActiveChunks',
    get: function get() {
      return parseInt(this.options.maxActive);
    }

    /**
     * Gets the file type
     */

  }, {
    key: 'fileType',
    get: function get() {
      return this.file.type;
    }

    /**
     * Gets the file size
     */

  }, {
    key: 'fileSize',
    get: function get() {
      return this.file.size;
    }

    /**
     * Gets action (url) to upload the file
     */

  }, {
    key: 'action',
    get: function get() {
      return this.options.action || null;
    }

    /**
     * Gets the body to be merged when sending the request in start phase
     */

  }, {
    key: 'startBody',
    get: function get() {
      return this.options.startBody || {};
    }

    /**
     * Gets the body to be merged when sending the request in upload phase
     */

  }, {
    key: 'uploadBody',
    get: function get() {
      return this.options.uploadBody || {};
    }

    /**
     * Gets the body to be merged when sending the request in finish phase
     */

  }, {
    key: 'finishBody',
    get: function get() {
      return this.options.finishBody || {};
    }

    /**
     * Gets the headers of the requests from options
     */

  }, {
    key: 'headers',
    get: function get() {
      return this.options.headers || {};
    }

    /**
     * Whether it's ready to upload files or not
     */

  }, {
    key: 'readyToUpload',
    get: function get() {
      return !!this.chunks;
    }

    /**
     * Gets the progress of the chunk upload
     * - Gets all the completed chunks
     * - Gets the progress of all the chunks that are being uploaded
     */

  }, {
    key: 'progress',
    get: function get() {
      var _this5 = this;

      var completedProgress = this.chunksUploaded.length / this.chunks.length * 100;
      var uploadingProgress = this.chunksUploading.reduce(function (progress, chunk) {
        return progress + (chunk.progress | 0) / _this5.chunks.length;
      }, 0);

      return Math.min(completedProgress + uploadingProgress, 100);
    }

    /**
     * Gets all the chunks that are pending to be uploaded
     */

  }, {
    key: 'chunksToUpload',
    get: function get() {
      return this.chunks.filter(function (chunk) {
        return !chunk.active && !chunk.uploaded;
      });
    }

    /**
     * Whether there are chunks to upload or not
     */

  }, {
    key: 'hasChunksToUpload',
    get: function get() {
      return this.chunksToUpload.length > 0;
    }

    /**
     * Gets all the chunks that are uploading
     */

  }, {
    key: 'chunksUploading',
    get: function get() {
      return this.chunks.filter(function (chunk) {
        return !!chunk.xhr && !!chunk.active;
      });
    }

    /**
     * Gets all the chunks that have finished uploading
     */

  }, {
    key: 'chunksUploaded',
    get: function get() {
      return this.chunks.filter(function (chunk) {
        return !!chunk.uploaded;
      });
    }
  }]);

  return ChunkUploadHandler;
}();

(function () {
  if (typeof document !== 'undefined') {
    var head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style'),
        css = "";style.type = 'text/css';if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }head.appendChild(style);
  }
})();

var InputFile = { render: function render() {
    var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;return _c('input', { attrs: { "type": "file", "name": _vm.$parent.name, "id": _vm.$parent.inputId || _vm.$parent.name, "accept": _vm.$parent.accept, "capture": _vm.$parent.capture, "webkitdirectory": _vm.$parent.directory && _vm.$parent.features.directory, "directory": _vm.$parent.directory && _vm.$parent.features.directory, "multiple": _vm.$parent.multiple && _vm.$parent.features.html5 }, on: { "change": _vm.change } });
  }, staticRenderFns: [],
  methods: {
    change: function change(e) {
      this.$destroy();
      this.$parent.addInputFile(e.target);
      // eslint-disable-next-line
      new this.constructor({
        parent: this.$parent,
        el: this.$el
      });
    }
  }
};

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

(function () {
  if (typeof document !== 'undefined') {
    var head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style'),
        css = " .file-uploads { overflow: hidden; position: relative; text-align: center; display: inline-block; } .file-uploads.file-uploads-html4 input[type=\"file\"] { opacity: 0; font-size: 20em; z-index: 1; top: 0; left: 0; right: 0; bottom: 0; position: absolute; width: 100%; height: 100%; } .file-uploads.file-uploads-html5 input[type=\"file\"] { overflow: hidden; position: fixed; width: 1px; height: 1px; z-index: -1; opacity: 0; } ";style.type = 'text/css';if (style.styleSheet) {
      style.styleSheet.cssText = css;
    } else {
      style.appendChild(document.createTextNode(css));
    }head.appendChild(style);
  }
})();

var CHUNK_DEFAULT_OPTIONS = {
  headers: {},
  action: '',
  minSize: 1048576,
  maxActive: 3,
  maxRetries: 5,

  handler: ChunkUploadHandler
};

var FileUpload = { render: function render() {
    var _vm = this;var _h = _vm.$createElement;var _c = _vm._self._c || _h;return _c('label', { class: _vm.className }, [_vm._t("default"), _vm._v(" "), _c('input-file')], 2);
  }, staticRenderFns: [],
  components: {
    InputFile: InputFile
  },
  props: {
    inputId: {
      type: String
    },

    name: {
      type: String,
      default: 'file'
    },

    accept: {
      type: String
    },

    capture: {},

    multiple: {
      type: Boolean
    },

    maximum: {
      type: Number,
      default: function _default() {
        return this.multiple ? 0 : 1;
      }
    },

    addIndex: {
      type: [Boolean, Number]
    },

    directory: {
      type: Boolean
    },

    postAction: {
      type: String
    },

    putAction: {
      type: String
    },

    customAction: {
      type: Function
    },

    headers: {
      type: Object,
      default: Object
    },

    data: {
      type: Object,
      default: Object
    },

    timeout: {
      type: Number,
      default: 0
    },

    drop: {
      default: false
    },

    dropDirectory: {
      type: Boolean,
      default: true
    },

    size: {
      type: Number,
      default: 0
    },

    extensions: {
      default: Array
    },

    value: {
      type: Array,
      default: Array
    },

    thread: {
      type: Number,
      default: 1
    },

    // Chunk upload enabled
    chunkEnabled: {
      type: Boolean,
      default: false
    },

    // Chunk upload properties
    chunk: {
      type: Object,
      default: function _default() {
        return CHUNK_DEFAULT_OPTIONS;
      }
    }
  },

  data: function data() {
    return {
      files: this.value,
      features: {
        html5: true,
        directory: false,
        drag: false
      },

      active: false,
      dropActive: false,

      uploading: 0,

      destroy: false
    };
  },


  /**
   * mounted
   * @return {[type]} [description]
   */
  mounted: function mounted() {
    var input = document.createElement('input');
    input.type = 'file';
    input.multiple = true;

    // html5 特征
    if (window.FormData && input.files) {
      // 上传目录特征
      if (typeof input.webkitdirectory === 'boolean' || typeof input.directory === 'boolean') {
        this.features.directory = true;
      }

      // 拖拽特征
      if (this.features.html5 && typeof input.ondrop !== 'undefined') {
        this.features.drop = true;
      }
    } else {
      this.features.html5 = false;
    }

    // files 定位缓存
    this.maps = {};

    this.$nextTick(function () {

      // 更新下父级
      if (this.$parent) {
        this.$parent.$forceUpdate();
      }

      // 拖拽渲染
      this.watchDrop(this.drop);
    });
  },


  /**
   * beforeDestroy
   * @return {[type]} [description]
   */
  beforeDestroy: function beforeDestroy() {
    // 已销毁
    this.destroy = true;

    // 设置成不激活
    this.active = false;
  },


  computed: {
    /**
     * uploading 正在上传的线程
     * @return {[type]} [description]
     */

    /**
     * uploaded 文件列表是否全部已上传
     * @return {[type]} [description]
     */
    uploaded: function uploaded() {
      var file = void 0;
      for (var i = 0; i < this.files.length; i++) {
        file = this.files[i];
        if (file.fileObject && !file.error && !file.success) {
          return false;
        }
      }
      return true;
    },
    chunkOptions: function chunkOptions() {
      return Object.assign(CHUNK_DEFAULT_OPTIONS, this.chunk);
    },
    className: function className() {
      return ['file-uploads', this.features.html5 ? 'file-uploads-html5' : 'file-uploads-html4', this.features.directory && this.directory ? 'file-uploads-directory' : undefined, this.features.drop && this.drop ? 'file-uploads-drop' : undefined];
    }
  },

  watch: {
    active: function active(_active) {
      this.watchActive(_active);
    },
    dropActive: function dropActive() {
      if (this.$parent) {
        this.$parent.$forceUpdate();
      }
    },
    drop: function drop(value) {
      this.watchDrop(value);
    },
    value: function value(files) {
      if (this.files === files) {
        return;
      }
      this.files = files;

      var oldMaps = this.maps;

      // 重写 maps 缓存
      this.maps = {};
      for (var i = 0; i < this.files.length; i++) {
        var file = this.files[i];
        this.maps[file.id] = file;
      }

      // add, update
      for (var key in this.maps) {
        var newFile = this.maps[key];
        var oldFile = oldMaps[key];
        if (newFile !== oldFile) {
          this.emitFile(newFile, oldFile);
        }
      }

      // delete
      for (var _key in oldMaps) {
        if (!this.maps[_key]) {
          this.emitFile(undefined, oldMaps[_key]);
        }
      }
    }
  },

  methods: {

    // 清空
    clear: function clear() {
      if (this.files.length) {
        var files = this.files;
        this.files = [];

        // 定位
        this.maps = {};

        // 事件
        this.emitInput();
        for (var i = 0; i < files.length; i++) {
          this.emitFile(undefined, files[i]);
        }
      }
      return true;
    },


    // 选择
    get: function get(id) {
      if (!id) {
        return false;
      }

      if ((typeof id === 'undefined' ? 'undefined' : _typeof(id)) === 'object') {
        return this.maps[id.id] || false;
      }

      return this.maps[id] || false;
    },


    // 添加
    add: function add(_files) {
      var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.addIndex;

      var files = _files;
      var isArray = files instanceof Array;

      // 不是数组整理成数组
      if (!isArray) {
        files = [files];
      }

      // 遍历规范对象
      var addFiles = [];
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        if (this.features.html5 && file instanceof Blob) {
          file = {
            file: file,
            size: file.size,
            name: file.webkitRelativePath || file.relativePath || file.name || 'unknown',
            type: file.type
          };
        }
        var fileObject = false;
        if (file.fileObject === false) {
          // false
        } else if (file.fileObject) {
          fileObject = true;
        } else if (typeof Element !== 'undefined' && file.el instanceof Element) {
          fileObject = true;
        } else if (typeof Blob !== 'undefined' && file.file instanceof Blob) {
          fileObject = true;
        }
        if (fileObject) {
          file = _extends({
            fileObject: true,
            size: -1,
            name: 'Filename',
            type: '',
            active: false,
            error: '',
            success: false,
            putAction: this.putAction,
            postAction: this.postAction,
            timeout: this.timeout
          }, file, {
            response: {},

            progress: '0.00', // 只读
            speed: 0 // 只读
            // xhr: false,                // 只读
            // iframe: false,             // 只读
          });

          file.data = _extends({}, this.data, file.data ? file.data : {});

          file.headers = _extends({}, this.headers, file.headers ? file.headers : {});
        }

        // 必须包含 id
        if (!file.id) {
          file.id = Math.random().toString(36).substr(2);
        }

        if (this.emitFilter(file, undefined)) {
          continue;
        }

        // 最大数量限制
        if (this.maximum > 1 && addFiles.length + this.files.length >= this.maximum) {
          break;
        }

        addFiles.push(file);

        // 最大数量限制
        if (this.maximum === 1) {
          break;
        }
      }

      // 没有文件
      if (!addFiles.length) {
        return false;
      }

      // 如果是 1 清空
      if (this.maximum === 1) {
        this.clear();
      }

      // 添加进去 files
      var newFiles = void 0;
      if (index === true || index === 0) {
        newFiles = addFiles.concat(this.files);
      } else if (index) {
        newFiles = addFiles.concat([]);
        newFiles.splice(index, 0, addFiles);
      } else {
        newFiles = this.files.concat(addFiles);
      }

      this.files = newFiles;

      // 定位
      for (var _i = 0; _i < addFiles.length; _i++) {
        var _file2 = addFiles[_i];
        this.maps[_file2.id] = _file2;
      }

      // 事件
      this.emitInput();
      for (var _i2 = 0; _i2 < addFiles.length; _i2++) {
        this.emitFile(addFiles[_i2], undefined);
      }

      return isArray ? addFiles : addFiles[0];
    },


    // 添加表单文件
    addInputFile: function addInputFile(el) {
      var files = [];
      if (el.files) {
        for (var i = 0; i < el.files.length; i++) {
          var file = el.files[i];
          files.push({
            size: file.size,
            name: file.webkitRelativePath || file.relativePath || file.name,
            type: file.type,
            file: file,
            el: el
          });
        }
      } else {
        files.push({
          name: el.value.replace(/^.*?([^\/\\\r\n]+)$/, '$1'),
          el: el
        });
      }
      return this.add(files);
    },


    // 添加 DataTransfer
    addDataTransfer: function addDataTransfer(dataTransfer) {
      var _this = this;

      var files = [];
      if (dataTransfer.items && dataTransfer.items.length) {
        var items = [];
        for (var i = 0; i < dataTransfer.items.length; i++) {
          var item = dataTransfer.items[i];
          if (item.getAsEntry) {
            item = item.getAsEntry() || item.getAsFile();
          } else if (item.webkitGetAsEntry) {
            item = item.webkitGetAsEntry() || item.getAsFile();
          } else {
            item = item.getAsFile();
          }
          if (item) {
            items.push(item);
          }
        }

        return new Promise(function (resolve, reject) {
          var forEach = function forEach(i) {
            var item = items[i];
            // 结束 文件数量大于 最大数量
            if (!item || _this.maximum > 0 && files.length >= _this.maximum) {
              return resolve(_this.add(files));
            }
            _this.getEntry(item).then(function (results) {
              files.push.apply(files, _toConsumableArray(results));
              forEach(i + 1);
            });
          };
          forEach(0);
        });
      }

      if (dataTransfer.files.length) {
        for (var _i3 = 0; _i3 < dataTransfer.files.length; _i3++) {
          files.push(dataTransfer.files[_i3]);
          if (this.maximum > 0 && files.length >= this.maximum) {
            break;
          }
        }
        return Promise.resolve(this.add(files));
      }

      return Promise.resolve([]);
    },


    // 获得 entry
    getEntry: function getEntry(entry) {
      var _this2 = this;

      var path = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

      return new Promise(function (resolve, reject) {
        if (entry.isFile) {
          entry.file(function (file) {
            resolve([{
              size: file.size,
              name: path + file.name,
              type: file.type,
              file: file
            }]);
          });
        } else if (entry.isDirectory && _this2.dropDirectory) {
          var files = [];
          var dirReader = entry.createReader();
          var readEntries = function readEntries() {
            dirReader.readEntries(function (entries) {
              var forEach = function forEach(i) {
                if (!entries[i] && i === 0 || _this2.maximum > 0 && files.length >= _this2.maximum) {
                  return resolve(files);
                }
                if (!entries[i]) {
                  return readEntries();
                }
                _this2.getEntry(entries[i], path + entry.name + '/').then(function (results) {
                  files.push.apply(files, _toConsumableArray(results));
                  forEach(i + 1);
                });
              };
              forEach(0);
            });
          };
          readEntries();
        } else {
          resolve([]);
        }
      });
    },
    replace: function replace(id1, id2) {
      var file1 = this.get(id1);
      var file2 = this.get(id2);
      if (!file1 || !file2 || file1 === file2) {
        return false;
      }
      var files = this.files.concat([]);
      var index1 = files.indexOf(file1);
      var index2 = files.indexOf(file2);
      if (index1 === -1 || index2 === -1) {
        return false;
      }
      files[index1] = file2;
      files[index2] = file1;
      this.files = files;
      this.emitInput();
      return true;
    },


    // 移除
    remove: function remove(id) {
      var file = this.get(id);
      if (file) {
        if (this.emitFilter(undefined, file)) {
          return false;
        }
        var files = this.files.concat([]);
        var index = files.indexOf(file);
        if (index === -1) {
          console.error('remove', file);
          return false;
        }
        files.splice(index, 1);
        this.files = files;

        // 定位
        delete this.maps[file.id];

        // 事件
        this.emitInput();
        this.emitFile(undefined, file);
      }
      return file;
    },


    // 更新
    update: function update(id, data) {
      var file = this.get(id);
      if (file) {
        var newFile = _extends({}, file, data);
        // 停用必须加上错误
        if (file.fileObject && file.active && !newFile.active && !newFile.error && !newFile.success) {
          newFile.error = 'abort';
        }

        if (this.emitFilter(newFile, file)) {
          return false;
        }

        var files = this.files.concat([]);
        var index = files.indexOf(file);
        if (index === -1) {
          console.error('update', file);
          return false;
        }
        files.splice(index, 1, newFile);
        this.files = files;

        // 删除  旧定位 写入 新定位 （已便支持修改id)
        delete this.maps[file.id];
        this.maps[newFile.id] = newFile;

        // 事件
        this.emitInput();
        this.emitFile(newFile, file);
        return newFile;
      }
      return false;
    },


    // 预处理 事件 过滤器
    emitFilter: function emitFilter(newFile, oldFile) {
      var isPrevent = false;
      this.$emit('input-filter', newFile, oldFile, function () {
        isPrevent = true;
        return isPrevent;
      });
      return isPrevent;
    },


    // 处理后 事件 分发
    emitFile: function emitFile(newFile, oldFile) {
      this.$emit('input-file', newFile, oldFile);
      if (newFile && newFile.fileObject && newFile.active && (!oldFile || !oldFile.active)) {
        this.uploading++;
        // 激活
        this.$nextTick(function () {
          var _this3 = this;

          setTimeout(function () {
            _this3.upload(newFile).then(function () {
              // eslint-disable-next-line
              newFile = _this3.get(newFile);
              if (newFile && newFile.fileObject) {
                _this3.update(newFile, {
                  active: false,
                  success: !newFile.error
                });
              }
            }).catch(function (e) {
              _this3.update(newFile, {
                active: false,
                success: false,
                error: e.code || e.error || e.message || e
              });
            });
          }, parseInt(Math.random() * 50 + 50, 10));
        });
      } else if ((!newFile || !newFile.fileObject || !newFile.active) && oldFile && oldFile.fileObject && oldFile.active) {
        // 停止
        this.uploading--;
      }

      // 自动延续激活
      if (this.active && (Boolean(newFile) !== Boolean(oldFile) || newFile.active !== oldFile.active)) {
        this.watchActive(true);
      }
    },
    emitInput: function emitInput() {
      this.$emit('input', this.files);
    },


    // 上传
    upload: function upload(id) {
      var file = this.get(id);

      // 被删除
      if (!file) {
        return Promise.reject('not_exists');
      }

      // 不是文件对象
      if (!file.fileObject) {
        return Promise.reject('file_object');
      }

      // 有错误直接响应
      if (file.error) {
        return Promise.reject(file.error);
      }

      // 已完成直接响应
      if (file.success) {
        return Promise.resolve(file);
      }

      // 后缀
      var extensions = this.extensions;
      if (extensions && (extensions.length || typeof extensions.length === 'undefined')) {
        if ((typeof extensions === 'undefined' ? 'undefined' : _typeof(extensions)) !== 'object' || !(extensions instanceof RegExp)) {
          if (typeof extensions === 'string') {
            extensions = extensions.split(',').map(function (value) {
              return value.trim();
            }).filter(function (value) {
              return value;
            });
          }
          extensions = new RegExp('\\.(' + extensions.join('|').replace(/\./g, '\\.') + ')$', 'i');
        }
        if (file.name.search(extensions) === -1) {
          return Promise.reject('extension');
        }
      }

      // 大小
      if (this.size > 0 && file.size >= 0 && file.size > this.size) {
        return Promise.reject('size');
      }

      if (this.customAction) {
        return this.customAction(file, this);
      }

      if (this.features.html5) {
        if (this.shouldUseChunkUpload(file)) {
          return this.uploadChunk(file);
        }
        if (file.putAction) {
          return this.uploadPut(file);
        }
        if (file.postAction) {
          return this.uploadHtml5(file);
        }
      }
      if (file.postAction) {
        return this.uploadHtml4(file);
      }
      return Promise.reject('No action configured');
    },


    /**
     * Whether this file should be uploaded using chunk upload or not
     *
     * @param Object file
     */
    shouldUseChunkUpload: function shouldUseChunkUpload(file) {
      return this.chunkEnabled && !!this.chunkOptions.handler && file.size > this.chunkOptions.minSize;
    },


    /**
     * Upload a file using Chunk method
     *
     * @param File file
     */
    uploadChunk: function uploadChunk(file) {
      var HandlerClass = this.chunkOptions.handler;
      file.chunk = new HandlerClass(file, this.chunkOptions);

      return file.chunk.upload();
    },
    uploadPut: function uploadPut(file) {
      var querys = [];
      var value = void 0;
      for (var key in file.data) {
        value = file.data[key];
        if (value !== null && value !== undefined) {
          querys.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
        }
      }
      var queryString = querys.length ? (file.putAction.indexOf('?') === -1 ? '?' : '&') + querys.join('&') : '';
      var xhr = new XMLHttpRequest();
      xhr.open('PUT', file.putAction + queryString);
      return this.uploadXhr(xhr, file, file.file);
    },
    uploadHtml5: function uploadHtml5(file) {
      var form = new window.FormData();
      var value = void 0;
      for (var key in file.data) {
        value = file.data[key];
        if (value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && typeof value.toString !== 'function') {
          if (value instanceof File) {
            form.append(key, value, value.name);
          } else {
            form.append(key, JSON.stringify(value));
          }
        } else if (value !== null && value !== undefined) {
          form.append(key, value);
        }
      }
      form.append(this.name, file.file, file.file.filename || file.name);
      var xhr = new XMLHttpRequest();
      xhr.open('POST', file.postAction);
      return this.uploadXhr(xhr, file, form);
    },
    uploadXhr: function uploadXhr(xhr, _file, body) {
      var _this4 = this;

      var file = _file;
      var speedTime = 0;
      var speedLoaded = 0;

      // 进度条
      xhr.upload.onprogress = function (e) {
        // 还未开始上传 已删除 未激活
        file = _this4.get(file);
        if (!e.lengthComputable || !file || !file.fileObject || !file.active) {
          return;
        }

        // 进度 速度 每秒更新一次
        var speedTime2 = Math.round(Date.now() / 1000);
        if (speedTime2 === speedTime) {
          return;
        }
        speedTime = speedTime2;

        file = _this4.update(file, {
          progress: (e.loaded / e.total * 100).toFixed(2),
          speed: e.loaded - speedLoaded
        });
        speedLoaded = e.loaded;
      };

      // 检查激活状态
      var interval = setInterval(function () {
        file = _this4.get(file);
        if (file && file.fileObject && !file.success && !file.error && file.active) {
          return;
        }

        if (interval) {
          clearInterval(interval);
          interval = false;
        }

        try {
          xhr.abort();
          xhr.timeout = 1;
        } catch (e) {}
      }, 100);

      return new Promise(function (resolve, reject) {
        var complete = void 0;
        var fn = function fn(e) {
          // 已经处理过了
          if (complete) {
            return;
          }
          complete = true;
          if (interval) {
            clearInterval(interval);
            interval = false;
          }

          file = _this4.get(file);

          // 不存在直接响应
          if (!file) {
            return reject('not_exists');
          }

          // 不是文件对象
          if (!file.fileObject) {
            return reject('file_object');
          }

          // 有错误自动响应
          if (file.error) {
            return reject(file.error);
          }

          // 未激活
          if (!file.active) {
            return reject('abort');
          }

          // 已完成 直接相应
          if (file.success) {
            return resolve(file);
          }

          var data = {};

          switch (e.type) {
            case 'timeout':
            case 'abort':
              data.error = e.type;
              break;
            case 'error':
              if (!xhr.status) {
                data.error = 'network';
              } else if (xhr.status >= 500) {
                data.error = 'server';
              } else if (xhr.status >= 400) {
                data.error = 'denied';
              }
              break;
            default:
              if (xhr.status >= 500) {
                data.error = 'server';
              } else if (xhr.status >= 400) {
                data.error = 'denied';
              } else {
                data.progress = '100.00';
              }
          }

          if (xhr.responseText) {
            var contentType = xhr.getResponseHeader('Content-Type');
            if (contentType && contentType.indexOf('/json') !== -1) {
              data.response = JSON.parse(xhr.responseText);
            } else {
              data.response = xhr.responseText;
            }
          }

          // 更新
          file = _this4.update(file, data);

          // 相应错误
          if (file.error) {
            return reject(file.error);
          }

          // 响应
          return resolve(file);
        };

        // 事件
        xhr.onload = fn;
        xhr.onerror = fn;
        xhr.onabort = fn;
        xhr.ontimeout = fn;

        // 超时
        if (file.timeout) {
          xhr.timeout = file.timeout;
        }

        // headers
        for (var key in file.headers) {
          xhr.setRequestHeader(key, file.headers[key]);
        }

        // 更新 xhr
        file = _this4.update(file, { xhr: xhr });

        // 开始上传
        xhr.send(body);
      });
    },
    uploadHtml4: function uploadHtml4(_file) {
      var _this5 = this;

      var file = _file;
      var onKeydown = function onKeydown(e) {
        if (e.keyCode === 27) {
          e.preventDefault();
        }
      };

      var iframe = document.createElement('iframe');
      iframe.id = 'upload-iframe-' + file.id;
      iframe.name = 'upload-iframe-' + file.id;
      iframe.src = 'about:blank';
      iframe.setAttribute('style', 'width:1px;height:1px;top:-999em;position:absolute; margin-top:-999em;');

      var form = document.createElement('form');

      form.action = file.postAction;

      form.name = 'upload-form-' + file.id;

      form.setAttribute('method', 'POST');
      form.setAttribute('target', 'upload-iframe-' + file.id);
      form.setAttribute('enctype', 'multipart/form-data');

      var value = void 0;
      var input = void 0;
      for (var key in file.data) {
        value = file.data[key];
        if (value && (typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object' && typeof value.toString !== 'function') {
          value = JSON.stringify(value);
        }
        if (value !== null && value !== undefined) {
          input = document.createElement('input');
          input.type = 'hidden';
          input.name = key;
          input.value = value;
          form.appendChild(input);
        }
      }
      form.appendChild(file.el);

      document.body.appendChild(iframe).appendChild(form);

      var getResponseData = function getResponseData() {
        var doc = void 0;
        try {
          if (iframe.contentWindow) {
            doc = iframe.contentWindow.document;
          }
        } catch (err) {}
        if (!doc) {
          try {
            doc = iframe.contentDocument ? iframe.contentDocument : iframe.document;
          } catch (err) {
            doc = iframe.document;
          }
        }
        if (doc && doc.body) {
          return doc.body.innerHTML;
        }
        return null;
      };

      return new Promise(function (resolve, reject) {
        setTimeout(function () {
          file = _this5.update(file, { iframe: iframe });

          // 不存在
          if (!file) {
            return reject('not_exists');
          }

          // 定时检查
          var interval = setInterval(function () {
            file = _this5.get(file);
            if (file && file.fileObject && !file.success && !file.error && file.active) {
              return;
            }

            if (interval) {
              clearInterval(interval);
              interval = false;
            }

            iframe.onabort({ type: file ? 'abort' : 'not_exists' });
          }, 100);

          var complete = void 0;
          var fn = function fn(e) {
            // 已经处理过了
            if (complete) {
              return;
            }
            complete = true;

            if (interval) {
              clearInterval(interval);
              interval = false;
            }

            // 关闭 esc 事件
            document.body.removeEventListener('keydown', onKeydown);

            file = _this5.get(file);

            // 不存在直接响应
            if (!file) {
              return reject('not_exists');
            }

            // 不是文件对象
            if (!file.fileObject) {
              return reject('file_object');
            }

            // 有错误自动响应
            if (file.error) {
              return reject(file.error);
            }

            // 未激活
            if (!file.active) {
              return reject('abort');
            }

            // 已完成 直接相应
            if (file.success) {
              return resolve(file);
            }

            var response = getResponseData();
            var data = {};
            switch (e.type) {
              case 'abort':
                data.error = 'abort';
                break;
              case 'error':
                if (file.error) {
                  data.error = file.error;
                } else if (response === null) {
                  data.error = 'network';
                } else {
                  data.error = 'denied';
                }
                break;
              default:
                if (file.error) {
                  data.error = file.error;
                } else if (data === null) {
                  data.error = 'network';
                } else {
                  data.progress = '100.00';
                }
            }

            if (response !== null) {
              if (response && response.substr(0, 1) === '{' && response.substr(response.length - 1, 1) === '}') {
                try {
                  response = JSON.parse(response);
                } catch (err) {}
              }
              data.response = response;
            }

            // 更新
            file = _this5.update(file, data);

            if (file.error) {
              return reject(file.error);
            }

            // 响应
            return resolve(file);
          };

          // 添加事件
          iframe.onload = fn;
          iframe.onerror = fn;
          iframe.onabort = fn;

          // 禁止 esc 键
          document.body.addEventListener('keydown', onKeydown);

          // 提交
          form.submit();
        }, 50);
      }).then(function (res) {
        iframe.parentNode && iframe.parentNode.removeChild(iframe);
        return res;
      }).catch(function (res) {
        iframe.parentNode && iframe.parentNode.removeChild(iframe);
        return res;
      });
    },
    watchActive: function watchActive(active) {
      var file = void 0;
      var index = 0;
      while (file = this.files[index]) {
        index++;
        if (!file.fileObject) {
          // 不是文件对象
        } else if (active && !this.destroy) {
          if (this.uploading >= this.thread || this.uploading && !this.features.html5) {
            break;
          }
          if (!file.active && !file.error && !file.success) {
            this.update(file, { active: true });
          }
        } else {
          if (file.active) {
            this.update(file, { active: false });
          }
        }
      }
      if (this.uploading === 0) {
        this.active = false;
      }
    },
    watchDrop: function watchDrop(_el) {
      var el = _el;
      if (!this.features.drop) {
        return;
      }

      // 移除挂载
      if (this.dropElement) {
        try {
          document.removeEventListener('dragenter', this.onDragenter, false);
          document.removeEventListener('dragleave', this.onDragleave, false);
          document.removeEventListener('drop', this.onDocumentDrop, false);
          this.dropElement.removeEventListener('dragover', this.onDragover, false);
          this.dropElement.removeEventListener('drop', this.onDrop, false);
        } catch (e) {}
      }

      if (!el) {
        el = false;
      } else if (typeof el === 'string') {
        el = document.querySelector(el) || this.$root.$el.querySelector(el);
      } else if (el === true) {
        el = this.$parent.$el;
      }

      this.dropElement = el;

      if (this.dropElement) {
        document.addEventListener('dragenter', this.onDragenter, false);
        document.addEventListener('dragleave', this.onDragleave, false);
        document.addEventListener('drop', this.onDocumentDrop, false);
        this.dropElement.addEventListener('dragover', this.onDragover, false);
        this.dropElement.addEventListener('drop', this.onDrop, false);
      }
    },
    onDragenter: function onDragenter(e) {
      e.preventDefault();
      if (!this.dropActive) {
        this.dropActive = true;
      }
    },
    onDragleave: function onDragleave(e) {
      e.preventDefault();
      if (e.target.nodeName === 'HTML' || e.screenX === 0 && e.screenY === 0 && !e.fromElement && e.offsetX <= 0) {
        this.dropActive = false;
      }
    },
    onDragover: function onDragover(e) {
      e.preventDefault();
    },
    onDocumentDrop: function onDocumentDrop() {
      this.dropActive = false;
    },
    onDrop: function onDrop(e) {
      e.preventDefault();
      this.addDataTransfer(e.dataTransfer);
    }
  }
};

var FileUpload$1 = Object.freeze({
	default: FileUpload
});

var require$$0 = ( FileUpload$1 && FileUpload ) || FileUpload$1;

var src = require$$0;

return src;

})));
//# sourceMappingURL=vue-upload-component.js.map


/***/ }),

/***/ 296:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(385)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(309),
  /* template */
  __webpack_require__(359),
  /* scopeId */
  "data-v-0759e358",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/AddMessageForm.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] AddMessageForm.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0759e358", Component.options)
  } else {
    hotAPI.reload("data-v-0759e358", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 297:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(312),
  /* template */
  __webpack_require__(373),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/MessagesList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] MessagesList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-75d94658", Component.options)
  } else {
    hotAPI.reload("data-v-75d94658", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 298:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(313),
  /* template */
  __webpack_require__(370),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectBasicInfo.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectBasicInfo.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5771e62d", Component.options)
  } else {
    hotAPI.reload("data-v-5771e62d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 299:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(396)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(321),
  /* template */
  __webpack_require__(378),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectTabs.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectTabs.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b120a046", Component.options)
  } else {
    hotAPI.reload("data-v-b120a046", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 302:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FileUpload = __webpack_require__(295);

Vue.filter('formatSize', function (size) {
    if (size > 1024 * 1024 * 1024 * 1024) {
        return (size / 1024 / 1024 / 1024 / 1024).toFixed(2) + ' TB';
    } else if (size > 1024 * 1024 * 1024) {
        return (size / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    } else if (size > 1024 * 1024) {
        return (size / 1024 / 1024).toFixed(2) + ' MB';
    } else if (size > 1024) {
        return (size / 1024).toFixed(2) + ' KB';
    }
    return size.toString() + ' B';
});

exports.default = {
    props: ['reset'],
    data: function data() {
        return {
            multiple: true,
            files: [],
            upload: [],
            title: 'Add upload files',
            drop: true,
            dropActive: true,
            auto: true,
            active: true,
            uploadedFiles: [],
            name: 'file',
            headers: {
                "X-Csrf-Token": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },

            data: {
                "_csrf_token": document.querySelector('meta[name="csrf-token"]').getAttribute('content')
            },

            events: {
                add: function add(file, component) {
                    component.active = true;
                    file.headers['X-Filename'] = encodeURIComponent(file.name);
                    file.data.finename = file.name;
                },
                progress: function progress(file, component) {},
                after: function after(file, component) {
                    component.$emit('fileUploaded', { id: file.id, path: file.response.path, filename: file.name });
                },
                before: function before(file, component) {}
            }
        };
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        apiList: 'apiList',
        attachmentsRules: 'attachmentsRules',
        loggedInUser: 'loggedInUser',
        rolesList: 'rolesList',
        openedProject: 'openedProject'
    }), {
        uploadRules: function uploadRules() {
            var rules = {};

            rules.max = 1024 * 1024 * 5;
            rules.extensions = 'jpg';

            if (this.attachmentsRules) {
                rules.max = 1024 * this.attachmentsRules.max;
                rules.extensions = this.attachmentsRules.extensions.join();

                if (this.loggedInUser.role_id === this.rolesList.admin || this.loggedInUser.id === this.openedProject.manager.id) {
                    rules.max = 1024 * this.attachmentsRules.manager_max;
                }
            }

            return rules;
        }
    }),
    watch: {
        uploadedFiles: function uploadedFiles(val) {
            this.$emit('filesListChange', val);
        },
        reset: function reset(val) {
            if (val) {
                this.files = [];
                this.uploadedFiles = [];
            }
        }
    },
    mounted: function mounted() {
        this.upload = this.$refs.upload.$data;
    },

    methods: {
        remove: function remove(file) {
            var index = this.files.indexOf(file);
            if (index != -1) {
                this.files.splice(index, 1);
                this.uploadedFiles.splice(index, 1);
            }
        },
        fileUploaded: function fileUploaded(file) {
            this.uploadedFiles.push(file);
        },
        fileStatus: function fileStatus(file) {
            if (file.success) {
                return '<span class="text-success">uploaded</span>';
            } else if (file.error === "extension") {
                return '<span class="text-danger">Extension not allowed</span>';
            } else if (file.error === "size") {
                return '<span class="text-danger">Over size</span>';
            } else if (file.error === "denied") {
                return '<span class="text-danger">Denied</span>';
            } else if (file.error) {
                return '<span class="text-danger">Error</span>';
            }
            return 'uploading ...';
        }
    },
    components: {
        FileUpload: FileUpload
    }
};

/***/ }),

/***/ 303:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault(_stringify);

var _vue = __webpack_require__(15);

var _vue2 = _interopRequireDefault(_vue);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    props: {
        id: String,
        cssClasses: String,
        name: String,
        placeholder: String,

        model: String,
        anchor: {
            type: String,
            required: true
        },

        label: String,

        url: {
            type: Array,
            required: true
        },

        param: {
            type: String,
            default: 'q'
        },

        min: {
            type: Number,
            default: 0
        },

        limit: {
            type: String,
            default: ''
        }
    },

    data: function data() {
        return {
            showList: false,
            type: "",
            json: [],
            focusList: ""
        };
    },


    watch: {
        type: function type(val, old) {
            return this.$parent.$data[this.model] = val;
        }
    },
    methods: {
        clearInput: function clearInput() {
            this.showList = false;
            this.type = "";
            this.json = [];
            this.focusList = "";
        },
        cleanUp: function cleanUp(data) {
            return JSON.parse((0, _stringify2.default)(data));
        },
        input: function input(val) {
            this.showList = true;

            this.getData(val);
            return this.$parent.$data[this.model] = val;
        },
        showAll: function showAll() {
            this.json = [];
            console.log('get data from shawAll');
            this.getData("");

            this.showList = true;
        },
        hideAll: function hideAll(e) {},
        focus: function focus(e) {
            this.focusList = 0;
        },
        mousemove: function mousemove(i) {
            this.focusList = i;
        },
        keydown: function keydown(e) {
            var key = e.keyCode;

            if (!this.showList) return;

            switch (key) {
                case 40:
                    this.focusList++;
                    break;
                case 38:
                    this.focusList--;
                    break;
                case 13:
                    this.selectList(this.json[this.focusList]);
                    this.showList = false;
                    break;
                case 27:
                    this.showList = false;
                    break;
            }

            var listLength = this.json.length - 1;
            this.focusList = this.focusList > listLength ? 0 : this.focusList < 0 ? listLength : this.focusList;
        },
        activeClass: function activeClass(i) {
            return {
                'focus-list': i == this.focusList
            };
        },
        getData: function getData(val) {
            this.json = this.url.filter(function (member) {
                var regex = new RegExp(val, "i");
                return member.name.search(regex) !== -1;
            });
        },
        selectList: function selectList(data) {
            var clean = this.cleanUp(data);

            this.type = clean[this.anchor];

            this.$parent.$data[this.model] = data;

            this.$emit('selected', data);

            this.showList = false;
        }
    },

    created: function created() {
        this.type = this.$parent.$data[this.model];
    }
};

/***/ }),

/***/ 304:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    data: function data() {
        return {};
    }
};

/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _quill = __webpack_require__(48);

var _quill2 = _interopRequireDefault(_quill);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {

  props: {
    options: {
      type: Object,
      default: function _default() {
        return {};
      }
    },
    autofocus: Boolean,
    id: {
      type: String,
      default: ''
    }
  },

  data: function data() {
    return {
      focused: this.autofocus,
      editor: null
    };
  },
  mounted: function mounted() {
    this.editor = new _quill2.default(this.$el.getElementsByClassName('editor')[0], {
      modules: {
        toolbar: { container: this.$el.getElementsByClassName('toolbar')[0] }
      },
      placeholder: 'Note about your decision...',
      theme: 'snow'
    });
  },


  methods: {
    publish: function publish(event) {
      var userInput = $(event.target).closest('.panel-body').find('.ql-editor');
      this.$emit('publish', userInput.html());
      userInput.html('');
    },
    cancel: function cancel(event) {
      $(event.target).closest('.panel-body').find('.ql-editor').html('');
      this.$emit('cancel');
    }
  },

  watch: {
    focused: function focused(val) {
      this.editor[val ? 'focus' : 'blur']();
    }
  }

};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

var _vueDatepicker = __webpack_require__(288);

var _vueDatepicker2 = _interopRequireDefault(_vueDatepicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var FileUpload = __webpack_require__(345);
var Quill = __webpack_require__(48);
var VueModal = __webpack_require__(289);

exports.default = {
  name: 'AddProjectMessage',
  props: ['project', 'tab', 'messageForm', 'actionForm'],
  data: function data() {
    return {
      messageFilesReset: false,
      addMessageErrors: { content: null },
      messageUploadedFiles: [],
      uploadFileOption: false,
      actionFilesReset: false,
      addActionErrors: { content: null },
      actionUploadedFiles: [],
      actionUploadFileOption: false,
      newMessageEditor: null,
      actionMessageEditor: null,
      newAction: {
        time: moment().format('YYYY/MM/DD'),
        type: 'approval',
        content: null
      },
      today: moment().format('dddd, MM Do, YYYY'),
      datepickerOptions: {
        type: 'day',
        week: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
        month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        format: 'YYYY/MM/DD',
        inputStyle: {
          'box-shadow': 'none',
          'padding': '6px 12px',
          'padding-left': '20px',
          'padding-right': '50px',
          'font-size': '110%',
          'display': 'block',
          'width': '100%',
          'height': '40px',
          'line-height': '14px',
          'color': '#57626d',
          'background-color': '#EEF1F6',
          'background-image': 'none',
          'border': '1px solid #EEF1F6',
          'border-radius': '4px',
          '-webkit-transition': 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s',
          'transition': 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s'
        },
        color: {
          header: '#434E60',
          headerText: '#F6F7F9'
        },
        buttons: {
          ok: 'Ok',
          cancel: 'Cancel'
        },
        overlayOpacity: 0.5,
        dismissible: true
      },
      showForm: false,
      msgFormInProgress: false,
      actFormInProgress: false,
      showMsgError: false
    };
  },

  computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
    actionTypes: 'actionTypes',
    userPermissions: 'userPermissions',
    projectActiveService: 'projectActiveService'
  })),
  mounted: function mounted() {
    this.initQuillPlugin();
  },

  methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['addProjectMessage']), {
    updateMessageFilesList: function updateMessageFilesList(files) {
      if (this.messageFilesReset) this.messageFilesReset = false;
      this.messageUploadedFiles = files;
    },
    updateActionFilesList: function updateActionFilesList(files) {
      if (this.actionFilesReset) this.actionFilesReset = false;
      this.actionUploadedFiles = files;
    },
    initQuillPlugin: function initQuillPlugin() {
      this.newMessageEditor = new Quill("#" + this.tab + "-message-form .editor", {
        modules: {
          toolbar: "#" + this.tab + "-message-form .toolbar"
        },
        placeholder: 'Add your message here ...',
        theme: 'snow'
      });
      this.actionMessageEditor = new Quill("#" + this.tab + "-add-action .editor", {
        modules: {
          toolbar: "#" + this.tab + "-add-action .toolbar"
        },
        placeholder: 'Action details here ...',
        theme: 'snow'
      });
    },
    publishMessage: function publishMessage(event) {
      var _this = this;

      var data = {};
      var msgEl = $(event.target).closest("#" + this.tab + "-add-message").find('.ql-editor');

      if (msgEl.text()) {
        this.msgInProgress(true);
        data.content = msgEl.html();
        if (this.projectActiveService) {
          data['project_service_id'] = this.projectActiveService.pivot.id;
        }
        if (this.messageUploadedFiles.length > 0) {
          data['attachments'] = this.messageUploadedFiles;
        }
        data.tab = this.tab;
        this.addProjectMessage({ projectId: this.project.id, data: data }).then(function (response) {
          msgEl.html('');
          _this.messageFilesReset = true;
          _this.addMessageErrors = { content: null };
          _this.messageUploadedFiles = [];
          _this.showForm = false;
          _this.msgInProgress(false);
        }).catch(function (error) {
          if (error.status === 422) {
            _this.addMessageErrors = error.body;
            _this.msgInProgress(false);
          } else {
            _this.showMsgError = true;
            _this.addMessageErrors = { content: null };
            _this.messageFilesReset = true;
            _this.messageUploadedFiles = [];
            _this.showForm = false;
            _this.msgInProgress(false);
          }
        });
      } else {
        this.addMessageErrors.content = ["Can not submit an empty message!"];
        this.msgInProgress(false);
      }
    },
    msgInProgress: function msgInProgress(status) {
      if (status) {
        this.msgFormInProgress = true;
        this.newMessageEditor.editor.disable();
      } else {
        this.msgFormInProgress = false;
        this.newMessageEditor.editor.enable();
      }
    },
    actInProgress: function actInProgress(status) {
      if (status) {
        this.actFormInProgress = true;
        this.actionMessageEditor.editor.disable();
      } else {
        this.actFormInProgress = false;
        this.actionMessageEditor.editor.enable();
      }
    },
    publishAction: function publishAction(event) {
      var _this2 = this;

      var msgEl = $(event.target).closest("#" + this.tab + "-add-action").find('.ql-editor');

      if (msgEl.text() && this.newAction.time && this.newAction.type) {
        this.actFormInProgress = true;
        this.newAction.content = msgEl.html();
        this.newAction.date_due = this.newAction.time;
        this.newAction.tab = this.tab;
        if (this.projectActiveService) {
          this.newAction['project_service_id'] = this.projectActiveService.pivot.id;
        }
        if (this.actionUploadedFiles.length > 0) {
          this.newAction['attachments'] = this.actionUploadedFiles;
        }
        this.addProjectMessage({ projectId: this.project.id, data: this.newAction }).then(function (response) {
          msgEl.html('');
          _this2.actionFilesReset = true;
          _this2.addActionErrors = { content: null };
          _this2.actionUploadedFiles = [];
          _this2.showForm = false;
          _this2.actInProgress(false);
        }).catch(function (error) {
          if (error.status === 422) {
            _this2.addActionErrors = error.body;
            _this2.actInProgress(false);
          } else {
            _this2.showMsgError = true;
            _this2.addActionErrors = { content: null };
            _this2.actionFilesReset = true;
            _this2.actionUploadedFiles = [];
            _this2.showForm = false;
            _this2.actInProgress(false);
          }
        });
      } else {
        if (!msgEl.text()) this.addActionErrors.content = ["Can not submit an empty note!"];
        if (!this.newAction.time) this.addActionErrors.due_date = ["Must submit a Due Date!"];
        if (!this.newAction.type) this.addActionErrors.type = ["Must choose a type!"];
        this.actInProgress(false);
      }
    }
  }),
  components: {
    'date-picker': _vueDatepicker2.default,
    FileUpload: FileUpload,
    VueModal: VueModal
  }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 312:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getIterator2 = __webpack_require__(285);

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

var _lodash = __webpack_require__(29);

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MessageItem = __webpack_require__(353);

exports.default = {
    name: 'MessagesList',
    props: ['messages'],
    data: function data() {
        return {};
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        projectActiveService: 'projectActiveService'
    }), {
        messagesGroups: function messagesGroups() {
            var groups = _lodash2.default.groupBy(this.messages, function (msg) {
                var dateFilter = Vue.filter('date');
                return dateFilter(msg.updated_at.date, "YYYY-MM-DD", 'YYYY-MM-DD HH:mm:ss');
            });
            return _lodash2.default.toPairs(groups);
        }
    }),
    methods: {
        groupHasActiveMessages: function groupHasActiveMessages(messages) {
            var _this = this;

            if (!this.projectActiveService) {
                return true;
            }
            return messages.filter(function (message) {
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = (0, _getIterator3.default)(message.services), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var service = _step.value;

                        if (service.id === _this.projectActiveService.pivot.id) {
                            return true;
                        }
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                return false;
            }).length > 0;
        }
    },
    components: {
        MessageItem: MessageItem
    }
};

/***/ }),

/***/ 313:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'ProjectBasicInfo',
    props: ['project'],
    data: function data() {
        return {
            showPmEdit: false,
            pm_id: null,
            formInProgress: false
        };
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        managersList: 'managersList'
    })),
    mounted: function mounted() {
        this.pm_id = this.project.manager.id;
    },

    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['updateProject']), {
        changePm: function changePm() {
            var _this = this;

            if (this.project.manager.id !== this.pm_id) {
                this.formInProgress = true;
                this.updateProject({
                    projectId: this.project.id,
                    data: {
                        pm_id: this.pm_id
                    }
                }).then(function (response) {
                    return _this.closePmEdit();
                }).catch(function (error) {
                    console.log(error);
                    _this.closePmEdit();
                });
            } else {
                this.closePmEdit();
            }
        },
        closePmEdit: function closePmEdit() {
            this.showPmEdit = false;
            this.formInProgress = false;
        }
    })
};

/***/ }),

/***/ 314:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    name: 'ProjectBudget',
    props: ['project'],
    data: function data() {
        return {
            showBudgetEditForm: false,
            budget: this.project.budget ? this.project.budget : 0.00,
            newExpense: {
                description: null,
                cost: 0.00
            },
            budgetFormLoading: false
        };
    },

    computed: {
        balanceStatus: function balanceStatus() {
            if (this.project.budget_balance > this.project.budget / 2) return 'success';

            if (this.project.budget_balance > 0) return 'warning';

            return 'danger';
        },
        totalExpenses: function totalExpenses() {
            if (this.project.expenses) {
                return this.project.expenses.reduce(function (pre, current) {
                    return parseFloat(pre) + parseFloat(current.cost);
                }, 0);
            }
            return 0;
        },
        totalBackground: function totalBackground() {
            var style = {
                'background-color': '#ef4036'
            };
            switch (this.balanceStatus) {
                case 'success':
                    style['background-color'] = '#38B449';
                    break;
                case 'warning':
                    style['background-color'] = 'orange';
                    break;
            }

            return style;
        }
    },
    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['updateProject', 'addProjectExpense', 'deleteProjectExpense']), {
        updateBudget: function updateBudget() {
            var _this = this;

            if (this.budget) {
                this.budgetFormLoading = true;
                this.updateProject({
                    projectId: this.project.id,
                    data: { budget: this.budget }
                }).then(function (response) {
                    _this.closeBudgetEdit();
                    _this.budget = response.project.budget;
                });
            } else {
                this.closeBudgetEdit();
                console.log('submitting empty budget for update!!');
            }
        },
        deleteExpense: function deleteExpense(id) {
            this.deleteProjectExpense({
                projectId: this.project.id,
                expenseId: id
            });
        },
        closeBudgetEdit: function closeBudgetEdit() {
            this.showBudgetEditForm = false;
            this.budgetFormLoading = false;
        },
        addExpense: function addExpense() {
            var _this2 = this;

            if (!this.newExpense.description) {}
            if (!this.newExpense.cost) {}
            if (this.newExpense.description && this.newExpense.cost) {
                this.budgetFormLoading = true;
                var data = {
                    projectId: this.project.id,
                    data: {
                        description: this.newExpense.description,
                        cost: this.newExpense.cost
                    }
                };
                this.addProjectExpense(data).then(function (response) {
                    _this2.newExpense.description = null;
                    _this2.newExpense.cost = 0.00;
                    _this2.budgetFormLoading = false;
                });
            }
        }
    })
};

/***/ }),

/***/ 315:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var vueScrollTo = __webpack_require__(290);

exports.default = {
    name: 'ProjectClientPendings',
    props: ['project', 'pendings'],
    data: function data() {
        return {};
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        projectActiveService: 'projectActiveService',
        loggedInUser: 'loggedInUser'
    })),
    methods: {
        goTo: function goTo(el, message) {
            if (message.visible_client) {
                $('a[href="#client-tab-content"]').tab('show');
            } else if (message.visible_team) {
                $('a[href="#team-tab-content"]').tab('show');
            } else {
                $('a[href="#notes-tab-content"]').tab('show');
            }
            var options = {
                easing: vueScrollTo.easing['ease-in']
            };
            vueScrollTo.scrollTo('#' + el, 300, options);
            var element = document.getElementById(el);
            element.style.boxShadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)';
            var t = setTimeout(function () {
                element.style.boxShadow = 'none';
            }, 10 * 1000);
        }
    },
    components: {
        vueScrollTo: vueScrollTo
    }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 316:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var vueScrollTo = __webpack_require__(290);
var ProjectTabs = __webpack_require__(299);

exports.default = {
    name: 'ProjectDatesAndFiles',
    props: ['project'],
    data: function data() {
        var locationOrigin = document.location.origin;
        return {
            circleIcon: locationOrigin + "/images/icons/svg/circle-solid.svg",
            minusIcon: locationOrigin + "/images/icons/svg/minus-solid.svg",
            calendarIcon: locationOrigin + "/images/icons/svg/calendar-alt-regular.svg",
            paperclipIcon: locationOrigin + "/images/icons/svg/paperclip-solid.svg",
            filesPopOptions: {
                trigger: 'click',
                placement: 'auto',
                closeable: false,
                animation: 'pop',
                dismissible: true,
                height: 250,
                width: 290
            },
            JiraLinkPopOptions: {
                trigger: 'click',
                placement: 'auto',
                closeable: false,
                animation: 'pop',
                dismissible: true,
                height: 250,
                width: 290
            },
            jiraProjects: [],
            jiraProjectsLoading: false,
            jiraLoadingMsg: 'Loading from Jira ...',
            jiraAuthLoading: false,
            jiraAuthLoadingMsg: 'Authenticating with Jira ...'
        };
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        projectFiles: 'projectFiles',
        loggedInUser: 'loggedInUser',
        apiList: 'apiList'
    }), {
        userHasJira: function userHasJira() {
            return this.loggedInUser.has_jira && this.loggedInUser.jira_tokens.expires_at > Math.floor(Date.now() / 1000);
        }
    }),
    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['updateProject']), (0, _vuex.mapMutations)(['setLoggedInUser']), {
        goTo: function goTo(el, visibility) {
            if (visibility.visible_client) {
                $('a[href="#client-tab-content"]').tab('show');
            } else if (visibility.visible_team) {
                $('a[href="#team-tab-content"]').tab('show');
            } else {
                $('a[href="#notes-tab-content"]').tab('show');
            }

            var options = {
                easing: vueScrollTo.easing['ease-in']
            };
            vueScrollTo.scrollTo('#' + el, 300, options);
            var element = document.getElementById(el);
            element.style.boxShadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)';
            var t = setTimeout(function () {
                element.style.boxShadow = 'none';
            }, 10 * 1000);
        },
        getJiraProjects: function getJiraProjects() {
            var _this = this;

            if (this.userHasJira) {
                this.startJiraLoading('Loading your projects from Jira ...');
                this.$http.get(this.apiList['jira.projects']).then(function (response) {
                    _this.jiraProjects = response.data;
                    _this.closeJiraLoading();
                }).catch(function (error) {
                    console.log(error);
                });
            }
        },
        linkToJira: function linkToJira(jProject) {
            var _this2 = this;

            if (this.userHasJira) {
                this.startJiraLoading('Linking project to Jira ...');
                this.updateProject({
                    projectId: this.project.id,
                    data: {
                        jira_name: jProject.name,
                        jira_key: jProject.key
                    }
                }).then(function (response) {
                    _this2.closeJiraLoading();
                    $('#jira-link').webuiPopover('hide');
                }).catch(function (error) {
                    if (error.status === 401) {
                        var user = JSON.parse((0, _stringify2.default)(_this2.loggedInUser));
                        user.has_jira = false;
                        _this2.setLoggedInUser(user);
                    }

                    console.log(error);
                });
            }
        },
        closeJiraLoading: function closeJiraLoading() {
            this.jiraProjectsLoading = false;
            this.jiraLoadingMsg = 'Loading from Jira ...';
        },
        startJiraLoading: function startJiraLoading(msg) {
            this.jiraProjectsLoading = true;
            this.jiraLoadingMsg = msg;
        },
        jiraAuth: function jiraAuth() {
            this.jiraAuthLoading = true;

            var refreshUserStatus = setInterval(function () {
                var _this3 = this;

                this.$http.get(this.apiList['jira.tokens']).then(function (response) {
                    if (response.data.has_jira) {
                        var user = JSON.parse((0, _stringify2.default)(_this3.loggedInUser));
                        user.has_jira = true;
                        user.jira_tokens = response.data.tokens;
                        _this3.setLoggedInUser(user);
                        _this3.jiraAuthLoading = false;
                        _this3.getJiraProjects();
                        clearInterval(refreshUserStatus);
                    }
                    console.log(response.data);
                }).catch(function (error) {
                    console.log(error);
                });
            }.bind(this), 5000);
        }
    }),
    components: {
        vueScrollTo: vueScrollTo,
        ProjectTabs: ProjectTabs
    }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 318:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault(_stringify);

var _getIterator2 = __webpack_require__(285);

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

var _toastr = __webpack_require__(197);

var _toastr2 = _interopRequireDefault(_toastr);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RejectApproveActions = __webpack_require__(358);
var vueScrollTo = __webpack_require__(290);
var vueModal = __webpack_require__(289);

exports.default = {
    name: "ProjectMessageGridItem",
    props: ['message'],
    data: function data() {
        return {
            jiraLoading: false,
            jiraNewIssueSummary: null,
            jiraNewIssueDescription: null,
            jiraNewIssueType: null,
            showJiraNewIssueModal: false
        };
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        projectActiveService: 'projectActiveService',
        openedProject: 'openedProject',
        loggedInUser: 'loggedInUser',
        jiraKeyLink: 'jiraKeyLink',
        apiList: 'apiList'
    }), {
        visibilityStatus: function visibilityStatus() {
            if (this.message.visible_client && this.message.visible_team) {
                return 'Team/client';
            } else if (this.message.visible_client) {
                return 'Client';
            } else if (this.message.visible_team) {
                return 'Team';
            } else {
                return 'Private';
            }
        },
        messageCssClasses: function messageCssClasses() {
            var cssClasses = {
                'row': true,
                'message': true,
                'panel': true,
                'flat': true
            };
            if (this.message.owner.type === 'client') {
                cssClasses['from-client'] = true;
                cssClasses['panel-warning'] = true;
            } else {
                cssClasses['panel-default'] = true;
                cssClasses['panel-inverse'] = true;
                cssClasses['borderless'] = true;
            }
            return cssClasses;
        },
        belongsToActiveService: function belongsToActiveService() {
            if (!this.projectActiveService) return true;

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = (0, _getIterator3.default)(this.message.services), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var service = _step.value;

                    if (service.id === this.projectActiveService.pivot.id) return true;
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        },
        servicesIds: function servicesIds() {
            return this.message.services.map(function (service) {
                return service.service.id;
            });
        },
        userHasJira: function userHasJira() {
            return this.loggedInUser.has_jira && this.loggedInUser.jira_tokens.expires_at > Math.floor(Date.now() / 1000);
        }
    }),
    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['changeMessageVisibility', 'makeActionDone', 'updateMessageService']), (0, _vuex.mapMutations)(['updateMessage']), {
        stripHtml: function stripHtml(html) {
            var text = html;
            text = text.replace(/<br \/>/gi, "\n");
            text = text.replace(/<\/p>/gi, "\n");
            text = text.replace(/<\/div>/gi, "\n");
            text = text.replace(/<li>/gi, "\t- ");
            text = text.replace(/<\/li>/gi, "\n");
            text = text.replace(/<a.*href="(.*?)".*>(.*?)<\/a>/gi, " $2 (Link->$1) ");
            text = text.replace(/<\/?[^>]+(>|$)/g, "");
            return text.trim();
        },
        getJiraIssueDetails: function getJiraIssueDetails() {
            if (!this.jiraLoading) {
                this.jiraNewIssueSummary = 'issue summary';

                this.jiraNewIssueDescription = this.stripHtml(this.message.content);
                this.jiraNewIssueType = "incident";
                this.showJiraNewIssueModal = true;
            }
        },
        shareToJira: function shareToJira() {
            var _this = this;

            this.showJiraNewIssueModal = false;
            if (!this.jiraLoading && this.openedProject.jira_key && this.userHasJira) {
                this.jiraLoading = true;
                this.$http.post(this.apiList['jira.issue'], {
                    message_id: this.message.id,
                    key: this.openedProject.jira_key,
                    summary: this.jiraNewIssueSummary,
                    description: this.jiraNewIssueDescription,
                    type: this.jiraNewIssueType
                }).then(function (response) {
                    var message = JSON.parse((0, _stringify2.default)(_this.message));
                    message.jira_key = response.data.key;
                    _this.updateMessage(message);
                    _this.jiraLoading = false;
                    console.log(response.data);
                    _this.jiraNewIssueSummary = null;
                    _this.jiraNewIssueDescription = null;
                    _this.jiraNewIssueType = null;
                }).catch(function (error) {
                    _toastr2.default.error('error while trying to open a Jira issue, try again later!', 'Error');
                    _this.jiraLoading = false;
                    console.log(error);
                });
            }
        },
        makeThisActionDone: function makeThisActionDone(message) {
            this.makeActionDone(message).then(function (response) {
                var el = 'project-message-' + message.id;
                var options = {
                    easing: vueScrollTo.easing['ease-in']
                };
                vueScrollTo.scrollTo('#' + el, 300, options);
                var element = document.getElementById(el);
                element.style.boxShadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)';
                setTimeout(function () {
                    element.style.boxShadow = 'none';
                }, 3 * 1000);
            });
        },
        addService: function addService(service) {
            this.updateMessageService({
                id: this.message.id,
                service_id: service.pivot.id,
                service_operation: 'service_add'
            });
        },
        removeService: function removeService(service) {
            this.updateMessageService({
                id: this.message.id,
                service_id: service.pivot.project_service_id,
                service_operation: 'service_remove'
            });
        },
        htmlFilter: function htmlFilter(html) {
            return html;
        },
        human: function human(date) {
            if (!date) {
                return 'N/A';
            }
            return moment(date, 'YYYY-MM-DD H:m:s').fromNow();
        },
        attachmentIcon: function attachmentIcon(file) {
            var ext = file.name.split('.').pop();
            switch (ext) {
                case 'jpg':
                    return 'icon-sm icon-image-dark';
                    break;
                case 'jpeg':
                    return 'icon-sm icon-image-dark';
                    break;
                case 'png':
                    return 'icon-sm icon-image-dark';
                    break;
                case 'pdf':
                    return 'icon-sm icon-pdf-dark';
                    break;
                case 'zip':
                    return 'glyphicon glyphicon-compressed';
                    break;
                default:
                    return 'glyphicon glyphicon-file';
            }
        }
    }),
    components: {
        RejectApproveActions: RejectApproveActions,
        vueScrollTo: vueScrollTo,
        vueModal: vueModal
    }
};

/***/ }),

/***/ 319:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});


var AddMessageForm = __webpack_require__(296);
var ProjectMessages = __webpack_require__(297);

exports.default = {
    name: 'ProjectTabs',
    props: ['project'],
    data: function data() {
        return {};
    },
    mounted: function mounted() {},

    methods: {},
    components: {
        AddMessageForm: AddMessageForm,
        ProjectMessages: ProjectMessages
    }
};

/***/ }),

/***/ 320:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

var _vueDatepicker = __webpack_require__(288);

var _vueDatepicker2 = _interopRequireDefault(_vueDatepicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Modal = __webpack_require__(289);

exports.default = {
  name: 'ProjectServices',
  props: ['project', 'statuses'],
  data: function data() {
    return {
      newService: {
        status: 'in-progress',
        start: {},
        finish: {},
        service_id: '1'
      },
      newServiceErrors: {},
      serviceInEdit: {
        status: '',
        start: {},
        finish: {},
        service_id: '1',
        service_pivot_id: '1',
        name: ''
      },
      serviceInEditErrors: {},
      addPopOptions: {
        trigger: 'click',
        placement: 'auto',
        closeable: true,
        animation: 'pop',
        dismissible: false
      },
      editPopOptions: {
        trigger: 'click',
        placement: 'auto',
        closeable: true,
        animation: 'pop',
        dismissible: false
      },
      datepickerOptions: {
        type: 'day',
        week: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
        month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        format: 'YYYY/MM/DD',
        inputStyle: {
          'box-shadow': 'none',
          'padding': '6px 12px',
          'padding-left': '20px',
          'padding-right': '50px',
          'font-size': '110%',
          'display': 'block',
          'width': '100%',
          'height': '40px',
          'line-height': '14px',
          'color': '#57626d',
          'background-color': '#EEF1F6',
          'background-image': 'none',
          'border': '1px solid #EEF1F6',
          'border-radius': '4px',
          '-webkit-transition': 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s',
          'transition': 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s'
        },
        color: {
          header: '#434E60',
          headerText: '#F6F7F9'
        },
        buttons: {
          ok: 'Ok',
          cancel: 'Cancel'
        },
        overlayOpacity: 0.5,
        dismissible: true },
      showDeleteModal: false,
      toRemoveService: null
    };
  },

  computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
    projectActiveService: 'projectActiveService',
    servicesList: 'servicesList'
  })),
  methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['addProjectService', 'removeProjectService', 'updateProjectService', 'setProjectActiveService']), {
    updateServiceInEdit: function updateServiceInEdit(service) {
      var serviceInEdit = JSON.parse((0, _stringify2.default)(this.serviceInEdit));
      serviceInEdit.service_id = service.id;
      serviceInEdit.id = service.pivot.id;
      serviceInEdit.name = service.name;
      serviceInEdit.status = service.pivot.status.replace(/\s/g, '-').toLowerCase();
      serviceInEdit.start.time = moment(service.pivot.date_start, 'YYYY-MM-DD').format('YYYY/MM/DD');
      serviceInEdit.finish.time = moment(service.pivot.date_finish, 'YYYY-MM-DD').format('YYYY/MM/DD');

      this.serviceInEdit = serviceInEdit;
    },
    updateService: function updateService(boxEl) {
      var _this = this;

      this.serviceInEdit.date_start = this.serviceInEdit.start.time;
      this.serviceInEdit.date_finish = this.serviceInEdit.finish.time;
      this.updateProjectService({
        projectId: this.project.id,
        service: this.serviceInEdit
      }).then(function (response) {
        $(boxEl).webuiPopover('hide');
      }).catch(function (error) {
        if (error.status === 422) {
          _this.serviceInEditErrors = error.body;
        } else {
          $(boxEl).webuiPopover('hide');
        }
      });
    },
    removeService: function removeService(service, boxEl) {
      this.toRemoveService = service;
      this.showDeleteModal = true;
    },
    cancelRemoveService: function cancelRemoveService() {
      this.showDeleteModal = false;
      this.toRemoveService = null;
    },
    confirmedRemoveService: function confirmedRemoveService() {
      this.showDeleteModal = false;
      this.removeProjectService({
        projectId: this.project.id,
        projectServiceId: this.toRemoveService.pivot.id
      });
      this.toRemoveService = null;
    },
    addService: function addService(boxEl) {
      var _this2 = this;

      if (!this.newService.start.time || !this.newService.finish.time) {
        var errors = JSON.parse((0, _stringify2.default)(this.newServiceErrors));
        if (!this.newService.start.time) {
          errors.date_start = ['Service start date can\'t be empty.'];
        } else if (errors.date_start) {
          delete errors.date_start;
        }
        if (!this.newService.finish.time) {
          errors.date_finish = ['Service finish date can\'t be empty.'];
        } else if (errors.date_finish) {
          delete errors.date_finish;
        }
        this.newServiceErrors = errors;
        return;
      }

      this.newService.date_start = this.newService.start.time;
      this.newService.date_finish = this.newService.finish.time;
      this.addProjectService({
        projectId: this.project.id,
        newService: this.newService
      }).then(function (response) {
        $(boxEl).webuiPopover('hide');
      }).catch(function (error) {
        if (error.status === 422) {
          _this2.newServiceErrors = error.body;
        } else {
          $(boxEl).webuiPopover('hide');
        }
      });
    },
    serviceStatus: function serviceStatus(service) {
      return service.pivot.status.replace(/-/g, ' ');
    },
    cssClasses: function cssClasses(service) {
      var cssClasses = { 'service-box': true };
      switch (service.pivot.status.toLowerCase()) {
        case 'archived':
          cssClasses['archived'] = true;
          break;
        case 'due':
          cssClasses['due'] = true;
          break;
        case 'pending':
          cssClasses['pending'] = true;
          break;
        case 'delivered':
          cssClasses['delivered'] = true;
          break;
        case 'in-progress':
          cssClasses['in-progress'] = true;
          break;
        case 'in progress':
          cssClasses['in-progress'] = true;
          break;
      }
      if (this.projectActiveService && this.projectActiveService.pivot.id === service.pivot.id) {
        cssClasses['highlight'] = true;
      }
      return cssClasses;
    }
  }),
  components: {
    'date-picker': _vueDatepicker2.default,
    Modal: Modal
  }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});


var AddMessageForm = __webpack_require__(296);
var ProjectMessages = __webpack_require__(297);
var projectBasicInfo = __webpack_require__(349);
var ProjectBudget = __webpack_require__(298);

exports.default = {
    name: 'ProjectTabs',
    props: ['project'],
    data: function data() {
        return {};
    },

    computed: {
        teamMessages: function teamMessages() {
            return this.project.messages.filter(function (message) {
                return message.visible_team;
            });
        },
        clientMessages: function clientMessages() {
            return this.project.messages.filter(function (message) {
                return message.visible_client;
            });
        },
        notesMessages: function notesMessages() {
            return this.project.messages.filter(function (message) {
                return !message.visible_client && !message.visible_team;
            });
        }
    },
    mounted: function mounted() {},

    methods: {},
    components: {
        AddMessageForm: AddMessageForm,
        ProjectMessages: ProjectMessages,
        ProjectBudget: ProjectBudget,
        projectBasicInfo: projectBasicInfo
    }
};

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getIterator2 = __webpack_require__(285);

var _getIterator3 = _interopRequireDefault(_getIterator2);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Modal = __webpack_require__(289);

var Autocomplete = __webpack_require__(346);

exports.default = {
    name: 'ProjectTeamGrid',
    props: ['project'],
    data: function data() {
        return {
            newMemberName: null,
            newMemberData: null,
            toRemoveMemberData: null,
            showDeleteModal: false,
            addPopOptions: {
                trigger: 'click',
                placement: 'auto',
                closeable: true,
                animation: 'pop',
                dismissible: false
            }
        };
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        projectActiveService: 'projectActiveService',
        membersList: 'membersList'
    })),
    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)({
        addProjectMember: 'addProjectMember',
        removeProjectMember: 'removeProjectMember'
    }), {
        serviceName: function serviceName(member) {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = (0, _getIterator3.default)(this.project.services), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var service = _step.value;

                    if (service.pivot.id === member.pivot.project_service_id) return service.name;
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        },
        newMemberSelected: function newMemberSelected(data) {
            this.newMemberData = data;
        },
        addNewMember: function addNewMember(boxEl) {
            if (this.newMemberData) {
                if (this.projectActiveService) {
                    this.newMemberData['project_service_id'] = this.projectActiveService.pivot.id;
                }
                this.addProjectMember({ projectId: this.project.id, data: this.newMemberData });
            }
            $(boxEl).webuiPopover('hide');
        },
        removeMember: function removeMember(member) {
            this.toRemoveMemberData = member;
            this.showDeleteModal = true;
        },
        cancelRemoveMember: function cancelRemoveMember() {
            this.showDeleteModal = false;
            this.toRemoveMemberData = null;
        },
        confirmedRemoveMember: function confirmedRemoveMember() {
            this.showDeleteModal = false;
            this.removeProjectMember({
                projectId: this.project.id,
                projectMemberId: this.toRemoveMemberData.id,
                projectServiceId: this.toRemoveMemberData.pivot ? this.toRemoveMemberData.pivot['project_service_id'] : null
            });
            this.toRemoveMemberData = null;
        }
    }),
    components: {
        Autocomplete: Autocomplete,
        Modal: Modal
    }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 324:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

var _quill = __webpack_require__(347);

var _quill2 = _interopRequireDefault(_quill);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var vueScrollTo = __webpack_require__(290);

exports.default = {
    name: 'ApproveRejectActions',
    props: ['message'],
    data: function data() {
        return {
            rejectIsClicked: false,
            approveIsClicked: false
        };
    },

    components: {
        Quill: _quill2.default,
        vueScrollTo: vueScrollTo
    },
    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['approveProjectMessage']), {
        triggerReject: function triggerReject() {
            this.rejectIsClicked = !this.rejectIsClicked;
            if (this.rejectIsClicked) {
                this.approveIsClicked = false;
            }
        },
        triggerApprove: function triggerApprove() {
            this.approveIsClicked = !this.approveIsClicked;
            if (this.approveIsClicked) {
                this.rejectIsClicked = false;
            }
        },
        cancel: function cancel() {
            this.rejectIsClicked = false;
            this.approveIsClicked = false;
        },
        publishMessage: function publishMessage(content) {
            var _this = this;

            console.log(content);
            var message = JSON.parse((0, _stringify2.default)(this.message));
            message.approval_state = this.rejectIsClicked ? 0 : 1;
            message.approval_note = content;
            this.approveProjectMessage(message).then(function (response) {
                _this.rejectIsClicked = false;
                _this.approveIsClicked = false;
                var el = 'project-message-' + message.id;
                var options = {
                    easing: vueScrollTo.easing['ease-in']
                };
                vueScrollTo.scrollTo('#' + el, 300, options);
                var element = document.getElementById(el);
                element.style.boxShadow = '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)';
                setTimeout(function () {
                    element.style.boxShadow = 'none';
                }, 3 * 1000);
            });
        }
    })
};

/***/ }),

/***/ 325:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProjectDatesAndFiles = __webpack_require__(351);
var ProjectBasicInfo = __webpack_require__(298);
var ProjectServices = __webpack_require__(355);
var ProjectTabs = __webpack_require__(299);
var ProjectMessages = __webpack_require__(354);
var ProjectClientPendings = __webpack_require__(350);
var ProjectTeamGrid = __webpack_require__(356);

exports.default = {
    name: 'ProjectShow',
    data: function data() {
        return {
            id: null
        };
    },

    watch: {
        '$route': 'initProject'
    },
    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        openedProject: 'openedProject',
        allProjects: 'allProjects',
        statusTypes: 'statusTypes',
        activeServiceDues: 'activeServiceDues',
        loggedInUser: 'loggedInUser'
    })),
    created: function created() {
        this.initProject();
    },
    mounted: function mounted() {
        initJqueryPlugins();
    },

    methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['setOpenedProject', 'resetProjects', 'resetOpenedProject']), {
        initProject: function initProject() {
            this.resetOpenedProject();
            if (this.$route.params.id) {
                this.id = parseInt(this.$route.params.id);
                this.setOpenedProject(this.id);
            }
        }
    }),
    components: {
        ProjectDatesAndFiles: ProjectDatesAndFiles,
        ProjectBasicInfo: ProjectBasicInfo,
        ProjectServices: ProjectServices,
        ProjectTabs: ProjectTabs,
        ProjectClientPendings: ProjectClientPendings,
        ProjectTeamGrid: ProjectTeamGrid,
        ProjectMessages: ProjectMessages
    }
};

/***/ }),

/***/ 326:
/***/ (function(module, exports) {

/**
 * https://github.com/gre/bezier-easing
 * BezierEasing - use bezier curve for transition easing function
 * by Gaëtan Renaudeau 2014 - 2015 – MIT License
 */

// These values are established by empiricism with tests (tradeoff: performance VS precision)
var NEWTON_ITERATIONS = 4;
var NEWTON_MIN_SLOPE = 0.001;
var SUBDIVISION_PRECISION = 0.0000001;
var SUBDIVISION_MAX_ITERATIONS = 10;

var kSplineTableSize = 11;
var kSampleStepSize = 1.0 / (kSplineTableSize - 1.0);

var float32ArraySupported = typeof Float32Array === 'function';

function A (aA1, aA2) { return 1.0 - 3.0 * aA2 + 3.0 * aA1; }
function B (aA1, aA2) { return 3.0 * aA2 - 6.0 * aA1; }
function C (aA1)      { return 3.0 * aA1; }

// Returns x(t) given t, x1, and x2, or y(t) given t, y1, and y2.
function calcBezier (aT, aA1, aA2) { return ((A(aA1, aA2) * aT + B(aA1, aA2)) * aT + C(aA1)) * aT; }

// Returns dx/dt given t, x1, and x2, or dy/dt given t, y1, and y2.
function getSlope (aT, aA1, aA2) { return 3.0 * A(aA1, aA2) * aT * aT + 2.0 * B(aA1, aA2) * aT + C(aA1); }

function binarySubdivide (aX, aA, aB, mX1, mX2) {
  var currentX, currentT, i = 0;
  do {
    currentT = aA + (aB - aA) / 2.0;
    currentX = calcBezier(currentT, mX1, mX2) - aX;
    if (currentX > 0.0) {
      aB = currentT;
    } else {
      aA = currentT;
    }
  } while (Math.abs(currentX) > SUBDIVISION_PRECISION && ++i < SUBDIVISION_MAX_ITERATIONS);
  return currentT;
}

function newtonRaphsonIterate (aX, aGuessT, mX1, mX2) {
 for (var i = 0; i < NEWTON_ITERATIONS; ++i) {
   var currentSlope = getSlope(aGuessT, mX1, mX2);
   if (currentSlope === 0.0) {
     return aGuessT;
   }
   var currentX = calcBezier(aGuessT, mX1, mX2) - aX;
   aGuessT -= currentX / currentSlope;
 }
 return aGuessT;
}

module.exports = function bezier (mX1, mY1, mX2, mY2) {
  if (!(0 <= mX1 && mX1 <= 1 && 0 <= mX2 && mX2 <= 1)) {
    throw new Error('bezier x values must be in [0, 1] range');
  }

  // Precompute samples table
  var sampleValues = float32ArraySupported ? new Float32Array(kSplineTableSize) : new Array(kSplineTableSize);
  if (mX1 !== mY1 || mX2 !== mY2) {
    for (var i = 0; i < kSplineTableSize; ++i) {
      sampleValues[i] = calcBezier(i * kSampleStepSize, mX1, mX2);
    }
  }

  function getTForX (aX) {
    var intervalStart = 0.0;
    var currentSample = 1;
    var lastSample = kSplineTableSize - 1;

    for (; currentSample !== lastSample && sampleValues[currentSample] <= aX; ++currentSample) {
      intervalStart += kSampleStepSize;
    }
    --currentSample;

    // Interpolate to provide an initial guess for t
    var dist = (aX - sampleValues[currentSample]) / (sampleValues[currentSample + 1] - sampleValues[currentSample]);
    var guessForT = intervalStart + dist * kSampleStepSize;

    var initialSlope = getSlope(guessForT, mX1, mX2);
    if (initialSlope >= NEWTON_MIN_SLOPE) {
      return newtonRaphsonIterate(aX, guessForT, mX1, mX2);
    } else if (initialSlope === 0.0) {
      return guessForT;
    } else {
      return binarySubdivide(aX, intervalStart, intervalStart + kSampleStepSize, mX1, mX2);
    }
  }

  return function BezierEasing (x) {
    if (mX1 === mY1 && mX2 === mY2) {
      return x; // linear
    }
    // Because JavaScript number are imprecise, we should guarantee the extremes are right.
    if (x === 0) {
      return 0;
    }
    if (x === 1) {
      return 1;
    }
    return calcBezier(getTForX(x), mY1, mY2);
  };
};


/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.icon-attachment-dark[data-v-0759e358]:hover {\n  cursor: pointer;\n}\n.message-form[data-v-0759e358] {\n  margin-top: 10px;\n}\n", ""]);

// exports


/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.btn-xs, .btn-group-xs > .btn {\n    padding: 1px 5px !important;\n}\n.file-upload {\n    text-align: center;\n    margin: 10px;\n    padding: 10px;\n}\n.file-uploads {\n    border: 2px dashed #efefef;\n    width: 100%;\n    height: 100px;\n    padding-top: 35px;\n    cursor: pointer;\n}\n.table {\n    margin-top: 20px;\n}\n.table tr:first-child td {\n    border: 0;\n}\n", ""]);

// exports


/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.project-view-top-bar .project-status-bar .separator img {\n    display: inline-block;\n    width: 10px;\n    margin-top: 15px;\n}\n.icon-container {\n    margin: 0 10px;\n}\n", ""]);

// exports


/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.modal-mask {\n    position: fixed;\n    z-index: 9998;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    background-color: rgba(0, 0, 0, .5);\n    display: table;\n    transition: opacity .3s ease;\n}\n.modal-wrapper {\n    display: table-cell;\n    vertical-align: middle;\n}\n.modal-container {\n    width: 50%;\n    margin: 0px auto;\n    padding: 20px 30px;\n    background-color: #fff;\n    border-radius: 2px;\n    box-shadow: 0 2px 8px rgba(0, 0, 0, .33);\n    transition: all .3s ease;\n    font-family: Helvetica, Arial, sans-serif;\n}\n.modal-header h3 {\n    margin-top: 0;\n    color: #42b983;\n}\n.modal-body {\n    margin: 20px 0;\n}\n.modal-default-button {\n    float: right;\n}\n\n/*\n * The following styles are auto-applied to elements with\n * transition=\"modal\" when their visibility is toggled\n * by Vue.js.\n *\n * You can easily play with the modal transition by editing\n * these styles.\n */\n.modal-enter {\n    opacity: 0;\n}\n.modal-leave-active {\n    opacity: 0;\n}\n.modal-enter .modal-container,\n.modal-leave-active .modal-container {\n    transform: scale(1.1);\n}\n@media screen and (max-width: 375px) {\n.modal-container {\n        width: 90%;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.pending-items {\n    height: 115px;\n    overflow-y: auto;\n}\n", ""]);

// exports


/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.archived-warning[data-v-7d403847] {\n    background-color: #5D6773;\n    color: #ffffff;\n    font-weight: bold;\n    width: 100%;\n    height: 30px;\n    left: 0;\n    text-align: center;\n    top: 144px;\n    padding-top: 5px;\n}\n@media only screen and (max-device-width: 480px) {\n.archived-warning[data-v-7d403847] {\n        top: 60px;\n        height: 20px;\n        padding-top: 2px;\n}\n}\n\n", ""]);

// exports


/***/ }),

/***/ 338:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.projects-top-tabs-container .project-thumb{\n    float:left;\n}\n.projects-top-tabs-container .project-color{\n    width : 45px;\n    height : 45px;\n}\n.projects-top-tabs-container .project-thumb img, .projects-top-tabs-container .project-thumb span {\n    display: inline-block;\n    float: left;\n}\n.projects-top-tabs-container .project-thumb .title{\n    margin-top: 4px;\n    margin-bottom: 4px;\n}\n.projects-top-tabs-container .project-concluded-info{\n    margin-left: 8px;\n}\n", ""]);

// exports


/***/ }),

/***/ 341:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.transition, .autocomplete, .showAll-transition, .autocomplete ul, .autocomplete ul li a {\n  transition: all 0.1s ease-out;\n  -moz-transition: all 0.1s ease-out;\n  -webkit-transition: all 0.1s ease-out;\n  -o-transition: all 0.1s ease-out;\n}\n.autocomplete ul {\n  font-family: sans-serif;\n  position: absolute;\n  list-style: none;\n  background: #f8f8f8;\n  padding: 10px 0;\n  margin: 0;\n  display: inline-block;\n  min-width: 75%;\n  margin-top: 10px;\n  z-index: 100;\n}\n.autocomplete ul:before {\n  content: \"\";\n  display: block;\n  position: absolute;\n  height: 0;\n  width: 0;\n  border: 10px solid transparent;\n  border-bottom: 10px solid #f8f8f8;\n  left: 46%;\n  top: -20px\n}\n.autocomplete ul li a {\n  text-decoration: none;\n  display: block;\n  background: #f8f8f8;\n  color: #2b2b2b;\n  padding: 5px;\n  padding-left: 10px;\n}\n.autocomplete ul li a:hover {\n  color: white;\n  background: #2F9AF7;\n}\n.autocomplete ul li a span {\n  display: block;\n  margin-top: 3px;\n  color: grey;\n  font-size: 13px;\n}\n.autocomplete ul li a:hover span, .autocomplete ul li.focus-list a span {\n  color: white;\n}\n.showAll-transition {\n  opacity: 1;\n  height: 50px;\n  overflow: hidden;\n}\n.showAll-enter {\n  opacity: 0.3;\n  height: 0;\n}\n.showAll-leave {\n  display: none;\n}\n\n", ""]);

// exports


/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(386)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(302),
  /* template */
  __webpack_require__(360),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/common/FileUpload.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] FileUpload.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-081497d4", Component.options)
  } else {
    hotAPI.reload("data-v-081497d4", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 346:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(399)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(303),
  /* template */
  __webpack_require__(381),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/common/VueAutocomplete.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] VueAutocomplete.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-df83a136", Component.options)
  } else {
    hotAPI.reload("data-v-df83a136", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(305),
  /* template */
  __webpack_require__(366),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/common/quill.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] quill.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2def613c", Component.options)
  } else {
    hotAPI.reload("data-v-2def613c", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(314),
  /* template */
  __webpack_require__(379),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectBudget.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectBudget.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b5f04b38", Component.options)
  } else {
    hotAPI.reload("data-v-b5f04b38", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(391)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(315),
  /* template */
  __webpack_require__(371),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectClientPendings.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectClientPendings.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-5f69b9c6", Component.options)
  } else {
    hotAPI.reload("data-v-5f69b9c6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 351:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(387)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(316),
  /* template */
  __webpack_require__(361),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectDatesAndFiles.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectDatesAndFiles.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1ed113b6", Component.options)
  } else {
    hotAPI.reload("data-v-1ed113b6", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(318),
  /* template */
  __webpack_require__(362),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectMessageGridItem.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectMessageGridItem.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1fe00c11", Component.options)
  } else {
    hotAPI.reload("data-v-1fe00c11", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 354:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(319),
  /* template */
  __webpack_require__(369),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectMessages.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectMessages.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4aa1a9aa", Component.options)
  } else {
    hotAPI.reload("data-v-4aa1a9aa", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 355:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(320),
  /* template */
  __webpack_require__(368),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectServices.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectServices.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ccd613d", Component.options)
  } else {
    hotAPI.reload("data-v-3ccd613d", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 356:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(322),
  /* template */
  __webpack_require__(363),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectTeamGrid.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectTeamGrid.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-28cc21a2", Component.options)
  } else {
    hotAPI.reload("data-v-28cc21a2", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(324),
  /* template */
  __webpack_require__(372),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/RejectApproveActions.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] RejectApproveActions.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-68c5aab9", Component.options)
  } else {
    hotAPI.reload("data-v-68c5aab9", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 359:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "text-center"
  }, [_c('button', {
    staticClass: "btn btn-info",
    on: {
      "click": function($event) {
        _vm.showForm = !_vm.showForm
      }
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-comment"
  }), _vm._v("\n      Add new message\n    ")])]), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showForm),
      expression: "showForm"
    }],
    staticClass: "col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 wysiwyg message-form",
    attrs: {
      "id": _vm.tab + '-message-form'
    }
  }, [_c('div', {
    staticClass: "panel panel-default panel-message-form flat"
  }, [_c('div', {
    staticClass: "panel-heading message-panel-heading"
  }, [_c('ul', {
    staticClass: "nav nav-pills"
  }, [(_vm.project.permissions['message.create'] && _vm.messageForm) ? _c('li', {
    class: {
      'active': _vm.project.permissions['message.create']
    }
  }, [_c('a', {
    attrs: {
      "href": '#' + _vm.tab + '-add-message',
      "data-toggle": "tab"
    }
  }, [_vm._v("\n              Add message\n            ")]), _vm._v(" "), _c('div', {
    staticClass: "triangle"
  })]) : _vm._e(), _vm._v(" "), (_vm.project.permissions['action.create'] && _vm.actionForm) ? _c('li', {
    class: {
      'active': !_vm.project.permissions['message.create'] && _vm.project.permissions['action.create']
    }
  }, [_c('a', {
    attrs: {
      "href": '#' + _vm.tab + '-add-action',
      "data-toggle": "tab"
    }
  }, [_vm._v("\n              Add action\n            ")]), _vm._v(" "), _c('div', {
    staticClass: "triangle"
  })]) : _vm._e()])]), _vm._v(" "), _c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "tab-content"
  }, [_c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.messageForm),
      expression: "messageForm"
    }],
    class: {
      'add-message': true, 'tab-pane': true, 'active': _vm.project.permissions['message.create']
    },
    attrs: {
      "id": _vm.tab + '-add-message'
    }
  }, [_c('div', {
    staticClass: "editor"
  }), _vm._v(" "), (_vm.addMessageErrors.content) ? _c('p', {
    staticClass: "text-danger text-center"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-warning-sign"
  }), _vm._v("\n              " + _vm._s(_vm.addMessageErrors.content[0]) + "\n            ")]) : _vm._e(), _vm._v(" "), (_vm.uploadFileOption) ? _c('file-upload', {
    attrs: {
      "reset": _vm.messageFilesReset
    },
    on: {
      "filesListChange": _vm.updateMessageFilesList
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "panel-message-footer"
  }, [_c('div', {
    staticClass: "row"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-sm-2 attachment"
  }, [(_vm.userPermissions['file.upload']) ? _c('span', {
    staticClass: "icon-sm icon-attachment-dark",
    on: {
      "click": function($event) {
        _vm.uploadFileOption = !_vm.uploadFileOption
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-5 buttons"
  }, [_c('button', {
    staticClass: "btn btn-sm btn-info",
    class: {
      'disabled': _vm.msgFormInProgress
    },
    on: {
      "click": function($event) {
        _vm.publishMessage($event)
      }
    }
  }, [(_vm.msgFormInProgress) ? _c('i', {
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }) : _vm._e(), _vm._v("\n                    Publish\n                  ")])])])])], 1), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.actionForm),
      expression: "actionForm"
    }],
    class: {
      'add-action': true, 'tab-pane': true, 'active': !_vm.project.permissions['message.create'] && _vm.project.permissions['action.create']
    },
    attrs: {
      "id": _vm.tab + '-add-action'
    }
  }, [_c('div', {
    staticClass: "content-padding row form-horizontal"
  }, [_c('div', {
    staticClass: "date"
  }, [_vm._v("\n                " + _vm._s(_vm.today) + "\n              ")]), _vm._v(" "), _c('br'), _vm._v(" "), _c('div', {
    staticClass: "col-md-6"
  }, [_c('label', [_vm._v("Note")]), _vm._v(" "), _c('div', {
    staticClass: "editor"
  }), _vm._v(" "), (_vm.addActionErrors.content) ? _c('p', {
    staticClass: "text-danger text-center"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-warning-sign"
  }), _vm._v("\n                  " + _vm._s(_vm.addActionErrors.content[0]) + "\n                ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-6"
  }, [_c('div', {
    staticClass: "form-group has-feedback"
  }, [_c('label', [_vm._v("Due date")]), _vm._v(" "), _c('date-picker', {
    attrs: {
      "date": _vm.newAction,
      "option": _vm.datepickerOptions,
      "required": "required"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "icon icon-calendar-dark form-control-feedback"
  }), _vm._v(" "), (_vm.addActionErrors.due_date) ? _c('p', {
    staticClass: "text-danger text-center"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-warning-sign"
  }), _vm._v("\n                    " + _vm._s(_vm.addActionErrors.due_date[0]) + "\n                  ")]) : _vm._e()], 1), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_vm._l((_vm.actionTypes), function(action) {
    return _c('div', [_c('input', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.newAction.type),
        expression: "newAction.type"
      }],
      attrs: {
        "type": "radio",
        "id": _vm.tab + '-' + action + '-radio',
        "name": _vm.tab + '-action-type',
        "disabled": _vm.actFormInProgress
      },
      domProps: {
        "value": action,
        "checked": _vm._q(_vm.newAction.type, action)
      },
      on: {
        "change": function($event) {
          _vm.$set(_vm.newAction, "type", action)
        }
      }
    }), _vm._v(" "), _c('label', {
      attrs: {
        "for": _vm.tab + '-' + action + '-radio'
      }
    }, [_vm._v(_vm._s(action))])])
  }), _vm._v(" "), (_vm.addActionErrors.type) ? _c('p', {
    staticClass: "text-danger text-center"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-warning-sign"
  }), _vm._v("\n                    " + _vm._s(_vm.addActionErrors.type[0]) + "\n                  ")]) : _vm._e()], 2)])]), _vm._v(" "), (_vm.actionUploadFileOption) ? _c('file-upload', {
    attrs: {
      "reset": _vm.actionFilesReset
    },
    on: {
      "filesListChange": _vm.updateActionFilesList
    }
  }) : _vm._e(), _vm._v(" "), _c('p', {
    staticClass: "text-right",
    staticStyle: {
      "padding-right": "10px"
    }
  }, [_vm._v("All fields are required")]), _vm._v(" "), _c('div', {
    staticClass: "panel-message-footer"
  }, [_c('div', {
    staticClass: "row"
  }, [_vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "col-sm-2 attachment"
  }, [(_vm.userPermissions['file.upload']) ? _c('span', {
    staticClass: "icon-sm icon-attachment-dark",
    on: {
      "click": function($event) {
        _vm.actionUploadFileOption = !_vm.actionUploadFileOption
      }
    }
  }) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-5 buttons"
  }, [_c('button', {
    staticClass: "btn btn-sm btn-primary",
    class: {
      'disabled': _vm.actFormInProgress
    },
    on: {
      "click": function($event) {
        _vm.publishAction($event)
      }
    }
  }, [(_vm.actFormInProgress) ? _c('i', {
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }) : _vm._e(), _vm._v("\n                    Publish\n                  ")])])])])], 1)])])])]), _vm._v(" "), (_vm.showMsgError) ? _c('vue-modal', [_c('h3', {
    attrs: {
      "slot": "header"
    },
    slot: "header"
  }, [_vm._v("\n      Failed saving new message\n    ")]), _vm._v(" "), _c('div', {
    attrs: {
      "slot": "body"
    },
    slot: "body"
  }, [_vm._v("\n      Server Error! Message is not saved.\n    ")]), _vm._v(" "), _c('div', {
    attrs: {
      "slot": "footer"
    },
    slot: "footer"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        _vm.showMsgError = false
      }
    }
  }, [_vm._v("\n        Close\n      ")])])]) : _vm._e()], 1)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "toolbar col-sm-5"
  }, [_c('span', {
    staticClass: "ql-format-group"
  }, [_c('span', {
    staticClass: "ql-format-button ql-bold",
    attrs: {
      "title": "Bold"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-italic",
    attrs: {
      "title": "Italic"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-underline",
    attrs: {
      "title": "Underline"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-list",
    attrs: {
      "title": "List"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-bullet",
    attrs: {
      "title": "Bullet"
    }
  }), _vm._v(" "), _c('select', {
    staticClass: "ql-align",
    attrs: {
      "title": "Text Alignment"
    }
  }, [_c('option', {
    attrs: {
      "label": "Left",
      "selected": "",
      "value": "left"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Center",
      "value": "center"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Right",
      "value": "right"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Justify",
      "value": "justify"
    }
  })])])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "toolbar col-sm-5"
  }, [_c('span', {
    staticClass: "ql-format-group"
  }, [_c('span', {
    staticClass: "ql-format-button ql-bold",
    attrs: {
      "title": "Bold"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-italic",
    attrs: {
      "title": "Italic"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-underline",
    attrs: {
      "title": "Underline"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-list",
    attrs: {
      "title": "List"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-bullet",
    attrs: {
      "title": "Bullet"
    }
  }), _vm._v(" "), _c('select', {
    staticClass: "ql-align",
    attrs: {
      "title": "Text Alignment"
    }
  }, [_c('option', {
    attrs: {
      "label": "Left",
      "selected": "",
      "value": "left"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Center",
      "value": "center"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Right",
      "value": "right"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Justify",
      "value": "justify"
    }
  })])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-0759e358", module.exports)
  }
}

/***/ }),

/***/ 360:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "file-upload"
  }, [_c('file-upload', {
    ref: "upload",
    attrs: {
      "title": "Browse or drage and drop files to upload",
      "events": _vm.events,
      "name": _vm.name,
      "post-action": _vm.apiList['file.upload'],
      "extensions": _vm.uploadRules.extensions,
      "multiple": _vm.multiple,
      "size": _vm.uploadRules.max,
      "headers": _vm.headers,
      "data": _vm.data,
      "drop": _vm.drop,
      "drop-active": _vm.dropActive,
      "files": _vm.files
    },
    on: {
      "fileUploaded": _vm.fileUploaded
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "restrictions note"
  }, [_c('b', [_vm._v("Restrictions:")]), _vm._v(" max size: " + _vm._s(_vm._f("formatSize")(_vm.uploadRules.max)) + ",\n            extensions: " + _vm._s(_vm.uploadRules.extensions) + "\n        ")]), _vm._v(" "), _c('br'), _vm._v(" "), (_vm.files.length > 0) ? _c('table', {
    staticClass: "table"
  }, [_c('tbody', _vm._l((_vm.files), function(file, index) {
    return _c('tr', [_c('td', {
      staticClass: "text-left"
    }, [_vm._v(_vm._s(file.name))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(_vm._f("formatSize")(file.size)))]), _vm._v(" "), _c('td', [_vm._v(_vm._s(parseInt(file.progress) + '%'))]), _vm._v(" "), _c('td', {
      domProps: {
        "innerHTML": _vm._s(_vm.fileStatus(file))
      }
    }), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-xs btn-default",
      attrs: {
        "disabled": file.progress === '100.00' || file.error,
        "title": "abort upload"
      },
      on: {
        "click": function($event) {
          file.active = false
        }
      }
    }, [_c('i', {
      staticClass: "glyphicon glyphicon-remove"
    })])]), _vm._v(" "), _c('td', [_c('button', {
      staticClass: "btn btn-xs btn-danger",
      attrs: {
        "title": "remove the file",
        "disabled": file.progress !== '100.00'
      },
      on: {
        "click": function($event) {
          _vm.remove(file)
        }
      }
    }, [_c('i', {
      staticClass: "glyphicon glyphicon-trash"
    })])])])
  }))]) : _vm._e()], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-081497d4", module.exports)
  }
}

/***/ }),

/***/ 361:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "row content-box project-view-top-bar"
  }, [(_vm.project.permissions['project.update']) ? _c('project-tabs', {
    attrs: {
      "project": _vm.project
    }
  }) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "project-status-bar"
  }, [_c('div', {
    staticClass: "icon-container"
  }, [_c('img', {
    attrs: {
      "src": _vm.calendarIcon,
      "width": "30"
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "status"
  }, [_c('div', {
    staticClass: "date"
  }, [_vm._v(_vm._s(_vm._f("date")(_vm.project.date_start)))]), _vm._v(" "), _c('div', {
    staticClass: "title"
  }, [_vm._v("Kick-Off")])]), _vm._v(" "), _c('div', {
    staticClass: "separator"
  }, [_c('img', {
    attrs: {
      "src": _vm.circleIcon
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": _vm.minusIcon
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": _vm.minusIcon
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": _vm.minusIcon
    }
  }), _vm._v(" "), _c('img', {
    attrs: {
      "src": _vm.circleIcon
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "status"
  }, [_c('div', {
    staticClass: "date"
  }, [_vm._v(_vm._s(_vm._f("date")(_vm.project.date_final)))]), _vm._v(" "), _c('div', {
    staticClass: "title"
  }, [_vm._v("Estimated end")])]), _vm._v(" "), _c('div', {
    staticClass: "padding"
  }), _vm._v(" "), _c('div', {
    staticClass: "icon-container"
  }, [_c('img', {
    attrs: {
      "src": _vm.paperclipIcon,
      "width": "30"
    }
  })]), _vm._v(" "), _c('div', {
    directives: [{
      name: "popover",
      rawName: "v-popover",
      value: (_vm.filesPopOptions),
      expression: "filesPopOptions"
    }],
    staticClass: "documents",
    attrs: {
      "title": "project files"
    }
  }, [_c('div', {
    staticClass: "title"
  }, [_vm._v("Documents")]), _vm._v(" "), _c('div', {
    staticClass: "count"
  }, [_c('span', [_vm._v(_vm._s(_vm.projectFiles.length) + " files")])])]), _vm._v(" "), _c('div', {
    staticClass: "webui-popover-content",
    attrs: {
      "id": "popover-project-files"
    }
  }, [_c('div', {
    staticClass: "list-group"
  }, _vm._l((_vm.projectFiles), function(file) {
    return _c('li', {
      staticClass: "list-group-item"
    }, [_c('a', {
      staticClass: "pull-right",
      attrs: {
        "href": '/' + file.path,
        "title": "download",
        "target": "_blank"
      }
    }, [_c('i', {
      staticClass: "glyphicon glyphicon-floppy-save"
    })]), _vm._v(" "), _c('a', {
      attrs: {
        "href": "#",
        "title": "see the message"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.goTo('project-message-' + file.id, file.visibility)
        }
      }
    }, [_vm._v("\n                            " + _vm._s(file.name) + "\n                        ")])])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "padding"
  })])], 1)])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1ed113b6", module.exports)
  }
}

/***/ }),

/***/ 362:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return (_vm.belongsToActiveService) ? _c('div', {
    class: _vm.messageCssClasses,
    attrs: {
      "id": 'project-message-' + _vm.message.id
    }
  }, [(_vm.message.owner.type === 'client') ? _c('div', {
    staticClass: "panel-heading"
  }, [_vm._v("\n    Message From Client\n  ")]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "col-author-avatar"
  }, [_c('div', {
    staticClass: "avatar-circle-lg",
    style: ({
      'background-color': _vm.message.owner.color
    })
  }, [_c('span', {
    staticClass: "initials"
  }, [_vm._v(_vm._s(_vm.message.owner.initials))])]), _vm._v(" "), ((!_vm.message.is_action && _vm.message.permissions['message.update']) || (_vm.message.is_action && _vm.message.permissions['action.update'])) ? _c('div', {
    staticClass: "message-visibility"
  }, [_c('span', {
    staticClass: "dropdown"
  }, [_c('button', {
    staticClass: "btn btn-link note",
    staticStyle: {
      "padding": "0"
    },
    attrs: {
      "id": "projects-index-filter",
      "data-toggle": "dropdown"
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-eye-open"
  }), _vm._v("\n            " + _vm._s(_vm.visibilityStatus) + "\n            "), _c('span', {
    staticClass: "caret"
  })]), _vm._v(" "), _c('ul', {
    staticClass: "dropdown-menu"
  }, [_c('li', [_c('a', {
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.changeMessageVisibility({
          message: _vm.message,
          target: 'all'
        })
      }
    }
  }, [_vm._v("\n                Client and Team\n              ")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.changeMessageVisibility({
          message: _vm.message,
          target: 'client'
        })
      }
    }
  }, [_vm._v("\n                Only Client\n              ")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.changeMessageVisibility({
          message: _vm.message,
          target: 'team'
        })
      }
    }
  }, [_vm._v("\n                Only Team\n              ")])]), _vm._v(" "), _c('li', [_c('a', {
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.changeMessageVisibility({
          message: _vm.message,
          target: 'hidden'
        })
      }
    }
  }, [_vm._v("\n                Private\n              ")])])])])]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "content"
  }, [_c('div', {
    staticClass: "info col-sm-12"
  }, [_c('div', {
    staticClass: "col-sm-6 author"
  }, [_c('h4', {
    staticClass: "name"
  }, [_vm._v(_vm._s(_vm.message.owner.name))]), _vm._v(" "), _c('span', {
    staticClass: "title"
  }, [_vm._v("\n            " + _vm._s(_vm.message.owner.role_description) + "\n          ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-6"
  }, [_c('div', {
    staticClass: "row"
  }, [_c('div', {
    staticClass: "col-sm-4 tags"
  }, [(_vm.message.type) ? _c('span', {
    staticClass: "label label-info"
  }, [_vm._v("\n                " + _vm._s(_vm.message.type) + "\n              ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-4 tags"
  }, [(_vm.openedProject.permissions['project.update']) ? _c('span', [(_vm.message.jira_key) ? _c('a', {
    staticClass: "label label-warning",
    attrs: {
      "href": _vm.jiraKeyLink + _vm.message.jira_key,
      "target": "_blank",
      "title": "issue on Jira"
    }
  }, [_c('img', {
    attrs: {
      "src": "/images/icons/logo-icon-jira.svg",
      "width": "16px"
    }
  }), _vm._v("\n                  " + _vm._s(_vm.message.jira_key) + "\n                ")]) : (_vm.openedProject.jira_key && _vm.userHasJira) ? _c('a', {
    staticClass: "label label-default",
    class: {
      disabled: _vm.jiraLoading
    },
    attrs: {
      "href": "#",
      "title": "open new issue on Jira"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.getJiraIssueDetails($event)
      }
    }
  }, [(!_vm.jiraLoading) ? _c('i', {
    staticClass: "glyphicon glyphicon-share-alt"
  }) : _c('i', {
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }), _vm._v("\n                  Jira\n                ")]) : _vm._e()]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-4 date"
  }, [_vm._v("\n              " + _vm._s(_vm._f("time")(_vm.message.updated_at.date)) + "\n              "), (_vm.message.updated_at.date !== _vm.message.created_at.date) ? _c('span', {
    staticClass: "glyphicon glyphicon-edit",
    attrs: {
      "title": 'originally created ' + _vm.human(_vm.message.created_at.date)
    }
  }) : _vm._e()])])])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('br'), _vm._v(" "), _c('div', {
    staticClass: "body"
  }, [_c('div', {
    domProps: {
      "innerHTML": _vm._s(_vm.htmlFilter(_vm.message.content))
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "footer row message-services"
  }, [(_vm.message.services && _vm.message.services.length > 0) ? _c('span', {
    staticClass: "title"
  }, [_vm._v("\n          Services:\n        ")]) : _vm._e(), _vm._v(" "), _vm._l((_vm.message.services), function(service) {
    return (_vm.message.services && _vm.message.services.length > 0) ? _c('span', {
      staticClass: "service-name label label-info"
    }, [_vm._v("\n          " + _vm._s(service.service.name) + "\n          "), (_vm.message.permissions['message.update'] ||
      _vm.message.is_action && _vm.message.permissions['action.update']) ? _c('span', {
      staticClass: "action-icon",
      on: {
        "click": function($event) {
          _vm.removeService(service)
        }
      }
    }, [_c('i', {
      staticClass: "glyphicon glyphicon-remove-sign"
    })]) : _vm._e()]) : _vm._e()
  }), _vm._v(" "), ((_vm.message.permissions['message.update'] ||
      _vm.message.is_action && _vm.message.permissions['action.update']) &&
    _vm.message.services.length < _vm.openedProject.services.length) ? _c('span', {
    staticClass: "dropdown add-service"
  }, [_c('span', {
    staticClass: "label label-default message-service",
    attrs: {
      "id": 'message-service' + _vm.message.id,
      "data-toggle": "dropdown"
    }
  }, [_vm._v("\n            Add service\n            "), _c('span', {
    staticClass: "caret"
  })]), _vm._v(" "), _c('ul', {
    staticClass: "dropdown-menu"
  }, _vm._l((_vm.openedProject.services), function(service) {
    return (_vm.servicesIds.indexOf(service.id) < 0) ? _c('li', [_c('a', {
      attrs: {
        "href": "#"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.addService(service)
        }
      }
    }, [_vm._v("\n                " + _vm._s(service.name) + "\n              ")])]) : _vm._e()
  }))]) : _vm._e()], 2), _vm._v(" "), (_vm.message.files && _vm.message.files.length > 0) ? _c('div', {
    staticClass: "footer"
  }, [_c('span', {
    staticClass: "title"
  }, [_vm._v("Attachments:")]), _vm._v(" "), _vm._l((_vm.message.files), function(file) {
    return _c('span', {
      staticClass: "file"
    }, [_c('span', {
      class: _vm.attachmentIcon(file)
    }), _vm._v(" "), _c('a', {
      attrs: {
        "href": '/' + file.path,
        "target": "_blank"
      }
    }, [_vm._v(_vm._s(file.name))])])
  })], 2) : _vm._e(), _vm._v(" "), (_vm.message.is_action) ? _c('div', {
    staticClass: "footer row"
  }, [(_vm.message.is_done) ? _c('span', {
    staticClass: "task-label success-task"
  }, [_vm._v("\n          Done\n        ")]) : _vm._e(), _vm._v(" "), (_vm.message.is_due) ? _c('span', {
    class: {
      'task-label': true, 'success-task': _vm.message.is_done, 'warning-task': _vm.message.is_due
    }
  }, [_vm._v("\n          Action Due\n        ")]) : _vm._e(), _vm._v(" "), _c('span', {
    staticClass: "segment"
  }, [_vm._v("\n          Due Date: " + _vm._s(_vm._f("date")(_vm.message.date_due)) + "\n        ")]), _vm._v(" "), (_vm.message.is_done) ? _c('span', {
    staticClass: "segment"
  }, [_vm._v("\n          Done on: " + _vm._s(_vm._f("date")(_vm.message.date_done)) + "\n        ")]) : _vm._e(), _vm._v(" "), (_vm.message.is_due && _vm.message.type !== 'approval') ? _c('span', {
    staticClass: "segment pull-right"
  }, [(_vm.message.permissions['action.update']) ? _c('button', {
    staticClass: "btn btn-sm btn-success",
    on: {
      "click": function($event) {
        _vm.makeThisActionDone(_vm.message)
      }
    }
  }, [_vm._v("\n            It's done!\n          ")]) : _vm._e()]) : _vm._e()]) : _vm._e(), _vm._v(" "), (_vm.message.type == 'approval' && _vm.message.is_done) ? _c('div', {
    staticClass: "message-approval-note footer"
  }, [_c('h4', [_vm._v(_vm._s(_vm.message.approval_state ? 'Approved' : 'Rejected'))]), _vm._v(" "), (_vm.message.approval_note) ? _c('p', {
    domProps: {
      "innerHTML": _vm._s(_vm.message.approval_note)
    }
  }) : _vm._e()]) : _vm._e(), _vm._v(" "), (_vm.message.type == 'approval' && _vm.message.is_due && _vm.message.permissions['message.approve']) ? _c('reject-approve-actions', {
    attrs: {
      "message": _vm.message
    }
  }) : _vm._e()], 1)]), _vm._v(" "), (_vm.showJiraNewIssueModal) ? _c('vue-modal', [_c('h3', {
    attrs: {
      "slot": "header"
    },
    slot: "header"
  }, [_c('img', {
    attrs: {
      "src": "/images/icons/logo-icon-jira.svg",
      "width": "32px"
    }
  }), _vm._v(" Create new issue\n    ")]), _vm._v(" "), _c('div', {
    attrs: {
      "slot": "body"
    },
    slot: "body"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Summary")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.jiraNewIssueSummary),
      expression: "jiraNewIssueSummary"
    }],
    staticClass: "form-control",
    domProps: {
      "value": (_vm.jiraNewIssueSummary)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.jiraNewIssueSummary = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Description")]), _vm._v(" "), _c('textarea', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.jiraNewIssueDescription),
      expression: "jiraNewIssueDescription"
    }],
    staticClass: "form-control",
    attrs: {
      "rows": "10"
    },
    domProps: {
      "value": (_vm.jiraNewIssueDescription)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.jiraNewIssueDescription = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', [_vm._v("Type")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.jiraNewIssueType),
      expression: "jiraNewIssueType"
    }],
    staticClass: "form-control",
    domProps: {
      "value": (_vm.jiraNewIssueType)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.jiraNewIssueType = $event.target.value
      }
    }
  })])]), _vm._v(" "), _c('div', {
    attrs: {
      "slot": "footer"
    },
    slot: "footer"
  }, [_c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": function($event) {
        _vm.showJiraNewIssueModal = false
      }
    }
  }, [_vm._v("\n        Cancel\n      ")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-success",
    on: {
      "click": _vm.shareToJira
    }
  }, [_vm._v("\n        Create Issue\n      ")])])]) : _vm._e()], 1) : _vm._e()
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-1fe00c11", module.exports)
  }
}

/***/ }),

/***/ 363:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "row content-box"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat",
    staticStyle: {
      "margin-bottom": "0"
    }
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "section-title"
  }, [_vm._v("team")]), _vm._v(" "), _c('div', {
    staticClass: "row project-team"
  }, [_vm._l((_vm.project.members), function(member) {
    return (!_vm.projectActiveService || (member.pivot.project_service_id && _vm.projectActiveService.pivot.id === member.pivot.project_service_id)) ? _c('div', {
      staticClass: "team-member"
    }, [(_vm.project.permissions['project.update']) ? _c('a', {
      staticClass: "member-remove",
      attrs: {
        "href": "#"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.removeMember(member)
        }
      }
    }, [_c('span', {
      staticClass: "icon-sm icon-remove-dark"
    })]) : _vm._e(), _vm._v(" "), _c('div', {
      staticClass: "avatar-circle",
      style: ({
        'background-color': member.color
      })
    }, [_c('span', {
      staticClass: "initials"
    }, [_vm._v(_vm._s(member.initials))])]), _vm._v(" "), _c('div', {
      staticClass: "member-info"
    }, [_c('div', {
      staticClass: "name"
    }, [_vm._v(_vm._s(member.name))]), _vm._v(" "), _c('div', {
      staticClass: "title"
    }, [_vm._v(_vm._s(member.role_description))]), _vm._v(" "), _c('div', {
      staticClass: "service note"
    }, [_vm._v(_vm._s(_vm.serviceName(member)))])])]) : _vm._e()
  }), _vm._v(" "), (_vm.project.permissions['project.update']) ? _c('div', {
    staticClass: "team-member"
  }, [_c('a', {
    directives: [{
      name: "popover",
      rawName: "v-popover",
      value: (_vm.addPopOptions),
      expression: "addPopOptions"
    }],
    staticClass: "member-add",
    attrs: {
      "href": "#",
      "id": "add-project-member-popover"
    }
  }, [_c('span', {
    staticClass: "icon-lg icon-create"
  })]), _vm._v(" "), _c('div', {
    staticClass: "webui-popover-content",
    attrs: {
      "id": "member-add-popover-content"
    }
  }, [_c('div', {
    staticClass: "content-padding"
  }, [_c('h1', [_vm._v("Add member")]), _vm._v(" "), _c('div', {
    staticClass: "form-horizontal"
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('autocomplete', {
    attrs: {
      "id": "member-name",
      "cssClasses": "form-control",
      "name": "members",
      "placeholder": "Ex: Mohamed",
      "url": _vm.membersList,
      "anchor": "name",
      "label": "id",
      "model": "newMemberName"
    },
    on: {
      "selected": _vm.newMemberSelected
    }
  })], 1), _vm._v(" "), _c('br'), _vm._v(" "), (_vm.newMemberData) ? _c('div', {
    staticClass: "row member-small-box"
  }, [_c('div', {
    staticClass: "avatar-circle",
    style: ({
      'background-color': _vm.newMemberData.color
    })
  }, [_c('span', {
    staticClass: "initials"
  }, [_vm._v(_vm._s(_vm.newMemberData.initials))])]), _vm._v(" "), _c('div', {
    staticClass: "name"
  }, [_vm._v(" " + _vm._s(_vm.newMemberData.name))])]) : _vm._e(), _vm._v(" "), _c('br'), _vm._v(" "), _c('div', {
    staticClass: "selected-service"
  }, [_vm._v("\n                                        Selected Service:\n                                        "), (_vm.projectActiveService) ? _c('strong', [_vm._v(_vm._s(_vm.projectActiveService.name))]) : _c('strong', [_vm._v("None")])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('button', {
    staticClass: "btn btn-block btn-success btn-lg",
    on: {
      "click": function($event) {
        _vm.addNewMember('#add-project-member-popover')
      }
    }
  }, [_vm._v("\n                                        Add\n                                    ")])])])])]) : _vm._e()], 2)])])]), _vm._v(" "), (_vm.showDeleteModal) ? _c('modal', [_c('div', {
    attrs: {
      "slot": "body"
    },
    slot: "body"
  }, [_vm._v("\n            You are going to remove `"), _c('b', [_vm._v(_vm._s(_vm.toRemoveMemberData.name))]), _vm._v("` member\n            from the project.\n            "), _c('br'), _vm._v(" "), _c('br'), _vm._v(" "), _c('h3', [_vm._v("Yes, Remove `"), _c('b', [_vm._v(_vm._s(_vm.toRemoveMemberData.name))]), _vm._v("` member!")])]), _vm._v(" "), _c('h3', {
    staticClass: "text-danger",
    attrs: {
      "slot": "header"
    },
    slot: "header"
  }, [_vm._v("Remove a member!")]), _vm._v(" "), _c('div', {
    attrs: {
      "slot": "footer"
    },
    slot: "footer"
  }, [_c('button', {
    staticClass: "btn btn-default btn-link cancel",
    on: {
      "click": _vm.cancelRemoveMember
    }
  }, [_vm._v("\n                Cancel\n            ")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": _vm.confirmedRemoveMember
    }
  }, [_vm._v("\n                OK\n            ")])])]) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-28cc21a2", module.exports)
  }
}

/***/ }),

/***/ 364:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('transition', {
    attrs: {
      "name": "modal"
    }
  }, [_c('div', {
    staticClass: "modal-mask"
  }, [_c('div', {
    staticClass: "modal-wrapper"
  }, [_c('div', {
    staticClass: "modal-container"
  }, [_c('div', {
    staticClass: "modal-header"
  }, [_vm._t("header")], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-body"
  }, [_vm._t("body")], 2), _vm._v(" "), _c('div', {
    staticClass: "modal-footer"
  }, [_vm._t("footer", [_c('button', {
    staticClass: "modal-default-button",
    on: {
      "click": function($event) {
        _vm.$emit('close')
      }
    }
  }, [_vm._v("\n                                OK\n                            ")])])], 2)])])])])], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-2a80d580", module.exports)
  }
}

/***/ }),

/***/ 366:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    attrs: {
      "id": 'add-reject-message-' + _vm.id
    }
  }, [_c('div', {
    staticClass: "panel panel-default flat panel-message-form approval-action"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "editor"
  }), _vm._v(" "), _c('div', {
    staticClass: "panel-message-footer"
  }, [_c('div', {
    staticClass: "row"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "col-sm-2 attachment"
  }), _vm._v(" "), _c('div', {
    staticClass: "col-sm-5 buttons"
  }, [_c('button', {
    staticClass: "btn btn-sm btn-link",
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.cancel($event)
      }
    }
  }, [_vm._v("\n                            Cancel\n                        ")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-sm btn-success",
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.publish($event)
      }
    }
  }, [_vm._v("\n                            Publish\n                        ")])])])])])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "toolbar col-sm-5"
  }, [_c('span', {
    staticClass: "ql-format-group"
  }, [_c('span', {
    staticClass: "ql-format-button ql-bold",
    attrs: {
      "title": "Bold"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-italic",
    attrs: {
      "title": "Italic"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-underline",
    attrs: {
      "title": "Underline"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-list",
    attrs: {
      "title": "List"
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "ql-format-button ql-bullet",
    attrs: {
      "title": "Bullet"
    }
  }), _vm._v(" "), _c('select', {
    staticClass: "ql-align",
    attrs: {
      "title": "Text Alignment"
    }
  }, [_c('option', {
    attrs: {
      "label": "Left",
      "selected": "",
      "value": "left"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Center",
      "value": "center"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Right",
      "value": "right"
    }
  }), _vm._v(" "), _c('option', {
    attrs: {
      "label": "Justify",
      "value": "justify"
    }
  })])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-2def613c", module.exports)
  }
}

/***/ }),

/***/ 368:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [((_vm.project.services && _vm.project.services.length > 0) || _vm.project.permissions['project.update']) ? _c('div', {
    staticClass: "row content-box"
  }, [_c('div', {
    staticClass: "outer-section-title"
  }, [_vm._v("\n      Services\n    ")]), _vm._v(" "), _c('div', {
    staticClass: "row services-boxes"
  }, [_vm._l((_vm.project.services), function(service) {
    return (_vm.project.services && _vm.project.services.length > 0) ? _c('div', {
      class: _vm.cssClasses(service),
      attrs: {
        "id": 'service-box-' + service.pivot.id
      }
    }, [(_vm.project.permissions['project.update']) ? _c('div', {
      staticClass: "actions"
    }, [_c('a', {
      attrs: {
        "href": "#",
        "title": "Remove"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.removeService(service, '#service-box-' + service.pivot.id)
        }
      }
    }, [_c('span', {
      staticClass: "icon-sm icon-remove-dark"
    })]), _vm._v(" "), _c('a', {
      directives: [{
        name: "popover",
        rawName: "v-popover",
        value: (_vm.editPopOptions),
        expression: "editPopOptions"
      }],
      staticClass: "service-edit",
      attrs: {
        "href": "#",
        "id": 'edit-service-' + service.pivot.id
      },
      on: {
        "click": function($event) {
          _vm.updateServiceInEdit(service, service.pivot)
        }
      }
    }, [_c('span', {
      staticClass: "icon-sm icon-edit-dark"
    })]), _vm._v(" "), _c('div', {
      staticClass: "webui-popover-content",
      attrs: {
        "id": 'popover-content-edit-service-' + service.pivot.id
      }
    }, [_c('div', {
      staticClass: "content-padding"
    }, [_c('h1', [_vm._v("Edit Service: " + _vm._s(service.name))]), _vm._v(" "), _c('form', {
      staticClass: "form-horizontal",
      attrs: {
        "id": 'edit-service-' + service.pivot.id + '-form'
      },
      on: {
        "submit": function($event) {
          $event.preventDefault();
          _vm.updateService('#edit-service-' + service.pivot.id)
        }
      }
    }, [_c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      attrs: {
        "for": "service-id-edit"
      }
    }, [_vm._v("Service")]), _vm._v(" "), _c('select', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.serviceInEdit.service_id),
        expression: "serviceInEdit.service_id"
      }],
      staticClass: "form-control",
      attrs: {
        "id": "service-id-edit"
      },
      on: {
        "change": function($event) {
          var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
            return o.selected
          }).map(function(o) {
            var val = "_value" in o ? o._value : o.value;
            return val
          });
          _vm.$set(_vm.serviceInEdit, "service_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
        }
      }
    }, _vm._l((_vm.servicesList), function(serviceItem) {
      return _c('option', {
        domProps: {
          "value": serviceItem.id
        }
      }, [_vm._v("\n                      " + _vm._s(serviceItem.name) + "\n                    ")])
    }))]), _vm._v(" "), _c('div', {
      staticClass: "form-group has-feedback"
    }, [_c('label', [_vm._v("Start date")]), _vm._v(" "), _c('date-picker', {
      attrs: {
        "date": _vm.serviceInEdit.start,
        "option": _vm.datepickerOptions
      }
    }), _vm._v(" "), _c('span', {
      staticClass: "icon icon-calendar-dark form-control-feedback"
    })], 1), _vm._v(" "), _c('div', {
      staticClass: "form-group has-feedback"
    }, [_c('label', [_vm._v("Finish date")]), _vm._v(" "), _c('date-picker', {
      attrs: {
        "date": _vm.serviceInEdit.finish,
        "option": _vm.datepickerOptions
      }
    }), _vm._v(" "), _c('span', {
      staticClass: "icon icon-calendar-dark form-control-feedback"
    })], 1), _vm._v(" "), _c('div', {
      staticClass: "form-group"
    }, [_c('label', {
      attrs: {
        "for": "status-id-edit"
      }
    }, [_vm._v("Status")]), _vm._v(" "), _c('select', {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: (_vm.serviceInEdit.status),
        expression: "serviceInEdit.status"
      }],
      staticClass: "form-control",
      attrs: {
        "id": "status-id-edit"
      },
      on: {
        "change": function($event) {
          var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
            return o.selected
          }).map(function(o) {
            var val = "_value" in o ? o._value : o.value;
            return val
          });
          _vm.$set(_vm.serviceInEdit, "status", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
        }
      }
    }, _vm._l((_vm.statuses), function(name, status) {
      return _c('option', {
        domProps: {
          "value": status
        }
      }, [_vm._v("\n                      " + _vm._s(name) + "\n                    ")])
    }))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('button', {
      staticClass: "btn btn-block btn-success btn-lg"
    }, [_vm._v("Update")]), _vm._v(" "), _c('p', {
      staticClass: "help-block"
    }, [_vm._v("\n                  all fields are required.\n                ")])])])])]) : _vm._e(), _vm._v(" "), _c('div', {
      staticClass: "info",
      on: {
        "click": function($event) {
          _vm.setProjectActiveService(service)
        }
      }
    }, [_c('h1', [_vm._v(_vm._s(service.name))]), _vm._v(" "), _c('p', {
      staticClass: "status"
    }, [_vm._v(_vm._s(_vm.serviceStatus(service)))]), _vm._v(" "), _c('p', {
      staticClass: "date"
    }, [_vm._v("\n            " + _vm._s(_vm._f("date")(service.pivot.date_finish)) + "\n          ")])])]) : _vm._e()
  }), _vm._v(" "), (_vm.project.permissions['project.update']) ? _c('div', {
    staticClass: "service-box-add service-add"
  }, [_c('div', {
    directives: [{
      name: "popover",
      rawName: "v-popover",
      value: (_vm.addPopOptions),
      expression: "addPopOptions"
    }],
    attrs: {
      "id": 'add-service-box-' + _vm.project.id
    }
  }, [_c('h1', [_vm._v("+")]), _vm._v(" "), _c('h1', [_vm._v("Add")]), _vm._v(" "), _c('h1', [_vm._v("Service")])]), _vm._v(" "), _c('div', {
    staticClass: "webui-popover-content",
    attrs: {
      "id": 'popover-content-add-service-' + _vm.project.id
    }
  }, [_c('div', {
    staticClass: "content-padding"
  }, [_c('h1', [_vm._v("Add Service")]), _vm._v(" "), _c('form', {
    staticClass: "form-horizontal",
    attrs: {
      "id": 'add-service-box-' + _vm.project.id + '-form'
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        _vm.addService('#add-service-box-' + _vm.project.id)
      }
    }
  }, [_c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    attrs: {
      "for": "service-id"
    }
  }, [_vm._v("Service")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newService.service_id),
      expression: "newService.service_id"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "service-id"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.newService, "service_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.servicesList), function(serviceItem) {
    return _c('option', {
      domProps: {
        "value": serviceItem.id
      }
    }, [_vm._v("\n                    " + _vm._s(serviceItem.name) + "\n                  ")])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "form-group has-feedback",
    class: {
      'has-error': _vm.newServiceErrors.date_start
    }
  }, [_c('label', [_vm._v("Start date")]), _vm._v(" "), _c('date-picker', {
    attrs: {
      "date": _vm.newService.start,
      "option": _vm.datepickerOptions
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "icon icon-calendar-dark form-control-feedback"
  }), _vm._v(" "), (_vm.newServiceErrors.date_start) ? _c('p', {
    staticClass: "help-block text-danger"
  }, [_vm._v("\n                  " + _vm._s(_vm.newServiceErrors.date_start[0]) + "\n                ")]) : _vm._e()], 1), _vm._v(" "), _c('div', {
    staticClass: "form-group has-feedback",
    class: {
      'has-error': _vm.newServiceErrors.date_finish
    }
  }, [_c('label', [_vm._v("Finish date")]), _vm._v(" "), _c('date-picker', {
    attrs: {
      "date": _vm.newService.finish,
      "option": _vm.datepickerOptions
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "icon icon-calendar-dark form-control-feedback"
  }), _vm._v(" "), (_vm.newServiceErrors.date_finish) ? _c('p', {
    staticClass: "help-block text-danger"
  }, [_vm._v("\n                  " + _vm._s(_vm.newServiceErrors.date_finish[0]) + "\n                ")]) : _vm._e()], 1), _vm._v(" "), _c('div', {
    staticClass: "form-group"
  }, [_c('label', {
    attrs: {
      "for": "status-id"
    }
  }, [_vm._v("Status")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newService.status),
      expression: "newService.status"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "status-id"
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.newService, "status", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.statuses), function(name, status) {
    return _c('option', {
      domProps: {
        "value": status
      }
    }, [_vm._v("\n                    " + _vm._s(name) + "\n                  ")])
  }))]), _vm._v(" "), _c('br'), _vm._v(" "), _c('button', {
    staticClass: "btn btn-block btn-success btn-lg"
  }, [_vm._v("Add")]), _vm._v(" "), _c('p', {
    staticClass: "help-block"
  }, [_vm._v("\n                all fields are required.\n              ")])])])])]) : _vm._e()], 2)]) : _vm._e(), _vm._v(" "), (_vm.showDeleteModal) ? _c('modal', [_c('div', {
    attrs: {
      "slot": "body"
    },
    slot: "body"
  }, [_vm._v("\n      You are going to remove `"), _c('b', [_vm._v(_vm._s(_vm.toRemoveService.name))]), _vm._v("` service\n      from the project, that would delete all messages and actions.\n      "), _c('br'), _vm._v(" "), _c('br'), _vm._v(" "), _c('h3', [_vm._v("Yes, Remove `"), _c('b', [_vm._v(_vm._s(_vm.toRemoveService.name))]), _vm._v("` service!")])]), _vm._v(" "), _c('h3', {
    staticClass: "text-danger",
    attrs: {
      "slot": "header"
    },
    slot: "header"
  }, [_vm._v("Remove a service!")]), _vm._v(" "), _c('div', {
    attrs: {
      "slot": "footer"
    },
    slot: "footer"
  }, [_c('button', {
    staticClass: "btn btn-default btn-link cancel",
    on: {
      "click": _vm.cancelRemoveService
    }
  }, [_vm._v("\n        Cancel\n      ")]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-default",
    on: {
      "click": _vm.confirmedRemoveService
    }
  }, [_vm._v("\n        OK\n      ")])])]) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-3ccd613d", module.exports)
  }
}

/***/ }),

/***/ 369:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _vm._m(0)
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "row content-box"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "tab-content"
  }, [_c('div', {
    staticClass: "tab-pane active",
    attrs: {
      "id": "messages-tab-content"
    }
  })])])])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-4aa1a9aa", module.exports)
  }
}

/***/ }),

/***/ 370:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "content-box"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat"
  }, [_c('div', {
    staticClass: "panel-body project-info"
  }, [_c('div', {
    staticClass: "project-thumb"
  }, [(_vm.project.image) ? _c('img', {
    attrs: {
      "src": _vm.project.image
    }
  }) : _c('div', {
    staticClass: "project-color",
    style: ({
      'background-color': _vm.project.color
    })
  })]), _vm._v(" "), _c('div', {
    staticClass: "project-description"
  }, [_c('div', {
    staticClass: "section-title"
  }, [_vm._v("\n            project\n          ")]), _vm._v(" "), _c('h1', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.project.name))]), _vm._v(" "), _c('p', {
    staticClass: "project-client"
  }, [_vm._v(_vm._s(_vm.project.company.name))]), _vm._v(" "), _c('p', {
    staticClass: "project-client-name small"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-user"
  }), _vm._v("\n            " + _vm._s(_vm.project.client.name) + "\n          ")])]), _vm._v(" "), _c('div', {
    staticClass: "project-leaders"
  }, [_c('div', {
    staticClass: "section-title"
  }, [_vm._v("\n            Leaders\n          ")]), _vm._v(" "), _c('p', [_c('span', {
    staticClass: "bold"
  }, [_vm._v("Project Manager: ")]), _vm._v(" "), (!_vm.showPmEdit && _vm.project.permissions['project.update']) ? _c('span', [_vm._v("\n              " + _vm._s(_vm.project.manager.name) + "\n              "), _c('a', {
    staticClass: "edit text-info",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.showPmEdit = true
      }
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-pencil"
  })])]) : _vm._e(), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showPmEdit && _vm.project.permissions['project.update']),
      expression: "showPmEdit && project.permissions['project.update']"
    }],
    staticClass: "form-inline"
  }, [_c('div', {
    staticClass: "input-group input-group-sm"
  }, [_c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.pm_id),
      expression: "pm_id"
    }],
    staticClass: "form-control input-sm",
    attrs: {
      "id": "change-project-pm",
      "disabled": _vm.formInProgress
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.pm_id = $event.target.multiple ? $$selectedVal : $$selectedVal[0]
      }
    }
  }, _vm._l((_vm.managersList), function(manager) {
    return _c('option', {
      domProps: {
        "value": manager.id
      }
    }, [_vm._v("\n                    " + _vm._s(manager.name) + "\n                  ")])
  })), _vm._v(" "), _c('span', {
    staticClass: "input-group-btn"
  }, [_c('button', {
    staticClass: "btn btn-default",
    attrs: {
      "title": "Cancel",
      "disabled": _vm.formInProgress
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.showPmEdit = false
      }
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-remove"
  })]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-success",
    attrs: {
      "title": "Update",
      "disabled": _vm.formInProgress
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.changePm($event)
      }
    }
  }, [(_vm.formInProgress) ? _c('i', {
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }) : _c('i', {
    staticClass: "glyphicon glyphicon-ok"
  })])])])])]), _vm._v(" "), _c('p', [_c('span', {
    staticClass: "bold"
  }, [_vm._v("Technical Control: ")]), _vm._v(" " + _vm._s(_vm.project.tech_lead.name) + "\n          ")])])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5771e62d", module.exports)
  }
}

/***/ }),

/***/ 371:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "row content-box",
    attrs: {
      "id": "client-pending-actions"
    }
  }, [(_vm.pendings.total > 0) ? _c('div', {
    staticClass: "outer-section-title"
  }, [(_vm.loggedInUser.type === 'client') ? _c('span', [_vm._v("Your")]) : _c('span', [_vm._v("Client")]), _vm._v("\n            action is needed (" + _vm._s(_vm.pendings.total) + ")\n        ")]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "row"
  }, [(_vm.pendings.requirement.length > 0) ? _c('div', {
    staticClass: "col-md-4 pending-actions-section"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "section-title"
  }, [_vm._v("Pending Requirements (" + _vm._s(_vm.pendings.requirement.length) + ")")]), _vm._v(" "), _c('div', {
    staticClass: "pending-items"
  }, _vm._l((_vm.pendings.requirement), function(req) {
    return _c('div', {
      staticClass: "row pending-action"
    }, [_c('div', {
      staticClass: "title col-xs-7"
    }, [_c('a', {
      class: {
        'not-visible': !req.visible_client
      },
      attrs: {
        "href": "#"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.goTo('project-message-' + req.id, req)
        }
      }
    }, [(!req.visible_client) ? _c('span', {
      staticClass: "glyphicon glyphicon-eye-close",
      attrs: {
        "title": "hidden to client"
      }
    }) : _vm._e(), _vm._v("\n                                        " + _vm._s(_vm._f("firstWords")(req.content, 3)) + ":\n                                    ")])]), _vm._v(" "), _c('div', {
      staticClass: "date col-xs-4"
    }, [_vm._v("\n                                    " + _vm._s(_vm._f("date")(req.dateDue)) + "\n                                ")])])
  }))])])]) : _vm._e(), _vm._v(" "), (_vm.pendings.approval.length > 0) ? _c('div', {
    staticClass: "col-md-4 pending-actions-section"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "section-title"
  }, [_vm._v("Pending Approvals (" + _vm._s(_vm.pendings.approval.length) + ")")]), _vm._v(" "), _c('div', {
    staticClass: "pending-items"
  }, _vm._l((_vm.pendings.approval), function(app) {
    return _c('div', {
      staticClass: "row pending-action"
    }, [_c('div', {
      staticClass: "title col-xs-7"
    }, [_c('a', {
      class: {
        'not-visible': !app.visible_client
      },
      attrs: {
        "href": "#"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.goTo('project-message-' + app.id, app)
        }
      }
    }, [(!app.visible_client) ? _c('span', {
      staticClass: "glyphicon glyphicon-eye-close",
      attrs: {
        "title": "hidden to client"
      }
    }) : _vm._e(), _vm._v("\n                                        " + _vm._s(_vm._f("firstWords")(app.content, 3)) + ":\n                                    ")])]), _vm._v(" "), _c('div', {
      staticClass: "date col-xs-4"
    }, [_vm._v("\n                                    " + _vm._s(_vm._f("date")(app.dateDue)) + "\n                                ")])])
  }))])])]) : _vm._e(), _vm._v(" "), (_vm.pendings.payment.length > 0) ? _c('div', {
    staticClass: "col-md-4 pending-actions-section"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat"
  }, [_c('div', {
    staticClass: "panel-body"
  }, [_c('div', {
    staticClass: "section-title"
  }, [_vm._v("Pending Payments (" + _vm._s(_vm.pendings.payment.length) + ")")]), _vm._v(" "), _c('div', {
    staticClass: "pending-items"
  }, _vm._l((_vm.pendings.payment), function(pay) {
    return _c('div', {
      staticClass: "row pending-action"
    }, [_c('div', {
      staticClass: "title col-xs-7"
    }, [_c('a', {
      class: {
        'not-visible': !pay.visible_client
      },
      attrs: {
        "href": "#"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.goTo('project-message-' + pay.id, pay)
        }
      }
    }, [(!pay.visible_client) ? _c('span', {
      staticClass: "glyphicon glyphicon-eye-close",
      attrs: {
        "title": "hidden to client"
      }
    }) : _vm._e(), _vm._v("\n                                        " + _vm._s(_vm._f("firstWords")(pay.content, 3)) + ":\n                                    ")])]), _vm._v(" "), _c('div', {
      staticClass: "date col-xs-4"
    }, [_vm._v("\n                                    " + _vm._s(_vm._f("date")(pay.dateDue)) + "\n                                ")])])
  }))])])]) : _vm._e()])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-5f69b9c6", module.exports)
  }
}

/***/ }),

/***/ 372:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('link', {
    attrs: {
      "href": "https://cdnjs.cloudflare.com/ajax/libs/quill/0.20.1/quill.snow.min.css",
      "rel": "stylesheet"
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "footer row"
  }, [_c('div', {
    staticClass: "pull-right action-buttons"
  }, [_c('button', {
    class: {
      btn: true, 'btn-default': true, active: _vm.rejectIsClicked
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.triggerReject($event)
      }
    }
  }, [_vm._v("\n                Reject\n            ")]), _vm._v(" "), _c('button', {
    class: {
      btn: true, 'btn-success': true, active: _vm.approveIsClicked
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.triggerApprove($event)
      }
    }
  }, [_vm._v("\n                Approve\n            ")])])]), _vm._v(" "), (_vm.approveIsClicked || _vm.rejectIsClicked) ? _c('quill', {
    attrs: {
      "options": {
        theme: 'snow'
      },
      "id": _vm.message.id + ''
    },
    on: {
      "cancel": _vm.cancel,
      "publish": _vm.publishMessage
    }
  }) : _vm._e()], 1)
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-68c5aab9", module.exports)
  }
}

/***/ }),

/***/ 373:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('div', {
    staticClass: "messages col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"
  }, _vm._l((_vm.messagesGroups), function(group) {
    return (_vm.groupHasActiveMessages(group[1])) ? _c('div', [_c('div', {
      staticClass: "group-date"
    }, [_c('span', [_vm._v(_vm._s(_vm._f("date")(group[0], "dddd, MMMM Do YYYY")))])]), _vm._v(" "), _vm._l((group[1]), function(message) {
      return _c('div', {
        key: message.id
      }, [_c('message-item', {
        attrs: {
          "message": message
        }
      })], 1)
    })], 2) : _vm._e()
  }))])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-75d94658", module.exports)
  }
}

/***/ }),

/***/ 375:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "ignore-major-container project-view-container container-fluid",
    attrs: {
      "id": "project-view-container"
    }
  }, [(_vm.openedProject) ? _c('div', [(_vm.openedProject.date_archived) ? _c('div', {
    staticClass: "archived-warning"
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-warning-sign"
  }), _vm._v("\n            Archived Project!\n        ")]) : _vm._e(), _vm._v(" "), _c('project-dates-and-files', {
    attrs: {
      "project": _vm.openedProject
    }
  }), _vm._v(" "), _c('div', {
    staticClass: "row content-box"
  }, [_c('div', {
    staticClass: "panel panel-default borderless flat"
  }, [_c('div', {
    staticClass: "panel-body project-panel-body"
  }, [_c('div', {
    staticClass: "tab-content"
  }, [_vm._m(0), _vm._v(" "), _c('div', {
    staticClass: "tab-pane active",
    attrs: {
      "id": "client-tab-content"
    }
  }, [_c('h1', [_vm._v("Messages")]), _vm._v(" "), _c('project-messages', {
    attrs: {
      "project": _vm.openedProject
    }
  })], 1), _vm._v(" "), _vm._m(1), _vm._v(" "), _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "budget-tab-content"
    }
  })])])])])], 1) : _vm._e(), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.openedProject === null),
      expression: "openedProject === null"
    }],
    staticClass: "loading"
  }, [_vm._v("Loading…")])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "team-tab-content"
    }
  }, [_c('h1', [_vm._v("Plan")])])
},function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "tab-pane",
    attrs: {
      "id": "notes-tab-content"
    }
  }, [_c('h1', [_vm._v("Notes")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-7d403847", module.exports)
  }
}

/***/ }),

/***/ 378:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "projects-top-tabs-container"
  }, [_c('div', {
    staticClass: "project-thumb"
  }, [(_vm.project.image) ? _c('img', {
    attrs: {
      "src": _vm.project.image,
      "width": "45",
      "height": "45"
    }
  }) : _c('span', {
    staticClass: "project-color",
    style: ({
      'background-color': _vm.project.color
    })
  }), _vm._v(" "), _c('span', {
    staticClass: "project-concluded-info"
  }, [_c('h3', {
    staticClass: "title"
  }, [_vm._v(_vm._s(_vm.project.name))]), _vm._v(" "), _c('p', {
    staticClass: "project-client"
  }, [_vm._v(_vm._s(_vm.project.company.name))])])]), _vm._v(" "), _vm._m(0)])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "row content-box",
    attrs: {
      "id": "project-sections-tabs"
    }
  }, [_c('ul', {
    staticClass: "nav nav-tabs"
  }, [_c('li', {
    staticClass: "trapezium",
    attrs: {
      "id": "team-tab"
    }
  }, [_c('a', {
    attrs: {
      "href": "#team-tab-content",
      "data-toggle": "tab"
    }
  }, [_vm._v("Plan")])]), _vm._v(" "), _c('li', {
    staticClass: "parallelogram active",
    attrs: {
      "id": "client-tab"
    }
  }, [_c('a', {
    attrs: {
      "href": "#client-tab-content",
      "data-toggle": "tab"
    }
  }, [_vm._v("Messages")])]), _vm._v(" "), _c('li', {
    staticClass: "trapezium-turned",
    attrs: {
      "id": "notes-tab"
    }
  }, [_c('a', {
    attrs: {
      "href": "#notes-tab-content",
      "data-toggle": "tab"
    }
  }, [_vm._v("Notes")])]), _vm._v(" "), _c('li', {
    staticClass: "trapezium",
    attrs: {
      "id": "budget-tab"
    }
  }, [_c('a', {
    attrs: {
      "href": "#budget-tab-content",
      "data-toggle": "tab"
    }
  }, [_vm._v("Ticket")])])])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-b120a046", module.exports)
  }
}

/***/ }),

/***/ 379:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('h1', [_vm._v("\n    Budget:\n    "), _c('a', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (!_vm.showBudgetEditForm),
      expression: "!showBudgetEditForm"
    }],
    staticClass: "label label-default",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.showBudgetEditForm = true
      }
    }
  }, [_vm._v("\n      " + _vm._s(_vm.budget) + " SAR\n    ")]), _vm._v(" "), _c('span', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showBudgetEditForm),
      expression: "showBudgetEditForm"
    }],
    staticClass: "form-inline"
  }, [_c('div', {
    staticClass: "input-group input-group-sm"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.budget),
      expression: "budget"
    }],
    staticClass: "form-control",
    attrs: {
      "step": "0.01",
      "disabled": _vm.budgetFormLoading
    },
    domProps: {
      "value": (_vm.budget)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.budget = $event.target.value
      }
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "input-group-btn"
  }, [_c('button', {
    staticClass: "btn btn-default",
    attrs: {
      "title": "Cancel",
      "disabled": _vm.budgetFormLoading
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.closeBudgetEdit($event)
      }
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-remove"
  })]), _vm._v(" "), _c('button', {
    staticClass: "btn btn-success",
    attrs: {
      "title": "Update",
      "disabled": _vm.budgetFormLoading || (isNaN(parseFloat(_vm.budget)) && !isFinite(_vm.budget))
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        return _vm.updateBudget($event)
      }
    }
  }, [(_vm.budgetFormLoading) ? _c('i', {
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }) : _c('i', {
    staticClass: "glyphicon glyphicon-ok"
  })])])])]), _vm._v(" "), _c('span', {
    staticClass: "pull-right"
  }, [_vm._v("\n      Balance:\n      "), _c('span', {
    class: 'label label-' + _vm.balanceStatus
  }, [_vm._v("\n        " + _vm._s(_vm.project.budget_balance) + " SAR\n      ")])])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('div', {
    staticClass: "row well"
  }, [_c('form', {
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.addExpense($event)
      }
    }
  }, [_c('div', {
    staticClass: "col-sm-6"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newExpense.description),
      expression: "newExpense.description"
    }],
    staticClass: "form-control",
    attrs: {
      "placeholder": "Expense description here",
      "required": "",
      "disabled": _vm.budgetFormLoading
    },
    domProps: {
      "value": (_vm.newExpense.description)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.newExpense, "description", $event.target.value)
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-2"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newExpense.cost),
      expression: "newExpense.cost"
    }],
    staticClass: "form-control",
    attrs: {
      "disabled": _vm.budgetFormLoading,
      "type": "number",
      "step": "0.01",
      "required": ""
    },
    domProps: {
      "value": (_vm.newExpense.cost)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.newExpense, "cost", $event.target.value)
      }
    }
  })]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-4"
  }, [_c('button', {
    staticClass: "btn btn-lg btn-success",
    attrs: {
      "type": "submit",
      "disabled": _vm.budgetFormLoading
    }
  }, [(_vm.budgetFormLoading) ? _c('i', {
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }) : _vm._e(), _vm._v("\n          Add to expenses\n        ")])])])]), _vm._v(" "), (_vm.project.expenses) ? _c('table', {
    staticClass: "table table-striped"
  }, [_vm._m(0), _vm._v(" "), _c('tfoot', [_c('tr', [_c('td', {
    style: (_vm.totalBackground)
  }, [_vm._v("Total")]), _vm._v(" "), _c('td', {
    style: (_vm.totalBackground)
  }, [_vm._v("\n        " + _vm._s(_vm.totalExpenses) + " SAR\n      ")]), _vm._v(" "), _c('td', {
    style: (_vm.totalBackground)
  })])]), _vm._v(" "), _c('tbody', _vm._l((_vm.project.expenses), function(expense) {
    return _c('tr', [_c('td', [_vm._v("\n        " + _vm._s(expense.description) + "\n      ")]), _vm._v(" "), _c('td', [_vm._v("\n        " + _vm._s(expense.cost) + " SAR\n      ")]), _vm._v(" "), _c('td', [_c('a', {
      staticClass: "text-danger",
      attrs: {
        "href": "#",
        "title": "Delete this expense"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.deleteExpense(expense.id)
        }
      }
    }, [_c('i', {
      staticClass: "glyphicon glyphicon-trash"
    })])])])
  }))]) : _vm._e()])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('thead', [_c('tr', [_c('th', [_vm._v("Description")]), _vm._v(" "), _c('th', [_vm._v("cost")]), _vm._v(" "), _c('th')])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-b5f04b38", module.exports)
  }
}

/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.type),
      expression: "type"
    }],
    class: _vm.cssClasses,
    attrs: {
      "type": "text",
      "id": _vm.id,
      "name": _vm.name,
      "placeholder": _vm.placeholder,
      "autocomplete": "off"
    },
    domProps: {
      "value": (_vm.type)
    },
    on: {
      "input": [function($event) {
        if ($event.target.composing) { return; }
        _vm.type = $event.target.value
      }, function($event) {
        _vm.input(_vm.type)
      }],
      "dblclick": _vm.showAll,
      "blur": _vm.hideAll,
      "keydown": _vm.keydown,
      "focus": _vm.focus
    }
  }), _vm._v(" "), _c('div', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.showList),
      expression: "showList"
    }],
    class: ( _obj = {
      autocomplete: true, transition: true
    }, _obj['autocomplete-' + _vm.name] = true, _obj ),
    attrs: {
      "id": 'autocomplete-' + _vm.name
    }
  }, [_c('ul', _vm._l((_vm.json), function(data) {
    return _c('li', {
      class: _vm.activeClass(_vm.$index)
    }, [_c('a', {
      attrs: {
        "href": "#"
      },
      on: {
        "click": function($event) {
          $event.preventDefault();
          _vm.selectList(data)
        },
        "mousemove": function($event) {
          _vm.mousemove(_vm.$index)
        }
      }
    }, [_c('b', [_vm._v(_vm._s(data[_vm.anchor]))])])])
  }))])])
  var _obj;
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-df83a136", module.exports)
  }
}

/***/ }),

/***/ 385:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(327);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("647da103", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-0759e358&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddMessageForm.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-0759e358&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./AddMessageForm.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 386:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(328);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("18098788", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-081497d4!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./FileUpload.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-081497d4!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./FileUpload.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 387:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(329);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("54291bdb", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-1ed113b6!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectDatesAndFiles.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-1ed113b6!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectDatesAndFiles.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 388:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(330);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("1032bf67", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-2a80d580!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VueModal.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-2a80d580!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VueModal.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 391:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(333);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("36466c54", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-5f69b9c6!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectClientPendings.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-5f69b9c6!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectClientPendings.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 393:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(335);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("5df0bf1b", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-7d403847&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Show.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-7d403847&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Show.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 396:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(338);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("1c96e3fd", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-b120a046!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectTabs.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-b120a046!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectTabs.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(341);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("099a3e98", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-df83a136!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VueAutocomplete.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-df83a136!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./VueAutocomplete.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});