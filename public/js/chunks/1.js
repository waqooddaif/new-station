webpackJsonp([1],{

/***/ 203:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(397)
__webpack_require__(398)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(311),
  /* template */
  __webpack_require__(380),
  /* scopeId */
  "data-v-dc7c0594",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/IndexList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] IndexList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-dc7c0594", Component.options)
  } else {
    hotAPI.reload("data-v-dc7c0594", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 282:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ }),

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(284)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction) {
  isProduction = _isProduction

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[data-vue-ssr-id~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ }),

/***/ 284:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 285:
/***/ (function(module, exports, __webpack_require__) {

module.exports = { "default": __webpack_require__(286), __esModule: true };

/***/ }),

/***/ 286:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(47);
__webpack_require__(46);
module.exports = __webpack_require__(287);


/***/ }),

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

var anObject = __webpack_require__(6);
var get = __webpack_require__(196);
module.exports = __webpack_require__(4).getIterator = function (it) {
  var iterFn = get(it);
  if (typeof iterFn != 'function') throw TypeError(it + ' is not iterable!');
  return anObject(iterFn.call(it));
};


/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(294)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(291),
  /* template */
  __webpack_require__(293),
  /* scopeId */
  "data-v-c26c2258",
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/node_modules/vue-datepicker/vue-datepicker.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] vue-datepicker.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c26c2258", Component.options)
  } else {
    hotAPI.reload("data-v-c26c2258", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 291:
/***/ (function(module, exports, __webpack_require__) {

"use strict";




var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault2(_stringify);

var _getIterator2 = __webpack_require__(285);

var _getIterator3 = _interopRequireDefault2(_getIterator2);

function _interopRequireDefault2(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _moment = __webpack_require__(0);

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

exports.default = {
  props: {
    required: false,
    date: {
      type: Object,
      required: true
    },
    option: {
      type: Object,
      default: function _default() {
        return {
          type: 'day',
          SundayFirst: false,
          week: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
          month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
          format: 'YYYY-MM-DD',
          color: {
            checked: '#F50057',
            header: '#3f51b5',
            headerText: '#fff'
          },
          inputStyle: {
            'display': 'inline-block',
            'padding': '6px',
            'line-height': '22px',
            'font-size': '16px',
            'border': '2px solid #fff',
            'box-shadow': '0 1px 3px 0 rgba(0, 0, 0, 0.2)',
            'border-radius': '2px',
            'color': '#5F5F5F'
          },
          placeholder: 'when?',
          buttons: {
            ok: 'OK',
            cancel: 'Cancel'
          },
          overlayOpacity: 0.5,
          dismissible: true
        };
      }
    },
    limit: {
      type: Array,
      default: function _default() {
        return [];
      }
    }
  },
  data: function data() {
    function hours() {
      var list = [];
      var hour = 24;
      while (hour > 0) {
        hour--;
        list.push({
          checked: false,
          value: hour < 10 ? '0' + hour : hour
        });
      }
      return list;
    }
    function mins() {
      var list = [];
      var min = 60;
      while (min > 0) {
        min--;
        list.push({
          checked: false,
          value: min < 10 ? '0' + min : min
        });
      }
      return list;
    }
    return {
      hours: hours(),
      mins: mins(),
      showInfo: {
        hour: false,
        day: false,
        month: false,
        year: false,
        check: false
      },
      displayInfo: {
        month: ''
      },
      library: {
        week: this.option.week,
        month: this.option.month,
        year: []
      },
      checked: {
        oldtime: '',
        currentMoment: null,
        year: '',
        month: '',
        day: '',
        hour: '00',
        min: '00'
      },
      dayList: [],
      selectedDays: []
    };
  },

  methods: {
    pad: function pad(n) {
      n = Math.floor(n);
      return n < 10 ? '0' + n : n;
    },
    nextMonth: function nextMonth(type) {
      var next = null;
      type === 'next' ? next = (0, _moment2.default)(this.checked.currentMoment).add(1, 'M') : next = (0, _moment2.default)(this.checked.currentMoment).add(-1, 'M');
      this.showDay(next);
    },
    showDay: function showDay(time) {
      if (time === undefined || !Date.parse(time)) {
        this.checked.currentMoment = (0, _moment2.default)();
      } else {
        this.checked.currentMoment = (0, _moment2.default)(time, this.option.format);
      }
      this.showOne('day');
      this.checked.year = (0, _moment2.default)(this.checked.currentMoment).format('YYYY');
      this.checked.month = (0, _moment2.default)(this.checked.currentMoment).format('MM');
      this.checked.day = (0, _moment2.default)(this.checked.currentMoment).format('DD');
      this.displayInfo.month = this.library.month[(0, _moment2.default)(this.checked.currentMoment).month()];
      var days = [];
      var currentMoment = this.checked.currentMoment;
      var firstDay = (0, _moment2.default)(currentMoment).date(1).day();

      var previousMonth = (0, _moment2.default)(currentMoment);
      var nextMonth = (0, _moment2.default)(currentMoment);
      nextMonth.add(1, 'months');
      previousMonth.subtract(1, 'months');
      var monthDays = (0, _moment2.default)(currentMoment).daysInMonth();
      var oldtime = this.checked.oldtime;
      for (var i = 1; i <= monthDays; ++i) {
        days.push({
          value: i,
          inMonth: true,
          unavailable: false,
          checked: false,
          moment: (0, _moment2.default)(currentMoment).date(i)
        });
        if (i === Math.ceil((0, _moment2.default)(currentMoment).format('D')) && (0, _moment2.default)(oldtime, this.option.format).year() === (0, _moment2.default)(currentMoment).year() && (0, _moment2.default)(oldtime, this.option.format).month() === (0, _moment2.default)(currentMoment).month()) {
          days[i - 1].checked = true;
        }
        this.checkBySelectDays(i, days);
      }
      if (firstDay === 0) firstDay = 7;
      for (var _i = 0; _i < firstDay - (this.option.SundayFirst ? 0 : 1); _i++) {
        var passiveDay = {
          value: previousMonth.daysInMonth() - _i,
          inMonth: false,
          action: 'previous',
          unavailable: false,
          checked: false,
          moment: (0, _moment2.default)(currentMoment).date(1).subtract(_i + 1, 'days')
        };
        days.unshift(passiveDay);
      }
      if (this.limit.length > 0) {
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = (0, _getIterator3.default)(this.limit), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var li = _step.value;

            switch (li.type) {
              case 'fromto':
                days = this.limitFromTo(li, days);
                break;
              case 'weekday':
                days = this.limitWeekDay(li, days);
                break;
            }
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator.return) {
              _iterator.return();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      }
      var passiveDaysAtFinal = 42 - days.length;
      for (var _i2 = 1; _i2 <= passiveDaysAtFinal; _i2++) {
        var _passiveDay = {
          value: _i2,
          inMonth: false,
          action: 'next',
          unavailable: false,
          checked: false,
          moment: (0, _moment2.default)(currentMoment).add(1, 'months').date(_i2)
        };
        days.push(_passiveDay);
      }
      this.dayList = days;
    },
    checkBySelectDays: function checkBySelectDays(d, days) {
      var _this = this;

      this.selectedDays.forEach(function (day) {
        if (_this.checked.year === (0, _moment2.default)(day).format('YYYY') && _this.checked.month === (0, _moment2.default)(day).format('MM') && d === Math.ceil((0, _moment2.default)(day).format('D'))) {
          days[d - 1].checked = true;
        }
      });
    },
    limitWeekDay: function limitWeekDay(limit, days) {
      days.map(function (day) {
        if (limit.available.indexOf(Math.floor(day.moment.format('d'))) === -1) {
          day.unavailable = true;
        }
      });
      return days;
    },
    limitFromTo: function limitFromTo(limit, days) {
      var _this2 = this;

      if (limit.from || limit.to) {
        days.map(function (day) {
          if (_this2.getLimitCondition(limit, day)) {
            day.unavailable = true;
          }
        });
      }
      return days;
    },
    getLimitCondition: function getLimitCondition(limit, day) {
      var tmpMoment = (0, _moment2.default)(this.checked.year + '-' + this.pad(this.checked.month) + '-' + this.pad(day.value));
      if (limit.from && !limit.to) {
        return !tmpMoment.isAfter(limit.from);
      } else if (!limit.from && limit.to) {
        return !tmpMoment.isBefore(limit.to);
      } else {
        return !tmpMoment.isBetween(limit.from, limit.to);
      }
    },
    checkDay: function checkDay(obj) {
      if (obj.unavailable || obj.value === '') {
        return false;
      }
      if (!obj.inMonth) {
        this.nextMonth(obj.action);
      }
      if (this.option.type === 'day' || this.option.type === 'min') {
        this.dayList.forEach(function (x) {
          x.checked = false;
        });
        this.checked.day = this.pad(obj.value);
        obj.checked = true;
      } else {
        var day = this.pad(obj.value);
        var ctime = this.checked.year + '-' + this.checked.month + '-' + day;
        if (obj.checked === true) {
          obj.checked = false;
          this.selectedDays.$remove(ctime);
        } else {
          this.selectedDays.push(ctime);
          obj.checked = true;
        }
      }
      switch (this.option.type) {
        case 'day':
          this.picked();
          break;
        case 'min':
          this.showOne('hour');

          this.shiftActTime();
          break;
      }
    },
    showYear: function showYear() {
      var _this3 = this;

      var year = (0, _moment2.default)(this.checked.currentMoment).year();
      this.library.year = [];
      var yearTmp = [];
      for (var i = year - 100; i < year + 5; ++i) {
        yearTmp.push(i);
      }
      this.library.year = yearTmp;
      this.showOne('year');
      this.$nextTick(function () {
        var listDom = document.getElementById('yearList');
        listDom.scrollTop = listDom.scrollHeight - 100;
        _this3.addYear();
      });
    },
    showOne: function showOne(type) {
      switch (type) {
        case 'year':
          this.showInfo.hour = false;
          this.showInfo.day = false;
          this.showInfo.year = true;
          this.showInfo.month = false;
          break;
        case 'month':
          this.showInfo.hour = false;
          this.showInfo.day = false;
          this.showInfo.year = false;
          this.showInfo.month = true;
          break;
        case 'day':
          this.showInfo.hour = false;
          this.showInfo.day = true;
          this.showInfo.year = false;
          this.showInfo.month = false;
          break;
        case 'hour':
          this.showInfo.hour = true;
          this.showInfo.day = false;
          this.showInfo.year = false;
          this.showInfo.month = false;
          break;
        default:
          this.showInfo.day = true;
          this.showInfo.year = false;
          this.showInfo.month = false;
          this.showInfo.hour = false;
      }
    },
    showMonth: function showMonth() {
      this.showOne('month');
    },
    addYear: function addYear() {
      var _this4 = this;

      var listDom = document.getElementById('yearList');
      listDom.addEventListener('scroll', function (e) {
        if (listDom.scrollTop < listDom.scrollHeight - 100) {
          var len = _this4.library.year.length;
          var lastYear = _this4.library.year[len - 1];
          _this4.library.year.push(lastYear + 1);
        }
      }, false);
    },
    setYear: function setYear(year) {
      this.checked.currentMoment = (0, _moment2.default)(year + '-' + this.checked.month + '-' + this.checked.day);
      this.showDay(this.checked.currentMoment);
    },
    setMonth: function setMonth(month) {
      var mo = this.library.month.indexOf(month) + 1;
      if (mo < 10) {
        mo = '0' + '' + mo;
      }
      this.checked.currentMoment = (0, _moment2.default)(this.checked.year + '-' + mo + '-' + this.checked.day);
      this.showDay(this.checked.currentMoment);
    },
    showCheck: function showCheck() {
      if (this.date.time === '') {
        this.showDay();
      } else {
        if (this.option.type === 'day' || this.option.type === 'min') {
          this.checked.oldtime = this.date.time;
          this.showDay(this.date.time);
        } else {
          this.selectedDays = JSON.parse(this.date.time);
          if (this.selectedDays.length) {
            this.checked.oldtime = this.selectedDays[0];
            this.showDay(this.selectedDays[0]);
          } else {
            this.showDay();
          }
        }
      }
      this.showInfo.check = true;
    },
    setTime: function setTime(type, obj, list) {
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = (0, _getIterator3.default)(list), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var item = _step2.value;

          item.checked = false;
          if (item.value === obj.value) {
            item.checked = true;
            this.checked[type] = item.value;
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    },
    picked: function picked() {
      if (this.option.type === 'day' || this.option.type === 'min') {
        var ctime = this.checked.year + '-' + this.checked.month + '-' + this.checked.day + ' ' + this.checked.hour + ':' + this.checked.min;
        this.checked.currentMoment = (0, _moment2.default)(ctime, 'YYYY-MM-DD HH:mm');
        this.date.time = (0, _moment2.default)(this.checked.currentMoment).format(this.option.format);
      } else {
        this.date.time = (0, _stringify2.default)(this.selectedDays);
      }
      this.showInfo.check = false;
      this.$emit('change', this.date.time);
    },
    dismiss: function dismiss(evt) {
      if (evt.target.className === 'datepicker-overlay') {
        if (this.option.dismissible === undefined || this.option.dismissible) {
          this.showInfo.check = false;
          this.$emit('cancel');
        }
      }
    },
    shiftActTime: function shiftActTime() {
      this.$nextTick(function () {
        if (!document.querySelector('.hour-item.active')) {
          return false;
        }
        document.querySelector('.hour-box').scrollTop = (document.querySelector('.hour-item.active').offsetTop || 0) - 250;
        document.querySelector('.min-box').scrollTop = (document.querySelector('.min-item.active').offsetTop || 0) - 250;
      });
    }
  }
};

/***/ }),

/***/ 292:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.datepicker-overlay[data-v-c26c2258] {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  z-index: 998;\n  top: 0;\n  left: 0;\n  overflow: hidden;\n  /* Safari, Chrome and Opera > 12.1 */\n  /* Firefox < 16 */\n  /* Internet Explorer */\n  /* Opera < 12.1 */\n  animation: fadein 0.5s;\n}\n@keyframes fadein {\nfrom {\n    opacity: 0;\n}\nto {\n    opacity: 1;\n}\n}\n/* Firefox < 16 */\n/* Safari, Chrome and Opera > 12.1 */\n/* Internet Explorer */\n/* Opera < 12.1 */\n.cov-date-body[data-v-c26c2258] {\n  display: inline-block;\n  background: #3F51B5;\n  overflow: hidden;\n  position: relative;\n  font-size: 16px;\n  font-family: 'Roboto';\n  font-weight: 400;\n  position: fixed;\n  display: block;\n  width: 400px;\n  max-width: 100%;\n  z-index: 999;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.2);\n}\n.cov-picker-box[data-v-c26c2258] {\n  background: #fff;\n  width: 100%;\n  display: inline-block;\n  padding: 25px;\n  box-sizing: border-box !important;\n  -moz-box-sizing: border-box !important;\n  -webkit-box-sizing: border-box !important;\n  -ms-box-sizing: border-box !important;\n  width: 400px;\n  max-width: 100%;\n  height: 280px;\n  text-align: start!important;\n}\n.cov-picker-box td[data-v-c26c2258] {\n  height: 34px;\n  width: 34px;\n  padding: 0;\n  line-height: 34px;\n  color: #000;\n  background: #fff;\n  text-align: center;\n  cursor: pointer;\n}\n.cov-picker-box td[data-v-c26c2258]:hover {\n  background: #E6E6E6;\n}\ntable[data-v-c26c2258] {\n  border-collapse: collapse;\n  border-spacing: 0;\n  width: 100%;\n}\n.day[data-v-c26c2258] {\n  width: 14.2857143%;\n  display: inline-block;\n  text-align: center;\n  cursor: pointer;\n  height: 34px;\n  padding: 0;\n  line-height: 34px;\n  color: #000;\n  background: #fff;\n  vertical-align: middle;\n}\n.week ul[data-v-c26c2258] {\n  margin: 0 0 8px;\n  padding: 0;\n  list-style: none;\n}\n.week ul li[data-v-c26c2258] {\n  width: 14.2%;\n  display: inline-block;\n  text-align: center;\n  background: transparent;\n  color: #000;\n  font-weight: bold;\n}\n.passive-day[data-v-c26c2258] {\n  color: #bbb;\n}\n.checked[data-v-c26c2258] {\n  background: #F50057;\n  color: #FFF !important;\n  border-radius: 3px;\n}\n.unavailable[data-v-c26c2258] {\n  color: #ccc;\n  cursor: not-allowed;\n}\n.cov-date-monthly[data-v-c26c2258] {\n  height: 150px;\n}\n.cov-date-monthly > div[data-v-c26c2258] {\n  display: inline-block;\n  padding: 0;\n  margin: 0;\n  vertical-align: middle;\n  color: #fff;\n  height: 150px;\n  float: left;\n  text-align: center;\n  cursor: pointer;\n}\n.cov-date-previous[data-v-c26c2258],\n.cov-date-next[data-v-c26c2258] {\n  position: relative;\n  width: 20% !important;\n  text-indent: -300px;\n  overflow: hidden;\n  color: #fff;\n}\n.cov-date-caption[data-v-c26c2258] {\n  width: 60%;\n  padding: 50px 0!important;\n  box-sizing: border-box;\n  font-size: 24px;\n}\n.cov-date-caption span[data-v-c26c2258]:hover {\n  color: rgba(255, 255, 255, 0.7);\n}\n.cov-date-previous[data-v-c26c2258]:hover,\n.cov-date-next[data-v-c26c2258]:hover {\n  background: rgba(255, 255, 255, 0.1);\n}\n.day[data-v-c26c2258]:hover {\n  background: #EAEAEA;\n}\n.unavailable[data-v-c26c2258]:hover {\n  background: none;\n}\n.checked[data-v-c26c2258]:hover {\n  background: #FF4F8E;\n}\n.cov-date-next[data-v-c26c2258]::before,\n.cov-date-previous[data-v-c26c2258]::before {\n  width: 20px;\n  height: 2px;\n  text-align: center;\n  position: absolute;\n  background: #fff;\n  top: 50%;\n  margin-top: -7px;\n  margin-left: -7px;\n  left: 50%;\n  line-height: 0;\n  content: '';\n  transform: rotate(45deg);\n}\n.cov-date-next[data-v-c26c2258]::after,\n.cov-date-previous[data-v-c26c2258]::after {\n  width: 20px;\n  height: 2px;\n  text-align: center;\n  position: absolute;\n  background: #fff;\n  margin-top: 6px;\n  margin-left: -7px;\n  top: 50%;\n  left: 50%;\n  line-height: 0;\n  content: '';\n  transform: rotate(-45deg);\n}\n.cov-date-previous[data-v-c26c2258]::after {\n  transform: rotate(45deg);\n}\n.cov-date-previous[data-v-c26c2258]::before {\n  transform: rotate(-45deg);\n}\n.date-item[data-v-c26c2258] {\n  text-align: center;\n  font-size: 20px;\n  padding: 10px 0;\n  cursor: pointer;\n}\n.date-item[data-v-c26c2258]:hover {\n  background: #e0e0e0;\n}\n.date-list[data-v-c26c2258] {\n  overflow: auto;\n  vertical-align: top;\n  padding: 0;\n}\n.cov-vue-date[data-v-c26c2258] {\n  display: inline-block;\n  color: #5D5D5D;\n}\n.button-box[data-v-c26c2258] {\n  background: #fff;\n  vertical-align: top;\n  height: 50px;\n  line-height: 50px;\n  text-align: right;\n  padding-right: 20px;\n}\n.button-box span[data-v-c26c2258] {\n  cursor: pointer;\n  padding: 10px 20px;\n}\n.watch-box[data-v-c26c2258] {\n  height: 100%;\n  overflow: hidden;\n}\n.hour-box[data-v-c26c2258],\n.min-box[data-v-c26c2258] {\n  display: inline-block;\n  width: 50%;\n  text-align: center;\n  height: 100%;\n  overflow: auto;\n  float: left;\n}\n.hour-box ul[data-v-c26c2258],\n.min-box ul[data-v-c26c2258] {\n  list-style: none;\n  margin: 0;\n  padding: 0;\n}\n.hour-item[data-v-c26c2258],\n.min-item[data-v-c26c2258] {\n  padding: 10px;\n  font-size: 36px;\n  cursor: pointer;\n}\n.hour-item[data-v-c26c2258]:hover,\n.min-item[data-v-c26c2258]:hover {\n  background: #E3E3E3;\n}\n.hour-box .active[data-v-c26c2258],\n.min-box .active[data-v-c26c2258] {\n  background: #F50057;\n  color: #FFF !important;\n}\n[data-v-c26c2258]::-webkit-scrollbar {\n  width: 2px;\n}\n[data-v-c26c2258]::-webkit-scrollbar-track {\n  background: #E3E3E3;\n}\n[data-v-c26c2258]::-webkit-scrollbar-thumb {\n  background: #C1C1C1;\n  border-radius: 2px;\n}\n", ""]);

// exports


/***/ }),

/***/ 293:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "cov-vue-date"
  }, [_c('div', {
    staticClass: "datepickbox"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.date.time),
      expression: "date.time"
    }],
    staticClass: "cov-datepicker",
    style: (_vm.option.inputStyle ? _vm.option.inputStyle : {}),
    attrs: {
      "type": "text",
      "title": "input date",
      "readonly": "readonly",
      "placeholder": _vm.option.placeholder,
      "required": _vm.required
    },
    domProps: {
      "value": (_vm.date.time)
    },
    on: {
      "click": _vm.showCheck,
      "foucus": _vm.showCheck,
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.date, "time", $event.target.value)
      }
    }
  })]), _vm._v(" "), (_vm.showInfo.check) ? _c('div', {
    staticClass: "datepicker-overlay",
    style: ({
      'background': _vm.option.overlayOpacity ? 'rgba(0,0,0,' + _vm.option.overlayOpacity + ')' : 'rgba(0,0,0,0.5)'
    }),
    on: {
      "click": function($event) {
        _vm.dismiss($event)
      }
    }
  }, [_c('div', {
    staticClass: "cov-date-body",
    style: ({
      'background-color': _vm.option.color ? _vm.option.color.header : '#3f51b5'
    })
  }, [_c('div', {
    staticClass: "cov-date-monthly"
  }, [_c('div', {
    staticClass: "cov-date-previous",
    on: {
      "click": function($event) {
        _vm.nextMonth('pre')
      }
    }
  }, [_vm._v("«")]), _vm._v(" "), _c('div', {
    staticClass: "cov-date-caption",
    style: ({
      'color': _vm.option.color ? _vm.option.color.headerText : '#fff'
    })
  }, [_c('span', {
    on: {
      "click": _vm.showYear
    }
  }, [_c('small', [_vm._v(_vm._s(_vm.checked.year))])]), _vm._v(" "), _c('br'), _vm._v(" "), _c('span', {
    on: {
      "click": _vm.showMonth
    }
  }, [_vm._v(_vm._s(_vm.displayInfo.month))])]), _vm._v(" "), _c('div', {
    staticClass: "cov-date-next",
    on: {
      "click": function($event) {
        _vm.nextMonth('next')
      }
    }
  }, [_vm._v("»")])]), _vm._v(" "), (_vm.showInfo.day) ? _c('div', {
    staticClass: "cov-date-box"
  }, [_c('div', {
    staticClass: "cov-picker-box"
  }, [_c('div', {
    staticClass: "week"
  }, [_c('ul', _vm._l((_vm.library.week), function(weekie) {
    return _c('li', [_vm._v(_vm._s(weekie))])
  }))]), _vm._v(" "), _vm._l((_vm.dayList), function(day) {
    return _c('div', {
      staticClass: "day",
      class: {
        'checked': day.checked, 'unavailable': day.unavailable, 'passive-day': !(day.inMonth)
      },
      style: (day.checked ? (_vm.option.color && _vm.option.color.checkedDay ? {
        background: _vm.option.color.checkedDay
      } : {
        background: '#F50057'
      }) : {}),
      attrs: {
        "track-by": "$index"
      },
      on: {
        "click": function($event) {
          _vm.checkDay(day)
        }
      }
    }, [_vm._v(_vm._s(day.value))])
  })], 2)]) : _vm._e(), _vm._v(" "), (_vm.showInfo.year) ? _c('div', {
    staticClass: "cov-date-box list-box"
  }, [_c('div', {
    staticClass: "cov-picker-box date-list",
    attrs: {
      "id": "yearList"
    }
  }, _vm._l((_vm.library.year), function(yearItem) {
    return _c('div', {
      staticClass: "date-item",
      attrs: {
        "track-by": "$index"
      },
      on: {
        "click": function($event) {
          _vm.setYear(yearItem)
        }
      }
    }, [_vm._v(_vm._s(yearItem))])
  }))]) : _vm._e(), _vm._v(" "), (_vm.showInfo.month) ? _c('div', {
    staticClass: "cov-date-box list-box"
  }, [_c('div', {
    staticClass: "cov-picker-box date-list"
  }, _vm._l((_vm.library.month), function(monthItem) {
    return _c('div', {
      staticClass: "date-item",
      attrs: {
        "track-by": "$index"
      },
      on: {
        "click": function($event) {
          _vm.setMonth(monthItem)
        }
      }
    }, [_vm._v(_vm._s(monthItem))])
  }))]) : _vm._e(), _vm._v(" "), (_vm.showInfo.hour) ? _c('div', {
    staticClass: "cov-date-box list-box"
  }, [_c('div', {
    staticClass: "cov-picker-box date-list"
  }, [_c('div', {
    staticClass: "watch-box"
  }, [_c('div', {
    staticClass: "hour-box"
  }, [_c('div', {
    staticClass: "mui-pciker-rule mui-pciker-rule-ft"
  }), _vm._v(" "), _c('ul', _vm._l((_vm.hours), function(hitem) {
    return _c('li', {
      staticClass: "hour-item",
      class: {
        'active': hitem.checked
      },
      on: {
        "click": function($event) {
          _vm.setTime('hour', hitem, _vm.hours)
        }
      }
    }, [_vm._v(_vm._s(hitem.value))])
  }))]), _vm._v(" "), _c('div', {
    staticClass: "min-box"
  }, [_c('div', {
    staticClass: "mui-pciker-rule mui-pciker-rule-ft"
  }), _vm._v(" "), _vm._l((_vm.mins), function(mitem) {
    return _c('div', {
      staticClass: "min-item",
      class: {
        'active': mitem.checked
      },
      on: {
        "click": function($event) {
          _vm.setTime('min', mitem, _vm.mins)
        }
      }
    }, [_vm._v(_vm._s(mitem.value))])
  })], 2)])])]) : _vm._e(), _vm._v(" "), _c('div', {
    staticClass: "button-box"
  }, [_c('span', {
    on: {
      "click": function($event) {
        _vm.showInfo.check = false
      }
    }
  }, [_vm._v(_vm._s(_vm.option.buttons ? _vm.option.buttons.cancel : 'Cancel'))]), _vm._v(" "), _c('span', {
    on: {
      "click": _vm.picked
    }
  }, [_vm._v(_vm._s(_vm.option.buttons ? _vm.option.buttons.ok : 'Ok'))])])])]) : _vm._e()])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-c26c2258", module.exports)
  }
}

/***/ }),

/***/ 294:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(292);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("47b522b8", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../css-loader/index.js!../vue-loader/lib/style-rewriter.js?id=data-v-c26c2258&scoped=true!../vue-loader/lib/selector.js?type=styles&index=0!./vue-datepicker.vue", function() {
     var newContent = require("!!../css-loader/index.js!../vue-loader/lib/style-rewriter.js?id=data-v-c26c2258&scoped=true!../vue-loader/lib/selector.js?type=styles&index=0!./vue-datepicker.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 310:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _stringify = __webpack_require__(30);

var _stringify2 = _interopRequireDefault(_stringify);

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

var _vueDatepicker = __webpack_require__(288);

var _vueDatepicker2 = _interopRequireDefault(_vueDatepicker);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  name: 'CreateProject',
  data: function data() {
    return {
      newProjectPopOptions: {
        trigger: 'click',
        placement: 'bottom-left',
        closeable: true,
        animation: 'pop',
        dismissible: false
      },
      newProject: {
        name: null,
        color: null,
        pm_id: null,
        tech_id: null,
        company: null,
        company_id: null,
        client_id: null,
        date_start: null,
        date_final: null,
        start: {},
        final: {}
      },
      newProjectErrors: {},
      formInProgress: false,
      datepickerOptions: {
        type: 'day',
        week: ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'],
        month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        format: 'YYYY/MM/DD',
        inputStyle: {
          'box-shadow': 'none',
          'padding': '6px 12px',
          'padding-left': '20px',
          'padding-right': '50px',
          'font-size': '110%',
          'display': 'block',
          'width': '100%',
          'height': '40px',
          'line-height': '14px',
          'color': '#57626d',
          'background-color': '#EEF1F6',
          'background-image': 'none',
          'border': '1px solid #EEF1F6',
          'border-radius': '4px',
          '-webkit-transition': 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s',
          'transition': 'border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s'
        },
        color: {
          header: '#434E60',
          headerText: '#F6F7F9'
        },
        buttons: {
          ok: 'Ok',
          cancel: 'Cancel'
        },
        overlayOpacity: 0.5,
        dismissible: true }
    };
  },
  mounted: function mounted() {
    this.fetchCompaniesList();
  },

  computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
    companiesList: 'companiesList',
    managersList: 'managersList',
    leadsList: 'leadsList'
  })),
  watch: {
    'newProject.company': function newProjectCompany(val) {
      if (val && val.clients) {
        this.newProject.client_id = val.clients[0].id;
      }
    }
  },
  methods: (0, _extends3.default)({}, (0, _vuex.mapActions)(['fetchCompaniesList', 'createProject']), {
    createNewProject: function createNewProject() {
      var _this = this;

      var data = JSON.parse((0, _stringify2.default)(this.newProject));
      if (!this.newProject.start.time || !this.newProject.final.time) {
        var errors = JSON.parse((0, _stringify2.default)(this.newProjectErrors));
        if (!this.newProject.start.time) {
          errors.date_start = ['Project start date can\'t be empty.'];
        } else if (errors.date_start) {
          delete errors.date_start;
        }
        if (!this.newProject.final.time) {
          errors.date_final = ['Project estimated end can\'t be empty.'];
        } else if (errors.date_final) {
          delete errors.date_final;
        }
        this.newProjectErrors = errors;
        return;
      }
      data.date_start = this.newProject.start.time;
      data.date_final = this.newProject.final.time;
      data.company_id = this.newProject.company.id;
      delete data.company;
      delete data.start.time;
      delete data.final.time;
      this.formInProgress = true;
      this.createProject(data).then(function (response) {
        _this.newProject = {
          name: null,
          color: null,
          pm_id: null,
          tech_id: null,
          company: null,
          company_id: null,
          client_id: null,
          date_start: null,
          date_final: null,
          start: { time: '' },
          final: { time: '' }
        };
        _this.newProjectErrors = {};
        $('#create-new-project').webuiPopover('hide');
        _this.formInProgress = false;
      }).catch(function (error) {
        if (error.status === 422) {
          _this.newProjectErrors = error.body;
        } else {
          $('#create-new-project').webuiPopover('hide');
        }
        _this.formInProgress = false;
      });
    }
  }),
  components: {
    'datePicker': _vueDatepicker2.default
  }
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(3)))

/***/ }),

/***/ 311:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends2 = __webpack_require__(195);

var _extends3 = _interopRequireDefault(_extends2);

var _vuex = __webpack_require__(45);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProjectsList = __webpack_require__(357);
var CreateProject = __webpack_require__(348);

exports.default = {
    name: 'ProjectsIndex',
    data: function data() {
        return {
            projectsFilter: 'all',
            pausedFilter: false,
            activeFilter: true,
            dueFilter: false,
            progressFilter: false,
            pendingFilter: false,
            deliveredFilter: false,
            supportFilter: false
        };
    },
    mounted: function mounted() {
        initJqueryPlugins();
    },

    computed: (0, _extends3.default)({}, (0, _vuex.mapGetters)({
        activeProjects: 'activeProjects',
        archivedProjects: 'archivedProjects',
        userPermissions: 'userPermissions'
    }), {
        filteredProjects: function filteredProjects() {
            if (!this.activeProjects) return [];

            var projects = this.activeProjects;

            var filtered = [];

            if (this.dueFilter) {
                var due = projects.filter(function (project) {
                    return project.status.toLowerCase() === 'due';
                });
                filtered = filtered.concat(due);
            }

            if (this.progressFilter) {
                var progress = projects.filter(function (project) {
                    return project.status.toLowerCase() === 'in-progress';
                });
                filtered = filtered.concat(progress);
            }

            if (this.pendingFilter) {
                var pending = projects.filter(function (project) {
                    return project.status.toLowerCase() === 'pending';
                });
                filtered = filtered.concat(pending);
            }

            if (this.deliveredFilter) {
                var delivered = projects.filter(function (project) {
                    return project.status.toLowerCase() === 'delivered';
                });
                filtered = filtered.concat(delivered);
            }

            if (this.supportFilter) {
                var support = projects.filter(function (project) {
                    return project.status.toLowerCase() === 'support';
                });
                filtered = filtered.concat(support);
            }

            if (this.pausedFilter) {
                var _support = projects.filter(function (project) {
                    return project.status.toLowerCase() === 'paused';
                });
                filtered = filtered.concat(_support);
            }

            if (!this.dueFilter && !this.progressFilter && !this.pendingFilter && !this.deliveredFilter && !this.pausedFilter && !this.supportFilter) filtered = projects;

            return filtered;
        }
    }),
    methods: {},
    components: {
        ProjectsList: ProjectsList,
        CreateProject: CreateProject
    }
};

/***/ }),

/***/ 317:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = {
    name: 'ProjectListItem',
    props: ['project'],
    data: function data() {
        var locationOrigin = document.location.origin;
        return {
            locationOrigin: locationOrigin,
            defaultProjectLogo: locationOrigin + "/images/project_logo_placeholder.png"
        };
    },

    computed: {
        cssClasses: function cssClasses() {
            var classes = {};
            switch (this.project.status.toLowerCase()) {
                case 'archived':
                    classes['archived'] = true;
                    break;
                case 'due':
                    classes['due'] = true;
                    break;
                case 'pending':
                    classes['pending'] = true;
                    break;
                case 'delivered':
                    classes['delivered'] = true;
                    break;
                case 'in-progress':
                    classes['inprogress'] = true;
                    break;
                default:
                    classes['default'] = true;
            }
            return classes;
        },
        progressIndicator: function progressIndicator() {
            return [8, 10];
        },
        lastUpdated: function lastUpdated() {
            if (!this.project.updated_at) {
                return this.project.created_at.date;
            } else {
                return this.project.updated_at.date;
            }
        },
        remainingDays: function remainingDays() {
            var final = this.project.date_final.indexOf('-') >= 0 ? this.project.date_final.split('-') : this.project.date_final.split('/');
            var final_date = moment([parseInt(final[0]), parseInt(final[1]) - 1, parseInt(final[2])]);
            var remaining = moment().diff(final_date, 'months');
            var unit = 'M';
            if (parseInt(remaining) === 0) {
                remaining = moment().diff(final_date, 'weeks');
                unit = 'W';
                if (parseInt(remaining) === 0) {
                    remaining = moment().diff(final_date, 'days');
                    unit = 'D';
                }
            }
            return remaining + unit;
        },
        projectReplyDelay: function projectReplyDelay() {
            if (!this.project.not_replied) return null;

            var momentFilter = Vue.filter('moment');
            var lastMessageDate = momentFilter(this.project.not_replied.date, 'YYYY-MM-DD HH:mm:ss');
            var delay = moment().diff(lastMessageDate, 'days');

            if (delay < 0) {
                return null;
            }

            if (delay >= 3) {
                return "!!!";
            }

            if (delay > 1) {
                return "!!";
            }

            if (delay === 1) {
                return "!";
            }
        }
    },
    methods: {
        goToProject: function goToProject(id) {
            this.$router.push({ name: 'ProjectShow', params: { id: id } });
        }
    }
};

/***/ }),

/***/ 323:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});


var ProjectListItem = __webpack_require__(352);

exports.default = {
    name: 'ProjectsList',
    props: ['projects'],
    data: function data() {
        var locationOrigin = document.location.origin;
        return {
            locationOrigin: locationOrigin,
            defaultClientLogo: locationOrigin + "/images/project_logo_placeholder.png"
        };
    },

    components: {
        ProjectListItem: ProjectListItem
    }
};

/***/ }),

/***/ 331:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n#project-sections-tabs.row.content-box {\n    margin-top: 30px;\n}\n", ""]);

// exports


/***/ }),

/***/ 337:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.project-box {\n    border: 1px solid #999999;\n    border-radius: 7px;\n    background: #fff;\n    margin-bottom: 10px;\n    cursor: pointer;\n    height: 310px;\n}\n.project-box .row-container {\n    padding: 20px 20px 0 20px;\n}\n.project-box .footer {\n    height: 70px;\n    padding-top: 25px;\n}\n.project-box .footer i {\n    font-size: 28px;\n}\n.project-box .footer .footer-container {\n    padding: 0 5px;\n}\n.project-box img {\n    width: 70% !important;\n}\n.project-logo {\n    height: 90px;\n}\n.project-progress {\n    font-size: 16px;\n}\n.project-progress .milestone-name {\n    font-size: 15px;\n    font-weight: bold;\n    margin-top: 5px;\n}\n.project-progress i {\n    font-size: 20px;\n}\n.project-progress span.filled {\n    background: #000;\n}\n.project-progress span {\n    display: inline-block;\n    width: 8px;\n    height: 8px;\n    border: 1px solid #000;\n    border-radius: 10px;\n    margin: 3px;\n    margin-top: 20px;\n}\n.project-box .project-last-updated {\n    text-align: left;\n    font-size: 12px;\n    color: #524e4f;\n    margin-top: 0px;\n}\n\n/*\n\n.project-box.due {\n    border-top-color: #FDB25E;\n}\n\n.project-box.inprogress {\n    border-top-color: #2AB0DD;\n}\n\n.project-box.pending {\n    border-top-color: #ff0000;\n}\n\n.project-box.delivered {\n    border-top-color: #4cae4c;\n}\n\n*/\n.project-status-icon {\n    position: relative;\n    top: -15px;\n}\n.project-status-icon img{\n    width: 27px !important;\n}\n.project-remaining {\n    font-size: 200%;\n    text-align: left;\n    margin-top: 15px;\n}\n.project-remaining.due {\n    color: #FDB25E;\n}\n.project-remaining.in-progress {\n    color: #2AB0DD;\n}\n.project-remaining.pending {\n    color: #ff0000;\n}\n.project-remaining.delivered {\n    color: #4cae4c;\n}\n.project-box .project-name h1 {\n    margin-top: 10px;\n    margin-bottom: 30px;\n    font-size: 20px;\n}\n.project-box .company-name h3 {\n    margin-top: 10px;\n    color: #86878D;\n}\n.project-box .project-start {\n    color: #C1C0C4;\n    text-align: right;\n    font-size: large;\n}\n.project-box .project-start p {\n    font-size: 65%;\n    margin: 0;\n}\n.project-box .project-status {\n    font-size: 200%;\n    color: #FDB25E;\n    text-align: center;\n}\n.project-logo {\n    clear: both;\n}\n", ""]);

// exports


/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.projct-item {\n    margin-top: 20px !important;\n}\nbutton#create-new-project {\n    float: right;\n}\n#grid-section-title.row {\n    padding-left: 15px;\n}\n#grid-section-title .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {\n    background-color: transparent;\n    font-weight: bolder;\n}\n#grid-section-title .nav > li > a {\n    color: #000;\n    border-bottom: 3px solid black;\n    border-radius: 0;\n    margin: 0 10px 0 0;\n    padding: 4px 0;\n    font-size: small;\n}\n#grid-section-title .nav > li > a:hover {\n    background-color: #ebebeb;\n    font-weight: normal;\n}\n#grid-section-title .nav > li > a.due {\n    border-bottom-color: #FDB25E;\n}\n#grid-section-title .nav > li > a.inprogress {\n    border-bottom-color: #2AB0DD;\n}\n#grid-section-title .nav > li > a.pending {\n    border-bottom-color: #ff0000;\n}\n#grid-section-title .nav > li > a.delivered {\n    border-bottom-color: #4cae4c;\n}\n.projects-status-container li.active > a:hover, .projects-status-container li.active > a:focus {\n    color: #000;\n}\n.nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {\n    background-color: #CECECE !important;\n}\n", ""]);

// exports


/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n#app >>> #major-content-container[data-v-dc7c0594] {\n    padding-left: 0px;\n    margin-top: 0px !important;\n}\n", ""]);

// exports


/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(282)();
// imports


// module
exports.push([module.i, "\n.webui-popover.right > .webui-arrow {\n  top: 23% !important;\n}\n.glyphicon.fast-right-spinner {\n  animation: glyphicon-spin-r 1s infinite linear;\n}\n@keyframes glyphicon-spin-r {\n0% {\n    transform: rotate(0deg);\n}\n100% {\n    transform: rotate(359deg);\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 348:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(401)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(310),
  /* template */
  __webpack_require__(383),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/CreateProject.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] CreateProject.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-efa73cba", Component.options)
  } else {
    hotAPI.reload("data-v-efa73cba", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(395)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(317),
  /* template */
  __webpack_require__(377),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectListItem.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectListItem.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-a5d5ffa0", Component.options)
  } else {
    hotAPI.reload("data-v-a5d5ffa0", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 357:
/***/ (function(module, exports, __webpack_require__) {


/* styles */
__webpack_require__(389)

var Component = __webpack_require__(194)(
  /* script */
  __webpack_require__(323),
  /* template */
  __webpack_require__(365),
  /* scopeId */
  null,
  /* cssModules */
  null
)
Component.options.__file = "/var/www/newstation.com/resources/assets/js/components/projects/ProjectsList.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key !== "__esModule"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] ProjectsList.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-loader/node_modules/vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-2b5c6602", Component.options)
  } else {
    hotAPI.reload("data-v-2b5c6602", Component.options)
  }
})()}

module.exports = Component.exports


/***/ }),

/***/ 365:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('span', _vm._l((_vm.projects), function(project) {
    return _c('div', {
      staticClass: "project-item col-md-3 col-sm-2"
    }, [(project && project.id) ? _c('project-list-item', {
      attrs: {
        "project": project
      }
    }) : _vm._e()], 1)
  }))
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-2b5c6602", module.exports)
  }
}

/***/ }),

/***/ 377:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "project-box",
    class: _vm.cssClasses
  }, [_c('div', {
    staticClass: "row row-container",
    on: {
      "click": function($event) {
        _vm.goToProject(_vm.project.id)
      }
    }
  }, [_c('div', {
    staticClass: "project-logo"
  }, [(_vm.project.image && _vm.project.image != null && _vm.project.image.length > 0) ? _c('img', {
    attrs: {
      "src": _vm.locationOrigin + '/' + _vm.project.image
    }
  }) : _vm._e(), _vm._v(" "), (!_vm.project.image || _vm.project.image == null || _vm.project.image.length == 0) ? _c('img', {
    attrs: {
      "src": _vm.defaultProjectLogo
    }
  }) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-12 project-name "
  }, [_c('h1', [_vm._v(_vm._s(_vm.project.name))])]), _vm._v(" "), _vm._m(0)]), _vm._v(" "), _c('div', {
    staticClass: "footer"
  }, [_c('div', {
    staticClass: "col-sm-12 project-remaining text-left",
    class: _vm.project.status
  }, [_c('span', [_vm._v("\n                " + _vm._s(_vm.remainingDays) + "\n              ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-8 project-last-updated footer-container"
  }, [_c('span', [_vm._v("Last updated  " + _vm._s(_vm._f("date")(_vm.lastUpdated)) + " ")])]), _vm._v(" "), _c('div', {
    staticClass: "col-sm-4 project-status-icon footer-container text-right"
  }, [(_vm.project.status == 'archived') ? _c('span', [_c('img', {
    attrs: {
      "src": "images/icons/svg/archive-solid.svg"
    }
  })]) : (_vm.project.status == 'delivered') ? _c('span', [_c('img', {
    attrs: {
      "src": "images/icons/svg/check-circle-regular-gray.svg"
    }
  })]) : (_vm.project.status == 'pending') ? _c('span', [_c('img', {
    attrs: {
      "src": "images/icons/svg/pencil-ruler-solid-gray.svg"
    }
  })]) : (_vm.project.status == 'paused') ? _c('span', [_c('img', {
    attrs: {
      "src": "images/icons/svg/pause-circle-solid-gray.svg"
    }
  })]) : (_vm.project.status == 'due') ? _c('span', [_c('img', {
    attrs: {
      "src": "images/icons/svg/exclamation-triangle-solid-gray.svg"
    }
  })]) : (_vm.project.status == 'inprogress' || _vm.project.status == 'in-progress') ? _c('span', [_c('img', {
    attrs: {
      "src": "images/icons/svg/sync-alt-solid-gray.svg"
    }
  })]) : (_vm.project.status == 'support') ? _c('span', [_c('img', {
    attrs: {
      "src": "images/icons/svg/bug-solid-gray.svg"
    }
  })]) : _vm._e()])])])
},staticRenderFns: [function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "col-sm-12 project-progress"
  }, [_c('i', {
    staticClass: "fa fa-caret-down"
  }), _vm._v(" Milestone Phase :\n            "), _c('div', {
    staticClass: "milestone-name text-center"
  }, [_vm._v("\n                First milestone\n            ")])])
}]}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-a5d5ffa0", module.exports)
  }
}

/***/ }),

/***/ 380:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "ignore-major-container",
    attrs: {
      "id": "projects-index"
    }
  }, [_c('div', {
    staticClass: "row projects-status-container"
  }, [_c('div', {
    staticClass: "col-sm-8 col-sm-offset-2"
  }, [_c('div', {
    staticClass: "text-center"
  }, [_c('ul', {
    staticClass: "nav nav-pills nav-justified"
  }, [_c('li', {
    class: {
      active: _vm.activeFilter
    }
  }, [_c('a', {
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.activeFilter = !_vm.activeFilter
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "images/icons/svg/creative-commons-sampling-brands.svg"
    }
  }), _vm._v(" "), _c('span', [_vm._v("Active")])])]), _vm._v(" "), _c('li', {
    class: {
      active: _vm.pendingFilter
    }
  }, [_c('a', {
    staticClass: "pending",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.pendingFilter = !_vm.pendingFilter
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "images/icons/svg/pencil-ruler-solid.svg"
    }
  }), _vm._v(" "), _c('span', [_vm._v("Planning")])])]), _vm._v(" "), _c('li', {
    class: {
      active: _vm.pausedFilter
    }
  }, [_c('a', {
    staticClass: "paused",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.pausedFilter = !_vm.pausedFilter
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "images/icons/svg/pause-circle-solid.svg"
    }
  }), _vm._v(" "), _c('span', [_vm._v("Paused")])])]), _vm._v(" "), _c('li', {
    class: {
      active: _vm.dueFilter
    }
  }, [_c('a', {
    staticClass: "due",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.dueFilter = !_vm.dueFilter
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "images/icons/svg/exclamation-triangle-solid.svg"
    }
  }), _vm._v(" "), _c('span', [_vm._v("Alert")])])]), _vm._v(" "), _c('li', {
    class: {
      active: _vm.progressFilter
    }
  }, [_c('a', {
    staticClass: "inprogress",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.progressFilter = !_vm.progressFilter
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "images/icons/svg/sync-alt-solid.svg"
    }
  }), _vm._v(" "), _c('span', [_vm._v("In Progress")])])]), _vm._v(" "), _c('li', {
    class: {
      active: _vm.deliveredFilter
    }
  }, [_c('a', {
    staticClass: "delivered",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.deliveredFilter = !_vm.deliveredFilter
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "images/icons/svg/check-circle-regular.svg"
    }
  }), _vm._v(" "), _c('span', [_vm._v("Delivered")])])]), _vm._v(" "), _c('li', {
    class: {
      active: _vm.supportFilter
    }
  }, [_c('a', {
    staticClass: "support",
    attrs: {
      "href": "#"
    },
    on: {
      "click": function($event) {
        $event.preventDefault();
        _vm.supportFilter = !_vm.supportFilter
      }
    }
  }, [_c('img', {
    attrs: {
      "src": "images/icons/svg/bug-solid.svg"
    }
  }), _vm._v(" "), _c('span', [_vm._v("Support")])])])])])])]), _vm._v(" "), _c('div', {
    staticClass: "col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1"
  }, [_c('div', {
    staticClass: "col-md-12"
  }, [_c('div', {
    staticClass: "row content-box projects-grid-list"
  }, [_c('projects-list', {
    attrs: {
      "projects": _vm.filteredProjects
    }
  }), _vm._v(" "), (!_vm.activeFilter) ? _c('projects-list', {
    attrs: {
      "projects": _vm.archivedProjects
    }
  }) : _vm._e()], 1)])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-dc7c0594", module.exports)
  }
}

/***/ }),

/***/ 383:
/***/ (function(module, exports, __webpack_require__) {

module.exports={render:function (){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', [_c('button', {
    directives: [{
      name: "popover",
      rawName: "v-popover",
      value: (_vm.newProjectPopOptions),
      expression: "newProjectPopOptions"
    }],
    staticClass: "btn btn-success",
    attrs: {
      "id": "create-new-project"
    }
  }, [_c('i', {
    staticClass: "glyphicon glyphicon-plus"
  })]), _vm._v(" "), _c('div', {
    staticClass: "webui-popover-content",
    attrs: {
      "id": "popover-create-new-project"
    }
  }, [_c('div', {
    staticClass: "content-padding"
  }, [_c('h1', [_vm._v("Create new project")]), _vm._v(" "), _c('form', {
    staticClass: "form-horizontal",
    attrs: {
      "id": "create-new-project-form"
    },
    on: {
      "submit": function($event) {
        $event.preventDefault();
        return _vm.createNewProject($event)
      }
    }
  }, [_c('div', {
    class: {
      'form-group': true, 'has-error': _vm.newProjectErrors.name
    }
  }, [_c('label', {
    attrs: {
      "for": "new-project-name"
    }
  }, [_vm._v("Project name *")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newProject.name),
      expression: "newProject.name"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-project-name",
      "disabled": _vm.formInProgress,
      "required": ""
    },
    domProps: {
      "value": (_vm.newProject.name)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.newProject, "name", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.newProjectErrors.name) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.name[0]) + "\n          ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    class: {
      'form-group': true, 'has-error': _vm.newProjectErrors.color
    }
  }, [_c('label', {
    attrs: {
      "for": "new-project-color"
    }
  }, [_vm._v("Project color")]), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newProject.color),
      expression: "newProject.color"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-project-color",
      "disabled": _vm.formInProgress
    },
    domProps: {
      "value": (_vm.newProject.color)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.$set(_vm.newProject, "color", $event.target.value)
      }
    }
  }), _vm._v(" "), (_vm.newProjectErrors.color) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.color[0]) + "\n          ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    class: {
      'form-group': true, 'has-error': _vm.newProjectErrors.pm_id
    }
  }, [_c('label', {
    attrs: {
      "for": "new-project-pm"
    }
  }, [_vm._v("Project manager *")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newProject.pm_id),
      expression: "newProject.pm_id"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-project-pm",
      "disabled": _vm.formInProgress,
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.newProject, "pm_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.managersList), function(manager) {
    return _c('option', {
      domProps: {
        "value": manager.id
      }
    }, [_vm._v("\n              " + _vm._s(manager.name) + "\n            ")])
  })), _vm._v(" "), (_vm.newProjectErrors.pm_id) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.pm_id[0]) + "\n          ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    class: {
      'form-group': true, 'has-error': _vm.newProjectErrors.tech_id
    }
  }, [_c('label', {
    attrs: {
      "for": "new-project-tech"
    }
  }, [_vm._v("Technical lead *")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newProject.tech_id),
      expression: "newProject.tech_id"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-project-tech",
      "disabled": _vm.formInProgress,
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.newProject, "tech_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.leadsList), function(tech) {
    return _c('option', {
      domProps: {
        "value": tech.id
      }
    }, [_vm._v("\n              " + _vm._s(tech.name) + "\n            ")])
  })), _vm._v(" "), (_vm.newProjectErrors.tech_id) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.tech_id[0]) + "\n          ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    class: {
      'form-group': true, 'has-error': _vm.newProjectErrors.company_id
    }
  }, [_c('label', {
    attrs: {
      "for": "new-project-company"
    }
  }, [_vm._v("Company *")]), _vm._v(" "), _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newProject.company),
      expression: "newProject.company"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-project-company",
      "disabled": _vm.formInProgress,
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.newProject, "company", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.companiesList), function(company) {
    return _c('option', {
      domProps: {
        "value": company
      }
    }, [_vm._v("\n              " + _vm._s(company.name) + "\n            ")])
  })), _vm._v(" "), (_vm.newProjectErrors.company_id) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.company_id[0]) + "\n          ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    class: {
      'form-group': true, 'has-error': _vm.newProjectErrors.client_id
    }
  }, [_c('label', {
    attrs: {
      "for": "new-project-client"
    }
  }, [_vm._v("Client *")]), _vm._v(" "), (_vm.newProject.company) ? _c('select', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.newProject.client_id),
      expression: "newProject.client_id"
    }],
    staticClass: "form-control",
    attrs: {
      "id": "new-project-client",
      "disabled": _vm.formInProgress || !_vm.newProject.company,
      "required": ""
    },
    on: {
      "change": function($event) {
        var $$selectedVal = Array.prototype.filter.call($event.target.options, function(o) {
          return o.selected
        }).map(function(o) {
          var val = "_value" in o ? o._value : o.value;
          return val
        });
        _vm.$set(_vm.newProject, "client_id", $event.target.multiple ? $$selectedVal : $$selectedVal[0])
      }
    }
  }, _vm._l((_vm.newProject.company.clients), function(client) {
    return _c('option', {
      domProps: {
        "value": client.id
      }
    }, [_vm._v("\n              " + _vm._s(client.name) + "\n            ")])
  })) : _c('select', {
    staticClass: "form-control",
    attrs: {
      "disabled": "disabled",
      "required": ""
    }
  }, [_c('option', [_vm._v("Choose a company")])]), _vm._v(" "), (_vm.newProjectErrors.client_id) ? _c('span', {
    staticClass: "help-block"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.client_id[0]) + "\n          ")]) : _vm._e()]), _vm._v(" "), _c('div', {
    staticClass: "form-group has-feedback",
    class: {
      'has-error': _vm.newProjectErrors.date_start
    }
  }, [_c('label', [_vm._v("Start date *")]), _vm._v(" "), _c('date-picker', {
    attrs: {
      "date": _vm.newProject.start,
      "option": _vm.datepickerOptions,
      "required": true
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "icon icon-calendar-dark form-control-feedback"
  }), _vm._v(" "), (_vm.newProjectErrors.date_start) ? _c('p', {
    staticClass: "help-block text-danger"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.date_start[0]) + "\n          ")]) : _vm._e()], 1), _vm._v(" "), _c('div', {
    staticClass: "form-group has-feedback",
    class: {
      'has-error': _vm.newProjectErrors.date_final
    }
  }, [_c('label', [_vm._v("Estimated end *")]), _vm._v(" "), _c('date-picker', {
    attrs: {
      "date": _vm.newProject.final,
      "option": _vm.datepickerOptions,
      "required": true
    }
  }), _vm._v(" "), _c('span', {
    staticClass: "icon icon-calendar-dark form-control-feedback"
  }), _vm._v(" "), (_vm.newProjectErrors.date_final) ? _c('p', {
    staticClass: "help-block text-danger"
  }, [_vm._v("\n            " + _vm._s(_vm.newProjectErrors.date_final[0]) + "\n          ")]) : _vm._e()], 1), _vm._v(" "), _c('br'), _vm._v(" "), _c('button', {
    staticClass: "btn btn-block btn-success btn-lg submit",
    attrs: {
      "type": "submit",
      "disabled": _vm.formInProgress
    }
  }, [_c('i', {
    directives: [{
      name: "show",
      rawName: "v-show",
      value: (_vm.formInProgress),
      expression: "formInProgress"
    }],
    staticClass: "glyphicon glyphicon-repeat fast-right-spinner"
  }), _vm._v("\n          Create\n        ")]), _vm._v(" "), _c('p', {
    staticClass: "help-block"
  }, [_vm._v("\n          all fields marked (*) are required.\n        ")])])])])])
},staticRenderFns: []}
module.exports.render._withStripped = true
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-loader/node_modules/vue-hot-reload-api").rerender("data-v-efa73cba", module.exports)
  }
}

/***/ }),

/***/ 389:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(331);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("19c0c674", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-2b5c6602!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectsList.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-2b5c6602!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectsList.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 395:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(337);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("15628f40", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-a5d5ffa0!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectListItem.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-a5d5ffa0!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./ProjectListItem.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(339);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("eb48249e", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-dc7c0594!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./IndexList.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-dc7c0594!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./IndexList.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(340);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("516affb6", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-dc7c0594&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./IndexList.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-dc7c0594&scoped=true!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./IndexList.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 401:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(343);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(283)("7ec1f5df", content, false);
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-efa73cba!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CreateProject.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-rewriter.js?id=data-v-efa73cba!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./CreateProject.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ })

});