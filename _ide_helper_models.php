<?php
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Policy
 *
 * @property int $id
 * @property string $name
 * @property string $method
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @method static \Illuminate\Database\Query\Builder|\App\Policy whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Policy whereMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Policy whereName($value)
 */
	class Policy extends \Eloquent {}
}

namespace App{
/**
 * App\Permission
 *
 * @property int $id
 * @property int $ability_id
 * @property int $role_id
 * @property-read \App\Ability $ability
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Policy[] $policies
 * @property-read \App\Role $role
 * @method static \Illuminate\Database\Query\Builder|\App\Permission forAbility($abilities)
 * @method static \Illuminate\Database\Query\Builder|\App\Permission whereAbilityId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Permission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Permission whereRoleId($value)
 */
	class Permission extends \Eloquent {}
}

namespace App{
/**
 * App\Message
 *
 * @property int $id
 * @property string $content
 * @property bool $visible_team
 * @property bool $visible_client
 * @property string $type
 * @property string $date_due
 * @property string $date_done
 * @property bool $approval_state
 * @property string $approval_note
 * @property int $user_id
 * @property int $project_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MessageFile[] $files
 * @property-read bool $is_action
 * @property-read bool $is_approval
 * @property-read bool $is_done
 * @property-read bool $is_due
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OutboundEmail[] $outboundEmails
 * @property-read \App\User $owner
 * @property-read \App\Project $project
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProjectService[] $projectServices
 * @method static \Illuminate\Database\Query\Builder|\App\Message action()
 * @method static \Illuminate\Database\Query\Builder|\App\Message approval()
 * @method static \Illuminate\Database\Query\Builder|\App\Message byOwnerId($user_id)
 * @method static \Illuminate\Database\Query\Builder|\App\Message byProjectId($project_id)
 * @method static \Illuminate\Database\Query\Builder|\App\Message general()
 * @method static \Illuminate\Database\Query\Builder|\App\Message isDone()
 * @method static \Illuminate\Database\Query\Builder|\App\Message isDue()
 * @method static \Illuminate\Database\Query\Builder|\App\Message notGeneral()
 * @method static \Illuminate\Database\Query\Builder|\App\Message payment()
 * @method static \Illuminate\Database\Query\Builder|\App\Message requirement()
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereApprovalNote($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereApprovalState($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereDateDone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereDateDue($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereVisibleClient($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Message whereVisibleTeam($value)
 */
	class Message extends \Eloquent {}
}

namespace App\ACL{
/**
 * App\ACL\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $role_id
 * @property string $type
 * @property string $description
 * @property string $color
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ACL\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Company
 *
 * @property int $id
 * @property string $name
 * @property string $website
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $clients
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Project[] $projects
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Company whereWebsite($value)
 */
	class Company extends \Eloquent {}
}

namespace App{
/**
 * App\ProjectService
 *
 * @property int $id
 * @property int $service_id
 * @property int $project_id
 * @property string $date_start
 * @property string $date_finish
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $members
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \App\Project $project
 * @property-read \App\Service $service
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereDateFinish($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereDateStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereProjectId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereServiceId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ProjectService whereUpdatedAt($value)
 */
	class ProjectService extends \Eloquent {}
}

namespace App{
/**
 * App\MessageFile
 *
 * @property int $id
 * @property int $message_id
 * @property string $name
 * @property string $path
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Message $message
 * @method static \Illuminate\Database\Query\Builder|\App\MessageFile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageFile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageFile whereMessageId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageFile whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageFile wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\MessageFile whereUpdatedAt($value)
 */
	class MessageFile extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $role_id
 * @property string $type
 * @property string $description
 * @property string $color
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Project[] $activeProjects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Project[] $clientProjects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $companies
 * @property-read string $firstname
 * @property-read string $initials
 * @property-read string $lastname
 * @property-read string $role_description
 * @property-read \App\OauthTocken $jiraTokens
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Project[] $leadedProjects
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Project[] $managedProjects
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OutboundEmail[] $outboundEmails
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProjectService[] $projectServices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Project[] $projects
 * @property-read \App\Role $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OauthTocken[] $tokens
 * @method static \Illuminate\Database\Query\Builder|\App\User isClient()
 * @method static \Illuminate\Database\Query\Builder|\App\User isCompanyClient()
 * @method static \Illuminate\Database\Query\Builder|\App\User isMember()
 * @method static \Illuminate\Database\Query\Builder|\App\User whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRoleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App{
/**
 * App\Project
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $date_start
 * @property string $date_final
 * @property string $date_archived
 * @property int $pm_id
 * @property int $tech_id
 * @property int $client_id
 * @property int $company_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $color
 * @property-read \App\User $client
 * @property-read \App\Company $company
 * @property-read string $not_replied
 * @property-read string $status
 * @property-read \App\User $manager
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $members
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\OutboundEmail[] $outboundEmails
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\ProjectService[] $projectServices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Service[] $services
 * @property-read \App\User $techLead
 * @method static \Illuminate\Database\Query\Builder|\App\Project allClientProjects($user)
 * @method static \Illuminate\Database\Query\Builder|\App\Project allMemberProjects($user)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereCompanyId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDateArchived($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDateFinal($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereDateStart($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project wherePmId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereTechId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Project withClient()
 * @method static \Illuminate\Database\Query\Builder|\App\Project withCompany()
 * @method static \Illuminate\Database\Query\Builder|\App\Project withLead()
 * @method static \Illuminate\Database\Query\Builder|\App\Project withManager()
 */
	class Project extends \Eloquent {}
}

namespace App{
/**
 * App\Role
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\App\Role findByName($name)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Role whereName($value)
 */
	class Role extends \Eloquent {}
}

namespace App{
/**
 * App\OauthTocken
 *
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Query\Builder|\App\OauthTocken jira()
 * @method static \Illuminate\Database\Query\Builder|\App\OauthTocken valid()
 * @method static \Illuminate\Database\Query\Builder|\App\OauthTocken validJira()
 */
	class OauthTocken extends \Eloquent {}
}

namespace App{
/**
 * App\OutboundEmail
 *
 * @property int $id
 * @property string $token
 * @property int $notifiable_id
 * @property string $notifiable_type
 * @property int $emailable_id
 * @property string $emailable_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $emailable
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $notifiable
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereEmailableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereEmailableType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereNotifiableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereNotifiableType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\OutboundEmail whereUpdatedAt($value)
 */
	class OutboundEmail extends \Eloquent {}
}

namespace App{
/**
 * App\Ability
 *
 * @property int $id
 * @property string $name
 * @property string $label
 * @property string $tag
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Permission[] $permissions
 * @method static \Illuminate\Database\Query\Builder|\App\Ability findByName($name)
 * @method static \Illuminate\Database\Query\Builder|\App\Ability whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Ability whereLabel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Ability whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Ability whereTag($value)
 */
	class Ability extends \Eloquent {}
}

