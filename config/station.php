<?php
/**
 * Application specific settings
 */
return [
    /**
     * Password strength
     */
    'password' => '/^(?=.*[a-zA-Z])(?=.*\d).+$/',
//    'password' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\[\]@\-\&\#\(\)\*\^\%\!\~\"]).+$/',

    /**
     * Define the action types for action messages
     */
    'actions' => ['approval', 'requirement', 'payment'],

    /**
     * Define the statuses types
     */
    'statuses' => [
        'in-progress' => 'In Progress',
        'pending' => 'Pending',
        'delivered' => 'Delivered'
    ],

    /**
     * For default assigning and selections
     */
    'roles' => [
        /**
         * Admin role id
         */
        'admin' => 1,

        /**
         * Project manager role id
         */
        'pm' => 2,

        /**
         * Technical lead role id
         */
        'lead' => 3,

        /**
         * Member role id
         */
        'member' => 4,

        /**
         * client role id
         */
        'client' => 5,
    ],

    /**
     * For default assigning and selections
     */
    'usersTypes' => [
        /**
         * Admin type
         */
        'admin' => 'member',

        /**
         * Project manager type
         */
        'pm' => 'member',

        /**
         * Technical lead type
         */
        'lead' => 'member',

        /**
         * Member type
         */
        'member' => 'member',

        /**
         * client type
         */
        'client' => 'client',
    ],



    /**
     * Define the API addresses list
     */
    'api' => [
        'company.index' => "/call/company",
        'company.create' => "/call/company",
        'client.create' => "/call/client",
        'member.create' => "/call/member",
        'members.schedule' => "/call/members/schedule/{year?}/{week?}",
        'projects.create' => "/call/projects",
        'projects.index' => "/call/projects",
        'projects.show' => "/call/projects/{id}",
        'projects.update' => "/call/projects/{id}",
        'projects.expenses' => "/call/projects/{id}/expenses",
        'projects.expenses.item' => "/call/expenses/{id}",
        'projects.member.add' => "/call/projects/{id}/members",
        'projects.message.add' => "/call/projects/{id}/messages",
        'projects.action.add' => "/call/projects/{id}/actions",
        'projects.service.add' => "/call/projects/{id}/services",
        'projects.service.update' => "/call/projects/{id}/services",
        'projects.service.remove' => "/call/projects/{id}/services/{serviceid}",
        'projects.member.remove' => "/call/projects/{id}/members/{memberid}/{projectserviceid?}",
        'message.visibility' => "/call/messages/{id}/visibility",
        'message.approve' => "/call/messages/{id}/approve",
        'message.done' => "/call/messages/{id}/done",
        'message.update' => "/call/messages/{id}",
        'file.upload' => "/call/file",
        'authorize' => "/call/authorize",
        'jira.auth' => "/jira/auth",
        'jira.tokens' => "/jira/tokens",
        'jira.projects' => "/jira/projects",
        'jira.issue' => "/jira/issue",
        'session.refresh' => "/session-refresh",
    ],

    /**
     * Models list for authorization API calls
     */
    'models' => ['message', 'project'],

    /**
     * Attachments rules
     */
    'attachments' => [
        'extensions' => ['png', 'jpeg', 'jpg', 'pdf', 'zip', 'apk', 'ipa'],
        'manager_max' => 60*1024, // KB
        'max' => 10*1024, // KB
        'path' => env('ATTACHMENT_PATH', 'public/uploads'),
        'url' => env('ATTACHMENT_URL', 'uploads'),
    ],

    /**
     * Notifications
     */
    'notification' => [
        'enable' => env('MESSAGE_NOTIFICATIONS', true),
    ],

    /**
     * IMAP
     */
    'imap' => [
        'auto_enable' => env('IMAP_AUTO_FETCH_ENABLE', false),
        'server' => env('IMAP_SERVER'),
        'folder' => env('IMAP_FOLDER', 'INBOX'),
        'user' => env('IMAP_USER'),
        'password' => env('IMAP_PASSWORD'),
        'schedule' => env('IMAP_Schedule', "*/10 * * * *"),
    ],

    /**
     * Expenses
     */
    'expenses' => [
        'currency' => 'SAR'
    ],

    /**
     * The email address to send exceptions errors notifications to
     */
    'reporter' => 'akordy@waqood.sa'
];