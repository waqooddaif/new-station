<?php

use Illuminate\Database\Seeder;

class PolicySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Policy 1
        \AuzoPolicy::create([
            'name'   => 'Project PM',
            'method' => 'App\ACL\ProjectPolicies@pm',
        ]);
        // Policy 2
        \AuzoPolicy::create([
            'name'   => 'Project techLead',
            'method' => 'App\ACL\ProjectPolicies@techLead',
        ]);
        // Policy 3
        \AuzoPolicy::create([
            'name'   => 'Project member',
            'method' => 'App\ACL\ProjectPolicies@member',
        ]);
        // Policy 4
        \AuzoPolicy::create([
            'name'   => 'Project client',
            'method' => 'App\ACL\ProjectPolicies@client',
        ]);
        // Policy 5
        \AuzoPolicy::create([
            'name'   => 'Project company member',
            'method' => 'App\ACL\ProjectPolicies@companyMember',
        ]);
        // Policy 6
        \AuzoPolicy::create([
            'name'   => 'Owner (user.id = model.user_id)',
            'method' => 'App\ACL\ProjectPolicies@owner',
        ]);
        // Policy 7
        \AuzoPolicy::create([
            'name'   => 'Message team visible',
            'method' => 'App\ACL\ProjectPolicies@msgTeamVisible',
        ]);
        // Policy 8
        \AuzoPolicy::create([
            'name'   => 'Message client visible',
            'method' => 'App\ACL\ProjectPolicies@msgClientVisible',
        ]);
    }
}
