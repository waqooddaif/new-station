<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Role 1
        \AuzoRole::create(['name' => 'waqood.admin', 'description' => 'Administrator']);
        // Role 2
        \AuzoRole::create(['name' => 'waqood.pm', 'description' => 'Project Manager']);
        // Role 3
        \AuzoRole::create(['name' => 'waqood.lead', 'description' => 'Technical Lead']);
        // Role 4
        \AuzoRole::create(['name' => 'waqood.member', 'description' => 'Team Member']);
        // Role 5
        \AuzoRole::create(['name' => 'client.member', 'description' => 'Client']);
        // Role 6
//        \AuzoRole::create(['name' => 'client.pm', 'description' => 'Client project Manager']);
    }
}
