<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DemoDatabaseSeeder extends Seeder
{
    private $domain;

    public $password = '123321';

    public $admins = 2;

    public $members = 5;

    public $pms = 2;

    public $leaders = 2;

    public $companies = 4;

    // per company
    public $clients = 2;
    public $projects = 3;

    // per project
    public $services = 4;
    public $pm_messages = 2;
    public $client_messages = 4;

    // per service
    public $approval = 2;
    public $requirement = 1;
    public $payment = 1;

    public $services_list = [
        'Backend',
        'API',
        'Android',
        'iOS',
        'Design',
        'Frontend',
        'Hybrid App',
        'Analytics',
        'SaaS',
        'DevOps',
        'IoT',
        'Wordpress plugin',
        'wordpress Theme',
        '3rd party Integration',
        'Consultancy'
    ];

    public $services_statuses = [
        'pending',
        'in progress',
        'delivered'
    ];

    protected $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
        $this->domain = env('DEMO_DOMAIN', 'waqood.sa');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Database Seeders
        $this->call(DatabaseSeeder::class);

        // admins
        $admins_ids = [];
        for($admi = 1; $admi <= $this->admins; $admi++) {
            $admin = \App\User::create([
                'name'      => "Admin {$admi}",
                'description' => "Admin",
                'email'     => "admin{$admi}@{$this->domain}",
                'password'  => bcrypt($this->password),
                'type'      => 'member',
                'role_id'   => 1,
                'color'     => null
            ]);
            $admins_ids[] = $admin->id;
        }
        // members
        $members_ids = [];
        for($mi = 1; $mi <= $this->members; $mi++) {
            $member = \App\User::create([
                'name'      => "Member {$mi}",
                'description' => $this->faker->jobTitle,
                'email'     => "member{$mi}@{$this->domain}",
                'password'  => bcrypt($this->password),
                'type'      => 'member',
                'role_id'   => 4,
                'color'     => null
            ]);
            $members_ids[] = $member->id;
        }
        // project managers
        $pms_ids = [];
        for($pi = 1; $pi <= $this->pms; $pi++) {
            $pm = \App\User::create([
                'name'      => "PM {$pi}",
                'description' => "Project Manager",
                'email'     => "pm{$pi}@{$this->domain}",
                'password'  => bcrypt($this->password),
                'type'      => 'member',
                'role_id'   => 2,
                'color'     => null
            ]);
            $pms_ids[] = $pm->id;
            $members_ids[] = $pm->id;
        }
        // project technical leaders
        $leaders_ids = [];
        for($li = 1; $li <= $this->leaders; $li++) {
            $leader = \App\User::create([
                'name'      => "Lead {$li}",
                'description' => "Technical Lead",
                'email'     => "lead{$li}@{$this->domain}",
                'password'  => bcrypt($this->password),
                'type'      => 'member',
                'role_id'   => 3,
                'color'     => null
            ]);
            $leaders_ids[] = $leader->id;
            $members_ids[] = $leader->id;
        }
        // companies
        $companies_ids = [];
        for($comi = 1; $comi <= $this->leaders; $comi++) {
            $company = \App\Company::create([
                'name'      => "Company {$comi}",
                'website'   => "www.company{$comi}.com",
            ]);
            $companies_ids[] = $company->id;
        }
        // clients
        $clients = [];
        $clients_ids = [];
        foreach ($companies_ids as $company) {
            for($cli = 1; $cli <= $this->clients; $cli++) {
                $client = \App\User::create([
                    'name'      => "Client {$cli}/{$company}",
                    'description' => "Client @ {$company}",
                    'email'     => "client{$cli}.com{$company}@{$this->domain}",
                    'password'  => bcrypt($this->password),
                    'type'      => 'client',
                    'role_id'   => 5,
                    'color'     => null
                ]);
                $client->addToCompany($company);
                $clients[$company][] = $client;
                $clients_ids[$company][] = $client->id;
            }
        }
        // Services
        $services_ids = [];
        foreach ($this->services_list as $service) {
            $service = \App\Service::create(['name' => $service]);
            $services_ids[] = $service->id;
        }
        // Projects
        $projects = [];
        $projects_ids = [];
        $active_pms = [];
        $active_leaders = [];
        foreach ($pms_ids as $pmid) {
            $active_pms[$pmid] = 0;
        }
        foreach ($leaders_ids as $leaderid) {
            $active_leaders[$leaderid] = 0;
        }
        foreach ($companies_ids as $selected_company) {
            for($pi = 1; $pi <= $this->projects; $pi++) {

                $services_team = $members_ids;
                shuffle($services_team);
                $services_shuffled = $services_ids;
                $selected_pm = array_keys($active_pms, min($active_pms))[0];
                $selected_leader = array_keys($active_leaders, min($active_leaders))[0];

                if ($pi <= ($this->projects / 2)) {
                    // create archived projects
                    $date_start = $this->faker->dateTimeBetween('-5 months','-2 months')->format('Y-m-d');
                    $date_final = Carbon::createFromFormat('Y-m-d',$date_start)->addWeeks(6)->format('Y-m-d');
                    $date_archived = $date_final;
                } else {
                    // create open projects
                    $date_start = $this->faker->dateTimeBetween('-1 months', '-15 days')->format('Y-m-d');
                    $date_final = Carbon::createFromFormat('Y-m-d',$date_start)->addWeeks(7)->format('Y-m-d');
                    $date_archived = null;
                }

                $selected_client_key = array_rand($clients_ids[$selected_company]);
                $selected_client = $clients_ids[$selected_company][$selected_client_key];

                $project = \App\Project::create([
                    'name' => "Project {$pi}/{$selected_company}",
                    'date_start' => $date_start,
                    'date_final' => $date_final,
                    'date_archived' => $date_archived,
                    'pm_id' => $selected_pm,
                    'tech_id' => $selected_leader,
                    'client_id' => $selected_client,
                    'company_id' => $selected_company,
                    'color'     => null
                ]);

                $active_pms[$selected_pm]++;
                $active_leaders[$selected_leader]++;

                $projects[] = $project;
                $projects_ids[] = $project->id;

                // Services for the project
                for ($psi = 1; $psi <= $this->services; $psi++) {
                    $selected_service = array_shift($services_shuffled);

                    if (!empty($services_team))
                        $selected_member = array_shift($services_team);
                    else
                        $selected_member = $members_ids[rand(0, last($members_ids))];

                    $status = $project->date_archived
                        ? last($this->services_statuses)
                        : $this->services_statuses[rand(0, count($this->services_statuses) - 2)];
                    $start = $this->faker->dateTimeBetween(
                        $project->date_start,
                        Carbon::createFromFormat('Y-m-d', $project->date_start)
                            ->addWeeks(rand(0, 2))
                            ->format('Y-m-d')
                    )->format('Y-m-d');
                    $finish = $this->faker->dateTimeBetween(
                        Carbon::createFromFormat('Y-m-d', $start)
                            ->addWeeks(1)
                            ->format('Y-m-d'),
                        Carbon::createFromFormat('Y-m-d', $start)
                            ->addWeeks(4)
                            ->format('Y-m-d')
                    )->format('Y-m-d');
                    $pivot = $project->attachService(
                        $selected_service,
                        ['date_start' => $start, 'date_finish' => $finish, 'status' => $status]
                    );
                    $selected_project_service = $pivot->id;
                        $project->attachMember($selected_member, $selected_project_service);

                    $messages_ids = [];
                    for ($appsi = 1; $appsi <= $this->approval; $appsi++) {

                        $message = $this->generateMessage(
                            $start,
                            $finish,
                            $appsi,
                            $selected_member,
                            $project,
                            $selected_project_service,
                            'approval',
                            0,
                            1,
                            true
                        );
                        
                        $messages_ids[] = $message->id;
                    }
                    for ($reqsi = 1; $reqsi <= $this->requirement; $reqsi++) {

                        $message = $this->generateMessage(
                            $start,
                            $finish, 
                            $reqsi, 
                            $selected_member, 
                            $project,
                            $selected_project_service,
                            'requirement',
                            1,
                            1,
                            true
                        );
                        
                        $messages_ids[] = $message->id;
                    }
                    for ($paysi = 1; $paysi <= $this->payment; $paysi++) {

                        $message = $this->generateMessage(
                            $start,
                            $finish,
                            $paysi,
                            $selected_member,
                            $project,
                            $selected_project_service,
                            'payment',
                            0,
                            1,
                            true
                        );

                        $messages_ids[] = $message->id;
                    }
                }
                for ($pmmi = 1; $pmmi <= $this->pm_messages; $pmmi++) {
                    $message = $this->generateMessage(
                        $project->date_start,
                        $project->date_final,
                        $pmmi,
                        $selected_pm,
                        $project,
                        null,
                        null,
                        'auto',
                        'auto',
                        1
                    );

                    $messages_ids[] = $message->id;
                }
                for ($cmi = 1; $cmi <= $this->client_messages; $cmi++) {
                    $message = $this->generateMessage(
                        $project->date_start,
                        $project->date_final,
                        $cmi,
                        $selected_client,
                        $project,
                        null,
                        null,
                        'auto',
                        1,
                        false
                    );

                    $messages_ids[] = $message->id;
                }
            }
        }

    }

    /**
     * @param $start
     * @param $finish
     * @param $counter
     * @param $selected_member
     * @param $project
     * @param $selected_project_service
     * @param $type
     * @param $visible_team
     * @param $visible_client
     * @param null $make_due
     * @return \App\Message
     */
    public function generateMessage(
        $start,
        $finish,
        $counter,
        $selected_member,
        $project,
        $selected_project_service = null,
        $type,
        $visible_team,
        $visible_client,
        $make_due = null
    )
    {
        $auto_visible = ($type && $counter <= ($this->$type / 2)) ? 1 : 0;
        $visible_team = ($visible_team !== 'auto')
            ? $visible_team
            : $auto_visible;
        $visible_client = ($visible_client !== 'auto')
            ? $visible_client
            : $auto_visible;
        $today = date('Y-m-d');
        $created_at = $finish > $today ? $this->faker->dateTimeBetween($start, $today)
            ->format('Y-m-d') : $this->faker->dateTimeBetween($start, $finish)
            ->format('Y-m-d');
        $due = $make_due ? $this->faker->dateTimeBetween($created_at, $finish)
            ->format('Y-m-d') : null;
        $done = ($due && $due < date('Y-m-d')) ? $due : null;

        $message = \App\Message::create([
            'content' => $this->faker->paragraphs(2, true),
            'visible_team' => $visible_team,
            'visible_client' => $visible_client,
            'type' => $type,
            'created_at' => $created_at,
            'updated_at' => $created_at,
            'date_due' => $due,
            'date_done' => $done,
            'approval_state' => $done ? rand(0, 1) : 0,
            'approval_note' => $done ? $this->faker->paragraphs(1, true) : null,
            'user_id' => $selected_member,
            'project_id' => $project->id
        ]);

        if ($selected_project_service) {
            $message->projectServices()->attach($selected_project_service);
        }

        return $message;
    }
}
