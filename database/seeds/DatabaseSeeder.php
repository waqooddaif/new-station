<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(RoleSeeder::class);
         $this->call(AbilitySeeder::class);
         $this->call(PolicySeeder::class);
         $this->call(PermissionSeeder::class);
    }
}
