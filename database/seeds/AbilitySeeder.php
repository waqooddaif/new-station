<?php

use Illuminate\Database\Seeder;

class AbilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // ability 1
        \AuzoAbility::create([
            'name' => 'wq.projects.index.own',
            'label' => 'Waqood member can index only projects they have been to as members.',
            'tag' => 'wq.project'
        ]);
        // ability 2
        \AuzoAbility::create([
            'name' => 'wq.projects.index.pm',
            'label' => 'Waqood member can index only projects they manage or as technical leads.',
            'tag' => 'wq.project'
        ]);
        // ability 3
        \AuzoAbility::create([
            'name' => 'wq.projects.index.all',
            'label' => 'Waqood member can index all projects.',
            'tag' => 'wq.project'
        ]);
        // ability 4
        \AuzoAbility::create([
            'name' => 'project.show',
            'label' => 'User can show a project.',
            'tag' => 'client.project, wq.project'
        ]);
        // ability 5
        \AuzoAbility::create([
            'name' => 'project.create',
            'label' => 'User can create a new project.',
            'tag' => 'wq.project'
        ]);
        // ability 6
        \AuzoAbility::create([
            'name' => 'project.update',
            'label' => 'User can update a project.',
            'tag' => 'wq.project'
        ]);
        // ability 7
        \AuzoAbility::create([
            'name' => 'project.show.members',
            'label' => 'User can show a project members.',
            'tag' => 'wq.project'
        ]);
        // ability 8
        \AuzoAbility::create([
            'name' => 'message.show',
            'label' => 'User can show a project messages.',
            'tag' => 'client.project, wq.project'
        ]);
        // ability 9
        \AuzoAbility::create([
            'name' => 'message.create',
            'label' => 'User can create a new general message.',
            'tag' => 'client.project, wq.project'
        ]);
        // ability 10
        \AuzoAbility::create([
            'name' => 'message.update',
            'label' => 'User can update a project message.',
            'tag' => 'wq.project'
        ]);
        // ability 11
        \AuzoAbility::create([
            'name' => 'message.approve',
            'label' => 'User can approve a project message.',
            'tag' => 'client.project, wq.project'
        ]);
        // ability 12
        \AuzoAbility::create([
            'name' => 'action.create',
            'label' => 'User can create a new action message.',
            'tag' => 'client.project, wq.project'
        ]);
        // ability 13
        \AuzoAbility::create([
            'name' => 'action.update',
            'label' => 'User can update a project message.',
            'tag' => 'wq.project'
        ]);
        // ability 14
        \AuzoAbility::create([
            'name' => 'client.projects.index.own',
            'label' => 'Client can index only projects they manage as clients.',
            'tag' => 'client.project'
        ]);
        // ability 15
        \AuzoAbility::create([
            'name' => 'client.projects.index.company',
            'label' => 'Client can index all projects under their company.',
            'tag' => 'client.project'
        ]);
        // ability 16
        \AuzoAbility::create([
            'name' => 'company.index',
            'label' => 'user can list all companies.',
            'tag' => 'admin'
        ]);
        // ability 17
        \AuzoAbility::create([
            'name' => 'company.create',
            'label' => 'user can create company.',
            'tag' => 'admin'
        ]);
        // ability 18
        \AuzoAbility::create([
            'name' => 'client.index',
            'label' => 'user can list all clients.',
            'tag' => 'admin'
        ]);
        // ability 19
        \AuzoAbility::create([
            'name' => 'client.create',
            'label' => 'user can create client.',
            'tag' => 'admin'
        ]);
        // ability 20
        \AuzoAbility::create([
            'name' => 'file.upload',
            'label' => 'user can upload files.',
            'tag' => 'client.project, wq.project'
        ]);
        // ability 21
        \AuzoAbility::create([
            'name' => 'member.index',
            'label' => 'user can list all members.',
            'tag' => 'admin'
        ]);
        // ability 22
        \AuzoAbility::create([
            'name' => 'member.create',
            'label' => 'user can create member.',
            'tag' => 'admin'
        ]);
        // ability 23
        \AuzoAbility::create([
            'name' => 'planner.view',
            'label' => 'user can view the planner.',
            'tag' => 'admin'
        ]);
    }
}
