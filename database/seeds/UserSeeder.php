<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            // Moayad
            [
                'name'      => "Moayad Alsowayegh",
                'description' => "System Analyst",
                'email'     => "malsowayegh@waqood.sa",
                'password'  => bcrypt(str_random(8)),
                'type'      => 'member',
                'role_id'   => 3,
                'color'     => null
            ],
            // Waleed
            [
                'name'      => "Waleed Arkanji",
                'description' => "Project Manager",
                'email'     => "warkanji@waqood.sa",
                'password'  => bcrypt(str_random(8)),
                'type'      => 'member',
                'role_id'   => 1,
                'color'     => null
            ],
            // Milyani
            [
                'name'      => "Mohame Milyani",
                'description' => "Managing Partner",
                'email'     => "milyani@waqood.sa",
                'password'  => bcrypt(str_random(8)),
                'type'      => 'member',
                'role_id'   => 1,
                'color'     => null
            ]
        ];

        $users_controller = new \App\Http\Controllers\UsersController;

        foreach ($users as $user) {
            $users_controller->createAndInvite($user);
        }
    }
}
