<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $services = ['Frontend', 'UI', 'UX', 'Backend', 'iOS', 'Android', 'Support', 'QA', 'Hosting'];
        foreach ($services as $service) {
            \App\Service::create(['name' => $service]);
        }
    }
}
