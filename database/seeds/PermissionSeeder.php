<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // See RoleSeeder, AbilitySeeder, and PolicySeeder for id numbers reference.

        // pm permissions
        $pm = \AuzoRole::find(2);
        // index projects assigned to him as pm or tech lead
        $pm->givePermissionTo('wq.projects.index.pm');
        // index projects assigned to him as a member
        $pm->givePermissionTo('wq.projects.index.own');
        // can show project page
        $pm->givePermissionTo('project.show')
            ->addPolicy(3) // has to be a member
            ->addPolicy([2 => ['operator' => 'or']]) // or the tech lead
            ->addPolicy([1 => ['operator' => 'or']]); // or the pm
        // can update project
        $pm->givePermissionTo('project.update')
            ->addPolicy(1); // has to be the pm
        // show messages if it's team visible or created by him
        $pm->givePermissionTo('message.show')
            ->addPolicy(3) // has to be a member
            ->addPolicy(7) // and msg has to be a team visible
            ->addPolicy([6 => ['operator' => 'or']]) // or created by him
            ->addPolicy([1 => ['operator' => 'or']]); // or the pm of the project
        // create a general message
        $pm->givePermissionTo('message.create')
            ->addPolicy(3) // has to be this member,
            ->addPolicy([2 => ['operator' => 'or']]) // or tech lead
            ->addPolicy([1 => ['operator' => 'or']]); // or the pm of the project
        // update a message
        $pm->givePermissionTo('message.update')
            ->addPolicy(1); // or the pm of the project
        // create an action
        $pm->givePermissionTo('action.create')
            ->addPolicy(3) // has to be this member,
            ->addPolicy([2 => ['operator' => 'or']]) // or tech lead
            ->addPolicy([1 => ['operator' => 'or']]); // or the pm of the project
        // update an action
        $pm->givePermissionTo('action.update')
            ->addPolicy(1); // has to the pm of the project
        // can upload files
        $pm->givePermissionTo('file.upload');
        // approve a message
//        $pm->givePermissionTo('message.approve')
//            ->addPolicy(1); // has to the pm of the project

        // Tech lead permissions
        $tech_lead = \AuzoRole::find(3);
        // index projects assigned to him as pm or tech lead
        $tech_lead->givePermissionTo('wq.projects.index.pm');
        // index projects assigned to him as a member
        $tech_lead->givePermissionTo('wq.projects.index.own');
        // can show project page
        $tech_lead->givePermissionTo('project.show')
            ->addPolicy(3) // has to be a member
            ->addPolicy([2 => ['operator' => 'or']]); // or tech lead of the project
        // can show project members
        $tech_lead->givePermissionTo('project.show.members')
            ->addPolicy(2); // has to be a tech lead
        // can update project
        // show messages if it's team visible or created by him
        $tech_lead->givePermissionTo('message.show')
            ->addPolicy(3) // has to be a member
            ->addPolicy([2 => ['operator' => 'or']]) // or tech lead of the project
            ->addPolicy(7) // and msg has to be a team visible
            ->addPolicy([6 => ['operator' => 'or']]); // or created by him
        // create a general message
        $tech_lead->givePermissionTo('message.create')
            ->addPolicy(3) // has to be this member,
            ->addPolicy([2 => ['operator' => 'or']]); // or tech lead of the project
        // create an action
        $tech_lead->givePermissionTo('action.create')
            ->addPolicy(3) // has to be this member,
            ->addPolicy([2 => ['operator' => 'or']]); // or tech lead of the project
        // can upload files
        $tech_lead->givePermissionTo('file.upload');

        // Member permissions
        $member = \AuzoRole::find(4);
        // index projects assigned to him as a member
        $member->givePermissionTo('wq.projects.index.own');
        // can show project page
        $member->givePermissionTo('project.show')
            ->addPolicy(3); // has to be a member
        // can update project
        // show messages if it's team visible or created by him
        $member->givePermissionTo('message.show')
            ->addPolicy(3) // has to be a member
            ->addPolicy(7) // and msg has to be a team visible
            ->addPolicy([6 => ['operator' => 'or']]); // or created by him
        // create a general message
        $member->givePermissionTo('message.create')
            ->addPolicy(3); // has to be this member,
        // create an action
        $member->givePermissionTo('action.create')
            ->addPolicy(3); // has to be this member,
        // can upload files
        $member->givePermissionTo('file.upload');

        // Client member permissions
        $client = \AuzoRole::find(5);
        // index projects assigned to him
//        $client->givePermissionTo('client.projects.index.own');
        // index projects assigned to his company
        $client->givePermissionTo('client.projects.index.company');
        // can show project page
        $client->givePermissionTo('project.show')
            ->addPolicy(4) // has to be a client
            ->addPolicy([5 => ['operator' => 'or']]); // or to be in same company
        // can update project
        // show messages if it's team visible or created by him
        $client->givePermissionTo('message.show')
            ->addPolicy(4) // has to be a client
            ->addPolicy([5 => ['operator' => 'or']]) // or member of the company
            ->addPolicy(8) // and msg has to be a client visible
            ->addPolicy([6 => ['operator' => 'or']]); // or created by him
        // create a general message
        $client->givePermissionTo('message.create')
            ->addPolicy(4); // has to be this client.
        // approve a message
        $client->givePermissionTo('message.approve')
            ->addPolicy(4); // has to be this client.
        // can upload files
        $client->givePermissionTo('file.upload');
    }
}
