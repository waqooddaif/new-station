<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Company::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->company,
        'website' => $faker->url
    ];
});

$factory->define(App\Project::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->words(3, true),
        'date_start' => $faker->date(),
        'date_final' => $faker->date(),
        'image' => $faker->imageUrl(200, 200, 'abstract')
    ];
});

$factory->state(App\Project::class, 'pm', function () {
    return [
        'pm_id' => function() {
            return factory(\App\User::class)->create()->id;
        }
    ];
});

$factory->state(App\Project::class, 'lead', function () {
    return [
        'tech_id' => function() {
            return factory(\App\User::class)->create()->id;
        }
    ];
});

$factory->state(App\Project::class, 'client', function () {
    return [
        'client_id' => function() {
            return factory(\App\User::class)->create()->id;
        }
    ];
});

$factory->state(App\Project::class, 'company', function () {
    return [
        'company_id' => function() {
            return factory(\App\Company::class)->create()->id;
        }
    ];
});
