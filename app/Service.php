<?php

namespace App;

class Service extends BaseModel
{
    protected $fillable = ['name'];

    /**
     * Get all projects that use this service.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_service')
                    ->wherePivot(['date_finish', 'status', 'id', 'created_at', 'updated_at']);
    }

    /**
     * Get all member users assigned to this service from all projects.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'project_member');
    }

    /**
     * Get all relations with projects.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectServices()
    {
        return $this->hasMany(ProjectService::class);
    }

    /**
     * Filter only pending services.
     *
     * @param $query
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->where('status', 'pending');
    }
}
