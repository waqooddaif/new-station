<?php

/**
 * Get the start and end dates of a week in a year.
 *
 * @param integer|null $week
 * @param integer|null $year
 * @return array
 */
function getStartAndEndDate($week = null, $year = null) {
    $date = new DateTime();

    // default current year
    $year = $year ?: date('Y');
    // default current week
    $week = $week ?: date('W');

    $week_period['start'] = $date->setISODate($year, $week)->format('Y-m-d');
    $week_period['end'] = $date->modify('+6 days')->format('Y-m-d');

    return $week_period;
}

/**
 * Get random hex color.
 *
 * @param integer|null $minVal
 * @param integer|null $maxVal
 * @return string
 */
function randomColor ($minVal = 0, $maxVal = 255)
{

    // Make sure the parameters will result in valid colours
    $minVal = $minVal < 0 || $minVal > 255 ? 0 : $minVal;
    $maxVal = $maxVal < 0 || $maxVal > 255 ? 255 : $maxVal;

    // Generate 3 values
    $r = mt_rand($minVal, $maxVal);
    $g = mt_rand($minVal, $maxVal);
    $b = mt_rand($minVal, $maxVal);

    // Return a hex colour ID string
    return sprintf('#%02X%02X%02X', $r, $g, $b);

}