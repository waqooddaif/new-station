<?php

namespace App;

use Kordy\Auzo\Traits\RoleTrait;

class Role extends BaseModel
{
    use RoleTrait;

    protected $fillable = ['name', 'description'];
    protected $table = 'roles';
    public $timestamps = false;
}
