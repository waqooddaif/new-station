<?php

namespace App;

class OutboundEmail extends BaseModel
{
    protected $fillable = ['token', 'notifiable_id', 'notifiable_type', 'emailable_id', 'emailable_type'];

    /**
     * Get the notifiable.
     */
    public function notifiable()
    {
        return $this->morphTo();
    }

    /**
     * Get the emailable.
     */
    public function emailable()
    {
        return $this->morphTo();
    }

    /**
     * Create record from notification event.
     *
     * @param $event
     * @return static|array
     */
    public function createFromNotificationEvent($event)
    {
        if (property_exists($event->notification, 'subject')) {
            $subject = $event->notification->subject;
        } else {
            $subject = $event->notifiable;
        }

        $email = $this->create([
            'token' => $event->notification->token,
            'notifiable_id' => $event->notifiable->id,
            'notifiable_type' => get_class($event->notifiable),
            'emailable_id' => $subject->id,
            'emailable_type' => get_class($subject),
        ]);

        return $email;
    }
}
