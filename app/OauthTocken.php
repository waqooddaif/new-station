<?php

namespace App;

class OauthTocken extends BaseModel
{
    protected $fillable = ['user_id', 'oauth_id', 'token', 'token_secret', 'service', 'expires_at'];

    public $timestamps = false;

    /**
     * Get user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Filter only Jira service oauth tokens.
     *
     * @param $query
     * @return mixed
     */
    public function scopeJira($query)
    {
        return $query->where('service', 'jira');
    }

    /**
     * Filter only valid oauth tokens.
     *
     * @param $query
     * @return mixed
     */
    public function scopeValid($query)
    {
        $now = time();
        return $query->where('expires_at', '>', $now);
    }

    /**
     * Filter only valid Jira oauth tokens.
     *
     * @param $query
     * @return mixed
     */
    public function scopeValidJira($query)
    {
        return $query->jira()->valid();
    }
}
