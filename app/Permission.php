<?php

namespace App;

use Kordy\Auzo\Traits\PermissionTrait;

class Permission extends BaseModel
{
    use PermissionTrait;

    protected $fillable = ['ability_id', 'role_id'];
    protected $table = 'permissions';
    public $timestamps = false;
}
