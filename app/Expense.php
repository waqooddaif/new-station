<?php

namespace App;

class Expense extends BaseModel
{
    protected $fillable = ['project_id', 'description', 'cost'];

    public $timestamps = false;

    /**
     * Project that this expense belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Currency to be shown in the view.
     *
     * @return string
     */
    public function getCurrencyAttribute()
    {
        return config('station.expenses.currency');
    }
}
