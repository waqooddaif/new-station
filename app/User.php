<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use DB;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use App\ACL\User as Authenticatable;
use Kordy\Auzo\Traits\HasRoleTrait;

class User extends Authenticatable
{
    use Notifiable, HasRoleTrait, CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'type', 'description', 'color'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'role',
        'companies',
//        'email',
//        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be automatically appended to the collection.
     *
     * @var array
     */
    protected $appends = ['initials', 'has_jira', 'role_description', 'firstname', 'lastname'];

    /**
     * Get first name part.
     *
     * @return string
     */
    public function getFirstnameAttribute()
    {
        return ucfirst(explode(' ', $this->name)[0]);
    }

    /**
     * Get last name part.
     *
     * @return string
     */
    public function getLastnameAttribute()
    {
        $name_parts = explode(' ', $this->name);
        return ucfirst(end($name_parts));
    }

    /**
     * Generate user initials, the first letter of each name part.
     *
     * @return string
     */
    public function getInitialsAttribute()
    {
        $name = preg_split('/\s+/', $this->name);

        $initials = '';

        foreach ($name as $i) {
            $initials .= $i[0];
        }

        return $initials;
    }

    /**
     * Generate user description.
     *
     * @return string
     */
    public function getRoleDescriptionAttribute()
    {
        if ($this->description) {
            return $this->description;
        }

        $description = "";

        if ($this->role_id) {
            $description .= $this->role->description;
        } else {
            $description .= $this->type === 'client' ?: 'member';
        }

        if ($this->company()) {
            $description .= "@{$this->company()->name}";
        } else {
            $description .= "@Waqood";
        }

        return $description;
    }

    /**
     * Check user has valid Jira token.
     *
     */
    public function getHasJiraAttribute()
    {
        return $this->jiraTokens && (int) $this->jiraTokens->expires_at > time();
    }

    /**
     * Set a random color for user if no color is provided.
     *
     * @param $value
     */
    public function setColorAttribute($value)
    {
        $this->attributes['color'] = $value ?: randomColor(0, 225);
    }

    /**
     * Set a random password for user if no password is provided.
     *
     * @param $value
     */
    public function setPasswordAttribute($value = null)
    {
        $this->attributes['password'] = $value ?: bcrypt(bin2hex(random_bytes(10)));
    }

    /**
     * Get all emails sent to this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function outboundEmails()
    {
        return $this->morphMany(OutboundEmail::class, 'notifiable');
    }

    /**
     * Oauth tokens.
     *
     * @return HasMany
     */
    public function tokens()
    {
        return $this->hasMany(OauthTocken::class);
    }

    /**
     * Jira Oauth tokens.
     *
     * @return HasOne
     */
    public function jiraTokens()
    {
        $now = time();

        return $this->hasOne(OauthTocken::class)
            ->where('service', 'jira')
            ->where('expires_at', '>', $now);
    }

    /**
     * Companies relationship.
     *
     * @return BelongsToMany
     */
    public function companies()
    {
        return $this->belongsToMany(Company::class, 'clients');
    }

    /**
     * Get the company that this user is an employee at.
     *
     * @return Company
     */
    public function company()
    {
        return $this->companies->first();
    }

    /**
     * Add client user account to a company clients table.
     *
     * @param $company_id
     * @return $this
     */
    public function addToCompany($company_id)
    {
        $company_id = is_int($company_id) ? [$company_id] : $company_id;

        $this->companies()->syncWithoutDetaching($company_id);

        return $this;
    }

    /**
     * Get all projects this user is assigned.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_member')->withPivot('project_service_id');
    }

    /**
     * Get all active projects this user is assigned.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function activeProjects()
    {
        return $this->belongsToMany(Project::class, 'project_member')
                    ->whereNull('projects.date_archived');
    }

    /**
     * Get all projects managed by this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function managedProjects()
    {
        return $this->hasMany(Project::class, 'pm_id');
    }

    /**
     * Get all projects technical leaded by this user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leadedProjects()
    {
        return $this->hasMany(Project::class, 'tech_id');
    }

    /**
     * Get all projects for this client.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientProjects()
    {
        return $this->hasMany(Project::class, 'client_id');
    }

    /**
     * Get all projects for this client company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function companyProjects()
    {
        return $this->company()
                    ->projects()
                    ->where('client_id', '!=', $this->id)
                    ->get();
    }

    /**
     * Get all services this user is assigned to from all projects.
     *
     */
    public function services()
    {
        return $this->projectServices()->with('service');
    }

    /**
     * Get all project services this user is assigned to from all projects.
     *
     */
    public function projectServices()
    {
        return $this->belongsToMany(ProjectService::class, 'project_member');
    }

    /**
     * Filter only members accounts.
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsMember($query)
    {
        return $query->where('type', 'member');
    }

    /**
     * Filter only clients accounts.
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsClient($query)
    {
        return $query->where('type', 'client');
    }

    /**
     * Filter only clients accounts that belongs to a company.
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsCompanyClient($query)
    {
        return $query->isClient()->has('companies');
    }
}
