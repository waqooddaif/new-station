<?php

namespace App;

use Kordy\Auzo\Traits\PolicyTrait;

class Policy extends BaseModel
{
    use PolicyTrait;

    protected $fillable = ['name', 'method'];
    protected $table = 'policies';
    public $timestamps = false;
}
