<?php

namespace App\Observers;

use Modules\Projects\Entities\Models\Message;
use App\Notifications\MessageApprovalUpdate;
use App\Notifications\MessageDone;
use App\Notifications\NewMessage;

class MessageObserver
{
    /**
     * Listen to the Message created event.
     *
     * @param Message $message
     * @return void
     */
    public function created(Message $message)
    {
        $message->load('project');

        $ids[] = $message->project->pm_id;

        $users = $this->getUsersByVisibility($message, $ids);

        \Notification::send($users, new NewMessage($message));
    }

    /**
     * Listen to the Message updated event.
     *
     * @param Message $message
     * @return void
     */
    public function updated(Message $message)
    {
        $ids[] = $message->project->pm_id;

        // Is it is a visibility change? notify the new party as a new message
        if ($message->isDirty('visible_client') || $message->isDirty('visible_team')) {
            $this->notifyVisibilityChange($message, $ids);
        }
        // an approve or reject, notify all involved parties
        elseif ($message->isDirty('date_done') && $message->is_approval) {
            $this->notifyApproval($message, $ids);
        }
        // Action is done
        elseif ($message->isDirty('date_done') && $message->is_action) {
            $users = $this->getUsersByVisibility($message, $ids);
            \Notification::send($users, new MessageDone($message));
        }

    }

    /**
     * Get all members involved in a message.
     *
     * @param Message $message
     * @param $ids
     * @return array
     */
    private function getMessageMembers(Message $message, $ids)
    {
        // add tech lead to recipients
        $ids[] = $message->project->tech_id;
        // if message belongs to a service, only add this service members to recipients
        if ($message->project_service_id) {
            $ids = array_unique(array_merge(
                $ids,
                $message->project->members()
                    ->wherePivot('project_service_id', $message->project_service_id)
                    ->pluck('users.id')
                    ->toArray()
            ));
            return $ids;
        } else {
            // else add all members
            $ids = array_unique(array_merge($ids, $message->project->members->pluck('id')->toArray()));
            return $ids;
        }
    }

    /**
     * @param Message $message
     * @param $ids
     */
    private function notifyVisibilityChange(Message $message, $ids)
    {
        // notify client?
        if ($message->isDirty('visible_client')
            && $message->visible_client
            && $message->user_id !== $message->project->client_id)
        {
            $ids[] = $message->project->client_id;
        }
        // notify team?
        if ($message->isDirty('visible_team') && $message->visible_team) {
            $ids = $this->getMessageMembers($message, $ids);
        }

        $users = \Modules\Users\Entities\Models\User::whereIn('id', $ids)->where('id', '!=', $message->user_id)->get();

        \Notification::send($users, new NewMessage($message));
    }

    /**
     * @param Message $message
     * @param $ids
     */
    private function notifyApproval(Message $message, $ids)
    {
        $users = $this->getUsersByVisibility($message, $ids);

        \Notification::send($users, new MessageApprovalUpdate($message));
    }

    /**
     * @param Message $message
     * @param $ids
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getUsersByVisibility(Message $message, $ids)
    {
        // don't notify the logged in user, or the one who created the message by email
        if (\Auth::user()) {
            $ignore = \Auth::user()->id;
        } else {
            $ignore = $message->user_id;
        }

        if ($message->visible_client) {
            $ids[] = $message->project->client_id;
        }

        if ($message->visible_team) {
            $ids = $this->getMessageMembers($message, $ids);
        }

        $users = \Modules\Users\Entities\Models\User::whereIn('id', $ids)->where('id', '!=', $ignore)->get();
        return $users;
    }
}