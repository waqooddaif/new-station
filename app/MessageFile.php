<?php

namespace App;

class MessageFile extends BaseModel
{
    protected $fillable = ['name', 'path', 'message_id'];

    /**
     * Get the message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo(Message::class);
    }
}
