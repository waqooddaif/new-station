<?php

namespace App;

use Kordy\Auzo\Traits\AbilityTrait;

class Ability extends BaseModel
{
    use AbilityTrait;

    protected $fillable = ['name', 'label', 'tag'];
    protected $table = 'abilities';
    public $timestamps = false;
}
