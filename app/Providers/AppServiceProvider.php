<?php

namespace App\Providers;

use Modules\Projects\Entities\Models\Message;
use App\Observers\MessageObserver;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Queue;
use Illuminate\Queue\Events\JobFailed;
use App\Service;
use Modules\Users\Entities\Models\User;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param UrlGenerator $url
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if(env('APP_ENV') !== 'local')
        {
            $url->forceSchema('https');
        }

        // Give admin role all permissions
        \Gate::before(function ($user, $ability) {
            if ($user->role_id === 1) {
                return true;
            }
        });


        // todo add tracking to failed notifications

        // Tracks message statuses for notifications
        if (config('station.notification.enable')) {
            Message::observe(MessageObserver::class);
        }

        $this->validatorExtensions();

        $this->prepareViewInitData();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            $this->app->register('Backpack\Generators\GeneratorsServiceProvider');
        }
    }

    /**
     * @return array
     */
    private function getUserData()
    {
        $user = \Auth::user();
        $permissions = [
            'file.upload' => $user->can('file.upload'),
            'project.create' => $user->can('project.create'),
            'company.index' => $user->can('company.index'),
            'company.create' => $user->can('company.create'),
            'client.index' => $user->can('client.index'),
            'client.create' => $user->can('client.create'),
            'member.index' => $user->can('member.index'),
            'member.create' => $user->can('member.create'),
            'planner.view' => $user->can('planner.view'),
        ];
        return array_merge($user->toArray(), ['permissions' => $permissions]);
    }

    /**
     * @param $user
     * @return array
     */
    private function getLists($user)
    {
        $lists = [];
        
        // if the user can update projects, send him members and services lists
        if ($user->can('project.update')) {
            // members list for pm to add members
            $lists['members_list'] = User::isMember()
                ->with('role')
                ->get()
                ->makeVisible(['role_id', 'role']);
            // services list for pm to add services
            $lists['services_list'] = Service::get(['id', 'name']);
        } else {
            $lists['members_list'] = [];
            $lists['services_list'] = [];
        }

        if ($user->can('member.create')) {
            // members list for pm to add members
            $lists['user_roles_list'] = \AuzoRole::all();
        } else {
            $lists['user_roles_list'] = [];
        }
        return $lists;
    }

    public function prepareViewInitData()
    {
        View::composer('main', function ($view) {

            $user = \Auth::user();

            $user_data = $this->getUserData();

            $view->with('user', $user_data);

            $lists = $this->getLists($user);

            // share with view
            $view->with('members_list', $lists['members_list']);
            $view->with('services_list', $lists['services_list']);
            $view->with('user_roles_list', $lists['user_roles_list']);
        });
    }

    /**
     * Add custom extensions to the controller validator
     */
    public function validatorExtensions()
    {
        // custom validation rule to verify message is not empty
        Validator::extend('hastext', function ($attribute, $value, $parameters, $validator) {
            return !empty(strip_tags($value));
        });
        Validator::replacer('hastext', function ($message, $attribute, $rule, $parameters) {
            return "message must has a content!";
        });
        // custom validation rule for password strength
        Validator::extend('strongpass', function ($attribute, $value, $parameters, $validator) {
            if (! is_string($value) && ! is_numeric($value)) {
                return false;
            }
            return preg_match(config('station.password'), $value) > 0;
        });
        Validator::replacer('strongpass', function ($message, $attribute, $rule, $parameters) {
            return 'Password must contain letters and numbers';
        });
//        Validator::replacer('strongpass', function ($message, $attribute, $rule, $parameters) {
//            return 'Strong password must contain small and capital letters, numbers, and any of the special characters
//            [ ] @ - & # ( ) * ^ % ! ~ " \'';
//        });
    }
}
