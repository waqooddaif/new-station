<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Failed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RecordFailedLoginAttempt
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Failed  $event
     * @return void
     */
    public function handle(Failed $event)
    {
        $sentry = app('sentry');
        $sentry->captureMessage('Failed login attempt', [], [
            'level' => 'info',
            'user' => [
                'id' => is_null($event->user) ? null : $event->user->id,
                'email' => $event->credentials['email'],
                'ip_address' => request()->ip()
            ]
        ]);
    }
}
