<?php

namespace App\Listeners;

use App\OutboundEmail;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationSent  $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
        // if it is an email message, log it to the outbound emails table
        if ($event->channel === 'mail') {
            (new OutboundEmail)->createFromNotificationEvent($event);
        }
    }
}
