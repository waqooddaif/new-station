<?php

namespace App\Notifications;

trait EmailableTrait {

    public $token;

    protected function getFingerPrint()
    {
        $this->token = $token = bin2hex(random_bytes(10));

        return ":ST-NT: [$token]";
    }

}