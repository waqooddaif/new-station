<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserInvitation extends Notification implements ShouldQueue
{
    use Queueable, EmailableTrait;

    public $subject;

    public $user_token;

    /**
     * Create a new notification instance.
     *
     * @param $user_token
     */
    public function __construct($user_token)
    {
        $this->user_token = $user_token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $expirin = config('auth.passwords.users.expire') / (60 * 24);
        $this->subject = $notifiable;
        return (new MailMessage)
                    ->greeting("Hello {$notifiable->firstname}")
                    ->subject("Invitation to join Waqood Station {$this->getFingerPrint()}")
                    ->line("A new account has been created for you at Waqood Station, 
                    click on the button below and enter your email ({$notifiable->email}) 
                    to set your account password.")
                    ->action('Set up your account', url("/invite/{$this->user_token}/client"))
                    ->line('Thank you for using our application!')
                    ->line("This invitation is valid for $expirin days, afterwards, 
                    you will need to reset your password by visiting this link " . route('password.reset',['token'=>$this->user_token]));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
