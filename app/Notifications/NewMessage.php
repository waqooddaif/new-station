<?php

namespace App\Notifications;

use Modules\Projects\Entities\Models\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewMessage extends Notification implements ShouldQueue
{
    use Queueable, EmailableTrait;

    public $subject;

    /**
     * Create a new notification instance.
     *
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->subject = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $type = $this->subject->type ? ucfirst($this->subject->type)." Request" : "Message";
        return (new MailMessage)
                    ->greeting("Hello {$notifiable->name}")
                    ->subject("New {$type} in {$this->subject->project->name} {$this->getFingerPrint()}")
                    ->line("{$this->subject->owner->name} added a new {$type} 
                    to {$this->subject->project->name}:")
                    ->line('"'.strip_tags($this->subject->content).'"')
                    ->action('Project page', url("/projects/{$this->subject->project->id}"));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
