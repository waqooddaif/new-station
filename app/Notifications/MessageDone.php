<?php

namespace App\Notifications;

use Modules\Projects\Entities\Models\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class MessageDone extends Notification implements ShouldQueue
{
    use Queueable, EmailableTrait;
    /**
     * @var Message
     */
    public $subject;

    /**
     * Create a new notification instance.
     *
     * @param Message $message
     */
    public function __construct(Message $message)
    {
        $this->subject = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // first 3 words of the message as title
        $message_words = explode(' ', strip_tags($this->subject->content));
        $title = implode(" ", array_slice($message_words, 0, 3));

        return (new MailMessage)
            ->greeting("Hello {$notifiable->name}")
            ->subject("{$title} is done in {$this->subject->project->name} {$this->getFingerPrint()}")
            ->line("{$title} is done in project {$this->subject->project->name}")
            ->action('Project page', url("/projects/{$this->subject->project->id}"));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
