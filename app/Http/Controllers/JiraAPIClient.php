<?php

namespace App\Http\Controllers;

use App\User;
use Exception;
use GuzzleHttp\Exception\BadResponseException;
use League\OAuth1\Client\Credentials\TokenCredentials;
use SocialiteProviders\Jira\Server;

class JiraAPIClient extends Server
{
    public $credentials;

    public function __construct()
    {
        $config = config('services.jira');

        $clientCredentials = array_merge([
            'identifier' => $config['client_id'],
            'secret' => $config['client_secret'],
            'callback_uri' => $config['redirect'],
        ], $config);

        parent::__construct($clientCredentials);
    }

    /**
     * Run Jira APIs, given the API path.
     *
     * @param string $api
     * @param null $params
     * @param string $method
     * @param null|User $user
     * @return array
     */
    public function runAPI($api, $params = null, $method = 'get', $user = null)
    {
        $user = $this->userOauthToken($user);

        $url = config('services.jira.base_uri').'/rest/api/2/'.$api;

        $client = $this->createHttpClient();

        $headers = $this->getHeaders($this->createTokenCredentials(), $method, $url);

        $options['headers'] = $headers;

        if ($params) {
            $options['json'] = $params;
        }

        try {

            $response = $client->$method($url, $options);

        } catch (BadResponseException $e) {
            $response = $e->getResponse();
            $body = $response->getBody();
            $statusCode = $response->getStatusCode();

            \Log::error(
                "Received error [$body] with status code [$statusCode] when running api call $url."
            );

            parse_str($body, $errors);

            if (key_exists('oauth_problem', $errors)) {
                // if user token is rejected, make it expired.
                if ($errors['oauth_problem'] === 'token_rejected') {
                    \Log::info("Change user $user->id jira tokens expired because the token is rejected.");
                    $user->jiraTokens()->update(['expires_at' => time()]);
                }
            }

            return [
                'error' => [
                    'statusCode' => $statusCode,
                    'errors' => $errors,
                ]
            ];
        }

        return  json_decode((string) $response->getBody(), true);
    }

    /**
     * Creates token credentials from the body response.
     *
     * @param array $tokens
     *
     * @return TokenCredentials
     */
    protected function createTokenCredentials($tokens = null)
    {
        $data = $tokens ?: $this->credentials;

        $tokenCredentials = new TokenCredentials();
        $tokenCredentials->setIdentifier($data['token']);
        $tokenCredentials->setSecret($data['token_secret']);

        return $tokenCredentials;
    }

    /**
     * Get current user information from Jira.
     *
     * @param null|User $user
     * @return array
     */
    public function userInfo($user = null)
    {
        return $this->runAPI('myself', null, 'get', $user);
    }

    /**
     * Get current user projects list from Jira.
     *
     * @param null|array $params
     * @param null|User $user
     * @return array
     */
    public function projects($params = null, $user = null)
    {
        return $this->runAPI('project', $params, 'get', $user);
    }

    /**
     * Create new issue on Jira.
     *
     * @param array $data
     * @param null $user
     * @return array
     */
    public function createIssue($data, $user = null)
    {
        return $this->runAPI('issue', $data, 'post', $user);
    }

    /**
     * @param null|User $user
     * @return User
     * @throws Exception
     */
    protected function userOauthToken($user = null)
    {
        $user = $user ?: \Auth::user();
        if (! $user->hasJira) {
            throw new Exception("No valid Jira tokens found for user $user->id");
        }
        $this->credentials = $user->jiraTokens->toArray();

        return $user;
    }
}