<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * Get a full list of all companies.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $user = \Auth::user();
        if ($user->cannot('company.index')) {
            ApiTransformer::notAuthorized();
        }

        return ApiTransformer::success(
            Company::with('clients')
                ->get()
                ->toArray()
        );
    }

    /**
     * Create a new company.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $user = \Auth::user();
        if ($user->cannot('company.create')) {
            ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'name' => 'required|string'
        ]);

        return ApiTransformer::success(
            Company::create($request->only(['name', 'website']))
        );
    }
}
