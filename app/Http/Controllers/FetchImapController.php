<?php

namespace App\Http\Controllers;

use App\Helpers\FileLogger;
use App\Message;
use App\OutboundEmail;
use App\Project;
use App\User;
use DB;
use Illuminate\Http\Request;
use PhpImap\Mailbox as ImapMailbox;

class FetchImapController extends Controller
{
    private $logger;

    public function __construct()
    {
        $this->logger = new FileLogger('imap', 'DEBUG');
    }

    /**
     * Fetch Imap emails and process each email to create a message.
     *
     * @return array|bool
     */
    public function fetch()
    {
        $mailbox = $this->openImapBox();

        // Read all unseen emails into an array:
        $mailsIds = $mailbox->searchMailbox('UNSEEN');

        if(!$mailsIds || empty($mailsIds)) {
            return [];
        }

        $messages = [];

        foreach ($mailsIds as $mailId) {

            $email = $mailbox->getMail($mailId);

            if (!$email_token = $this->getEmailToken($email)) {
                // if no valid token just escape it
                continue;
            }

            if (!$user = $this->getEmailUser($email, $email_token)) {
                // if no valid user of this email, just escape it
                continue;
            }

            $clean_text = $this->cleanText($email);

            if ($message = $this->CreateMessage($clean_text, $user, $user->outboundEmails->first(), $email->getAttachments())) {
                $messages[] = $message;
            }
        }

        return $messages;
    }

    /**
     * Open IMAP connection.
     *
     * @return ImapMailbox
     */
    protected function openImapBox()
    {
        $imap_server = config('station.imap.server');
        $imap_folder = config('station.imap.folder');
        $imap_path = '{' . $imap_server . '}' . $imap_folder;
        $imap_user = config('station.imap.user');
        $imap_pass = config('station.imap.password');
        $attachment_path = base_path(config('station.attachments.path'));
        return new ImapMailbox($imap_path, $imap_user, $imap_pass, $attachment_path);
    }

    /**
     * Get the email text.
     *
     * @param $email
     * @return string
     */
    private function cleanText($email)
    {
        if ($email->textPlain) {
            $text = $email->textPlain;
        } else {
            $text = $email->textHtml;
        }
        // Remove quoted lines (lines that begin with '>').
        $text = preg_replace('/(^\w.+:\n)?(^>.*(\n|$))+/mi', '', $text);
        // Remove lines beginning with 'On' and ending with 'wrote:' (matches
        // Mac OS X Mail, Gmail).
        $text = preg_replace('/^(On).*(wrote:).*$/sm', '', $text);
        // Remove lines like '----- Original Message -----' (some other clients).
        // Also remove lines like '--- On ... wrote:' (some other clients).
        $text = preg_replace('/^---.*$/mi', '', $text);
        // Remove lines like '____________' (some other clients).
        $text = preg_replace('/^____________.*$/mi', '', $text);
        // Remove blocks of text with formats like:
        //   - 'From: Sent: To: Subject:'
        //   - 'From: To: Sent: Subject:'
        //   - 'From: Date: To: Reply-to: Subject:'
        $text = preg_replace('/From:.*^(To:).*^(Subject:).*/sm', '', $text);
        // Remove all text after "###### write your reply above this line ######"
//        $text = preg_replace('/\#{6}.*/sm', '', $text);
        // Remove any remaining whitespace.
        return trim($text);
    }

    /**
     * Get a valid email token.
     *
     * @param $email
     * @return bool|string
     */
    protected function getEmailToken($email)
    {
        preg_match_all('/\[([a-z0-9]{20}?)\]/', $email->subject, $email_token);

        if (empty($email_token)) {
            // if no token is supplied to the email or invalid token
            $this->invalidToken($email);
            return false;
        }

        $email_token = $email_token[1];

        return $email_token;
    }

    /**
     * Get valid email sender user.
     *
     * @param $email
     * @param $email_token
     * @return User|bool
     */
    private function getEmailUser($email, $email_token)
    {
        $user = User::with(['outboundEmails' => function($query) use($email_token) {
            return $query->with('emailable')->where('token', $email_token);
        }])
            ->Where('email', $email->fromAddress)
            ->first();

        if (!$user) {
            // log and escape if sender is not a registered user
            $this->notRegisteredUser($email);
            return false;
        }

        if ($user->outboundEmails->isEmpty()) {
            // if no token is supplied to the email or invalid token
            $this->invalidToken($email);
            return false;
        }

        return $user;
    }

    /**
     * Create the message.
     *
     * @param $clean_text
     * @param $user
     * @param $outbound_email
     * @param null $attachments
     * @return bool|Message
     */
    private function CreateMessage($clean_text, $user, $outbound_email, $attachments = null)
    {
        $notification = $outbound_email->emailable;

        if ($notification instanceof Message) {
            $project_id = $notification->project_id;
            $project_service_id = $notification->project_service_id;
        }
        elseif ($notification instanceof Project) {
            $project_id = $notification->id;
            $project_service_id = null;
        }
        else {
            $this->logger->addRecord("alert", "Notification instance is not recognized! Outbound email id: {$outbound_email->id}");
            return false;
        }

        // decide visibility
        list($visible_team, $visible_client) = $this->decideVisibility($notification, $user);

        try { // if any error happened rollback the message creation
            DB::beginTransaction();
            $message = Message::create([
                'content' => $clean_text,
                'user_id' => $user->id,
                'project_id' => $project_id,
                'project_service_id' => $project_service_id,
                'visible_team' => $visible_team,
                'visible_client' => $visible_client,
            ]);

            // attach email attachments to the message
            if ($attachments && $valid_attachments = $this->prepareAttachments($attachments)) {
                FilesController::attachToMessage($valid_attachments, $message->id);
            }
            DB::commit();

            $this->logger->addRecord("info", "User {$user->name} created a message (id: {$message->id}) in project (id: {$project_id}) by 
        replying to outbound email (id: {$outbound_email->id}) (token: [{$outbound_email->token}]).");

            return $message;
        } catch (\Exception $e) {
            DB::rollBack();
            \Log::error("error happened while trying to create message by email 
            to outbound email (id: {$outbound_email->id}) (token: [{$outbound_email->token}])! {$e->getMessage()}");
            app('sentry')->captureException($e);
        }

    }

    /**
     * Email with invalid token.
     *
     * @param $email
     */
    private function invalidToken($email)
    {
        $this->logger->addRecord("alert", "Invalid token found in email {$email->subject} from {$email->fromAddress}");
    }

    /**
     * Got an email from not the same user that has been notified.
     *
     * @param $email
     */
    private function invalidUser($email)
    {
        $this->logger->addRecord("alert", "Invalid user found in email {$email->subject} from {$email->fromAddress}");
    }

    /**
     * Got an email from not registered user.
     *
     * @param $email
     */
    private function notRegisteredUser($email)
    {
        $this->logger->addRecord("alert", "User not found in email {$email->subject} from {$email->fromAddress}");
    }

    /**
     * Prepare email attachments for attaching to a message.
     *
     * @param $attachments
     * @return array|bool
     */
    private function prepareAttachments($attachments)
    {
        $file_controller = new FilesController;

        $files = [];

        foreach ($attachments as $file) {
            $filename = $file_controller->generateFileName($file->name);
            $temp_path = $file->filePath;
            $info = pathinfo($temp_path);
            $validate = in_array($info['extension'], config('station.attachments.extensions'));
            if (!$validate) {
                unlink($temp_path);
                $this->logger->addRecord("alert", "not valid attachment {$temp_path}");
                continue;
            } else {
                $savepath = base_path(config('station.attachments.path')).'/'.$filename;
                rename($temp_path, $savepath);
                $files[] = ['path' => config('station.attachments.url').'/'.$filename, 'filename' => $file->name];
            }
        }

        return empty($files) ? null : $files;
    }

    /**
     * Decide what visibility values for a message.
     *
     * @param $notification
     * @param $user
     * @return array
     */
    protected function decideVisibility($notification, $user)
    {
        $visible_team = false;
        $visible_client = false;

        if ($notification instanceof Message) {
            $project = $notification->project;
        }
        elseif ($notification instanceof Project) {
            $project = $notification;
        }
        else {
            $project = null;
        }

        if ($user->can('project.update', $project)) {
            // if we know what message is this a reply to, then give it same visibility
            if ($notification instanceof Message) {
                $visible_client = $notification->visible_client;
                $visible_team = $notification->visible_team;
            }
            return array($visible_team, $visible_client);
        } else {
            if ($user->type === 'member')
                $visible_team = true;
            elseif ($user->type === 'client')
                $visible_client = true;
        }
        return array($visible_team, $visible_client);
    }
}
