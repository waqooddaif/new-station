<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthorizeController extends Controller
{
    public function front(Request $request)
    {
        // authorization calls are allowed against these models only
        $available_models = config('station.models');

        $this->validate($request, [
            'ability' => 'required|string',
            'model' => 'string|in:'.implode(',', $available_models),
            'id' => 'int|required_with:model'
        ]);

        $user = \Auth::user();
        $ability = $request->input('ability');
        $result = false;

        if (!$request->has('model')) {
            $result = $user->can($ability);
        }
        if ($request->has(['model', 'id'])) {
            $model = 'App\\'.ucfirst($request->input('model'));
            $record = $model::find($request->input('id'));
            if ($record) {
                $result = $user->can($ability, $record);
            }
        }

        return ApiTransformer::success([
            'ability' => $ability,
            'result' => $result
        ]);
    }
}
