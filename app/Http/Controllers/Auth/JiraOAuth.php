<?php

namespace App\Http\Controllers\Auth;

use App\OauthTocken;
use Response;
use Socialite;
use App\Http\Controllers\Controller;
use Symfony\Component\Debug\Exception\FatalThrowableError;

class JiraOAuth extends Controller
{
    /**
     * Redirect the user to the Jira authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('jira')->redirect();
    }

    /**
     * Obtain the user information from Jira.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function handleProviderCallback()
    {
        try {
            $jira_user = Socialite::driver('jira')->user();

            $user = \Auth::user();

            $oauth_tokens = $user->jiraTokens ?: new OauthTocken;

            $oauth_tokens->user_id = $user->id;
            $oauth_tokens->oauth_id = $jira_user->id;
            $oauth_tokens->token = $jira_user->accessTokenResponseBody['oauth_token'];
            $oauth_tokens->token_secret = $jira_user->accessTokenResponseBody['oauth_token_secret'];
            $oauth_tokens->expires_at =
                (int) (time() + ((int)$jira_user->accessTokenResponseBody['oauth_expires_in'] / 1000));
            $oauth_tokens->service = 'jira';

            $oauth_tokens->save();

            return view('jira.close')->with('status', 'success');
        } catch (FatalThrowableError $e) {
            return view('jira.close')->with('status', 'fail');
        }

    }
}
