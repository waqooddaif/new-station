<?php

namespace App\Http\Controllers;

use App\Message;
use App\MessageFile;
use Illuminate\Http\Request;
use Validator;

class FilesController extends Controller
{

    /**
     * Upload files.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function upload(Request $request)
    {
        $user = \Auth::user();

        if ($user->cannot('file.upload')) {
            return ApiTransformer::notAuthorized();
        }

        if ($user->role_id === config('station.roles.admin') || $user->role_id === config('station.roles.pm')) {
            $max = config('station.attachments.manager_max');
        } else {
            $max = config('station.attachments.max');
        }

        $extensions = implode(',', config('station.attachments.extensions'));

        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:jar,'. $extensions .'|max:'.$max
        ]);

        if ($validator->fails()) {
            return ApiTransformer::validationError($validator->errors());
        }

        $filename = $this->generateFileName($request->file->getClientOriginalName());

        return ApiTransformer::success(
            ['path' => $request->file->storeAs('uploads', $filename, 'public')]
        );
    }

    /**
     * @param $attachments
     * @param Message|int $message
     * @return MessageFile|array
     * @throws \Exception
     */
    public static function attachToMessage($attachments, $message)
    {
        if ($message instanceof Message) {
            $message = $message->id;
        } else if (!is_int($message)) {
            throw new \Exception("Wrong message id format!");
        }

        if (key_exists('path', $attachments) && key_exists('filename', $attachments)) {
            // single attachment
//            $info = pathinfo($attachments['filename']);
//            $name =  basename($attachments['filename'],'.'.$info['extension']);
            $name =  basename($attachments['filename']);
            return MessageFile::create([
                'name' => $name,
                'path' => $attachments['path'],
                'message_id' => $message
            ]);
        } elseif (is_array($attachments)) {
            // multiple attachments
            $files = [];
            foreach ($attachments as $attachment) {
                $files[] = self::attachToMessage($attachment, $message);
            }
            return $files;
        } else {
            throw new \Exception($attachments." is not a right file attachment!");
        }
    }

    /**
     * Get a safe filename hash-sanitized_name.ext
     *
     * @param $file
     * @return string
     */
    public function generateFileName($file)
    {
        $info = pathinfo($file);
        return uniqid().'-'.$this->sanitizedFileName($info['filename']).'.'.$info['extension'];
    }

    /**
     * Get a sanitized file name
     *
     * @param $file
     * @return string
     */
    public function sanitizedFileName($file)
    {
        // Remove anything which isn't a word, whitespace, number, any of the following caracters -_~,;[]()
        $file = mb_ereg_replace('([^\w\d\-_~,;\[\]\(\).])', '_', $file);
        // remove any repeated periods
        $file = mb_ereg_replace('([\.]{2,})', '.', $file);
        // remove any repeated _
        $file = mb_ereg_replace('([_]{2,})', '_', $file);
        // if happened, replace any seq _. with .
        return str_replace('_.', '.', $file);
    }
}
