<?php

namespace App\Http\Controllers;

use App\ProjectService;
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * Get all active services.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function activeIndex()
    {
        $projectServices = ProjectService::with('service', 'project')->get()->toArray();
        return ApiTransformer::success($projectServices);
    }
}
