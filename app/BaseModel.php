<?php

namespace App;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use CrudTrait;
}
