<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UsersRolesTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test creating a user and assign a role to them.
     *
     */
    public function test_create_a_user_with_role()
    {
        $role = \AuzoRole::create(['name' => 'client_pm', 'description' => 'A company user with PM role']);

        $role->users()->create([
            'name'  => 'Test User',
            'email' => 'test@exampl.com',
            'password'  => bcrypt('123'),
            'type'      => 'client'
        ]);

        $this->assertEquals(App\User::find(1)->role->name, 'client_pm');
    }

    /**
     * Test the permissions.
     *
     */
    public function test_user_permissions()
    {
        $role = \AuzoRole::create(['name' => 'client_pm', 'description' => 'A company user with PM role']);

        $role->users()->create([
            'name'  => 'Test User',
            'email' => 'test@exampl.com',
            'password'  => bcrypt('123'),
            'type'      => 'client'
        ]);

        $role->users()->create([
            'name'      => 'Test User2',
            'email'     => 'test2@exampl.com',
            'password'  => bcrypt('123'),
            'type'      => 'client'
        ]);

        $ability = AuzoAbility::create(['name' => 'test.ability', 'label' => 'testing abilities']);

        $policy = AuzoPolicy::create(['name' => 'dummy policy', 'method' => 'UsersRolesTest@dummyPolicy']);

        $role->givePermissionTo($ability)->addPolicy($policy);

        AuzoPermissionRegistrar::registerPermissions();

        $this->assertTrue(App\User::find(1)->can('test.ability'));

        $this->assertFalse(App\User::find(2)->can('test.ability'));
    }

    /**
     *  Testing that the registration is disabled for public.
     */
    public function test_public_registration_disabled()
    {
        $this->get('/register')->seeStatusCode(404);
    }

    /**
     * A dummy test policy.
     *
     * @param $user
     * @param $model
     * @return bool
     */
    public function dummyPolicy($ability, $role, $user, $model)
    {
        return $user->id == 1;
    }
}
