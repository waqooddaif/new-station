<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProjectBudgetUnitTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Add budget.
     *
     * @return void
     */
    public function testAddBudget()
    {
        $project = factory(\App\Project::class)
            ->states(['pm', 'lead', 'client', 'company'])
            ->create();

        $project->update(['budget' => 199.50]);

        $this->seeInDatabase('projects', ['budget' => 199.50]);
    }

    /**
     * Add expense.
     *
     * @return void
     */
    public function testAddExpense()
    {
        $project = factory(\App\Project::class)
            ->states(['pm', 'lead', 'client', 'company'])
            ->create(['budget' => 199.50]);

        $project->expenses()->create([
            'description' => 'some exp. desc',
            'cost' => 10.00
        ]);

        $this->seeInDatabase('expenses', [
            'project_id' => 1,
            'cost' => 10.00,
            'description' => 'some exp. desc'
        ]);
    }

    /**
     * Get project budget balance.
     *
     * @return void
     */
    public function testGetBalance()
    {
        $project = factory(\App\Project::class)
            ->states(['pm', 'lead', 'client', 'company'])
            ->create(['budget' => 200]);

        $project->expenses()->create([
            'description' => 'some exp. desc',
            'cost' => 10.00
        ]);

        $this->assertEquals(190, $project->budget_balance);
    }
}
