<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientsTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Create a company.
     *
     * @return void
     */
    public function test_create_a_new_company()
    {
        $company = [
            'name'      => 'Some Company',
            'website'   => 'www.example.com'
        ];

        App\Company::create($company);

        $this->seeInDatabase('companies', $company);
    }

    /**
     * Create a client.
     *
     * @return void
     */
    public function test_add_a_client_user_to_a_company()
    {
        $company = App\Company::create([
            'name'      => 'Some Company',
            'website'   => 'www.example.com'
        ]);

        $user = App\User::create([
            'name'  => 'Test User',
            'email' => 'test@exampl.com',
            'password'  => bcrypt('123'),
            'type'      => 'client'
        ]);

        $user->addToCompany($company->id);
        // or
        $company->addClients($user->id);

        $this->assertEquals($company->clients->count(), 1);
        $this->seeInDatabase('clients', ['company_id' => 1, 'user_id' => 1]);
    }
}
