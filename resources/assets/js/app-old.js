
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap_basic');
require('./bootstrap_full');

const app = new Vue({
    el: '#app',
    mounted() {
        this.initPlugins();
    },
    methods: {
        initPlugins() {
            // Retina images plugin
            retinajs( $('img') );

            // Webui-popover plugin
            $('.member-add').webuiPopover({
                trigger: 'click',
                placement: 'auto', //bottom-right
                width: 380,
                height: 445,
                closeable: false,
                animation: 'pop',
                content: function () {
                    return $("#member-add-popover-content").html();
                }
            });
            $('.service-add').webuiPopover({
                trigger: 'click',
                placement: 'auto', //bottom-right
                width: 380,
                height: 445,
                closeable: false,
                animation: 'pop',
                content: function () {
                    return $("#service-add-popover-content").html();
                }
            });
            $('.service-edit').webuiPopover({
                trigger: 'click',
                placement: 'auto', //bottom-right
                width: 380,
                height: 445,
                closeable: false,
                animation: 'pop',
                content: function () {
                    return $("#service-edit-popover-content").html();
                }
            });
            $('#notifications-menu-button').webuiPopover({
                trigger: 'click',
                placement: 'auto', //bottom-right
                width: 232,
                closeable: false,
                animation: 'pop',
                content: function () {
                    return $("#notifications-menu-dropdown-content").html();
                }
            });
            $('#user-settings-menu-button').webuiPopover({
                trigger: 'click',
                placement: 'auto', //bottom-right
                width: 175,
                closeable: false,
                animation: 'pop',
                content: function () {
                    return $("#user-settings-menu-dropdown-content").html();
                }
            });

            // Quilljs
            let quill1 = new Quill('#message-form .editor', {
                modules: {
                    toolbar: "#message-form .toolbar"
                },
                placeholder: 'Add your message here ...',
                theme: 'snow'
            });
            let quill2 = new Quill('#note-form .editor', {
                modules: {
                    toolbar: "#note-form .toolbar"
                },
                placeholder: 'Add your message here ...',
                theme: 'snow'
            });
            let quill3 = new Quill('#add-reject-message .editor', {
                modules: {
                    toolbar: "#add-reject-message .toolbar"
                },
                placeholder: 'Add your message here ...',
                theme: 'snow'
            });
        }
    }
});
