import Vue from 'vue'
import Vuex from 'vuex'
// modules
import companyStore from './modules/company'
import projectStore from './modules/project'
import coreStore from './modules/core'
import userStore from './modules/user'
import serviceStore from './modules/service'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    modules: {
        coreStore: coreStore,
        userStore: userStore,
        projectStore: projectStore,
        companyStore: companyStore,
        serviceStore: serviceStore,
    },
    strict: debug
})