import companyApi from '../../api/company'

const state = {
    companiesList: null
};

const getters = {
    companiesList: state => state.companiesList
};

const actions = {
    fetchCompaniesList ({ commit, dispatch, rootState }) {
        companyApi.fetchList(rootState.coreStore.apiList)
            .then(response => {
                commit('fetchCompaniesList', response);
                // context.dispatch('triggerApiCalls', {
                //     status: 'success', response: response,
                //     msg: 'companies list has been fetched successfully!'
                // }, { root: true }); // -> root 'triggerApiCalls'
            }).catch(error => {
            dispatch('triggerApiCalls', {
                status: 'error', response: error,
                msg: 'Failed to fetch the list of companies!'
            }, { root: true }); // -> root 'triggerApiCalls'
        });
    },
    createCompany ({ commit, dispatch, rootState }, data) {
        return new Promise((resolve, reject) => {
            companyApi.createCompany(rootState.coreStore.apiList, data)
                .then(response => {
                    commit('addCompany', response);
                    dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Created new company successfully!'
                    }, { root: true }); // -> root 'triggerApiCalls'
                    resolve(response);
                }).catch(error => {
                dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to create a new company!'
                }, { root: true }); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    createClient ({ commit, dispatch, rootState }, data) {
        return new Promise((resolve, reject) => {
            companyApi.createClient(rootState.coreStore.apiList, data)
                .then(response => {
                    commit('addClient', {client: response, companyId: data.company_id});
                    dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Created new client successfully!'
                    }, { root: true }); // -> root 'triggerApiCalls'
                    resolve(response);
                }).catch(error => {
                dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to create a new client!'
                }, { root: true }); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    }
};

const mutations = {
    fetchCompaniesList (state, companiesList) {
        state.companiesList = companiesList;
    },
    addCompany (state, company) {
        if (!company.clients) {
            company.clients = [];
        }
        state.companiesList.push(company);
    },
    addClient (state, {client, companyId}) {
        console.log('commiting');
        console.log(client);
        console.log(companyId);
        for (let i = 0; i < state.companiesList.length; i++) {
            if (state.companiesList[i].id === companyId) {
                console.log('found');
                if (! state.companiesList[i].clients) {
                     state.companiesList[i].clients = [];
                }
                console.log('pushing');
                state.companiesList[i].clients.push(client);
                console.log('pushed');
            }
        }

    }
};

export default ({
    state,
    getters,
    actions,
    mutations
});