import serviceApi from '../../api/service'

const state = {
    servicesList: null,
    activeServicesList: null,
};

const getters = {
    servicesList: state => state.servicesList,
    activeServicesList: state => state.activeServicesList,
};

const actions = {
    setServicesList ({ commit }, servicesList) {
        commit('setServicesList', servicesList)
    },
    fetchActiveServicesList ({ commit, dispatch, rootState }) {
        return new Promise((resolve, reject) => {
            serviceApi.getActiveServices(rootState.coreStore.apiList)
                .then(response => {
                    commit('setActiveServicesList', response);
                    // dispatch('triggerApiCalls', {
                    //     status: 'success', response: response,
                    //     msg: 'Created new member successfully!'
                    // }, { root: true }); // -> root 'triggerApiCalls'
                    resolve(response);
                }).catch(error => {
                dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to fetch services list!'
                }, { root: true }); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    }
};

const mutations = {
    setServicesList (state, servicesList) {
        state.servicesList = servicesList;
    },
    setActiveServicesList (state, services) {
        state.activeServicesList = services;
    }
};

export default ({
    state,
    getters,
    actions,
    mutations
});