import projectApi from '../../api/project'
import _ from 'lodash'

const state = {
    projects: [],
    openedProject: null,
    projectActiveService: null
};

const getters = {
    projectActiveService: state => state.projectActiveService,
    allProjects: state => state.projects,
    activeProjects: state => state.projects.filter(
        p => p.status != 'archived'
    ),
    archivedProjects: state => state.projects.filter(
        p => p.status == 'archived'
    ),
    openedProject: state => state.openedProject,
    projectFiles: (state) => {
        let files = [];
        if (state.openedProject && state.openedProject.messages) {
            let messages = state.openedProject.messages;
            for (let m = 0; m < messages.length; m++) {
                if (messages[m].files && messages[m].files.length > 0) {
                    for (let f = 0; f < messages[m].files.length; f++) {
                        files.push({
                            id: messages[m].id,
                            projectServiceId: messages[m].project_service_id,
                            name: messages[m].files[f].name,
                            path: messages[m].files[f].path,
                            visibility: {
                                visible_client: messages[m].visible_client,
                                visible_team: messages[m].visible_team
                            }
                        });
                    }
                }
            }
        }
        
        return files;
    },
    openedProjectDues: (state, getters, rootState) => {
        let dues = {total: 0};
        if (state.openedProject && state.openedProject.messages) {
            let actionTypes = rootState.coreStore.actionTypes;
            // build a list for each action type
            for (let t = 0; t < actionTypes.length; t++) {
                dues[actionTypes[t]] = [];
            }
            let messages = state.openedProject.messages;
            for (let m = 0; m < messages.length; m++) {
                if (messages[m].is_action && messages[m].is_due) {
                    dues.total += 1;
                    dues[messages[m].type].push({
                        id: messages[m].id,
                        projectServiceId: messages[m].project_service_id,
                        dateDue: messages[m].date_due,
                        content: messages[m].content,
                        visible_client: messages[m].visible_client,
                        visible_team: messages[m].visible_team,
                    });
                }
            }
        }
        
        return dues;
    },
    activeServiceDues: (state, getters, rootState) => {
        let dues = {total: 0};
        if (state.openedProject && state.openedProject.messages) {
            let actionTypes = rootState.coreStore.actionTypes;
            // build a list for each action type
            for (let t = 0; t < actionTypes.length; t++) {
                dues[actionTypes[t]] = [];
            }
            let messages = state.openedProject.messages;
            for (let m = 0; m < messages.length; m++) {
                if (
                    state.projectActiveService
                    && !messages[m].services.filter(
                        s => s.id === state.projectActiveService.pivot.id
                    ).length > 0)
                    continue;
                if (messages[m].is_action && messages[m].is_due) {
                    dues.total += 1;
                    dues[messages[m].type].push({
                        id: messages[m].id,
                        projectServiceId: messages[m].project_service_id,
                        dateDue: messages[m].date_due,
                        content: messages[m].content,
                        visible_client: messages[m].visible_client,
                        visible_team: messages[m].visible_team,
                    });
                }
            }
        }
        
        return dues;
    },
    groupedMessages(state) {
        if (state.openedProject && state.openedProject.messages) {
            let groups = _.groupBy(
                state.openedProject.messages,
                function (msg) {
                    let dateFilter = Vue.filter('date');
                    return dateFilter(msg.updated_at.date, "YYYY-MM-DD", 'YYYY-MM-DD HH:mm:ss');
                });
            return _.toPairs(groups);
        } else {
            return [];
        }
        
    }
    
};

const actions = {
    //Set project active service (highlighted)
    setProjectActiveService ({commit}, service) {
        commit('setProjectActiveService', service);
    },
    // Get projects list
    resetProjects (context) {
        projectApi.getProjects(context.rootState.coreStore.apiList)
            .then((response) => {
                context.commit('resetProjects', response);
                // if the current page is a project page, set the project data
                if (context.rootState.route.name == 'ProjectShow') {
                    context.dispatch(
                        'setOpenedProject',
                        parseInt(context.rootState.route.params.id)
                    );
                }
                // context.dispatch('triggerApiCalls', {
                //     status: 'success', response: response,
                //     msg: 'Projects list is loaded successfully!'
                // }, { root: true }); // -> root 'triggerApiCalls'
            })
            .catch((error) => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Error while trying to load the projects list!'
                }, {root: true}); // -> root 'triggerApiCalls'
            });
    },
    setOpenedProject (context, id) {
        return new Promise((resolve, reject) => {
            let search = state.projects.length > 0 ? state.projects.find(
                p => parseInt(p.id) === id
            ) : {};
            let openedProject = search ? search : {};
            if (openedProject.id) {
                if (context.rootState.route.name == 'ProjectShow' && !openedProject.services) {
                    projectApi.getProjectRecords(context.rootState.coreStore.apiList, id)
                        .then(response => {
                            // project  services, members, and messages
                            let openedProjectRelated = response;
                            // clone openedproject object to be able to modify and add data
                            let project = JSON.parse(JSON.stringify(openedProject));
                            for (let key in openedProjectRelated) {
                                if (openedProjectRelated.hasOwnProperty(key)) {
                                    project[key] = openedProjectRelated[key];
                                }
                            }
                            context.commit('setOpenedProject', project);
                            // context.dispatch('triggerApiCalls', {
                            //     status: 'success', response: response,
                            //     msg: 'The project is loaded successfully!'
                            // }, { root: true }); // -> root 'triggerApiCalls'
                            resolve();
                        }).catch(error => {
                        context.commit('setOpenedProject', openedProject);
                        context.dispatch('triggerApiCalls', {
                            status: 'error', response: error,
                            msg: 'Error while trying to load the project!'
                        }, {root: true}); // -> root 'triggerApiCalls'
                        reject(error);
                    })
                } else {
                    context.commit('setOpenedProject', openedProject);
                    resolve();
                }
            } else {
                context.commit('setOpenedProject', null);
                reject('404');
            }
        });
    },
    createProject (context, data) {
        return new Promise(function (resolve, reject) {
            projectApi.createProject(context.rootState.coreStore.apiList, data)
                .then(response => {
                    context.commit('createProject', response);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Created new project successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve(response);
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to create new project!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    addProjectExpense (context, {projectId, data}) {
        return new Promise(function (resolve, reject) {
            projectApi.addExpense(context.rootState.coreStore.apiList, projectId, data)
                .then(response => {
                    context.commit('addProjectExpense', {expense: response, projectId: projectId});
                    context.commit('reCalcProjectBudgetBalance', projectId);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Added the expense to the project successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({data: response});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to add the expense to the project!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    deleteProjectExpense (context, {expenseId: expenseId, projectId: projectId}) {
        return new Promise(function (resolve, reject) {
            projectApi.deleteExpense(context.rootState.coreStore.apiList, expenseId)
                .then(response => {
                    context.commit('deleteProjectExpense', {expenseId: expenseId, projectId: projectId});
                    context.commit('reCalcProjectBudgetBalance', projectId);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Deleted the expense successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({data: response});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to delete the expense!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    addProjectMessage (context, {projectId, data}) {
        return new Promise(function (resolve, reject) {
            projectApi.addMessage(context.rootState.coreStore.apiList, projectId, data)
                .then(response => {
                    context.commit('resetProject', response);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Add the message to the project successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({message: response.messages[0], projectId: projectId});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to add the message to the project!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    approveProjectMessage (context, data) {
        return new Promise(function (resolve, reject) {
            projectApi.approveMessage(context.rootState.coreStore.apiList, data)
                .then(response => {
                    let message = response;
                    context.commit('updateMessage', message);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Changed the message approval state!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve(message);
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to change the message approval!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    changeMessageVisibility (context, {message, target}) {
        return new Promise(function (resolve, reject) {
            projectApi.changeMessageVisibility(context.rootState.coreStore.apiList, message, target)
                .then(response => {
                    let message = response;
                    context.commit('updateMessage', message);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Changed the message visibility!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({message: message});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to change the message visibility!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    makeActionDone (context, message) {
        return new Promise(function (resolve, reject) {
            projectApi.makeActionDone(context.rootState.coreStore.apiList, message)
                .then(response => {
                    let message = response;
                    context.commit('updateMessage', message);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Changed the message to done!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({message: message});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to change the message status!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    updateMessageService (context, data) {
        return new Promise(function (resolve, reject) {
            projectApi.updateMessageService(context.rootState.coreStore.apiList, data)
                .then(response => {
                    let message = response;
                    context.commit('updateMessage', message);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Changed the message service!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({message: message});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to change the message service!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    addProjectMember (context, {projectId, data}) {
        return new Promise(function (resolve, reject) {
            projectApi.addMember(context.rootState.coreStore.apiList, projectId, data)
                .then(response => {
                    let member = response;
                    context.commit('addProjectMember', {member: member, projectId: projectId});
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Add ' + member.name + ' to the project successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({member: member, projectId: projectId});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to add the service to the project!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    addProjectService (context, {projectId, newService}) {
        return new Promise(function (resolve, reject) {
            projectApi.addService(context.rootState.coreStore.apiList, projectId, newService)
                .then(response => {
                    context.commit('resetProject', response);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Added to the project successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({service: response.services[response.services.length - 1], projectId: projectId});
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to add the service to the project!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    updateProject (context, {projectId, data}) {
        return new Promise(function (resolve, reject) {
            projectApi.updateProject(context.rootState.coreStore.apiList, projectId, data)
                .then(response => {
                    context.commit('resetProject', response);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Updated successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({project: response, projectId: projectId})
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to update the project!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error)
            })
        });
    },
    updateProjectService (context, {projectId, service}) {
        return new Promise(function (resolve, reject) {
            projectApi.updateService(context.rootState.coreStore.apiList, projectId, service)
                .then(response => {
                    context.commit('resetProject', response);
                    context.dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Updated successfully!'
                    }, {root: true}); // -> root 'triggerApiCalls'
                    resolve({service: service, projectId: projectId})
                }).catch(error => {
                context.dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to update the service!'
                }, {root: true}); // -> root 'triggerApiCalls'
                reject(error)
            })
        });
    },
    removeProjectService (context, {projectId, projectServiceId}) {
        projectApi.removeService(context.rootState.coreStore.apiList, projectId, projectServiceId)
            .then(response => {
                context.commit('resetProject', response);
                context.dispatch('triggerApiCalls', {
                    status: 'success', response: response,
                    msg: 'Removed service from the project successfully!'
                }, {root: true}); // -> root 'triggerApiCalls'
            }).catch(error => {
            context.dispatch('triggerApiCalls', {
                status: 'error', response: error,
                msg: 'Failed to remove the service to the project!'
            }, {root: true}); // -> root 'triggerApiCalls'
        })
    },
    removeProjectMember (context, {projectId, projectMemberId, projectServiceId}) {
        projectApi.removeMember(context.rootState.coreStore.apiList, projectId, projectMemberId, projectServiceId)
            .then(response => {
                context.commit('removeProjectMember', {
                    projectMemberId: projectMemberId,
                    projectId: projectId,
                    projectServiceId: projectServiceId
                });
                context.dispatch('triggerApiCalls', {
                    status: 'success', response: response,
                    msg: 'Removed member from the project successfully!'
                }, {root: true}); // -> root 'triggerApiCalls'
            }).catch(error => {
            context.dispatch('triggerApiCalls', {
                status: 'error', response: error,
                msg: 'Failed to remove the member to the project!'
            }, {root: true}); // -> root 'triggerApiCalls'
        })
    },
    resetOpenedProject ({commit}) {
        commit('resetOpenedProject');
    }
};

const mutations = {
    setProjectActiveService (state, service) {
        if (state.projectActiveService && state.projectActiveService.pivot.id === service.pivot.id) {
            state.projectActiveService = null;
        } else {
            state.projectActiveService = service;
        }
        
    },
    resetProjects (state, projects) {
        state.projects = projects;
    },
    createProject (state, project) {
        state.projects.unshift(project);
    },
    setOpenedProject (state, project) {
        state.openedProject = project;
        if (project && state.projects.length > 0) {
            for (let i = 0; i < state.projects.length; i++) {
                if (state.projects[i].id === project.id) {
                    Vue.set(state.projects, i, project);
                    break;
                }
            }
        }
    },
    resetProject (state, project) {
        if (state.openedProject.id === project.id) {
            state.openedProject = project
        }
        if (project && state.projects.length > 0) {
            for (let i = 0; i < state.projects.length; i++) {
                if (state.projects[i].id === project.id) {
                    Vue.set(state.projects, i, project);
                    break;
                }
            }
            if (state.openedProject.id === project.id) {
                state.openedProject = project
            }
        } else if (project) {
            state.projects.push(project)
        }
    },
    updateMessage(state, message) {
        let projectId = message.project_id;
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                for (let x = 0; x < state.projects[i].messages.length; x++) {
                    if (state.projects[i].messages[x].id === message.id) {
                        Vue.set(state.projects[i].messages, x, message);
                        if (state.openedProject.id === projectId) {
                            Vue.set(state.openedProject.messages, x, message);
                        }
                        break;
                    }
                }
            }
        }
    },
    addProjectExpense(state, {expense, projectId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                state.projects[i].expenses.push(expense);
                break;
            }
        }
    },
    deleteProjectExpense(state, {expenseId, projectId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                let expenses = JSON.parse(JSON.stringify(state.projects[i].expenses));
                for (let j = 0; j < expenses.length; j++) {
                    if (expenses[j].id === expenseId) {
                        expenses.splice(j, 1);
                        state.projects[i].expenses = expenses;
                        break;
                    }
                }
                break;
            }
        }
    },
    reCalcProjectBudgetBalance(state, projectId) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                state.projects[i].budget_balance = parseFloat(
                    parseFloat(state.projects[i].budget) - state.projects[i].expenses.reduce(
                        function (pre, current) {
                            return parseFloat(pre) + parseFloat(current.cost);
                        }, 0
                    )).toFixed(2);
                break;
            }
        }
    },
    addProjectMessage(state, {message, projectId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                state.projects[i].messages.unshift(message);
                break;
            }
        }
    },
    addProjectMember(state, {member, projectId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                state.projects[i].members.push(member);
                break;
            }
        }
    },
    addProjectService(state, {service, projectId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                state.projects[i].services.push(service);
                break;
            }
        }
    },
    updateProjectService(state, {service, projectId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                for (let x = 0; x < state.projects[i].services.length; x++) {
                    if (state.projects[i].services[x].pivot.id === service.pivot.id) {
                        Vue.set(state.projects[i].services, x, service);
                        break;
                    }
                }
            }
        }
    },
    removeProjectService(state, {projectServiceId, projectId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                for (let x = 0; x < state.projects[i].services.length; x++) {
                    if (state.projects[i].services[x].pivot.id === projectServiceId) {
                        state.projects[i].services.splice(x, 1);
                        break;
                    }
                }
            }
        }
    },
    removeProjectMember(state, {projectMemberId, projectId, projectServiceId}) {
        for (let i = 0; i < state.projects.length; i++) {
            if (state.projects[i].id === projectId) {
                for (let x = 0; x < state.projects[i].members.length; x++) {
                    if (state.projects[i].members[x].id === projectMemberId) {
                        if (
                            (
                                !projectServiceId && !state.projects[i].members[x].pivot['project_service_id']
                            )
                            ||
                            (
                                projectServiceId
                                && state.projects[i].members[x].pivot['project_service_id'] === projectServiceId
                            )
                        ) {
                            state.projects[i].members.splice(x, 1);
                            break;
                        }
                    }
                }
            }
        }
    },
    resetOpenedProject(state) {
        state.openedProject = null;
    }
};

export default ({
    state,
    getters,
    actions,
    mutations
});