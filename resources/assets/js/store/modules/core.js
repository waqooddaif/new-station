const state = {
    apiList: null,
    appInfo: null,
    jira: null,
    attachmentsRules: null,
    actionTypes: null,
    statusTypes: null,
    apiCallsCounter: 0,
    apiCallStatus: {
        status: '',
        msg: ''
    }
};

const getters = {
    apiList: state => state.apiList,
    appInfo: state => state.appInfo,
    jira: state => state.jira,
    jiraKeyLink: state => state.jira.url + "/browse/",
    attachmentsRules: state => state.attachmentsRules,
    actionTypes: state => state.actionTypes,
    statusTypes: state => state.statusTypes,
    apiCallsCounter: state => state.apiCallsCounter,
    apiCallStatus: state => state.apiCallStatus,
};

const actions = {
    setApiList ({ commit }, apiList) {
        commit('setApiList', apiList)
    },
    setAppInfo ({ commit }, appInfo) {
        commit('setAppInfo', appInfo)
    },
    setJira ({ commit }, jira) {
        commit('setJira', jira)
    },
    setAttachmentsRules ({ commit }, attachmentsRules) {
        commit('setAttachmentsRules', attachmentsRules)
    },
    setActionTypes ({ commit }, actionTypes) {
        commit('setActionTypes', actionTypes)
    },
    setStatusTypes ({ commit }, statusTypes) {
        commit('setStatusTypes', statusTypes)
    },
    triggerApiCalls ({ commit }, apiCall) {
        commit('triggerApiCalls', apiCall)
    }
};

const mutations = {
    setApiList (state, apiList) {
        state.apiList = apiList;
    },
    setAppInfo (state, appInfo) {
        state.appInfo = appInfo;
    },
    setJira (state, jira) {
        state.jira = jira;
    },
    setAttachmentsRules (state, attachmentsRules) {
        state.attachmentsRules = attachmentsRules;
    },
    setActionTypes (state, actionTypes) {
        state.actionTypes = actionTypes;
    },
    setStatusTypes (state, statusTypes) {
        state.statusTypes = statusTypes;
    },
    triggerApiCalls (state, apiCall) {
        state.apiCallsCounter += 1;
        state.apiCallStatus = apiCall;
    }
};

export default ({
    state,
    getters,
    actions,
    mutations
});