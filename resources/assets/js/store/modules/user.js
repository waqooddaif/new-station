import userApi from '../../api/user'

const state = {
    loggedInUser: null,
    rolesList: null,
    membersList: null,
    membersWeekSchedule: {},
    userRolesList: null,
};

const getters = {
    loggedInUser: state => state.loggedInUser,
    userPermissions: state => state.loggedInUser ? state.loggedInUser.permissions : [],
    rolesList: state => state.rolesList,
    userRolesList: state => state.userRolesList,
    membersList: state => state.membersList,
    managersList: state => {
        let roleId = state.rolesList.pm;
        let adminRoleId = state.rolesList.admin;
        return state.membersList.filter(member => member.role_id === roleId || member.role_id === adminRoleId);
    },
    leadsList: state => {
        let roleId = state.rolesList.lead;
        let adminRoleId = state.rolesList.admin;
        return state.membersList.filter(member => member.role_id === roleId || member.role_id === adminRoleId);
    },
    membersWeekSchedule: state => state.membersWeekSchedule
};

const actions = {
    setLoggedInUser ({ commit }, user) {
        commit('setLoggedInUser', user)
    },
    setRolesList ({ commit }, rolesList) {
        commit('setRolesList', rolesList)
    },
    setUserRolesList ({ commit }, userRolesList) {
        commit('setUserRolesList', userRolesList)
    },
    setMembersList ({ commit }, membersList) {
        commit('setMembersList', membersList)
    },
    createMember ({ commit, dispatch, rootState }, data) {
        return new Promise((resolve, reject) => {
            userApi.createUser(rootState.coreStore.apiList, data)
                .then(response => {
                    commit('addMember', response);
                    dispatch('triggerApiCalls', {
                        status: 'success', response: response,
                        msg: 'Created new member successfully!'
                    }, { root: true }); // -> root 'triggerApiCalls'
                    resolve(response);
                }).catch(error => {
                dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to create a new member!'
                }, { root: true }); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    },
    fetchMembersWeekSchedule ({ commit, dispatch, rootState }, {year, week}) {
        return new Promise((resolve, reject) => {
            userApi.getMembersWeekSchedule(rootState.coreStore.apiList, year, week)
                .then(response => {
                    commit('setMembersWeekSchedule', {schedule: response, week: week, year: year});
                    // dispatch('triggerApiCalls', {
                    //     status: 'success', response: response,
                    //     msg: 'Created new member successfully!'
                    // }, { root: true }); // -> root 'triggerApiCalls'
                    resolve(response);
                }).catch(error => {
                dispatch('triggerApiCalls', {
                    status: 'error', response: error,
                    msg: 'Failed to fetch members schedule!'
                }, { root: true }); // -> root 'triggerApiCalls'
                reject(error);
            });
        });
    }
};

const mutations = {
    setLoggedInUser (state, user) {
        state.loggedInUser = user;
    },
    setRolesList (state, rolesList) {
        state.rolesList = rolesList;
    },
    setUserRolesList (state, userRolesList) {
        state.userRolesList = userRolesList;
    },
    setMembersList (state, membersList) {
        state.membersList = membersList;
    },
    addMember (state, member) {
        state.membersList.push(member);
    },
    setMembersWeekSchedule (state, {schedule, week, year}) {
        let newSchedule = JSON.parse(JSON.stringify(state.membersWeekSchedule));
        if (!newSchedule[year]) newSchedule[year] = {};
        newSchedule[year][week] = schedule;
        state.membersWeekSchedule = newSchedule;
    }
};

export default ({
    state,
    getters,
    actions,
    mutations
});