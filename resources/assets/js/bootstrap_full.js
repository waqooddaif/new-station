/**
 * webui-popover
 * https://github.com/sandywalker/webui-popover/
 */
require('webui-popover');


/**
 * Moment 2.17
 * http://momentjs.com/
 */
window.moment = require('moment');

/**
 * Toastr 2.1
 * https://github.com/CodeSeven/toastr
 */
// window.toastr = require('toastr');

/**
 * Initialize the jquery plugins after Vue finishes
 */
window.initJqueryPlugins = function () {
  // Retina images plugin
  retinajs($('img'));
};

/**
 * Vuejs.
 * vuejs.org
 */

window.Vue = require('vue');
require('vue-resource');

/**
 * Sentry.io Raven script and configuration
 */
import RavenVue from 'raven-js/plugins/vue'

window.Raven = require('raven-js');

window.Raven
  .config('https://a06a17fad4694fc18800c740d341db62@sentry.io/161269', {
    logger: 'javascript',
    environment: document.querySelector('meta[name="app-env"]').getAttribute('content')
  })
  .addPlugin(RavenVue, Vue)
  .install();

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

Vue.http.interceptors.push((request, next) => {
  request.headers.set(
    'X-CSRF-TOKEN',
    document.querySelector('meta[name="csrf-token"]').getAttribute('content')
  );
  
  next((response) => {
    if (response.status === 500) {
      // Send to Sentry.io
      let err = "";
      if (response.body.hasOwnProperty("message")) err = response.body.message;
      else err = response.statusText;
      
      let urlParts = request.url.split("/");
      let apiName = request.method + " " + urlParts.slice(1).join(" / ");
      let apiPath = request.method + " " + urlParts.slice(1).filter(function (part) {
        return isNaN(parseInt(part));
      }).join(" / ");
      let ApiError =  apiPath + " - Error: " + response.statusText;
      
      let responseText = (typeof response.body === 'object')
        ? JSON.stringify(response.body).substring(0, 6000) : response.body.substring(0, 6000);
      Raven.captureException(ApiError, {
        extra: {
          name: apiName,
          type: request.method,
          url: request.url,
          data: request.body ? request.body : request.params,
          status: response.status,
          error: err,
          response: responseText
        }
      });
    }
    return response;
  })
});

/**
 * Vue Filters
 */
require('./components/common/Filters');

/**
 * Vue Directives
 */
Vue.directive('popover', {
  bind: function (el, binding, vnode) {
    let options = binding.value;
    $(el).webuiPopover(options);
  }
});

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
