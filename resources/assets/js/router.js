import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const CompanyIndex = resolve => {
    // require.ensure is Webpack's special syntax for a code-split point.
    require.ensure(['./components/companies/CompanyIndex.vue'], () => {
        resolve(require('./components/companies/CompanyIndex.vue'))
    });
};

const ClientIndex = resolve => {
    // require.ensure is Webpack's special syntax for a code-split point.
    require.ensure(['./components/clients/ClientIndex.vue'], () => {
        resolve(require('./components/clients/ClientIndex.vue'))
    });
};

const MemberIndex = resolve => {
    // require.ensure is Webpack's special syntax for a code-split point.
    require.ensure(['./components/members/MemberIndex.vue'], () => {
        resolve(require('./components/members/MemberIndex.vue'))
    });
};

const PlannerView = resolve => {
    // require.ensure is Webpack's special syntax for a code-split point.
    require.ensure(['./components/planner/PlannerView.vue'], () => {
        resolve(require('./components/planner/PlannerView.vue'))
    });
};

const ProjectsIndex = resolve => {
    // require.ensure is Webpack's special syntax for a code-split point.
    require.ensure(['./components/projects/IndexList.vue'], () => {
        resolve(require('./components/projects/IndexList.vue'))
    });
};
const ProjectShow = resolve => {
    // require.ensure is Webpack's special syntax for a code-split point.
    require.ensure(['./components/projects/Show.vue'], () => {
        resolve(require('./components/projects/Show.vue'))
    });
};
const NotFoundComponent = resolve => {
    // require.ensure is Webpack's special syntax for a code-split point.
    require.ensure(['./components/NotFoundComponent.vue'], () => {
        resolve(require('./components/NotFoundComponent.vue'))
    });
};

const routes = [
    { path: '/home', redirect: { name: 'ProjectsIndex' } },
    { path: '/', redirect: { name: 'ProjectsIndex' } },

    { path: '/projects', component: ProjectsIndex, name: 'ProjectsIndex' },
    { path: '/projects/:id', component: ProjectShow, name: 'ProjectShow' },

    { path: '/companies', redirect: { name: 'CompanyIndex' } },
    { path: '/company', component: CompanyIndex, name: 'CompanyIndex' },

    { path: '/clients', redirect: { name: 'ClientIndex' } },
    { path: '/client', component: ClientIndex, name: 'ClientIndex' },

    { path: '/members', redirect: { name: 'MemberIndex' } },
    { path: '/member', component: MemberIndex, name: 'MemberIndex' },

    { path: '/planner/:year/:week', component: PlannerView, name: 'PlannerView', props: true },

    { path: '/404', component: NotFoundComponent },
    { path: '*', component: NotFoundComponent }
];

const router = new VueRouter({
    mode: 'history',
    history: true,
    base: '/',
    linkActiveClass: 'active',
    scrollBehavior (to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition
        } else if (to.hash) {
            return {
                selector: to.hash
            }
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes
});

module.exports = router;