Vue.filter('date', function (date, format = 'YYYY/MM/DD', parse = 'YYYY-MM-DD', utc = true) {
    if (!date) {
        return 'N/A'
    }


    if (utc) {
        return moment.utc(date, parse).local().format(format);
    } else {
        return moment(date, parse).format(format);
    }
});

Vue.filter('moment', function (date, parse = 'YYYY-MM-DD HH:mm:ss', utc = true) {
    if (!date) {
        return 'N/A'
    }
    
    if (utc) {
        return moment.utc(date, parse).local();
    } else {
        return moment(date, parse);
    }
});

Vue.filter('human', function (date, parse = 'YYYY-MM-DD HH:mm:ss', utc = true) {
    if (!date) {
        return 'N/A'
    }

    if (utc) {
        return moment.utc(date, parse).local().fromNow();
    } else {
        return moment(date, parse).fromNow();
    }

});

Vue.filter('time', function (date, format = 'hh:mm A', parse = 'YYYY-MM-DD HH:mm:ss', utc = true) {
    if (!date) {
        return 'N/A'
    }

    if (utc) {
        return moment.utc(date, parse).local().format(format);
    } else {
        return moment(date, parse).format(format);
    }
});

Vue.filter('weekdayDate', function (day, format = null) {
    if (!day) {
        return 'N/A'
    }
    if (!format) {
        format = 'YYYY/MM/DD';
    }
    return moment().day(day).format(format);
});

Vue.filter('firstWords', function (str, number = null) {
    if (!str) {
        return 'N/A';
    }
    if (!number) {
        number = 2;
    }
    return str.replace(/(<([^>]+)>)/ig, " ").trim().split(/\s/).slice(0, number).join(" ");
});