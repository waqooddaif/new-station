export default {
    // Get a full list of all active services
    getActiveServices(apiList) {
        return Vue.http.get(apiList['service.active'])
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

}