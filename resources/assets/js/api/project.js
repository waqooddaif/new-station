export default {
    // Get a full list of all user projects
    getProjects(apiList) {
        return Vue.http.get(apiList['projects.index'])
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },
    // Get a project related records
    getProjectRecords(apiList, id) {
        let url = apiList['projects.show'].replace(/\{id\}/g, id);
        return Vue.http.get(url)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },
    // Update a project record
    updateProject(apiList, id, data) {
        let url = apiList['projects.update'].replace(/\{id\}/g, id);
        return Vue.http.put(url, data)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // Create a project
    createProject(apiList, data) {
        return Vue.http.post(apiList['projects.create'], data)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // Add an expense to a project
    addExpense(apiList, id, data) {
        let url = apiList['projects.expenses'].replace(/\{id\}/g, id);
        return Vue.http.post(url, data)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // delete an expense
    deleteExpense(apiList, id) {
        let url = apiList['projects.expenses.item'].replace(/\{id\}/g, id);
        return Vue.http.delete(url)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // Add a message to a project
    addMessage(apiList, id, newMessage) {
        let url = apiList['projects.message.add'].replace(/\{id\}/g, id);
        return Vue.http.post(url, newMessage)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // change a message approval state
    approveMessage(apiList, message) {
        let url = apiList['message.approve'].replace(/\{id\}/g, message.id);
        return Vue.http.put(url, message)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // change a message visibility
    changeMessageVisibility(apiList, message, target) {
        let url = apiList['message.visibility'].replace(/\{id\}/g, message.id);
        return Vue.http.put(url, {target: target})
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // Make a message done
    makeActionDone(apiList, message) {
        let url = apiList['message.done'].replace(/\{id\}/g, message.id);
        return Vue.http.put(url)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // change a message service
    updateMessageService(apiList, data) {
        let url = apiList['message.update'].replace(/\{id\}/g, data.id);
        return Vue.http.put(url, data)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // Add a member to a project
    addMember(apiList, id, newMember) {
        let url = apiList['projects.member.add'].replace(/\{id\}/g, id);
        return Vue.http.post(url, newMember)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // Add a service to a project
    addService(apiList, id, newService) {
        let url = apiList['projects.service.add'].replace(/\{id\}/g, id);
        return Vue.http.post(url, newService)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // Add a service to a project
    updateService(apiList, id, service) {
        let url = apiList['projects.service.update'].replace(/\{id\}/g, id);
        return Vue.http.put(url, service)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // remove a service from a project
    removeService(apiList, id, projectServiceId) {
        let url = apiList['projects.service.remove'].replace(/\{id\}/g, id).replace(/\{serviceid\}/g, projectServiceId);
        return Vue.http.delete(url)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

    // remove a member from a project
    removeMember(apiList, id, projectMemberId, projectServiceId) {
        let url = apiList['projects.member.remove'].replace(/\{id\}/g, id).replace(/\{memberid\}/g, projectMemberId).replace(/\{projectserviceid\?\}/g, projectServiceId);
        return Vue.http.delete(url)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

}