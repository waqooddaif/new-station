export default {
    // Get a full list of all companies
    fetchList(apiList) {
        return Vue.http.get(apiList['company.index'])
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },
    // Get a full list of all companies
    createCompany(apiList, data) {
        return Vue.http.post(apiList['company.create'], data)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },
    // Get a full list of all companies
    createClient(apiList, data) {
        return Vue.http.post(apiList['client.create'], data)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

}