export default {
    // Create new user
    createUser(apiList, data) {
        return Vue.http.post(apiList['member.create'], data)
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },
    // Get all members week schedule default to current week schedule
    getMembersWeekSchedule(apiList, yearN=null, weekN=null) {
        return Vue.http.get(apiList['members.schedule'].replace(/\{year\?\}/g, yearN).replace(/\{week\?\}/g, weekN))
            .then((response) => Promise.resolve(response.data))
            .catch((error) => Promise.reject(error));
    },

}