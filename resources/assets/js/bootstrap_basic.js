
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin.
 */

window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

/**
 * retinajs
 * http://imulus.github.io/retinajs/
 */
require('retinajs');