require('./bootstrap_basic');
require('./bootstrap_full');
import { sync } from 'vuex-router-sync'
import store from './store'

const router = require('./router');

const NavView = require('./components/common/NavView.vue');

sync(store, router);

new Vue({
    router,
    store,
    components: {
        NavView
    }
}).$mount('#app');
