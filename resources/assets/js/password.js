import retinajs from 'retinajs';
import Vue from 'vue';

const app = new Vue({
    el: '#app',
    data() {
        return {
            // msg: 'Strong password must contain small and capital letters, numbers, and any of the special characters \
            //     [ ] @ - & # ( ) * ^ % ! ~ " \'',
            msg: 'Password must contain letters and numbers',
            email: emailValue,
            password: null,
            passwordConfirm: null,
            passwordError: passwordHasError ? passwordHasError : false,
            passwordErrorMsg: passwordErrorMessage ? passwordErrorMessage : null,
            passwordSuccess: false,
            passwordConfirmError: passwordConfirmHasError ? passwordConfirmHasError : false,
            passwordConfirmErrorMsg: passwordConfirmErrorMessage ? passwordConfirmErrorMessage : null,
            passwordConfirmSuccess: false,
            passwordTotalSuccess: false
        }
    },
    watch: {
        'password': 'checkPassword',
        'passwordConfirm': 'checkPasswordConfirm'
    },
    mounted() {
        retinajs( $('img') );
        if (this.password) {
            this.checkPassword(this.password);
        }
    },
    methods: {
        checkPassword(val) {
            let errorBag = '';

            if (val.length < 8) {
                errorBag += "Must be at least 8 characters"
            }

            // if (!/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\[\]@\-\&\#\(\)\*\^\%\!\~\"]).+$/.test(val)) {
            if (!/^(?=.*[a-zA-Z]).+$/.test(val)) {
                let msg = "Must contain letters and numbers";
                errorBag = errorBag ? errorBag + " and " + msg : msg;
            }

            if (errorBag) {
                this.passwordError = true;
                this.passwordSuccess = false;
                this.passwordErrorMsg = errorBag;
            } else {
                this.passwordError = false;
                this.passwordSuccess = true;
                this.passwordErrorMsg = null;
            }
        },
        checkPasswordConfirm(val) {
            this.passwordConfirmError = val !== this.password;
            this.passwordConfirmErrorMsg = (val !== this.password) ? 'Passwords must be matched' : null;
            this.passwordConfirmSuccess = val === this.password;
            this.passwordTotalSuccess = this.passwordSuccess && this.passwordConfirmSuccess;
        },
        submitForm() {
            let email = document.getElementById("email");
            email.setAttribute("name", "email");
            email.setAttribute("value", this.email);
            let password = document.getElementById("password");
            password.setAttribute("name", "password");
            password.setAttribute("value", this.password);
            let passwordConfirm = document.getElementById("password-confirm");
            passwordConfirm.setAttribute("name", "password_confirmation");
            passwordConfirm.setAttribute("value", this.passwordConfirm);

            event.target.submit();
        }
    }
});