@extends('layouts.password')

@section('content')
    <h1>
        @if(isset($type))
            Set up your password
        @else
            Reset Password
        @endif
    </h1>

    <br>

    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/invite/reset') }}"
          @submit.prevent="submitForm">
        {{ csrf_field() }}

        <input type="hidden" name="token" value="{{ $token }}">
        <input type="hidden" name="type" value="{{ isset($type) ? $type : '' }}">

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">E-Mail Address</label>

            <input id="email" type="email" class="form-control" v-model="email"
                   required autofocus>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div :class="{'form-group': true, 'has-error': passwordError, 'has-success': passwordSuccess}">
            <label for="password">Password <i class="glyphicon glyphicon-info-sign" :title="msg"></i> </label>

            <input id="password" type="password" class="form-control" v-model="password" required>

            <span class="help-block" v-show="passwordError">
                <strong>@{{ passwordErrorMsg }}</strong>
            </span>
        </div>

        <div :class="{'form-group': true, 'has-error': passwordConfirmError, 'has-success': passwordConfirmSuccess}">
            <label for="password-confirm">Confirm Password</label>

            <input id="password-confirm" type="password" class="form-control"
                   v-model="passwordConfirm" :disabled="!passwordSuccess" required>

            <span class="help-block" v-show="passwordConfirmError">
                <strong>@{{ passwordConfirmErrorMsg }}</strong>
            </span>
        </div>

        <br>

        <button type="submit" class="btn btn-success btn-block btn-lg bold" :disabled="!passwordTotalSuccess">
            @if(isset($type))
                Set password
            @else
                Reset Password
            @endif
        </button>
    </form>
@endsection
