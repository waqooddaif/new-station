@extends('layouts.login')

@section('content')

    <h1>Log in</h1>
    <br>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}

        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">E-Mail Address</label>

            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                   autofocus>

            @if ($errors->has('email'))
                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
            @endif
        </div>

        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password">Password</label>

            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
            @endif
        </div>

        <br>

        <button type="submit" class="btn btn-lg btn-success btn-block bold">
            Login
        </button>

        <br>

        <div class="form-group">
            <input type="checkbox" id="remember" name="remember" value="false">
            <label for="remember">Remember Me</label>
        </div>

        <div class="row">
            <a class="btn btn-link" href="{{ url('/password/reset') }}">
                Forgot Your Password?
            </a>
        </div>
    </form>
@endsection
