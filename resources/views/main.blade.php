@extends('layouts.spa')

@section('production-script')
    <script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@endsection

@section('dev-script')
<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@endsection

@section('content')
    <div id="app">
        {{--nav bars component, located at assets/js/components/common/NavView.vue--}}
        <nav-view user-obj="{{ json_encode($user) }}"
                  app-info="{{ json_encode(['env' => config('app.env')]) }}"
                  jira="{{ json_encode(['url' => config('services.jira.base_uri')]) }}"
                  api-list="{{ json_encode(config('station.api')) }}"
                  action-types="{{ json_encode(config('station.actions')) }}"
                  status-types="{{ json_encode(config('station.statuses')) }}"
                  services-list="{{ json_encode($services_list) }}"
                  members-list="{{ json_encode($members_list) }}"
                  user-roles-list="{{ json_encode($user_roles_list) }}"
                  attachments-rules="{{ json_encode(config('station.attachments')) }}"
                  roles-list="{{ $user['permissions']['project.create'] ? json_encode(config('station.roles')) : '[]' }}"
        >
        </nav-view>
        {{--router app, located at assets/js/app.js--}}
        <div id="major-content-container">
            <router-view></router-view>
        </div>

    </div>
@endsection
