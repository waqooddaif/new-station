<!DOCTYPE html>
<html lang="en">
<head>
    <title>Link Jira with Station</title>
</head>
<body>

@if($status === 'success')
<h2 style="padding-left: 37%">Authenticated successfully with Jira.</h2>
@else
<h2 style="padding-left: 37%; color: red">Authentication has failed with Jira!</h2>
@endif

<script>
    setTimeout(function () {
        window.close()
    }, 4000)
</script>
</body>
</html>