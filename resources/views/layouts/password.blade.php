<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Waqood Station') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/vendors.css') ?: url('css/vendors.min.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') ?: url('css/app.min.css') }}" rel="stylesheet">

    <!--[if lte IE 8]>
    <link href="{{ url('css/ie8.css') }}" rel="stylesheet">
    <![endif]-->

</head>
<body id="login">

<div id="ie-notification">
    <h1>Your browser is out dated!</h1>
    <p>
        We recommend you any of the modern browsers for better security and performance.
        <br>
        <br>
        Try
        <a href="http://www.google.com/chrome">Google Chrome</a> or
        <a href="http://getfirefox.com">Mozilla Firefox</a>
    </p>
</div>

<div id="app">

    <div id="logo">
        <img src="{{ url('images/logo.png') }}" alt="Waqood Station" data-rjs="3">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-lg-offset-4_5 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <div class="panel panel-default flat borderless">
                    <div class="panel-body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row text-center">
        <div id="footer">
            &copy; <?php echo date('Y'); ?> Waqood Station
        </div>
    </div>
</div>


<script>
    var emailValue = "{{ $email or old('email') }}";
    var passwordHasError = "{{ $errors->has('password') ? true : false }}";
    var passwordErrorMessage = "{{ $errors->has('password') ? $errors->first('password') : null }}";
    var passwordConfirmHasError = "{{ $errors->has('password_confirmation') ? true : false }}";
    var passwordConfirmErrorMessage = "{{ $errors->has('password_confirmation') ? $errors->first('password_confirmation') : null }}"
</script>

<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/password.js') }}"></script>
</body>
</html>
