<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=800">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- App Enviroment -->
    <meta name="app-env" content="{{ config('app.env') }}">

    <title>{{ config('app.name', 'Waqood Station') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/vendors.css') ?: url('css/vendors.min.css') }}" rel="stylesheet">
    <link href="{{ mix('css/app.css') ?: url('css/app.min.css') }}" rel="stylesheet">

    <!--[if lte IE 8]>
    <link href="{{ url('css/ie8.css') }}" rel="stylesheet">
    <![endif]-->

</head>
<body>
{{--show this only if the user browser is IE 8 or older--}}
<div id="ie-notification">
    <h1>Your browser is out dated!</h1>
    <p>
        We recommend you any of the modern browsers for better security and performance.
        <br>
        <br>
        Try
        <a href="http://www.google.com/chrome">Google Chrome</a> or
        <a href="http://getfirefox.com">Mozilla Firefox</a>
    </p>
</div>

<div>

    @yield('content')

</div>

<!-- Scripts -->
{{--Enviroment setting can be found in .env file--}}
@if(env('APP_ENV') == 'production')
    @php ($script_path = url('js/app.js'))
    @yield('production-script', '<script src="'.$script_path.'"></script>')
@else
    @php ($script_path = url('js/app.js'))
    @yield('dev-script', '<script src="'.$script_path.'"></script>')
@endif
</body>
</html>
