<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Waqood Station') }}</title>

    <!-- Styles -->
    @if(env('APP_ENV') == 'production')
        <link href="{{ url('css/vendors.min.css') }}" rel="stylesheet">
        <link href="{{ url('css/app.min.css') }}" rel="stylesheet">
    @else
        <link href="{{ url('css/vendors.css') }}" rel="stylesheet">
        <link href="{{ url('css/vendors.css.map') }}" rel="stylesheet">
        <link href="{{ url('css/app.css') }}" rel="stylesheet">
        <link href="{{ url('css/app.css.map') }}" rel="stylesheet">
    @endif

    <!--[if lte IE 8]>
    <link href="{{ url('css/ie8.css') }}" rel="stylesheet">
    <![endif]-->

</head>
<body>

<div id="ie-notification">
    <h1>Your browser is out dated!</h1>
    <p>
        We recommend you any of the modern browsers for better security and performance.
        <br>
        <br>
        Try
        <a href="http://www.google.com/chrome">Google Chrome</a> or
        <a href="http://getfirefox.com">Mozilla Firefox</a>
    </p>
</div>

<div>
    <nav class="navbar navbar-themed" id="top-nav">
        <div class="container-fluid">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a class="navbar-brand" href="#">
                    <img src="images/logo.png" alt="Waqood Station" data-rjs="3">
                </a>
            </div>
            <div>
                <ul class="nav navbar-nav">
                    <li>
                        <select id="projects-quick-links" class="form-control">
                            <option>Project 1</option>
                            <option>Project 2</option>
                            <option>Project 3</option>
                        </select>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        {{--<li><a href="{{ url('/register') }}">Register</a></li>--}}
                    @else

                        <li id="notifications-menu">
                            <a href="#" id="notifications-menu-button" class="dropdown-toggle">
                                <span class="icon-sm icon-notification-white">
                                    <span class="notification-badge">2</span>
                                </span>
                            </a>
                        </li>

                        <li id="user-id-card">
                            <div class="avatar-circle-sm avatar-border" style="background-color: #92278F;">
                                <span class="initials">{{ $user->initials }}</span>
                            </div>
                            <div class="content">
                                <div class="name">
                                    {{ $user->name }}
                                </div>
                                <div class="title">
                                    {{ $user->role_description }}
                                </div>
                            </div>
                        </li>

                        <li id="user-settings-menu">
                            <a href="#" id="user-settings-menu-button">
                                <span class="icon-sm icon-menu-white"></span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <nav class="navbar navbar-themed" id="second-nav">
        <div class="container-fluid">
            <div>
                <ul class="nav navbar-nav">
                    <li @if(active('projects.main'))class="active"@endif>
                        <a href="{{ route('projects.main') }}">
                            Projects
                        </a>
                    </li>
                    <li>
                        <a href="#">Actions</a>
                    </li>
                    <li>
                        <a href="#">Messages</a>
                    </li>
                    <li>
                        <a href="#">Activities</a>
                    </li>
                    <li>
                        <a href="#">Team</a>
                    </li>
                    <li>
                        <a href="#">Planner</a>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
    </nav>

    <div class="container-fluid">
        @yield('content')
    </div>

    <!-- Notifications menu -->


    <div id="notifications-menu-dropdown-content" class="hide">
        <ul id="notifications-menu-dropdown">
            <li class="row">
                <div class="avatar-circle-sm" style="background-color: #C3B49D;">
                    <span class="initials">EG</span>
                </div>
                <div class="content">
                    <div class="desciption">
                        <span>Eman <strong>deleted</strong> a comment</span>
                    </div>
                    <div class="date">
                        18 H
                    </div>
                </div>
            </li>
            <li class="row">
                <div class="avatar-circle-sm" style="background-color: #92278F;">
                    <span class="initials">WE</span>
                </div>
                <div class="content">
                    <div class="desciption">
                        <span>Wahib <strong>published a new post</strong>
                            on <a href="#">Cooper</a> project</span>
                    </div>
                    <div class="date">
                        1 Day
                    </div>
                </div>
            </li>
            <li class="see-all">
                <a href="#">
                    See all notifications
                </a>
            </li>
        </ul>
    </div>
    <!-- End of the notifications menu -->

    <!-- User popover menu -->
    <div id="user-settings-menu-dropdown-content" class="hide">
        <ul id="user-settings-menu-dropdown" class="dropdown-menu">
            <li>
                <a href="#">
                    <span class="icon-sm icon-profile"></span> Profile Settings
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="icon-sm icon-settings"></span> Account Settings
                </a>
            </li>
            <li>
                <a href="#">
                    <span class="icon-sm icon-help"></span> Help
                </a>
            </li>
            <li role="separator" class="divider"></li>
            <li>
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();"
                >
                    <span class="icon-sm icon-logout"></span> Logout
                </a>

                <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
    <!-- End of user popover menu -->

</div>

<!-- Scripts -->
@if(env('APP_ENV') == 'production')
    @php ($script_path = url('js/app.min.js'))
    @yield('production-script', '<script src="'.$script_path.'"></script>')
@else
    @php ($script_path = url('js/app.js'))
    @yield('dev-script', '<script src="'.$script_path.'"></script>')
@endif
</body>
</html>
