
# Waqood Station
A tool for the communication between the project manager and 
clients, it manages project and their services, payments,tasks 
assigned to clients and progress.

# Readme Index
1. [Frameworks used](#frameworks-user)
2. [installation](#installation)
3. [structure](#structure)
4. [usage](#usage)
    - [Commands](#commands)
5. [Roles and Permissions](#roles-and-permissions)
6. [Generate Demo Data](#generate-demo-data)
7. [Notes](#notes)

# Frameworks used
1. Laravel 5.3
2. Auzo laravel package 1.1
3. Relational database
4. Vuejs 2 (Vue resource, Vue router, Vuex)
5. Vueify for Vue components development
6. Twitter Bootstrap
7. Jquery
8. SCSS for CSS development
9. Webpack for frontend packaging
10. Retinajs for retina images
11. quill as WYGIWYS editor
12. webui-popover for nav and others popover menus

# Installation
1. clone the repo
2. do command `composer install`
3. create a new database
4. copy .env.example file to .env
5. adjust the .env configurations
6. do command `php artisan migrate`
7. fill the database with initial data (users, roles, permissions) by issuing this command `php artisan db:seed`
8. make the domain points to public directory path
9. `storage` and the `bootstrap/cache` directories should be writable by your web server
10. for development environment, do `npm install` and `composer install --dev`
11. create a supervisor worker for Laravel queues executions
12. create a cronjob for Laravel scheduler
13. php.ini `post_max_size` && `upload_max_filesize` && nginx.conf `client_max_body_size` must be > 50 MB

## Supervisor worker Setup

```shell
# /etc/supervisor/conf.d/laravel-worker.conf

[program:laravel-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /var/www/station.waqood.sa/artisan queue:listen --queue=default --sleep=3 --tries=3
autostart=true
autorestart=true
user=www-data
numprocs=2
redirect_stderr=true
stdout_logfile=/var/www/station.waqood.sa/storage/logs/worker.log
```

```shell
apt-get install supervisor
service supervisor restart
supervisorctl reload
supervisorctl reread
supervisorctl update
supervisorctl start laravel-worker:*

# Make sure Supervisor comes up after a reboot.
sudo systemctl enable supervisor

# Bring Supervisor up right now.
sudo systemctl start supervisor
```

## Create cronjob for Laravel scheduler
```shell
* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```


# Structure
1. All frontend source files are located at `resources` dir
2. All frontend Single-Page-Application work is divided into Vue modules and are located in `resources/assets/js`
2. All application classes are located at `app` dir
3. Tests files are located at `tests` dir
4. Routes are divided into two files `routes/web.php` for web interface, and `routes/api.php` for API routes
5. `vendor` includes all third parties and composer packages

# Usage
- Most of the tasks have or will have its web UI interface
- For managing tasks that do not yet have their own web UI, you can use [Laravel artisan command](https://laravel.com/docs/5.3/artisan), from shell session. go to the project root, and issue `php artisan` for all commands list.
- Another (not recommended) way to manage the backend data, is by using [artisan tinker](http://laravel-recipes.com/recipes/280/interacting-with-your-application) to interact directly with the data models.
## Commands 
(until the backend finishes)

all commands are done from the app root
```php
// go the root path
cd /var/www/station/back
```

Create new member user (user and assign a member role)
```php
 // log in to a tinker session
 php artisan tinker
 
$user = App\User::create(['name' => 'User Full Name', 'email' => 'email@example.com', 'password' => bcrypt('password')]);
$user->assignRole('waqood.member');

// or edit a user by its ID, ex user id is 1

$user = App\User::find(1)
$user->name = 'a new name';
$user->password = bcrypt('a-new-password');
$user->save();
```

Create new project
```php
 // log in to a tinker session
 php artisan tinker
 
 $project = App\Project::create([
'name' => "Project name",
'date_start' => '2017-01-07',
'date_final' => '2017-03-01',
'date_archived' => null,
'pm_id' => 1,
'tech_id' => 2,
'client_id' => 10,
'company_id' => 1
 ]);
 
 // or edit a project by its ID
 $project = App\Project::find(2)
 // change the project pm user
 $project->pm_id = 3;
 $project->save()
```
 
Create new service
```php
 // log in to a tinker session
 php artisan tinker
 
 $service = App\Service::create(['name' => 'service name']);
```
 
Roles and permissions commands (no tinker is needed)
```shell
# create new role
php artisan auzo:role create 'testRole' --description='testing role'
# delete role
php artisan auzo:role delete 'testRole'

# assign role to users
php artisan auzo:user assign '1,2,3' 'SomeRole'
# revoke role from users
php artisan auzo:user revoke '1,2,3' 'SomeRole'

# Using auzo:permission artisan command to manage permissions:
php artisan auzo:permission give 'testRole' 'ability.test,ability.test2' --policies='1,2:||'
```

# Roles and Permissions
- Default roles: 
    - waqood.admin (not usable yet)
    - waqood.pm (project manager)
    - waqood.lead (Technical Lead)
    - waqood.member (Team member)
    - client.member (Client)
- Default abilities (that you can enable/disable for each role):
    - wq.projects.index.own (can index only projects they have been to as members)
    - wq.projects.index.pm (can index only projects they manage or as technical leads.)
    - wq.projects.index.all (can index all projects)
    - project.show ( can show a project)
    - project.create (can create a new project)
    - project.update (can add/remove services and members of the project)
    - project.show.members (can show a project members)
    - message.show (show a project messages)
    - message.create (can create a new general message)
    - message.update (can update a project message, like message visibility and status)
    - message.approve (can approve a project message)
    - action.create (can create a new action message)
    - action.update (can update a project action message, like message visibility and status)
    - client.projects.index.own (can index only projects they manage as clients)
    - client.projects.index.company (can index all projects under their company)
- Available Policies (You can give any ability to any roles, and restrict the ability by a policy, so the role won't be able to do the ability unless the role passes the policy restrictions)
    1. Project PM (Insure the user is the project manager)
    2. Project techLead (Insure the user is the project tech lead)
    3. Project member (Insure the user is a member of the project)
    4. Project client (Insure the user is the client of the project)
    5. Project company member (Insure the user is a member pf the client company of the project)
    6. Owner (Insure the user is the creator of the resource)
    7. Message team visible (Insure the message has "visible_team" flag)
    8. Message client visible (Insure the message has "visible_client" flag)
- Default Permissions (What each role can do)
    - You can review the default permissions for each role from the [PermissionSeeder file](https://bitbucket.org/waqood/station-backend/src/ff954e3518fd143882b00221601efe48e632e009/database/seeds/PermissionSeeder.php?at=master&fileviewer=file-view-default)

# Generate Demo Data
You can generate a demo data (users, roles, projects, ..etc) by issuing this artisan command:
```shell
php artisan db:seed --class=DemoDatabaseSeeder
```

# Notes:
1. The application supports only modern browsers, IE8 browser will be blocked and get a message with links to modern browsers.
2. You can modify the initial or demo data seeders before you do `php artisan db:seed` by modifying the seeder files at `database/seeds`