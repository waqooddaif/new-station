<?php

namespace Modules\Core\Entities\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    use CrudTrait;
}
