<?php

Route::group(['middleware' => ['web','auth'], 'namespace' => 'Modules\Core\Http\Controllers'], function()
{
    // error test
    Route::get('error', function(){
        if (config('app.env') !== 'production')
            throw new Exception('Error Testing!!');
    });

    Route::get('/session-refresh', function() {
        return response('', 204);
    });

    // upload file
    Route::post('/call/file', 'FilesController@upload')
        ->name('file.upload');
});
