<?php

namespace Modules\Core\Http\Controllers;

class ApiTransformer extends Controller
{
    public static $code = [
        'success' => 200,
        'not-authorized' => 403,
        'not-valid' => 422,
        'server-error' => 500,
    ];

    /**
     * Return a success json response.
     *
     * @param $data
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public static function success($data)
    {
        return response(json_encode($data), self::$code['success'])
            ->header('Content-Type', 'application/json');
    }

    /**
     * Return an authorization failed json response.
     *
     * @param null $msg
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public static function notAuthorized($msg = null)
    {
        if (!$msg) {
            $msg = 'Not authorized access!';
        }

        return response(json_encode(['message' => $msg]), self::$code['not-authorized'])
            ->header('Content-Type', 'application/json');
    }

    /**
     * Return validation errors in json.
     *
     * @param $errors
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public static function validationError($errors)
    {
        return response(json_encode([$errors]), self::$code['not-valid'])
            ->header('Content-Type', 'application/json');
    }

    public static function error($msg = 'Server Error', $code = null)
    {
        $code = $code ?: self::$code['server-error'];
        return response(json_encode(['message' => $msg]), $code)
            ->header('Content-Type', 'application/json');
    }
}
