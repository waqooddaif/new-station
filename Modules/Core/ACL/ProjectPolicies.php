<?php

namespace Modules\Core\ACL;

use Modules\Projects\Entities\Models\Message;
use Modules\Projects\Entities\Models\Project;
use Modules\Users\Entities\Models\User;

class ProjectPolicies
{
    /**
     * Check if the user is the assigned project manager.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param null $model
     * @return bool
     */
    public function pm($ability, $role, User $user, $model = null)
    {
        if (!$model) {
            return $user->type === 'member';
        }
        if ($model instanceof Project) {
            return $user->id === $model->pm_id;
        }
        // if $model is not Project instance, $model should has project relationship
        return $user->id === $model->project->pm_id;
    }

    /**
     * Check if the user is the assigned project technical lead.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param null $model
     * @return bool
     */
    public function techLead($ability, $role, User $user, $model = null)
    {
        if (!$model) {
            return $user->type === 'member';
        }
        if ($model instanceof Project) {
            return $user->id === $model->tech_id;
        }
        // if $model is not Project instance, $model should has project relationship
        return $user->id === $model->project->tech_id;
    }

    /**
     * Check if the user is a member in any of the project services.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param null $model
     * @return bool
     */
    public function member($ability, $role, User $user, $model = null)
    {
        if (!$model) {
            return $user->type === 'member';
        }

        if ($model instanceof Project) {
            return $user->projects->find($model->id) !== null;
        }
        // if $model is not Project instance, $model should has project relationship
        return $user->projects->find($model->project->id) !== null;
    }

    /**
     * Check if the user is teh assigned project client.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param null $model
     * @return bool
     */
    public function client($ability, $role, User $user, $model = null)
    {
        if (!$model) {
            return $user->type === 'client';
        }
        if ($model instanceof Project) {
            return $user->id === $model->client_id;
        }
        // if $model is not Project instance, $model should has project relationship
        return $user->id === $model->project->client_id;
    }

    /**
     * Check if the user is member of the project company.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param null $model
     * @return bool
     */
    public function companyMember($ability, $role, User $user, $model = null)
    {
        if (!$model) {
            return $user->type === 'client';
        }

        if ($model instanceof Project) {
            return $user->company()->id === $model->company_id;
        }
        // if $model is not Project instance, $model should has project relationship
        return $user->company()->id === $model->project->company_id;
    }

    /**
     * Compares model user_id with user id.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param null $model
     * @return bool
     */
    public function owner($ability, $role, User $user, $model = null)
    {
        if (!$model) {
            return true;
        }
        return $user->id === $model->user_id;
    }

    /**
     * If message has visible_team flag.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param Message|null $model
     * @return bool
     */
    public function msgTeamVisible($ability, $role, User $user, Message $model = null)
    {
        if (!$model) {
            return true;
        }
        return $model->visible_team;
    }

    /**
     * If message has visible_client flag.
     *
     * @param $ability
     * @param $role
     * @param User $user
     * @param Message|null $model
     * @return bool
     */
    public function msgClientVisible($ability, $role, User $user, Message $model = null)
    {
        if (!$model) {
            return true;
        }
        return $model->visible_client;
    }
}