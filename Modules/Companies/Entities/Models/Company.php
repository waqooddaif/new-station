<?php

namespace Modules\Companies\Entities\Models;

use Modules\Core\Entities\Models\BaseModel;
use Modules\Users\Entities\Models\User;
use Modules\Projects\Entities\Models\Project;

class Company extends BaseModel
{
    protected $fillable = ['name', 'website', 'logo'];

    /**
     * Get the company employees users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clients()
    {
        return $this->belongsToMany(User::class, 'clients');
    }

    /**
     * Attach a client user account to this company.
     *
     * @param array $client_ids
     * @return $this
     */
    public function addClients($client_ids)
    {
        $client_ids = is_int($client_ids) ? [$client_ids] : $client_ids;

        $this->clients()->syncWithoutDetaching($client_ids);

        return $this;
    }

    /**
     * Get company projects.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
