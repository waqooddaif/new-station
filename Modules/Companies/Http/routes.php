<?php

Route::group(['middleware' => ['web','auth'], 'namespace' => 'Modules\Companies\Http\Controllers'], function () {
    /**
     * Companies API, starts with path url/call/company
     */
    // companies full list
    Route::get('/call/company', 'CompaniesController@index')->name('company.index');
    // create a new company
    Route::post('/call/company', 'CompaniesController@store')->name('company.create');
});
