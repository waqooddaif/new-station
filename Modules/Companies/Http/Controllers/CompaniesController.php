<?php

namespace Modules\Companies\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Http\Controllers\ApiTransformer;

use Modules\Companies\Entities\Models\Company;


class CompaniesController extends Controller
{
    /**
     * Get a full list of all companies.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->cannot('company.index')) {
            ApiTransformer::notAuthorized();
        }

        return ApiTransformer::success(
            Company::with('clients')
                ->get()
                ->toArray()
        );
    }

    /**
     * Create a new company.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user->cannot('company.create')) {
            ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'name' => 'required|string',
            'website' => 'required|string',
            'logo' => 'required|string',
            'imageType' => 'required|string'
        ]);

        $logoDir = 'uploads/clients/c' . time() . '.' . $request->imageType;

        $logoImg = str_replace('data:image/jpeg;base64,', '', $request->logo);
        $logoImg = str_replace('data:image/pneg;base64,', '', $logoImg);
        $logoImg = str_replace('data:image/' . $request->imageType . ';base64,', '', $logoImg);
        $logoImg = str_replace(' ', '+', $logoImg);
        $logoImg = base64_decode($logoImg);
        $uploaded = Storage::disk('public')->put('/' . $logoDir, $logoImg);
        if ($uploaded) {
            $data = $request->only(['name', 'website']);
            $data['logo'] = $logoDir;
            return ApiTransformer::success(
                Company::create($data)
            );
        } else {
            return ApiTransformer::error('Logo could not be uploaded');
        }

    }
}
