<?php

Route::group(['middleware' => ['web','auth'], 'namespace' => 'Modules\Home\Http\Controllers'], function()
{
    /**
     * widecard catch-all
     */
    Route::get('/', 'HomeController@main')->name('main');
    Route::get('/home', 'HomeController@main')->name('home');
    Route::get('/projects/{id?}', 'HomeController@main')->name('projects');
    Route::get('/companies/{id?}', 'HomeController@main')->name('companies');
    Route::get('/company/{id?}', 'HomeController@main')->name('company');
    Route::get('/clients/{id?}', 'HomeController@main')->name('clients');
    Route::get('/client/{id?}', 'HomeController@main')->name('client');
    Route::get('/members/{id?}', 'HomeController@main')->name('members');
    Route::get('/member/{id?}', 'HomeController@main')->name('member');
    Route::get('/planner/{year?}/{week?}', 'HomeController@main')->name('planner');
});
