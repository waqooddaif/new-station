<?php

namespace Modules\Home\Http\Controllers;

use Modules\Core\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function main()
    {
        return view('main');
    }
}
