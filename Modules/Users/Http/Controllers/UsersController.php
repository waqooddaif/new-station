<?php

namespace Modules\Users\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Http\Controllers\ApiTransformer;
use Modules\Users\Entities\Models\User;
use App\Notifications\UserInvitation;

class UsersController extends Controller
{
    /**
     * Create new client and send an invitation.
     *
     * @param Request $request
     * @return \App\User|\Illuminate\Contracts\Routing\ResponseFactory|null|\Symfony\Component\HttpFoundation\Response
     */
    public function storeClient(Request $request)
    {
        $user = \Auth::user();

        if ($user->cannot('client.create')) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'company_id' => 'required|exists:companies,id'
        ]);

        $data = $request->only(['name', 'email', 'color', 'description']);
        $data['role_id'] = config('station.roles')['client'];
        $data['type'] = config('station.usersTypes')['client'];

        $user = $this->createAndInvite($data);

        $user->addToCompany($request->input('company_id'));

        return ApiTransformer::success($user);
    }

    /**
     * Create new member and send an invitation.
     *
     * @param Request $request
     * @return \App\User|\Illuminate\Contracts\Routing\ResponseFactory|null|\Symfony\Component\HttpFoundation\Response
     */
    public function storeMember(Request $request)
    {
        $user = \Auth::user();

        if ($user->cannot('member.create')) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'role_id' => 'required|exists:roles,id'
        ]);

        $data = $request->only(['name', 'email', 'color', 'description', 'role_id']);
        $data['type'] = config('station.usersTypes')['member'];

        $user = $this->createAndInvite($data);

        return ApiTransformer::success($user->load('role')->makeVisible(['role_id', 'role'])->toArray());
    }

    /**
     * Create a user account and send an invitation email.
     *
     * @param $data
     * @return User
     */
    public function createAndInvite($data)
    {
        $data['password'] = isset($data['password']) ? $data['password'] : null;
        $user = User::create($data);
        $token = Password::broker()->createToken($user);
        $user->notify(new UserInvitation($token));
        return $user;
    }

    /**
     * Get all members and their assigned services.
     *
     * @param integer|null $week
     * @param integer|null $year
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function weekSchedule($year = null, $week = null)
    {
        $user = \Auth::user();
        if ($user->cannot('planner.view')) {
            return ApiTransformer::notAuthorized();
        }
        $week_period = getStartAndEndDate($week, $year);

        $members = User::isMember()->with(['projectServices' => function($query) use($week_period) {
            return $query->with('project', 'service')->where('date_start', '<', $week_period['end'])
                        ->where('date_finish', '>', $week_period['start'])
                        ->orderBy('date_start');
        }])->get();

        return ApiTransformer::success($members->toArray());
    }

}
