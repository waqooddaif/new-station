<?php

Route::group(['middleware' => ['web','auth'], 'namespace' => 'Modules\Users\Http\Controllers'], function()
{
    /**
     * Members API, starts with path url/call/member
     */
    // create a new member
    Route::post('/call/member', 'UsersController@storeMember')
        ->name('member.create');
    // get all members and their assigned services
    Route::get('/call/member', 'UsersController@weekSchedule')
        ->name('members.schedule');

    /**
     * Clients API, starts with path url/call/client
     */
    // create a new client
    Route::post('/call/client', 'UsersController@storeClient')
        ->name('client.create');
});
