<?php
/**
 * Jira addon routes
 */

Route::group(['middleware' => ['web','auth'], 'prefix' => 'jira', 'namespace' => 'Modules\JIRA\Http\Controllers'], function () {
    Route::get('tokens', 'JiraApiController@tokens');
    Route::get('user', 'JiraApiController@userInfo');
    Route::get('projects', 'JiraApiController@projects');
    Route::post('issue', 'JiraApiController@createIssue');
});
