<?php

namespace Modules\JIRA\Http\Controllers;

use Illuminate\Http\Request;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Http\Controllers\ApiTransformer;
use Modules\Projects\Entities\Models\Message;

class JiraApiController extends Controller
{
    protected $api_client;

    public function __construct(JiraAPIClient $APIClient)
    {
        $this->api_client = $APIClient;
    }

    /**
     * Get Jira tokens for the current user
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function tokens()
    {
        $user = \Auth::user();
        $data = [];
        if ($user->has_jira) {
            $data['has_jira'] = true;
            $data['tokens'] = $user->jiraTokens;
            return ApiTransformer::success($data);
        }

        return response('', 403);
    }

    /**
     * Get all projects for the current user
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function projects()
    {
        $results = $this->api_client->projects();

        return $this->processApiResults($results);
    }

    /**
     * Create new issue on jira and link it to a message
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function createIssue(Request $request)
    {
        $message = Message::findOrFail($request->input('message_id'));

        $data = [
            "fields" => [
                "project" => [
                    "key" => $request->input('key')
                ],
                "summary" => strip_tags($request->input('summary')),
                "description" => strip_tags($request->input('description')),
                "issuetype" => [
                    "name" => $request->input('type') ?: "Bug"
                ]
            ]
        ];

        $results = $this->api_client->createIssue($data);

        if (key_exists('key', $results)) {
            $message->update(['jira_key' => $results['key']]);
        }

        return $this->processApiResults($results);
    }

    /**
     * Get user jira profile info
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function userInfo()
    {
        $results = $this->api_client->userInfo();

        return $this->processApiResults($results);
    }

    /**
     * @param $results
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function processApiResults($results)
    {
        if (key_exists('error', $results)) {
            if (key_exists('oauth_problem', $results['error']['errors'])) {
                if ($results['error']['errors']['oauth_problem'] === 'token_rejected') {
                    return ApiTransformer::error('token rejected', 401);
                }
            } else {
                return ApiTransformer::error('uncaught error');
            }
        } else {
            return ApiTransformer::success($results);
        }
    }
}
