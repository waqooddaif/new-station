<?php

namespace Modules\Auth\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Http\Controllers\ApiTransformer;

class AuthorizeController extends Controller
{
    public function front(Request $request)
    {
        // authorization calls are allowed against these models only
        $available_models = config('station.models');

        $this->validate($request, [
            'ability' => 'required|string',
            'model' => 'string|in:'.implode(',', $available_models),
            'id' => 'int|required_with:model'
        ]);

        $user = \Auth::user();
        $ability = $request->input('ability');
        $result = false;

        if (!$request->has('model')) {
            $result = $user->can($ability);
        }

        if ($request->has(['model', 'id'])) {
            $modelName = ucfirst($request->input('model'));
            $modelsNamespaces = config('auth.modelsNamespaces');
            $model = $modelsNamespaces[$modelName].'\\'.$modelName;
            $record = $model::find($request->input('id'));
            if ($record) {
                $result = $user->can($ability, $record);
            }
        }

        return ApiTransformer::success([
            'ability' => $ability,
            'result' => $result
        ]);
    }
}
