<?php

Route::group(['middleware' => ['web'], 'namespace' => 'Modules\Auth\Http\Controllers'], function () {

    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...
    //Route::get('register', 'RegisterController@showRegistrationForm')->name('register');
    //Route::post('register', 'RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.resetToken');;
    Route::post('password/reset', 'ResetPasswordController@reset');

});

Route::group(['middleware' => ['web'], 'prefix' => config('backpack.base.route_prefix'), 'namespace' => 'Modules\Auth\Http\Controllers'], function () {
    Route::get('logout', '\Backpack\Base\app\Http\Controllers\Auth\LoginController@logout');
});


Route::group(['prefix' => 'jira/auth', 'namespace' => 'Modules\Auth\Http\Controllers'], function () {
    /**
     * Jira OAuth
     */
    Route::get('/', 'JiraOAuth@redirectToProvider')->name('jira.auth');
    Route::get('callback', 'JiraOAuth@handleProviderCallback');
});

Route::group(['prefix' => 'call','middleware' => ['web','auth'], 'namespace' => 'Modules\Auth\Http\Controllers'], function () {
    /**
     * Authorize check API
     */
    Route::get('/authorize', 'AuthorizeController@front');
});

Route::group(['namespace' => 'Modules\Auth\Http\Controllers'], function()
{
    // Invitations Password setup Routes...
    Route::get('invite/{token}/{type?}', 'InviteController@showResetForm')
        ->name('invitation.form');
    Route::post('invite/reset', 'InviteController@reset');
});
