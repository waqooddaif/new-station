<?php

return [
    'name' => 'Auth',
    'modelsNamespaces' => ['Expense' => 'Modules\Projects\Entities\Models',
        'Message' => 'Modules\Projects\Entities\Models',
        'MessageFile' => 'Modules\Projects\Entities\Models',
        'Project' => 'Modules\Projects\Entities\Models',
        'ProjectService' => 'Modules\Projects\Entities\Models',
        'Service' => 'Modules\Projects\Entities\Models',
        'Company' => 'Modules\Companies\Entities\Models',
        'OauthTocken' => 'Modules\Core\Entities\Models',
        'OutboundEmail' => 'Modules\Core\Entities\Models',
        'User' => 'Modules\Users\Entities\Models']
];
