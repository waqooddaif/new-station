<?php

namespace Modules\Projects\Entities\Models;

use HTMLPurifier;
use Modules\Core\Entities\Models\BaseModel;
use Modules\Users\Entities\Models\User;
use Modules\Core\Entities\Models\OutboundEmail;

class Message extends BaseModel
{
    protected $fillable = [
        'content',
        'visible_team',
        'visible_client',
        'type',
        'date_due',
        'date_done',
        'approval_state',
        'approval_note',
        'user_id',
        'project_id',
        'jira_key',
        'created_at',
        'updated_at',
    ];

    protected $appends = ['is_due', 'is_done', 'is_action', 'is_approval'];

    /**
     * Return a clean html.
     *
     * @param $content
     * @return string
     */
    public function getContentAttribute($content)
    {
        $purifier = new HTMLPurifier();
        return $purifier->purify($content);
    }

    /**
     * Return a clean html.
     *
     * @param $content
     * @return string
     */
    public function getApprovalNoteAttribute($content)
    {
        $purifier = new HTMLPurifier();
        return $purifier->purify($content);
    }

    /**
     * Check if this message is action.
     *
     * @return bool
     */
    public function getIsActionAttribute()
    {
        return in_array($this->type, config('station.actions'));
    }

    /**
     * Check if this message is action.
     *
     * @return bool
     */
    public function getIsApprovalAttribute()
    {
        return $this->type === 'approval';
    }

    /**
     * Check if this message is due.
     *
     * @return bool
     */
    public function getIsDueAttribute()
    {
        return $this->date_due !== null && $this->date_done === null;
    }

    /**
     * Check if this message is done.
     *
     * @return bool
     */
    public function getIsDoneAttribute()
    {
        return $this->date_due !== null && $this->date_done !== null;
    }

    /**
     * Clean message content html.
     *
     * @param  string  $value
     * @return void
     */
    public function setContentAttribute($value)
    {
        $purifier = new HTMLPurifier();
        $this->attributes['content'] = $purifier->purify($value);
    }

    /**
     * Get all email notifications sent for this message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function outboundEmails()
    {
        return $this->morphMany(OutboundEmail::class, 'emailable');
    }

    /**
     * Get the author of the message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Get the project of this message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * Get the project service of this message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projectServices()
    {
        return $this->belongsToMany(ProjectService::class, 'message_project_service');
    }

    /**
     * Get the service of this message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function services()
    {
        return $this->projectServices()->with('service');
    }

    /**
     * Get files attached to this message.
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function files()
    {
        return $this->hasMany(MessageFile::class);
    }

    /**
     * Filter only due messages.
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsDue($query)
    {
        return $query->whereNotNull('date_due')
                    ->whereNull('date_done');
    }

    /**
     * Filter only done messages.
     *
     * @param $query
     * @return mixed
     */
    public function scopeIsDone($query)
    {
        return $query->whereNotNull('date_due')
                    ->whereNotNull('date_done');
    }

    /**
     * Filter only approval requests.
     *
     * @param $query
     * @return mixed
     */
    public function scopeApproval($query)
    {
        return $query->where('type', 'approval');
    }

    /**
     * Filter only the requirement messages.
     *
     * @param $query
     * @return mixed
     */
    public function scopeRequirement($query)
    {
        return $query->where('type', 'requirement');
    }

    /**
     * Filter only the payment messages.
     *
     * @param $query
     * @return mixed
     */
    public function scopePayment($query)
    {
        return $query->where('type', 'payment');
    }

    /**
     * Filter only actions type messages.
     *
     * @param $query
     * @return mixed
     */
    public function scopeAction($query)
    {
        $actions_types = config('station.actions');
        return $query->whereIn('type', $actions_types);
    }

    /**Filter only general messages.
     *
     * @param $query
     * @return mixed
     */
    public function scopeGeneral($query)
    {
        return $query->whereNull('type');
    }

    /**
     * Filter only the not general messages.
     *
     * @param $query
     * @return mixed
     */
    public function scopeNotGeneral($query)
    {
        return $query->whereNotNull('type');
    }

    /**
     * Filter by project id.
     *
     * @param $query
     * @param $project_id
     * @return mixed
     */
    public function scopeByProjectId($query, $project_id)
    {
        return $query->where('project_id', $project_id);
    }

    /**
     * Filter by owner id.
     *
     * @param $query
     * @param $user_id
     * @return mixed
     */
    public function scopeByOwnerId($query, $user_id)
    {
        return $query->where('user_id', $user_id);
    }
}
