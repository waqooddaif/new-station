<?php

namespace Modules\Projects\Entities\Models;

use Modules\Core\Entities\Models\BaseModel;
use Modules\Companies\Entities\Models\Company;
use Modules\Users\Entities\Models\User;
use Modules\Core\Entities\Models\OutboundEmail;

class Project extends BaseModel
{
    protected $fillable = [
        'name',
        'image',
        'jira_name',
        'jira_key',
        'date_start',
        'date_final',
        'date_archived',
        'pm_id',
        'tech_id',
        'client_id',
        'company_id',
        'color',
        'budget'
    ];

    protected $appends = ['status', 'not_replied'];

    /**
     * Calculate the project status.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        if ($this->date_archived) {
            return 'archived';
        }
        if ($this->services()->pending()->count() > 0) {
            return 'pending';
        }
        $due_actions = $this->messages()->where('visible_client', 1)->isDue()->count();
        if ($due_actions > 0) {
            return 'due';
        }
        if ($this->date_final < date('Y-m-d')) {
            return 'delivered';
        }
        return 'in-progress';
    }

    /**
     * Calculate the project last not replied message date.
     *
     * @return string
     */
    public function getNotRepliedAttribute()
    {
        try {
            if (!$this->messages()->count()) return false;

            $message = $this->messages()->latest()->first();
            if ($message && $message->visible_client && $message->owner->type === 'client') {
                return $message->created_at;
            }
        } catch (\Exception $e) {
            \Log::error("project $this->id");
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }

        return false;
    }

    /**
     * Get current budget balance.
     *
     * @return float
     */
    public function getBudgetBalanceAttribute()
    {
        return $this->budget - $this->expenses->sum('cost');
    }

    /**
     * Set a random color for project if no color is provided.
     *
     * @param $value
     */
    public function setColorAttribute($value)
    {
        $this->attributes['color'] = $value ?: randomColor(0, 225);
    }

    /**
     * Get all email notifications sent for this project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function outboundEmails()
    {
        return $this->morphMany(OutboundEmail::class, 'emailable');
    }

    /**
     * Get all expenses of this project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses()
    {
        return $this->hasMany(Expense::class);
    }

    /**
     * Get all member users of this project.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'project_member')
            ->withPivot('project_service_id');
    }

    /**
     * Get the project manager account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(User::class, 'pm_id');
    }

    /**
     * Get the project Technical Lead account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function techLead()
    {
        return $this->belongsTo(User::class, 'tech_id');
    }

    /**
     * Get the Client account.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    /**
     * Get the client company.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

    /**
     * Get all project services.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function services()
    {
        return $this->belongsToMany(Service::class, 'project_service')
                    ->withPivot(['date_start', 'date_finish', 'status', 'id', 'created_at', 'updated_at']);
    }

    /**
     * Get all project active services.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function activeServices()
    {
        return $this->services()->activeByStatus()->get();
    }

    /**
     * Get all project services relation model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectServices()
    {
        return $this->hasMany(ProjectService::class);
    }

    /**
     * Get all messages.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    /**
     * Get all files attached to this project messages.
     *
     * @return array
     */
    public function files()
    {
        return $this->messages->files;
    }

    /**
     * Add a service to the project.
     *
     * @param int $service_id
     * @param array $service_params
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function attachService($service_id, $service_params)
    {
        return $this->projectServices()->create(
            array_merge(['service_id' => $service_id], $service_params)
        );
    }

    /**
     * Remove a group of services from the project.
     *
     * @param array $services
     * @return $this
     */
    public function detachServices($services)
    {
        $this->services()->detach($services);
        return $this;
    }

    /**
     * Add a member to the project.
     *
     * @param int $member_id
     * @param int $project_service_id
     * @return $this
     */
    public function attachMember($member_id, $project_service_id = null)
    {
        $this->members()->attach([$member_id => ['project_service_id' => $project_service_id]]);
        return $this;
    }

    /**
     * Add a group of members to the project.
     *
     * @param array $members
     * @return $this
     */
    public function attachMembers($members)
    {
        $this->members()->attach($members);
        return $this;
    }

    /**
     * Remove a member from the project service.
     *
     * @param $member_id
     * @param $project_service_id
     * @return $this
     */
    public function detachServiceMember($member_id, $project_service_id)
    {
        $this->members()
            ->where('id', $member_id)
            ->wherePivot('project_service_id', $project_service_id)
            ->detach($member_id);
        return $this;
    }

    /**
     * Remove a group of members from the project.
     *
     * @param array $members
     * @return $this
     */
    public function detachMembers($members)
    {
        $this->members()->detach($members);
        return $this;
    }

    /**
     * Filter all member projects includes project members, pm, tech lead
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeAllMemberProjects($query, $user)
    {
        $ids = $user->projects->pluck('id');
        return $query->whereIn('id', $ids)
                     ->orWhere('pm_id', $user->id)
                     ->orWhere('tech_id', $user->id);
    }

    /**
     * Filter all client projects includes client id, or company id
     * @param $query
     * @param $user
     * @return mixed
     */
    public function scopeAllClientProjects($query, $user)
    {
        return $query->where('client_id', $user->id)
                     ->orWhere('company_id', $user->company()->id);
    }

    /**
     * Only specific pm fields
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithManager($query)
    {
        return $query->with([
                'manager' => function($query)
                {
                    $query->addSelect(['id', 'name']);
                }
            ]);
    }

    /**
     * Only specific lead fields
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithLead($query)
    {
        return $query->with([
            'techLead' => function($query)
            {
                $query->addSelect(['id', 'name']);
            }
        ]);
    }

    /**
     * Only specific company fields
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithCompany($query)
    {
        return $query->with([
            'company' => function($query)
            {
                $query->addSelect(['id', 'name']);
            }
        ]);
    }

    /**
     * Only specific client fields
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithClient($query)
    {
        return $query->with([
            'client' => function($query)
            {
                $query->addSelect(['id', 'name']);
            }
        ]);
    }
}
