<?php

namespace Modules\Projects\Entities\Models;

use Modules\Core\Entities\Models\BaseModel;
use Modules\Users\Entities\Models\User;

class ProjectService extends BaseModel
{
    protected $table = 'project_service';

    protected $fillable = [
        'service_id',
        'date_start',
        'date_finish',
        'status',
        'project_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(User::class, 'project_member');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function messages()
    {
        return $this->belongsToMany(Message::class, 'message_project_service');
    }
}
