<?php

namespace Modules\Projects\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Http\Controllers\ApiTransformer;
use Modules\Core\Http\Controllers\FilesController;
use Modules\Projects\Entities\Models\Message;
use Modules\Projects\Entities\Models\Project;
use Modules\Users\Entities\Models\User;

class MessageController extends Controller
{

    /**
     * Add a message to a project.
     *
     * @param Request $request
     * @param $project_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function storeMessage(Request $request, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $user = Auth::user();

        if ($user->cannot('message.create', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'content' => 'required|string|hastext',
            'project_service_id' => [
                'integer',
                Rule::exists('project_service', 'id')->where(function ($query) use ($request, $project_id) {
                    $query->where('project_id', $project_id);
                }),
            ],
        ]);

        // decide visibility
        list($visible_team, $visible_client) = $this->decideVisibility($request, $user, $project);

        try { // if any error happened rollback the message creation
            DB::beginTransaction();
            $message = Message::create([
                'content' => $request->input('content'),
                'project_id' => $project->id,
                'user_id' => $user->id,
                'visible_team' => $visible_team,
                'visible_client' => $visible_client,
            ]);

            $message->projectServices()->attach($request->input('project_service_id'));

            // check for attachments with names
            if ($request->has('attachments')) {
                $attachments = $request->input('attachments');
                if (is_array($attachments)) {
                    FilesController::attachToMessage($attachments, $message->id);
                }
            }
            DB::commit();

            return ApiTransformer::success(MessageController::messageShowPolicy($message, $user));
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            app('sentry')->captureException($e);
            return ApiTransformer::error();
        }
    }

    /**
     * Create a new action.
     *
     * @param $request
     * @param $project_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function storeAction($request, $project_id)
    {
        $project = Project::findOrFail($project_id);
        $user = Auth::user();

        if ($user->cannot('action.create', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $action_types = implode(',', config('station.actions'));

        $this->validate($request, [
            'content' => 'required|string',
            'type' => 'required|in:'.$action_types,
            'date_due' => 'required|date_format:"Y/m/d"',
            'project_service_id' => [
                'integer',
                Rule::exists('project_service', 'id')->where(function ($query) use($request, $project_id) {
                    $query->where('project_id', $project_id);
                }),
            ],
        ]);
        // decide visibility
        list($visible_team, $visible_client) = $this->decideVisibility($request, $user, $project);

        try { // if any error happened rollback the message creation
            DB::beginTransaction();
            $message = Message::create([
                'content' => $request->input('content'),
                'project_id' => $project->id,
                'user_id'   => $user->id,
                'type' => $request->input('type'),
                'date_due' => $request->input('date_due'),
                'visible_team' => $visible_team,
                'visible_client' => $visible_client,
            ]);

            $message->projectServices()->attach($request->input('project_service_id'));

            // check for attachments with names
            if ($request->has('attachments')) {
                $attachments = $request->input('attachments');
                if (is_array($attachments)) {
                    FilesController::attachToMessage($attachments, $message->id);
                }
            }
            DB::commit();

            return ApiTransformer::success(MessageController::messageShowPolicy($message, $user));
        } catch (Exception $e) {
            DB::rollBack();
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
            app('sentry')->captureException($e);
            return ApiTransformer::error();
        }
    }

    /**
     * Change the message approval state.
     *
     * @param Request $request
     * @param $id
     * @return Message|bool|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function approve(Request $request, $id)
    {
        $message = Message::findOrFail($id);
        $user = Auth::user();

        if (!$message->is_action && $user->cannot('message.approve', $message)) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'approval_state' => 'required|boolean',
            'approval_note' => 'string',
        ]);

        $message->approval_state = $request->input('approval_state');
        $message->approval_note = $request->input('approval_note');
        $message->date_done = date('Y-m-d');

        $message->save();

        return ApiTransformer::success($this->messageShowPolicy($message, $user));
    }
    /**
     * Change the message visibility.
     *
     * @param Request $request
     * @param $id
     * @return Message|bool|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function visibility(Request $request, $id)
    {
        $message = Message::findOrFail($id);
        $user = Auth::user();

        if (!$message->is_action && $user->cannot('message.update', $message)) {
            return ApiTransformer::notAuthorized();
        }
        elseif ($message->is_action && $user->cannot('action.update', $message)) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'target' => 'required|string',
        ]);

        $target = $request->input('target');

        switch ($target) {
            case 'all':
                $message->visible_team = 1;
                $message->visible_client = 1;
                break;
            case 'team':
                $message->visible_team = 1;
                $message->visible_client = 0;
                break;
            case 'client':
                $message->visible_team = 0;
                $message->visible_client = 1;
                break;
            case 'hidden':
                $message->visible_team = 0;
                $message->visible_client = 0;
                break;
        }

        $message->save();

        return ApiTransformer::success($this->messageShowPolicy($message, $user));
    }

    /**
     * Change the message to done.
     *
     * @param Request $request
     * @param $id
     * @return Message|bool|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function makeMessageDone(Request $request, $id)
    {
        $message = Message::findOrFail($id);
        $user = Auth::user();

        if (!$message->is_action) {
            return ApiTransformer::notAuthorized();
        }
        elseif ($message->is_action && $user->cannot('action.update', $message)) {
            return ApiTransformer::notAuthorized();
        }

        $message->date_done = date('Y-m-d');

        $message->save();

        return ApiTransformer::success($this->messageShowPolicy($message, $user));
    }

    /**
     * Update message data.
     *
     * @param Request $request
     * @param $id
     * @return Message|bool|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $message = Message::findOrFail($id);
        $user = Auth::user();

        if ($message->is_action && $user->cannot('action.update', $message)) {
            return ApiTransformer::notAuthorized();
        }
        else if (!$message->is_action && $user->cannot('message.update', $message)) {
            return ApiTransformer::notAuthorized();
        }

        if ($request->has(['service_id', 'detach_service_id']) && $request->input('service_operation') === 'service_replace') {
            $message->projectServices()->detach($request->input('detach_service_id'));
            $message->projectServices()->attach($request->input('service_id'));
        }
        elseif ($request->has('service_id') &&  $request->input('service_operation') === 'service_add') {
            $message->projectServices()->attach($request->input('service_id'));
        }
        elseif ($request->has('service_id') &&  $request->input('service_operation') === 'service_remove') {
            $message->projectServices()->detach($request->input('service_id'));
        }

        return ApiTransformer::success($this->messageShowPolicy($message, $user));
    }

    /**
     * Filter message data according to user authorization.
     *
     * @param Message $message
     * @param User $user
     * @return array|bool
     */
    public static function messageShowPolicy(Message $message, User $user)
    {
        if ($user->cannot('message.show', $message)) {
            return false;
        }

        $data = [];
        $data['id'] = $message->id;
        $data['content'] = $message->content;
        $data['visible_team'] = $message->visible_team;
        $data['visible_client'] = $message->visible_client;
        $data['type'] = $message->type;
        $data['date_due'] = $message->date_due;
        $data['date_done'] = $message->date_done;
        $data['is_done'] = $message->is_done;
        $data['is_due'] = $message->is_due;
        $data['is_action'] = $message->is_action;
        $data['approval_state'] = $message->approval_state;
        $data['approval_note'] = $message->approval_note;
        $data['user_id'] = $message->user_id;
        $data['project_id'] = $message->project_id;
        $data['jira_key'] = $message->jira_key;
        $data['created_at'] = $message->created_at;
        $data['updated_at'] = $message->updated_at;
        $data['services'] = $message->services;
        $data['files'] = $message->files;
        $data['owner'] = [
            'id' => $message->owner->id,
            'initials' => $message->owner->initials,
            'name' => $message->owner->name,
            'color' => $message->owner->color,
            'type' => $message->owner->type,
            'role_description' => $message->owner->role_description,
        ];

        $data['permissions'] = [
            'action.update' => $user->can('action.update', $message),
            'message.update' => $user->can('message.update', $message),
            'message.approve' => $user->can('message.approve', $message)
        ];

        return $data;
    }

    /**
     * @param Request $request
     * @param $user
     * @param $project
     * @return array
     */
    protected function decideVisibility(Request $request, $user, $project)
    {
        $visible_team = false;
        $visible_client = false;

        if ($user->can('project.update', $project)) {
            // find if the message submitted under active tab
            if ($tab = $request->input('tab')) {
                if ($tab === 'team')
                    $visible_team = true;
                elseif ($tab === 'client')
                    $visible_client = true;
            }
            return array($visible_team, $visible_client);
        } else {
            if ($user->type === 'member')
                $visible_team = true;
            elseif ($user->type === 'client')
                $visible_client = true;
        }
        return array($visible_team, $visible_client);
    }
}
