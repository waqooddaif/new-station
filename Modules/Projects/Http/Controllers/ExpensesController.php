<?php

namespace Modules\Projects\Http\Controllers;

use Illuminate\Http\Request;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Http\Controllers\ApiTransformer;
use Modules\Projects\Entities\Models\Expense;
use Modules\Projects\Entities\Models\Project;

class ExpensesController extends Controller
{
    /**
     * Add an expense to a project.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, $id)
    {
        $user = \Auth::user();
        $project = Project::findOrFail($id);

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'description' => 'required',
            'cost' => 'required|numeric'
        ]);

        $expense = Expense::create([
            'description' => $request->input('description'),
            'cost' => $request->input('cost'),
            'project_id' => $id,
        ]);

        return ApiTransformer::success($expense);
    }

    public function destroy($id)
    {
        $user = \Auth::user();
        $expense = Expense::with('project')->findOrFail($id);
        $project = $expense->project;

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $expense->delete();

        return ApiTransformer::success($id);
    }
}
