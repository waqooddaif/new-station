<?php

namespace Modules\Projects\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Modules\Core\Http\Controllers\Controller;
use Modules\Core\Http\Controllers\ApiTransformer;
use Modules\Projects\Entities\Models\Project;
use Modules\Projects\Entities\Models\ProjectService;
use Modules\Users\Entities\Models\User;

class ProjectsController extends Controller
{
    /**
     * Get all projects related to an authenticated user as their assigned permissions.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        // Set joint relations
        $query  = Project::orderBy('id', 'desc')->withManager()->withLead()->withCompany()->withClient();

        $user = Auth::user();

        if ($user->can('wq.projects.index.all')) {
            $projects = $query->get();
        }
        elseif ($user->can('wq.projects.index.pm')) {
            $projects = $query->allMemberProjects($user)->get();
        }
        elseif ($user->can('wq.projects.index.own')) {
            $projects = $user->projects()
                ->withManager()
                ->withLead()
                ->withCompany()
                ->withClient()
                ->get();
        }
        elseif ($user->can('client.projects.index.company')) {
            $projects = $query->allClientProjects($user)->get();
        }
        elseif ($user->can('client.projects.index.own')) {
            $projects = $query->where('client_id', $user->id)->get();
        } else {
            return abort(403);
        }

        $results = [];

        foreach ($projects as $project) {
            $results[] = $this->projectsIndexFieldsPolicy($project, true);
        }

        return ApiTransformer::success($results);
    }

    /**
     * Filter project fields to be sent to the user.
     *
     * @param Project $project
     * @return array
     */
    public static function projectsIndexFieldsPolicy(Project $project, $can_show)
    {
        $authorized_project_data = [];
        $authorized_project_data['id'] = $project->id;
        $authorized_project_data['name'] = $project->name;
        $authorized_project_data['color'] = $project->color;
        $authorized_project_data['image'] = $project->image;
        $authorized_project_data['jira_name'] = $project->jira_name;
        $authorized_project_data['jira_key'] = $project->jira_key;
        $authorized_project_data['date_start'] = $project->date_start;
        $authorized_project_data['date_final'] = $project->date_final;
        $authorized_project_data['date_archived'] = $project->date_archived;
        $authorized_project_data['status'] = $project->status;
        $authorized_project_data['created_at'] = $project->created_at;
        $authorized_project_data['updated_at'] = $project->updated_at;
        $authorized_project_data['not_replied'] = $project->not_replied;
        $authorized_project_data['manager']['name'] = $project->manager->name;
        $authorized_project_data['manager']['id'] = $project->pm_id;
        $authorized_project_data['tech_lead']['name'] = $project->techLead->name;
        $authorized_project_data['tech_lead']['id'] = $project->tech_id;
        $authorized_project_data['client']['name'] = $project->client->name;
        $authorized_project_data['client']['id'] = $project->client_id;
        $authorized_project_data['company']['name'] = $project->company->name;
        $authorized_project_data['permissions'] = [
            'project.show' => $can_show,
        ];

        return $authorized_project_data;
    }

    /**
     * Get all projects related to an authenticated user as their assigned permissions.
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        $project = Project::findOrFail($id);

        $can_update = $user->can('project.update', $project);
        $can_see_members = $user->can('project.show.members', $project);
        $can_show = $user->can('project.show', $project);

        if (!$can_update && !$can_show) {
            return ApiTransformer::notAuthorized();
        }

        // loading message.project only for policies check
        $project->load([
            'messages.owner', 'messages.files', 'messages.project', 'messages.services', 'services'
        ]);

        if ($can_update || $can_see_members) {
            $project->load(['members']);
        }

        $results = $this->projectShowFieldsPolicy($project, $user, $can_show, $can_update, $can_see_members);

        return ApiTransformer::success($results);
    }

    /**
     * Filter project fields to be sent to the user.
     *
     * @param Project $project
     * @param User $user
     * @param boolean $can_show
     * @param boolean $can_update
     * @param boolean $can_see_members
     * @return array
     */
    public static function projectShowFieldsPolicy(Project $project, User $user, $can_show, $can_update, $can_see_members)
    {
        $authorized_project_data = self::projectsIndexFieldsPolicy($project, true);

        $authorized_project_data['services'] = $project->services ?: [];

        if ($can_update || $can_see_members) {
            $authorized_project_data['members'] = $project->members ?: [];
        }

        if ($can_update || $can_see_members) {

            $authorized_project_data['budget'] = $project->budget;

            $authorized_project_data['budget_balance'] = $project->budget_balance;

            $authorized_project_data['expenses'] = $project->expenses;
        }

        $msg_controller = new MessageController;
        $messages = $project->messages->sortByDesc('updated_at');
        $messages_data = [];
        foreach ($messages as $message) {
            // Get only the authorized message content
            $msg_data = $msg_controller::messageShowPolicy($message, $user);
            unset($msg_data['project']); // not to send project info with every msg
            if ($msg_data) $messages_data[] = $msg_data;
        }
        $authorized_project_data['messages'] = $messages_data;

        $authorized_project_data['permissions'] = [
            'message.create' => $user->can('message.create', $project),
            'action.create' => $user->can('action.create', $project),
            'project.show' => $can_show,
            'project.update' => $can_update,
            'project.show.members' => $can_see_members,
        ];

        return $authorized_project_data;
    }

    /**
     * Store new project.
     *
     * @param Request $request
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        if (!$user->can('project.create')) {
            return ApiTransformer::notAuthorized();
        }

        $roles = config('station.roles');

        $this->validate($request, [
            'name' => 'required',
            'pm_id' => [
                'required',
                Rule::exists('users', 'id')->where(function ($query) use($request, $roles) {
                    $query->where('id', $request->input('pm_id'))
                        ->whereIn('role_id', [$roles['pm'], $roles['admin']]);
                }),
            ],
            'tech_id' => [
                'required',
                Rule::exists('users', 'id')->where(function ($query) use($request, $roles) {
                    $query->where('id', $request->input('tech_id'))
                        ->whereIn('role_id', [$roles['lead'], $roles['admin']]);
                }),
            ],
            'client_id' => 'required|exists:users,id,type,client',
            'company_id' => 'required|exists:companies,id',
            'date_start' => 'date_format:"Y/m/d"',
            'date_final' => 'date_format:"Y/m/d"'
        ]);

        $project = Project::create($request->only([
            'name',
            'color',
            'pm_id',
            'tech_id',
            'client_id',
            'company_id',
            'date_start',
            'date_final'
        ]));

        return $this->projectsIndexFieldsPolicy($project, true);
    }

    /**
     * Update project.
     *
     * @param Request $request
     * @param integer $id
     * @return array|\Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $user = Auth::user();

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $roles = config('station.roles');

        $this->validate($request, [
            'pm_id' => [
                Rule::exists('users', 'id')->where(function ($query) use($request, $roles) {
                    $query->where('id', $request->input('pm_id'))
                        ->whereIn('role_id', [$roles['pm'], $roles['admin']]);
                }),
            ],
            'budget' => 'numeric'
        ]);

        $updatables = ['pm_id', 'jira_name', 'jira_key', 'budget'];

        $data = array_intersect_key($request->toArray(), array_flip($updatables));

        $project->update($data);

        return $this->show($id);
    }

    /**
     * Add a message to a project.
     * todo transfer this function to MessagesController
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addMessage(Request $request, $id)
    {
        $message_controller = new MessageController();

        // if not general message, move to the action message function
        if ($request->has('type')) {
            $message_controller->storeAction($request, $id);
        } else {
            // Proceed for creating a general message
            $message_controller->storeMessage($request, $id);
        }

        return $this->show($id);
    }

    /**
     * Add a member to a project.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addMember(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $user = Auth::user();

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'id' => 'required|exists:users',
            'project_service_id' => [
                'integer',
                Rule::exists('project_service', 'id')->where(function ($query) use($request, $id) {
                    $query->where('project_id', $id);
                }),
            ],
        ]);

        $project->attachMember($request->input('id'), $request->input('project_service_id'));
        // return user data with project relation pivot
        $pivot = ['pivot' => ['project_id' => $id, 'project_service_id' => $request->input('project_service_id')]];
        return ApiTransformer::success(array_merge(User::find($request->input('id'))->toArray(), $pivot));
    }

    /**
     * Remove a member from a project service.
     *
     * @param $id
     * @param $member_id
     * @param null $project_service_id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function removeMember($id, $member_id, $project_service_id = null)
    {
        $project = Project::findOrFail($id);
        $user = Auth::user();

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $member = User::findOrFail($member_id);

        $project->detachServiceMember($member_id, $project_service_id);

        return ApiTransformer::success(['msg' => 'success']);
    }

    /**
     * Add a service to a project.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addService(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $user = Auth::user();

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'service_id' => 'required|integer|exists:services,id',
            'date_start' => 'date_format:"Y/m/d"',
            'date_finish' => 'date_format:"Y/m/d"',
            'status' => 'required|string',
        ]);

        $project->attachService(
            $request->input('service_id'),
            $request->only(['date_start', 'date_finish', 'status'])
            );

        return $this->show($id);
    }

    /**
     * Update a project service.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function updateService(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $user = Auth::user();

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $this->validate($request, [
            'service_id' => 'required|integer|exists:services,id',
            'date_start' => 'date_format:"Y/m/d"',
            'date_finish' => 'date_format:"Y/m/d"',
            'status' => 'required|string',
        ]);

        $project_service = ProjectService::where('project_id', $id)
            ->where('id', $request->input('id'))
            ->firstOrFail();

        $project_service->update($request->only(['service_id', 'status', 'date_start', 'date_finish']));

        return $this->show($id);
    }

    /**
     * Remove a service from a project.
     *
     * @param $id
     * @param $serviceid
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function removeService($id, $serviceid)
    {
        $project = Project::findOrFail($id);
        $user = Auth::user();

        if ($user->cannot('project.update', $project)) {
            return ApiTransformer::notAuthorized();
        }

        $project_service = ProjectService::where('project_id', $id)
                                        ->where('id', $serviceid)
                                        ->firstOrFail();

        $project_service->delete();

        return $this->show($id);
    }
}
