<?php

Route::group(['middleware' => ['web','auth'], 'prefix' => 'call', 'namespace' => 'Modules\Projects\Http\Controllers'], function()
{

    Route::group(['prefix' => 'projects'], function() {

        /**
         * Projects API, starts with path url/call/projects
         */

        // create new project
        Route::post('/', 'ProjectsController@store')
            ->name('projects.create');

        // projects full list
        Route::get('/', 'ProjectsController@index')
            ->name('projects.index');

        // project details
        Route::get('/{id}', 'ProjectsController@show')
            ->name('projects.show');

        // project update
        Route::put('/{id}', 'ProjectsController@update')
            ->name('projects.update');

        // add message to project
        Route::post('/{id}/messages', 'ProjectsController@addMessage')->name('projects.message.add');

        // add member to project
        Route::post('/{id}/members', 'ProjectsController@addMember')
            ->name('projects.member.add');

        // add service to project
        Route::post('/{id}/services', 'ProjectsController@addService')
            ->name('projects.service.add');

        // update project service
        Route::put('/{id}/services', 'ProjectsController@updateService')
            ->name('projects.service.update');

        // remove service from project
        Route::delete('/{id}/services/{serviceid}', 'ProjectsController@removeService')
            ->name('projects.service.remove');

        // remove member from project
        Route::delete('/{id}/members/{memberid}/{projectserviceid?}', 'ProjectsController@removeMember')
            ->name('projects.member.remove');

        // add action to project
        Route::post('/{id}/actions', 'ProjectsController@addAction')
            ->name('projects.action.add');

        /**
         * Project expenses
         */
        Route::post('/{id}/expenses', 'ExpensesController@store')
            ->name('projects.expenses');

    });

    Route::group(['prefix' => 'expenses'], function() {
        /**
         * Expenses API, url/call/expenses
         */
        Route::delete('/{id}', 'ExpensesController@destroy')
            ->name('projects.expenses.destroy');
    });

    Route::group(['prefix' => 'messages'], function() {
        /**
         * Messages API, url/call/messages
         */
        // change message visibility
        Route::put('/{id}/visibility', 'MessageController@visibility')
            ->name('message.visibility');
        // change message approval state
        Route::put('/{id}/approve', 'MessageController@approve')
            ->name('message.approve');
        // make message status done
        Route::put('/{id}/done', 'MessageController@makeMessageDone')
            ->name('message.done');
        // change message service
        Route::put('/{id}', 'MessageController@update')
            ->name('message.update');

    });

});
