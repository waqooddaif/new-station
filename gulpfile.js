const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

elixir.config.browserSync.proxy = 'station.dev';
elixir.config.notifications = true;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

if(elixir.config.production) {
    elixir(mix => {
        mix.sass('app.scss', 'public/css/app.min.css')
            .styles([
                // 'sweetalert.css',
                'toastr.css',
                'jquery.webui-popover.css',
                'quill.snow.0.2.css',
            ], 'public/css/vendors.min.css')
            .webpack('vendor.js', 'public/js/vendor.min.js')
            .webpack('login.js', 'public/js/login.min.js')
            .webpack('app.js', 'public/js/app.min.js');
    });
} else {
    elixir.config.sourcemaps = true;
    elixir(mix => {
        mix.sass('app.scss')
            .webpack('vendor.js')
            .webpack('login.js')
            .webpack('app.js');
    });
}

