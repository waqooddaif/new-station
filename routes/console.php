<?php

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

// Fetch IMAP email account to read clients email and create messages for them
Artisan::command('imap:fetch', function () {
    $fetch_imap = new \App\Http\Controllers\FetchImapController;
    $messages = $fetch_imap->fetch();
    if ($messages) {
        $this->info("Created ".count($messages)." messages!");
    } elseif ($messages === []) {
        $this->info("No new emails!");
    } else {
        $this->error("Something wrong, review the logs!");
    }
})->describe('Fetching imap email and create messages.');
