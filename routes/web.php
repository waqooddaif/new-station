<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

$api = config('station.api');

// Authentication Routes...
/*moved
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
*/
// Registration Routes...
//Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
//Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
/*
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.resetToken');;
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
*/
// Invitations Password setup Routes...
//Route::get('invite/{token}/{type?}', 'InviteController@showResetForm')
//        ->name('invitation.form');
//Route::post('invite/reset', 'InviteController@reset');


/**
 * Jira addon routes
 */
/*
Route::group(['prefix' => 'jira'], function () {

    /*moved
    Route::group(['middleware' => 'auth'], function () {
        Route::get('tokens', 'JiraApiController@tokens');
        Route::get('user', 'JiraApiController@userInfo');
        Route::get('projects', 'JiraApiController@projects');
        Route::post('issue', 'JiraApiController@createIssue');
    });
    Route::group(['prefix' => 'auth'], function () {
        /**
         * Jira OAuth

        /*moved
        Route::get('/', 'Auth\JiraOAuth@redirectToProvider')
            ->name('jira.auth');
        Route::get('callback', 'Auth\JiraOAuth@handleProviderCallback');

    });
});
*/

// Routes for only authenticated users
Route::group(['middleware' => 'auth'], function () use ($api) {

    /*
    // error test
    Route::get('error', function(){
        if (config('app.env') !== 'production')
            throw new Exception('Error Testing!!');
    });

    Route::get($api["session.refresh"], function() {
        return response('', 204);
    });

    // upload file
    Route::post($api["file.upload"], 'FilesController@upload')
        ->name('file.upload');
    */

    /**
     * Members API, starts with path url/call/member
     */
    // create a new member
    /*
    Route::post($api["member.create"], 'UsersController@storeMember')
        ->name('member.create');
    // get all members and their assigned services
    Route::get($api["members.schedule"], 'UsersController@weekSchedule')
        ->name('members.schedule');
    */
    /**
     * Companies API, starts with path url/call/company
     */
    /*
    // companies full list
    Route::get($api["company.index"], 'CompaniesController@index')
        ->name('company.index');
    // create a new company
    Route::post($api["company.create"], 'CompaniesController@store')
        ->name('company.create');
    */

    /**
     * Clients API, starts with path url/call/client
     */
    // create a new client
    /*
    Route::post($api["client.create"], 'UsersController@storeClient')
        ->name('client.create');
    */
    /**
     * Projects API, starts with path url/call/projects
     */
    /*moved
    // create new project
    Route::post($api["projects.create"], 'ProjectsController@store')
        ->name('projects.create');

    // projects full list
    Route::get($api["projects.index"], 'ProjectsController@index')
        ->name('projects.index');

    // project details
    Route::get($api["projects.show"], 'ProjectsController@show')
        ->name('projects.show');

    // project update
    Route::put($api["projects.update"], 'ProjectsController@update')
        ->name('projects.update');
    */

    /* messages moved */
    // add message to project
    //Route::post($api["projects.message.add"], 'ProjectsController@addMessage')->name('projects.message.add');

    // add action to project
//    Route::post($api["projects.action.add"], 'ProjectsController@addAction')
//        ->name('projects.action.add');


    /*moved
    // add member to project
    Route::post($api["projects.member.add"], 'ProjectsController@addMember')
        ->name('projects.member.add');

    // add service to project
    Route::post($api["projects.service.add"], 'ProjectsController@addService')
        ->name('projects.service.add');

    // update project service
    Route::put($api["projects.service.update"], 'ProjectsController@updateService')
        ->name('projects.service.update');

    // remove service from project
    Route::delete($api["projects.service.remove"], 'ProjectsController@removeService')
        ->name('projects.service.remove');

    // remove member from project
    Route::delete($api["projects.member.remove"], 'ProjectsController@removeMember')
        ->name('projects.member.remove');
*/

    /**
     * Messages API, url/call/messages
     */

    /* messages moved */
    // change message visibility
    //Route::put($api["message.visibility"], 'MessageController@visibility')->name('message.visibility');
    // change message approval state
    //Route::put($api["message.approve"], 'MessageController@approve')->name('message.approve');
    // make message status done
    //Route::put($api["message.done"], 'MessageController@makeMessageDone')->name('message.done');
    // change message service
    //Route::put($api["message.update"], 'MessageController@update')->name('message.update');

    /**
     * Project expenses
     */
    /*
     * moved
    Route::post($api["projects.expenses"], 'ExpensesController@store')
        ->name('projects.expenses');
    Route::delete($api["projects.expenses.item"], 'ExpensesController@destroy')
        ->name('projects.expenses.destroy');
    */

    /**
     * Authorize check API
     */
    //Route::get($api['authorize'], 'AuthorizeController@front');

    /**
     * widecard catch-all
     */
    /*
     * Route::get('/', 'HomeController@main')->name('main');
    Route::get('/home', 'HomeController@main')->name('home');
    Route::get('/projects/{id?}', 'HomeController@main')->name('projects');
    Route::get('/companies/{id?}', 'HomeController@main')->name('companies');
    Route::get('/company/{id?}', 'HomeController@main')->name('company');
    Route::get('/clients/{id?}', 'HomeController@main')->name('clients');
    Route::get('/client/{id?}', 'HomeController@main')->name('client');
    Route::get('/members/{id?}', 'HomeController@main')->name('members');
    Route::get('/member/{id?}', 'HomeController@main')->name('member');
    Route::get('/planner/{year?}/{week?}', 'HomeController@main')->name('planner');*/
});

// Admin Interface Routes
Route::group(['prefix' => 'admin', 'middleware' => ['web']], function()
{
    // Backpack\CRUD: Define the resources for the entities you want to CRUD.
    Route::resource('projects', 'Admin\ProjectCrudController');

});


Route::group(
    [
        'middleware' => ['web'],
        'prefix'     => config('backpack.base.route_prefix'),
    ],
    function () {
        Route::auth();
        /*moved*/
        //Route::get('logout', '\Backpack\Base\app\Http\Controllers\Auth\LoginController@logout');
        Route::get('dashboard', '\Backpack\Base\app\Http\Controllers\AdminController@dashboard')
                ->middleware('auzo.acl');
        Route::get('/', '\Backpack\Base\app\Http\Controllers\AdminController@redirect');
    });